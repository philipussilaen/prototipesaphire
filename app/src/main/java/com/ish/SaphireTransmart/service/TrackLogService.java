package com.ish.SaphireTransmart.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ish.SaphireTransmart.database.TableTrackLog;
import com.ish.SaphireTransmart.database.database_adapter.TableTrackLogAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by adminmc on 29/09/16.
 */
public class TrackLogService extends Service {

    private String TAG = TrackLogService.class.getSimpleName();
    private static Timer timer = new Timer();
    private Context ctx;
    private Utility utils;
    private TableTrackLogAdapter adapter;
    private String userId="";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate()
    {
        super.onCreate();

    }


    private void startService()
    {
        try {
//            utils.setGeoLocation();
//            timer.scheduleAtFixedRate(new mainTask(), 0, 60000);
//            //60000
//            timer.scheduleAtFixedRate(new requestServerTask(), 0, 3600000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class mainTask extends TimerTask
    {
        public void run()
        {
            //toastHandler.sendEmptyMessage(0);
            try {
                String time = utils.getCurrentDateandTime();
                String lng = utils.getLongitude();
                String lat = utils.getLatitude();
                Log.e("TrackLogService", "time="+time+"  longitude="+lng+"  latitude="+lat);
                adapter.insertData(new TableTrackLog(), generateId(), userId, utils.getCurrentDateandTime(),
                        utils.getLongitude(), utils.getLatitude(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class requestServerTask extends TimerTask {
        @Override
        public void run() {
            try {
                List<TableTrackLog> list = adapter.getDatabyCondition("flag", 0);
                Log.e(TAG, "tracklogsend ="+list.size());
                //adapter.delete(ctx, 0);
                for (int j=0; j<list.size();j++) {
                    sendData(list.get(j));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            ctx = this;

            utils   = new Utility(ctx);
            adapter = new TableTrackLogAdapter(ctx);
            userId = MyApplication.getInstance().getPrefManager().getUser().getId();

            //boolean status = (boolean) intent.getExtras().get("data");
            startService();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return START_STICKY;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("TrackLogService", "Service stoped");
        if (timer!=null) {
            timer.cancel();
        }
        //Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }

    private String generateId() {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append("track");
        sb.append("_");
        sb.append(userId);
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));

        sb.toString();
        return sb.toString();
    }

    private void sendData(final TableTrackLog item) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.TRACKLOG_ENTRY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        //listAttendanceModel.clear();
                        adapter.updatePartial(ctx, "flag", 1, "trackid", obj.getString("trackid"));
                        adapter.delete(ctx, 1);
                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("userid", item.getUserid());
                params.put("date", item.getDate());
                params.put("trackid", item.getTrackid());
                params.put("lng", item.getLng());
                params.put("lat", item.getLat());

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }
}
