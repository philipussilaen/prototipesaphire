package com.ish.SaphireTransmart.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableAttendance;
import com.ish.SaphireTransmart.database.TableImage;
import com.ish.SaphireTransmart.database.database_adapter.TableAttendanceAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableImageAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableTrackLogAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.utils.Utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by adminmc on 03/11/16.
 */

public class DeleteService extends Service {

        private String TAG = TrackLogService.class.getSimpleName();
        private static Timer timer = new Timer();
        private Context ctx;
        private Utility utils;
        private TableTrackLogAdapter adapter;
        private String userId="";

        @Nullable
        @Override
        public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate()
    {
        super.onCreate();

    }


    private void startService()
    {
        try {
          //  utils.setGeoLocation();
         //   timer.scheduleAtFixedRate(new DeleteService.mainTask(), 0, 60000);
            //60000
            //timer.scheduleAtFixedRate(new TrackLogService.requestServerTask(), 0, 3600000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class mainTask extends TimerTask
    {
        public void run()
        {
            //toastHandler.sendEmptyMessage(0);
            try {


                TableAttendanceAdapter adapter = new TableAttendanceAdapter(ctx);
                List<TableAttendance> listAttendance = adapter.getAllData();
                for (int i=0;i<listAttendance.size();i++) {
                    TableAttendance item = listAttendance.get(i);
                    if (getWeeks(item.getTanggal(), utils.getCurrentDate()) >= 14) {
                        adapter.delete(ctx, item.getId());
                    }
                }

                TableImageAdapter imageAdapter = new TableImageAdapter(ctx);
                List<TableImage> listImage = imageAdapter.getAllData();
                for (int i=0;i<listImage.size();i++) {
                    TableImage item = listImage.get(i);
                    if (getWeeks(item.getDate(), utils.getCurrentDateandTime()) >= 14) {
                        imageAdapter.delete(ctx, item.getId());
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            ctx = this;

            utils   = new Utility(ctx);
            adapter = new TableTrackLogAdapter(ctx);
            userId = MyApplication.getInstance().getPrefManager().getUser().getId();

            //boolean status = (boolean) intent.getExtras().get("data");
            startService();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return START_STICKY;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("TrackLogService", "Service stoped");
        if (timer!=null) {
            timer.cancel();
        }
        //Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }

    public static int getWeeks(String aa, String bb) {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date a, b;
        int workDays = 0;

        try {
            a = df.parse(aa);
            b = df.parse(bb);

            Calendar startCal = Calendar.getInstance();
            startCal.setTime(a);

            Calendar endCal = Calendar.getInstance();
            endCal.setTime(b);

            //Return 0 if start and end are the same
            if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
                return 0;
            }

            if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
                startCal.setTime(a);
                endCal.setTime(b);
            }

            do {
                //excluding start date
                startCal.add(Calendar.DAY_OF_MONTH, 1);
                if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                    ++workDays;
                }
            } while (startCal.getTimeInMillis() < endCal.getTimeInMillis()); //excluding end date

        } catch (Exception e) {
            e.printStackTrace();
        }
        return workDays;
    }
}
