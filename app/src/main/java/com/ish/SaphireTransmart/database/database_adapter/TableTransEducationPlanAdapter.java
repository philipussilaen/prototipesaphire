package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableTransEducationPlan;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 21/09/16.
 */
public class TableTransEducationPlanAdapter {

    static private TableTransEducationPlanAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableTransEducationPlanAdapter(ctx);
        }
    }

    static public TableTransEducationPlanAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableTransEducationPlanAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableTransEducationPlan> getAllData() {
        List<TableTransEducationPlan> tblsatu = null;
        try {
            tblsatu = getHelper().getTableTransEducationPlanDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableTransEducationPlan> getDatabyCondition(String condition, Object param) {
        List<TableTransEducationPlan> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableTransEducationPlanDAO();
            QueryBuilder<TableTransEducationPlan, Integer> queryBuilder = dao.queryBuilder();
            Where<TableTransEducationPlan, Integer> where = queryBuilder.where();
            where.eq(condition, Integer.parseInt(param.toString()));
            queryBuilder.orderBy("judul", true);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */

    public void insertData(TableTransEducationPlan tbl, String id, String userid, String kode_office, String tgl_input,
                           String tgl_edu, String waktu_awal, String waktu_akhir, String judul, String pembicara,
                           int jml_peserta , int flag, int flag_approval, String lup, String notes) {
        try {
            tbl.setId(id);
           //tbl.setProduct_category(id_sales);
            tbl.setUserid(userid);
            tbl.setKode_office(kode_office);
            tbl.setTgl_input(tgl_input);
            tbl.setTgl_edu(tgl_edu);
            tbl.setWaktu_awal(waktu_awal);
            tbl.setWaktu_akhir(waktu_akhir);
            tbl.setJudul(judul);
            tbl.setPembicara(pembicara);
            tbl.setNotes(notes);

            tbl.setJml_peserta(jml_peserta);

            tbl.setFlag(flag);
            tbl.setFlagApprove(flag_approval);
            tbl.setLup(lup);

            //  tbl.setUnixId(unixId);
            Log.d("save data ", tbl.toString());
            getHelper().getTableTransEducationPlanDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTransEducationPlan.class);
            UpdateBuilder<TableTransEducationPlan, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableTransEducationPlan> tbl = null;
        try {
            tbl = getHelper().getTableTransEducationPlanDAO().queryForAll();
            getHelper().getTableTransEducationPlanDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, String id) {
        try {
            getHelper().getTableTransEducationPlanDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTransEducationPlan.class);
            DeleteBuilder<TableTransEducationPlan, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
            Log.e("isitabelEDUPLAN", id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public List<TableTransEducationPlan> getAllDatabyLup() {
        List<TableTransEducationPlan> tblsatu = null;
        try {
            tblsatu = getHelper().getTableTransEducationPlanDAO()
                    .queryBuilder().orderBy("lup",
                            false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }
}
