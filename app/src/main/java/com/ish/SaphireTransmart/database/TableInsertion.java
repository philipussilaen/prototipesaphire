package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 16/10/16.
 */
@DatabaseTable(tableName = "m_insertion")
public class TableInsertion implements Serializable {

    public TableInsertion() {}

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String kode_office, product_category, id_product, nama_product;

    @DatabaseField
    private  int stock, actual, flag;

    public void setId(String id) {
        this.id = id;
    }

    public int getActual() {
        return actual;
    }

    public void setActual(int actual) {
        this.actual = actual;
    }

    public int getStock() {
        return stock;
    }

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getId() {
        return id;
    }

    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getId_product() {
        return id_product;
    }

    public void setProduct_category(String product_category) {
        this.product_category = product_category;
    }

    public String getKode_office() {
        return kode_office;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getProduct_category() {
        return product_category;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setNama_product(String nama_product) {
        this.nama_product = nama_product;
    }

    public String getNama_product() {
        return nama_product;
    }
}


