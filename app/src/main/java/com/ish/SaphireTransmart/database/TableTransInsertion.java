package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by adminmc on 16/10/16.
 */
@DatabaseTable(tableName = "trans_insertion")
public class TableTransInsertion {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String kode_office, product_category, id_product, clockin,image1,image2,userid;

    @DatabaseField
    private  int stock, actual, flag ;

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getProduct_category() {
        return product_category;
    }

    public void setProduct_category(String product_category) {
        this.product_category = product_category;
    }

    public String getKode_office() {
        return kode_office;
    }

    public void setActual(int actual) {
        this.actual = actual;
    }

    public String getId_product() {
        return id_product;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getActual() {
        return actual;
    }

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public int getStock() {
        return stock;
    }

    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getId() {
        return id;
    }

    public void setClockin(String clockin) {
        this.clockin = clockin;
    }

    public String getClockin() {
        return clockin;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }
    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

}
