package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableSellOut;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by ACER on 02/11/2016.
 */
public class TableSellOutAdapter {

    static private TableSellOutAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableSellOutAdapter(ctx);
        }
    }

    static public TableSellOutAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableSellOutAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableSellOut> getAllData() {
        List<TableSellOut> tblsatu = null;
        try {
            tblsatu = getHelper().getTableSellOut()
                    .queryBuilder().orderBy("perner",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableSellOut> getDatabyCondition(String condition, Object param) {
        List<TableSellOut> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableSellOut();
            QueryBuilder<TableSellOut, Integer> queryBuilder = dao.queryBuilder();
            Where<TableSellOut, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("perner", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableSellOut tbl, String perner, String nama, String kode_office,
                           String product,int normalprice,
                           String keterangan,
                           String periodeawal, String periodeakhir,String tgl_input,int flag,
                           String unixId, String photo,int promoprice,String competitor
                          ) {
        try {
            //tbl.setId(id);
            tbl.setPerner(perner);
            tbl.setNama(nama);
            tbl.setKode_office(kode_office);

            //tbl.setKategori_sellout(kategori_sellout);
            tbl.setProduct(product);
            tbl.setNormalPrice(normalprice);
            tbl.setKeterangan(keterangan);

            tbl.setFlag(flag);
            tbl.setUnixId(unixId);
            tbl.setPeriodeawal(periodeawal);
            tbl.setPeriodeakhir(periodeakhir);
            tbl.setTgl_input(tgl_input);
            tbl.setPhoto(photo);
            tbl.setPromoPrice(promoprice);
            tbl.setCompetitor(competitor);


            Log.d("save data ", tbl.toString());
            getHelper().getTableSellOut().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableSellOut.class);
            UpdateBuilder<TableSellOut, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableSellOut> tbl = null;
        try {
            tbl = getHelper().getTableSellOut().queryForAll();
            getHelper().getTableSellOut().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
