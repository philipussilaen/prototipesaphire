package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableMLogin;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by admin on 8/29/2016.
 */
public class TableMLoginAdapter {

    static private TableMLoginAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableMLoginAdapter(ctx);
        }
    }

    static public TableMLoginAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableMLoginAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableMLogin> getAllData() {
        List<TableMLogin> tblsatu = null;
        try {
            tblsatu = getHelper().getTableMLoginDAO()
                    .queryBuilder().orderBy("userid",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableMLogin> getDatabyCondition(String condition, Object param) {
        List<TableMLogin> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableMLoginDAO();
            QueryBuilder<TableMLogin, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMLogin, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("userid", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableMLogin> getDatabyUser(String condition1, String condition2,
                                            Object param1, Object param2) {
        List<TableMLogin> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableMLoginDAO();
            QueryBuilder<TableMLogin, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMLogin, Integer> where = queryBuilder.where();
            where.eq(condition1, param1);
            where.and();
            where.eq(condition2, param2);
            //queryBuilder.orderBy("userid", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableMLogin tbl, String  userid, String username,
                           String password, String userlevel, String userstatus, String jenis_absen,
                           String area, String perner, String xarea, String kd_area,
                           String klien_layanan, String kd_layanan, String sub_area, String kd_sub_area,
                           String jabatan, String skill_layanan, String kd_skill_layanan,
                           String fail_login, String ket, String upd, String region, String teritory) {
        try {
            tbl.setUserid(userid);
            tbl.setUsername(username);
            tbl.setPassword(password);
            tbl.setUserlevel(userlevel);
            tbl.setUserstatus(userstatus);
            tbl.setJenis_absen(jenis_absen);
            tbl.setArea(area);
            tbl.setPerner(perner);
            tbl.setXarea(xarea);
            tbl.setKd_area(kd_area);
            tbl.setKlien_layanan(klien_layanan);
            tbl.setKd_layanan(kd_layanan);
            tbl.setSub_area(sub_area);
            tbl.setKd_sub_area(kd_sub_area);
            tbl.setJabatan(jabatan);
            tbl.setSkill_layanan(skill_layanan);
            tbl.setKd_skill_layanan(kd_skill_layanan);
            tbl.setFail_login(fail_login);
            tbl.setKet(ket);
            tbl.setUpd(upd);
            tbl.setRegion(region);
            tbl.setTeritory(teritory);
            getHelper().getTableMLoginDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMLogin.class);
            UpdateBuilder<TableMLogin, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableMLogin> tbl = null;
        try {
            tbl = getHelper().getTableMLoginDAO().queryForAll();
            getHelper().getTableMLoginDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, String id) {
        try {
            getHelper().getTableMLoginDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMLogin.class);
            DeleteBuilder<TableMLogin, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("userid", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public List<TableMLogin> getAllDatabyLup() {
        List<TableMLogin>tblsatu = null;
        try {
            tblsatu = getHelper().getTableMLoginDAO()
                    .queryBuilder().orderBy("lup",
                            false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

}
