package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 14/10/16.
 */
@DatabaseTable(tableName = "trans_outgimmick")
public class TableTransOutGimmick implements Serializable {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String id_gimmick, userid, nama, image1, image2, date, kode_office;


    @DatabaseField
    private int jumlah, flag;

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public String getId() {
        return id;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setId_gimmick(String id_gimmick) {
        this.id_gimmick = id_gimmick;
    }

    public String getId_gimmick() {
        return id_gimmick;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage2() {
        return image2;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getKode_office() {
        return kode_office;
    }
}
