package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 8/30/2016.
 */
@DatabaseTable(tableName = "m_office")
public class TableMOffice {
    @DatabaseField(id = true)
    private int id;

    @DatabaseField
    private String office, kode_office, channel, jenis, alamat, kota, propinsi, pic,
            pic_phone, map_lat, map_lng, upd, lup, ket, region,rek, bank, rental,teritory;;

    public void setOffice(String office) {
        this.office = office;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getChannel() {
        return channel;
    }


    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public String getKode_office() {
        return kode_office;
    }

    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getLup() {
        return lup;
    }

    public void setLup(String lup) {
        this.lup = lup;
    }

    public String getMap_lat() {
        return map_lat;
    }

    public void setMap_lat(String map_lat) {
        this.map_lat = map_lat;
    }

    public String getMap_lng() {
        return map_lng;
    }

    public void setMap_lng(String map_lng) {
        this.map_lng = map_lng;
    }

    public String getOffice() {
        return office;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPic() {
        return pic;
    }

    public void setPic_phone(String pic_phone) {
        this.pic_phone = pic_phone;
    }

    public String getPic_phone() {
        return pic_phone;
    }

    public void setPropinsi(String propinsi) {
        this.propinsi = propinsi;
    }

    public String getPropinsi() {
        return propinsi;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public String getUpd() {
        return upd;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBank() {


        if (bank == null){
            return "";
        } else if (bank.equalsIgnoreCase("null")){
            return "";
        }else{
            return bank;
        }

    }

    public void setRek(String rek) {
        this.rek = rek;
    }

    public String getRek() {
        if (rek == null){
            return "";
        } else if (rek.equalsIgnoreCase("null")){
            return "";
        }else{
            return rek;
        }



    }

    public void setRental(String rental) {
        this.rental = rental;
    }

    public String getRental() {

        if (rental == null){
            return "";
        } else if (rental.equalsIgnoreCase("null")){
            return "";
        }else{
            return rental;
        }


    }
    public void setTeritory(String teritory) {
        this.teritory = teritory;
    }

    public String getTeritory() {
        return teritory;
    }
}
