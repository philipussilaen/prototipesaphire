package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 12/10/16.
 */
@DatabaseTable(tableName = "m_product_category")
public class TableProdukKategori implements Serializable {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String name, image;

    @DatabaseField
    int isinserting;

    public TableProdukKategori() {}

    public void setId(String id) {
        this.id = id;
    }

    public int getIsinserting() {
        return isinserting;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setIsinserting(int isinserting) {
        this.isinserting = isinserting;
    }

    public String getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
