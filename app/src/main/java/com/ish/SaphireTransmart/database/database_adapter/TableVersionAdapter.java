package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableAppVersion;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by admin on 8/29/2016.
 */
public class TableVersionAdapter {

    static private TableVersionAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableVersionAdapter(ctx);
        }
    }

    static public TableVersionAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableVersionAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableAppVersion> getAllData() {
        List<TableAppVersion> tblsatu = null;
        try {
            tblsatu = getHelper().getTableAppVersionDAO()
                    .queryBuilder().orderBy("app_version_code",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableAppVersion> getDatabyCondition(String condition, Object param) {
        List<TableAppVersion> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableAppVersionDAO();
            QueryBuilder<TableAppVersion, Integer> queryBuilder = dao.queryBuilder();
            Where<TableAppVersion, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("app_version_code", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableAppVersion tbl, String app_version_code, String app_version_name,
                           String app_desc, String url, String kd_user, String tgl_upload) {
        try {
            tbl.setApp_version_code(app_version_code);
            tbl.setApp_version_name(app_version_name);
            tbl.setApp_desc(app_desc);
            tbl.setUrl_encrypt(url);
            tbl.setKd_user(kd_user);
            tbl.setTgl_upload(tgl_upload);

            getHelper().getTableAppVersionDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableAppVersion.class);
            UpdateBuilder<TableAppVersion, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableAppVersion> tbl = null;
        try {
            tbl = getHelper().getTableAppVersionDAO().queryForAll();
            getHelper().getTableAppVersionDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
