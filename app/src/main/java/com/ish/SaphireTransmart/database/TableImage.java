package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 14/09/16.
 */
@DatabaseTable(tableName = "m_image")
public class TableImage implements Serializable {

    public TableImage() {}

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String id_image, id_absen;

    @DatabaseField
    private String image, name, m_path, userid, type, date;

    @DatabaseField
    private  int index, total, flag;

    public void setId(String id) {
        this.id = id;
    }

    public String  getId() {
        return id;
    }

    public void setId_image(String id_image) {
        this.id_image = id_image;
    }

    public String getId_image() {
        return id_image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setM_path(String m_path) {
        this.m_path = m_path;
    }

    public String getM_path() {
        return m_path;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setId_absen(String id_absen) {
        this.id_absen = id_absen;
    }

    public String getId_absen() {
        return id_absen;
    }
}
