package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 12/10/16.
 */
@DatabaseTable(tableName = "m_gimmick")
public class TableMGimmick implements Serializable {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String name, period, upd, lup, ket;

    @DatabaseField
    private int outstanding;

    public TableMGimmick() {}

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public int getOutstanding() {
        return outstanding;
    }

    public void setLup(String lup) {
        this.lup = lup;
    }

    public String getKet() {
        return ket;
    }

    public void setOutstanding(int outstanding) {
        this.outstanding = outstanding;
    }

    public String getLup() {
        return lup;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPeriod() {
        return period;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public String getUpd() {
        return upd;
    }

}
