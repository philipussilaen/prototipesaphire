package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableMOffice;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by admin on 8/30/2016.
 */
public class TableMOfficeAdapter {

    static private TableMOfficeAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableMOfficeAdapter(ctx);
        }
    }

    static public TableMOfficeAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableMOfficeAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableMOffice> getAllData() {
        List<TableMOffice> tblsatu = null;
        try {
            tblsatu = getHelper().getTableMOfficeDAO()
                    .queryBuilder().orderBy("office",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableMOffice> getDatabyCondition(String condition, Object param) {
        List<TableMOffice> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableMOfficeDAO();
            QueryBuilder<TableMOffice, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMOffice, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("office", true);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableMOffice tbl, int id, String office, String kode_office, String channel,
                           String jenis, String alamat, String kota, String propinsi, String pic,
                           String pic_phone, String map_lat, String map_lng, String upd, String lup,
                           String ket, String region, String rek, String bank, String rental,String teritory) {
        try {
            tbl.setId(id);
            tbl.setOffice(office);
            tbl.setJenis(jenis);
            tbl.setKode_office(kode_office);
            tbl.setChannel(channel);
            tbl.setAlamat(alamat);
            tbl.setKota(kota);
            tbl.setPropinsi(propinsi);
            tbl.setPic(pic);
            tbl.setPic_phone(pic_phone);
            tbl.setMap_lat(map_lat);
            tbl.setMap_lng(map_lng);
            tbl.setUpd(upd);
            tbl.setLup(lup);
            tbl.setKet(ket);
            tbl.setRegion(region);
            tbl.setRek(rek);
            tbl.setBank(bank);
            tbl.setRental(rental);
            tbl.setTeritory(teritory);

            getHelper().getTableMOfficeDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMOffice.class);
            UpdateBuilder<TableMOffice, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {

        List<TableMOffice> tbl = null;
        try {

            tbl = getHelper().getTableMOfficeDAO().queryForAll();
            getHelper().getTableMOfficeDAO().delete(tbl);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, int id) {
        try {
            getHelper().getTableMOfficeDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMOffice.class);
            DeleteBuilder<TableMOffice, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
            Log.e("isitabeloffice", Integer.toString(id));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<TableMOffice> getAllDatabyLup() {
        List<TableMOffice> tblsatu = null;
        try {
            tblsatu = getHelper().getTableMOfficeDAO()
                    .queryBuilder().orderBy("lup",
                            false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }
}
