package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 21/09/16.
 */
@DatabaseTable(tableName = "m_displayproduct")
public class TableProduk implements Serializable {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String product_category, model_name, channel, entity, price, url,region_prod,detail_category;

    @DatabaseField
    int qty, gram, total_jual, flag;

    public TableProduk() {}

    public TableProduk(int id, String product_category, String model_name, String gram, String channel,
                       String entity, String price, String url) {}

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setGram(int gram) {
        this.gram = gram;
    }

    public int getGram() {
        return gram;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getChannel() {
        return channel;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getEntity() {
        return entity;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setProduct_category(String product_category) {
        this.product_category = product_category;
    }

    public String getProduct_category() {
        return product_category;
    }

    public void setDetail_category(String detail_category) {
        this.detail_category = detail_category;
    }

    public String getDetail_category() {
        return detail_category;
    }



    public void setRegion_prod(String region_prod) {
        this.region_prod = region_prod;
    }

    public String getRegion_prod() {
        return region_prod;
    }




    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getQty() {
        return qty;
    }

    public void setTotal_jual(int total_jual) {
        this.total_jual = total_jual;
    }

    public int getTotal_jual() {
        return total_jual;
    }
}
