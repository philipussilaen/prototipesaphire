package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableDisplay;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by ACER on 02/11/2016.
 */
public class TableDisplayAdapter {

    static private TableDisplayAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableDisplayAdapter(ctx);
        }
    }

    static public TableDisplayAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableDisplayAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableDisplay> getAllData() {
        List<TableDisplay> tblsatu = null;
        try {
            tblsatu = getHelper().getTableDisplay()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableDisplay> getDatabyCondition(String condition, Object param) {
        List<TableDisplay> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableDisplay();
            QueryBuilder<TableDisplay, Integer> queryBuilder = dao.queryBuilder();
            Where<TableDisplay, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableDisplay tbl, String kode_office,
                           String display,
                           int flag, String photo, String unixId, String tgl_input, String clockin
//                           String periodeawal, String periodeakhir
                          ) {
        try {
            tbl.setKode_office(kode_office);
            tbl.setDisplay(display);
//            tbl.setSize(size);
//            tbl.setActivity(activity);
//            tbl.setArea_activity(area_activity);
            tbl.setFlag(flag);
            tbl.setUnixId(unixId);
            tbl.setTgl_input(tgl_input);
            tbl.setPhoto(photo);
            tbl.setClockin(clockin);
//            tbl.setKeterangan(keterangan);
//            tbl.setPeriodeawal(periodeawal);
//            tbl.setPeriodeakhir(periodeakhir);

            Log.d("save data ", tbl.toString());
            getHelper().getTableDisplay().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableDisplay.class);
            UpdateBuilder<TableDisplay, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableDisplay> tbl = null;
        try {
            tbl = getHelper().getTableDisplay().queryForAll();
            getHelper().getTableDisplay().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
