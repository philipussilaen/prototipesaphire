package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableStatusDamage;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 21/09/16.
 */
public class TableStatusDamageAdapter {

    static private TableStatusDamageAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableStatusDamageAdapter(ctx);
        }
    }

    static public TableStatusDamageAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableStatusDamageAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableStatusDamage> getAllData() {
        List<TableStatusDamage> tblsatu = null;
        try {
            tblsatu = getHelper().getTableStatusDamage()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }



    /**
     * Get Data By ID
     * @return
     */
    public List<TableStatusDamage> getDatabyCondition(String condition, Object param) {
        List<TableStatusDamage> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableStatusDamage();
            QueryBuilder<TableStatusDamage, Integer> queryBuilder = dao.queryBuilder();
            Where<TableStatusDamage, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableStatusDamage tbl, int id, String status_damage) {
        try {
            tbl.setStatus_damage(status_damage);
            tbl.setId(id);

            getHelper().getTableStatusDamage().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableStatusDamage.class);
            UpdateBuilder<TableStatusDamage, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableStatusDamage> tbl = null;
        try {
            tbl = getHelper().getTableStatusDamage().queryForAll();
            getHelper().getTableStatusDamage().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, int id) {
        try {
            getHelper().getTableStatusDamage().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableStatusDamage.class);
            DeleteBuilder<TableStatusDamage, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public List<TableStatusDamage> getAllDatabyLup() {
        List<TableStatusDamage> tblsatu = null;
        try {
            tblsatu = getHelper().getTableStatusDamage()
                    .queryBuilder().orderBy("lup",
                            false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }
}
