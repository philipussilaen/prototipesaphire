package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableTransInsertion;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 16/10/16.
 */
public class TableTransInsertionAdapter {
    static private TableTransInsertionAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableTransInsertionAdapter(ctx);
        }
    }

    static public TableTransInsertionAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableTransInsertionAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableTransInsertion> getAllData() {
        List<TableTransInsertion> tblsatu = null;
        try {
            tblsatu = getHelper().getTableTransInsertionDAO()
                    .queryBuilder().orderBy("product_category",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableTransInsertion> getDatabyCondition(String condition, Object param) {
        List<TableTransInsertion> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableTransInsertionDAO();
            QueryBuilder<TableTransInsertion, Integer> queryBuilder = dao.queryBuilder();
            Where<TableTransInsertion, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("product_category", true);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableTransInsertion tbl, String id, String kode_office, String product_category,
                           String id_product, String clockin, int stock, int actual, int flag, String image1, String image2, String userid) {
        try {
            tbl.setId(id);
            tbl.setId_product(id_product);
            tbl.setKode_office(kode_office);
            tbl.setProduct_category(product_category);
            tbl.setStock(stock);
            tbl.setActual(actual);
            tbl.setFlag(flag);
            tbl.setClockin(clockin);
            tbl.setImage1(image1);
            tbl.setImage2(image2);
            tbl.setUserid(userid);

            getHelper().getTableTransInsertionDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTransInsertion.class);
            UpdateBuilder<TableTransInsertion, String> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableTransInsertion> tbl = null;
        try {
            tbl = getHelper().getTableTransInsertionDAO().queryForAll();
            getHelper().getTableTransInsertionDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
