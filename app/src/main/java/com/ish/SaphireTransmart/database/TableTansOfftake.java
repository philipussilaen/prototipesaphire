package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 21/09/16.
 */
@DatabaseTable(tableName = "m_trans_offtake")
public class TableTansOfftake implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String userid, time, office, product_category, model_name, ket_status,userid_approval,
            ip_address, mc_address, clockin, unixId;

    @DatabaseField
    private int gram, qty, flag;

    public TableTansOfftake() {}

    public void setProduct_category(String product_category) {
        this.product_category = product_category;
    }

    public String getProduct_category() {
        return product_category;
    }

    public void setModel_name(String model_name) {
        this.model_name = model_name;
    }

    public String getModel_name() {
        return model_name;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setGram(int gram) {
        this.gram = gram;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGram() {
        return gram;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public int getId() {
        return id;
    }

    public void setKet_status(String ket_status) {
        this.ket_status = ket_status;
    }

    public int getQty() {
        return qty;
    }

    public void setMc_address(String mc_address) {
        this.mc_address = mc_address;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getKet_status() {
        return ket_status;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getMc_address() {
        return mc_address;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getOffice() {
        return office;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getTime() {
        return time;
    }

    public void setUserid_approval(String userid_approval) {
        this.userid_approval = userid_approval;
    }

    public String getUserid() {
        return userid;
    }

    public String getUserid_approval() {
        return userid_approval;
    }

    public void setClockin(String clockin) {
        this.clockin = clockin;
    }

    public String getClockin() {
        return clockin;
    }

    public void setUnixId(String unixId) {
        this.unixId = unixId;
    }

    public String getUnixId() {
        return unixId;
    }
}
