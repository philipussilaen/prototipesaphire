package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableStatusFeed;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 21/09/16.
 */
public class TableStatusFeedAdapter {

    static private TableStatusFeedAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableStatusFeedAdapter(ctx);
        }
    }

    static public TableStatusFeedAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableStatusFeedAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableStatusFeed> getAllData() {
        List<TableStatusFeed> tblsatu = null;
        try {
            tblsatu = getHelper().getTableStatusFeed()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }



    /**
     * Get Data By ID
     * @return
     */
    public List<TableStatusFeed> getDatabyCondition(String condition, Object param) {
        List<TableStatusFeed> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableStatusFeed();
            QueryBuilder<TableStatusFeed, Integer> queryBuilder = dao.queryBuilder();
            Where<TableStatusFeed, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableStatusFeed tbl, int id,String status_feed) {
        try {
            tbl.setStatus_feed(status_feed);
            tbl.setId(id);
            getHelper().getTableStatusFeed().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableStatusFeed.class);
            UpdateBuilder<TableStatusFeed, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableStatusFeed> tbl = null;
        try {
            tbl = getHelper().getTableStatusFeed().queryForAll();
            getHelper().getTableStatusFeed().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, int id) {
        try {
            getHelper().getTableStatusFeed().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableStatusFeed.class);
            DeleteBuilder<TableStatusFeed, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public List<TableStatusFeed> getAllDatabyLup() {
        List<TableStatusFeed> tblsatu = null;
        try {
            tblsatu = getHelper().getTableStatusFeed()
                    .queryBuilder().orderBy("lup",
                            false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }
}
