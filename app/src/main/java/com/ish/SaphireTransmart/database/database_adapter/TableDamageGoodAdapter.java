package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableDamageGood;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by ACER on 02/11/2016.
 */
public class TableDamageGoodAdapter {

    static private TableDamageGoodAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableDamageGoodAdapter(ctx);
        }
    }

    static public TableDamageGoodAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableDamageGoodAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableDamageGood> getAllData() {
        List<TableDamageGood> tblsatu = null;
        try {
            tblsatu = getHelper().getTableDamageGood()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableDamageGood> getDatabyCondition(String condition, Object param) {
        List<TableDamageGood> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableDamageGood();
            QueryBuilder<TableDamageGood, Integer> queryBuilder = dao.queryBuilder();
            Where<TableDamageGood, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableDamageGood tbl, String kode_office,
                           String product_type,int qty,
                           String serial_number,
                           String type_damage, String keterangan,String tgl_input,int flag,
                           String unixId, String photo,String photo2,String photo3,String status,String userid
                          ) {
        try {
            //tbl.setId(id);

            tbl.setKode_office(kode_office);
            tbl.setProduct_type(product_type);
            tbl.setQty(qty);
            tbl.setSerial_number(serial_number);
            tbl.setType_damage(type_damage);
            tbl.setKeterangan(keterangan);
            tbl.setTgl_input(tgl_input);
            tbl.setFlag(flag);
            tbl.setUnixId(unixId);
            tbl.setPhoto(photo);
            tbl.setPhoto2(photo2);
            tbl.setPhoto3(photo3);
            tbl.setStatus(status);
            tbl.setUserid(userid);

            Log.d("save data ", tbl.toString());
            getHelper().getTableDamageGood().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableDamageGood.class);
            UpdateBuilder<TableDamageGood, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableDamageGood> tbl = null;
        try {
            tbl = getHelper().getTableDamageGood().queryForAll();
            getHelper().getTableDamageGood().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
