package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 26/09/16.
 */
@DatabaseTable(tableName = "m_visibility")
public class TableVisibility implements Serializable {

    @DatabaseField(id = true)
    private String visibility_id;

    @DatabaseField
    private String userid, kategori, tipe, clockin, office,image1,image2;

    @DatabaseField
    private int isClean, isRightPosition, isGoodCondition, isTopShelf, isSheflFull, flag;

    public TableVisibility() {}

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

    public void setIsClean(int isClean) {
        this.isClean = isClean;
    }

    public int getIsClean() {
        return isClean;
    }

    public void setIsGoodCondition(int isGoodCondition) {
        this.isGoodCondition = isGoodCondition;
    }

    public int getIsGoodCondition() {
        return isGoodCondition;
    }

    public void setIsRightPosition(int isRightPosition) {
        this.isRightPosition = isRightPosition;
    }

    public int getIsRightPosition() {
        return isRightPosition;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getKategori() {
        return kategori;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getTipe() {
        return tipe;
    }

    public void setVisibility_id(String visibility_id) {
        this.visibility_id = visibility_id;
    }

    public String getVisibility_id() {
        return visibility_id;
    }

    public void setIsSheflFull(int isSheflFull) {
        this.isSheflFull = isSheflFull;
    }

    public int getIsSheflFull() {
        return isSheflFull;
    }

    public void setIsTopShelf(int isTopShelf) {
        this.isTopShelf = isTopShelf;
    }

    public int getIsTopShelf() {
        return isTopShelf;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setClockin(String clockin) {
        this.clockin = clockin;
    }

    public String getClockin() {
        return clockin;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getOffice() {
        return office;
    }
    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }
    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

}
