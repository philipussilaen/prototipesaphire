package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 8/29/2016.
 */
@DatabaseTable(tableName = "app_version")
public class TableAppVersion {
    @DatabaseField(id = true)
    private String app_version_code;

    @DatabaseField
    private String app_version_name, app_desc, url_encrypt, kd_user, tgl_upload;

    public void setApp_desc(String app_desc) {
        this.app_desc = app_desc;
    }

    public String getApp_desc() {
        return app_desc;
    }

    public void setApp_version_code(String app_version_code) {
        this.app_version_code = app_version_code;
    }

    public String getApp_version_code() {
        return app_version_code;
    }

    public void setApp_version_name(String app_version_name) {
        this.app_version_name = app_version_name;
    }

    public String getApp_version_name() {
        return app_version_name;
    }

    public void setKd_user(String kd_user) {
        this.kd_user = kd_user;
    }

    public String getKd_user() {
        return kd_user;
    }

    public void setTgl_upload(String tgl_upload) {
        this.tgl_upload = tgl_upload;
    }

    public String getTgl_upload() {
        return tgl_upload;
    }

    public void setUrl_encrypt(String url_encrypt) {
        this.url_encrypt = url_encrypt;
    }

    public String getUrl_encrypt() {
        return url_encrypt;
    }
}
