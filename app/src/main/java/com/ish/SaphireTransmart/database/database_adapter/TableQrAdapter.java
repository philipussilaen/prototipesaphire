package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableQr;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by ACER on 02/11/2016.
 */
public class TableQrAdapter {

    static private TableQrAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableQrAdapter(ctx);
        }
    }

    static public TableQrAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableQrAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableQr> getAllData() {
        List<TableQr> tblsatu = null;
        try {
            tblsatu = getHelper().getTableQrDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableQr> getDatabyCondition(String condition, Object param) {
        List<TableQr> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableQrDAO();
            QueryBuilder<TableQr, Integer> queryBuilder = dao.queryBuilder();
            Where<TableQr, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableQr tbl, String userid_pelanggan, String userid_kasir, String kode_office,
                           int total_belanja, int flag, String unixId) {
        try {
            //tbl.setId(id);
            tbl.setUserid_pelanggan(userid_pelanggan);
            tbl.setUserid_kasir(userid_kasir);
            tbl.setKode_office(kode_office);
            tbl.setTotal_belanja(total_belanja);
            tbl.setFlag(flag);
            tbl.setUnixId(unixId);

            Log.d("save data ", tbl.toString());
            getHelper().getTableQrDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableQr.class);
            UpdateBuilder<TableQr, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableQr> tbl = null;
        try {
            tbl = getHelper().getTableQrDAO().queryForAll();
            getHelper().getTableQrDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, int id) {
        try {
            getHelper().getTableQrDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableQr.class);
            DeleteBuilder<TableQr, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
