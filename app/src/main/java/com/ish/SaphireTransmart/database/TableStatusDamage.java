package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 21/09/16.
 */
@DatabaseTable(tableName = "status_damage")
public class TableStatusDamage implements Serializable {

    @DatabaseField
    private int id;

    @DatabaseField
    private String status_damage;


    public TableStatusDamage() {}

    public TableStatusDamage(int id, String status_damage) {}

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setStatus_damage(String type_damage) {
        this.status_damage = type_damage;
    }

    public String getStatus_damage() {
        return status_damage;
    }



}
