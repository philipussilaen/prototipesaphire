package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableProduk;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 21/09/16.
 */
public class TableProdukAdapter {

    static private TableProdukAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableProdukAdapter(ctx);
        }
    }

    static public TableProdukAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableProdukAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableProduk> getAllData() {
        List<TableProduk> tblsatu = null;
        try {
            tblsatu = getHelper().getTableProdukDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    public List<TableProduk> getDatabyConditionz(String condition, Object param,String condition2, Object param2,String condition3, Object param3) {
        List<TableProduk> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableProdukDAO();
            QueryBuilder<TableProduk, Integer> queryBuilder = dao.queryBuilder();
            Where<TableProduk, Integer> where = queryBuilder.where();
            where.eq(condition, param).and().eq(condition2,param2).and().eq(condition3,param3);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableProduk> getDatabyCondition(String condition, Object param) {
        List<TableProduk> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableProdukDAO();
            QueryBuilder<TableProduk, Integer> queryBuilder = dao.queryBuilder();
            Where<TableProduk, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableProduk tbl, String id, String product_category, String model_name,
                           int gram, String channel, String entity, String price, String url, int qty,String region_prod,String detail_category) {
        try {
            tbl.setId(id);
            tbl.setProduct_category(product_category);
            tbl.setModel_name(model_name);
            tbl.setGram(gram);
            tbl.setChannel(channel);
            tbl.setEntity(entity);
            tbl.setPrice(price);
            tbl.setUrl(url);
            tbl.setQty(qty);
            tbl.setRegion_prod(region_prod);
            tbl.setDetail_category(detail_category);

            getHelper().getTableProdukDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableProduk.class);
            UpdateBuilder<TableProduk, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableProduk> tbl = null;
        try {
            tbl = getHelper().getTableProdukDAO().queryForAll();
            getHelper().getTableProdukDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, String id) {
        try {
            getHelper().getTableProdukDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableProduk.class);
            DeleteBuilder<TableProduk, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public List<TableProduk> getAllDatabyLup() {
        List<TableProduk> tblsatu = null;
        try {
            tblsatu = getHelper().getTableProdukDAO()
                    .queryBuilder().orderBy("lup",
                            false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }
}
