package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 21/09/16.
 */
@DatabaseTable(tableName = "m_judul")
public class TableJudul implements Serializable {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String judul;


    public TableJudul() {}

    public TableJudul(int id, String product_category, String model_name, String gram, String channel,
                      String entity, String price, String url) {}

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getJudul() {
        return judul;
    }


}
