package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TablePembicara;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 21/09/16.
 */
public class TablePembicaraAdapter {

    static private TablePembicaraAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TablePembicaraAdapter(ctx);
        }
    }

    static public TablePembicaraAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TablePembicaraAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TablePembicara> getAllData() {
        List<TablePembicara> tblsatu = null;
        try {
            tblsatu = getHelper().getTablePembicaraDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TablePembicara> getDatabyCondition(String condition, Object param) {
        List<TablePembicara> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTablePembicaraDAO();
            QueryBuilder<TablePembicara, Integer> queryBuilder = dao.queryBuilder();
            Where<TablePembicara, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TablePembicara tbl, String id, String pembicara) {
        try {
            tbl.setId(id);
            tbl.setPembicara(pembicara);


            getHelper().getTablePembicaraDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TablePembicara.class);
            UpdateBuilder<TablePembicara, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TablePembicara> tbl = null;
        try {
            tbl = getHelper().getTablePembicaraDAO().queryForAll();
            getHelper().getTablePembicaraDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, String id) {
        try {
            getHelper().getTablePembicaraDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TablePembicara.class);
            DeleteBuilder<TablePembicara, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public List<TablePembicara> getAllDatabyLup() {
        List<TablePembicara> tblsatu = null;
        try {
            tblsatu = getHelper().getTablePembicaraDAO()
                    .queryBuilder().orderBy("lup",
                            false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }
}
