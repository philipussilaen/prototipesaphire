package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableTypeDamage;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 21/09/16.
 */
public class TableTypeDamageAdapter {

    static private TableTypeDamageAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableTypeDamageAdapter(ctx);
        }
    }

    static public TableTypeDamageAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableTypeDamageAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableTypeDamage> getAllData() {
        List<TableTypeDamage> tblsatu = null;
        try {
            tblsatu = getHelper().getTableTypeDamage()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }



    /**
     * Get Data By ID
     * @return
     */
    public List<TableTypeDamage> getDatabyCondition(String condition, Object param) {
        List<TableTypeDamage> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableTypeDamage();
            QueryBuilder<TableTypeDamage, Integer> queryBuilder = dao.queryBuilder();
            Where<TableTypeDamage, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableTypeDamage tbl, int id, String type_damage,String kategori) {
        try {
            tbl.setId(id);
            tbl.setType_damage(type_damage);
            tbl.setKategori(kategori);

            getHelper().getTableTypeDamage().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTypeDamage.class);
            UpdateBuilder<TableTypeDamage, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableTypeDamage> tbl = null;
        try {
            tbl = getHelper().getTableTypeDamage().queryForAll();
            getHelper().getTableTypeDamage().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, int id) {
        try {
            getHelper().getTableTypeDamage().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTypeDamage.class);
            DeleteBuilder<TableTypeDamage, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public List<TableTypeDamage> getAllDatabyLup() {
        List<TableTypeDamage> tblsatu = null;
        try {
            tblsatu = getHelper().getTableTypeDamage()
                    .queryBuilder().orderBy("lup",
                            false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }
}
