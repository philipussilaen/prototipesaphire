package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableCompetitor;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by ACER on 02/11/2016.
 */
public class TableCompetitorAdapter {

    static private TableCompetitorAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableCompetitorAdapter(ctx);
        }
    }

    static public TableCompetitorAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableCompetitorAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableCompetitor> getAllData() {
        List<TableCompetitor> tblsatu = null;
        try {
            tblsatu = getHelper().getTableCompetitor()
                    .queryBuilder().orderBy("perner",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableCompetitor> getDatabyCondition(String condition, Object param) {
        List<TableCompetitor> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableCompetitor();
            QueryBuilder<TableCompetitor, Integer> queryBuilder = dao.queryBuilder();
            Where<TableCompetitor, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("perner", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableCompetitor tbl, String perner, String nama, String kode_office,
                           String product,int normalprice,
                           String keterangan,
                           String periodeawal, String periodeakhir,String tgl_input,int flag,
                           String unixId, String photo,String photo2,String photo3,String photo4,String photo5,int promoprice,String competitor
                          ) {
        try {
            //tbl.setId(id);
            tbl.setPerner(perner);
            tbl.setNama(nama);
            tbl.setKode_office(kode_office);

            //tbl.setKategori_sellout(kategori_sellout);
            tbl.setProduct(product);
            tbl.setNormalPrice(normalprice);
            tbl.setKeterangan(keterangan);

            tbl.setFlag(flag);
            tbl.setUnixId(unixId);
            tbl.setPeriodeawal(periodeawal);
            tbl.setPeriodeakhir(periodeakhir);
            tbl.setTgl_input(tgl_input);
            tbl.setPhoto(photo);
            tbl.setPhoto2(photo2);
            tbl.setPhoto3(photo3);
            tbl.setPhoto4(photo4);
            tbl.setPhoto5(photo5);
            tbl.setPromoPrice(promoprice);
            tbl.setCompetitor(competitor);


            Log.d("save data ", tbl.toString());
            getHelper().getTableCompetitor().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableCompetitor.class);
            UpdateBuilder<TableCompetitor, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableCompetitor> tbl = null;
        try {
            tbl = getHelper().getTableCompetitor().queryForAll();
            getHelper().getTableCompetitor().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
