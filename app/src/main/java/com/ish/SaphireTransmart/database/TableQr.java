package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by ACER on 02/11/2016.
 */

    @DatabaseTable(tableName = "trans_qr")
    public class TableQr implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String userid_pelanggan, userid_kasir, kode_office,  unixId;

    @DatabaseField
    private int total_jual, flag, total_belanja;

    public TableQr() {}

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }


    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getKode_office() {
        return kode_office;
    }


    public void setTotal_belanja(int total_belanja) {
        this.total_belanja = total_belanja;
    }

    public int getTotal_belanja() {
        return total_belanja;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }


    public String getUserid_pelanggan() {
        return userid_pelanggan;
    }

    public void setUserid_pelanggan(String userid_pelanggan) {
        this.userid_pelanggan = userid_pelanggan;
    }

    public String getUserid_kasir() {
        return userid_kasir;
    }

    public void setUserid_kasir(String userid_kasir) {
        this.userid_kasir = userid_kasir;
    }


    public void setUnixId(String unixId) {
        this.unixId = unixId;
    }

    public String getUnixId() {
        return unixId;
    }
}
