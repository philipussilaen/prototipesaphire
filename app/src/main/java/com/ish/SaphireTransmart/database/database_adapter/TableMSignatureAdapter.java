package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableMSIgnature;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 13/09/16.
 */
public class TableMSignatureAdapter {

    static private TableMSignatureAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableMSignatureAdapter(ctx);
        }
    }

    static public TableMSignatureAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableMSignatureAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableMSIgnature> getAllData() {
        List<TableMSIgnature> tblsatu = null;
        try {
            tblsatu = getHelper().getTableSignatureDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableMSIgnature> getDatabyCondition(String condition, Object param) {
        List<TableMSIgnature> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableSignatureDAO();
            QueryBuilder<TableMSIgnature, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMSIgnature, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableMSIgnature tbl, String id, String path, String name,
                           String image, int status) {
        try {
            tbl.setId(id);
            tbl.setPath(path);
            tbl.setName(name);
            tbl.setImage(image);
            tbl.setStatus(status);

            getHelper().getTableSignatureDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMSIgnature.class);
            UpdateBuilder<TableMSIgnature, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableMSIgnature> tbl = null;
        try {
            tbl = getHelper().getTableSignatureDAO().queryForAll();
            getHelper().getTableSignatureDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
