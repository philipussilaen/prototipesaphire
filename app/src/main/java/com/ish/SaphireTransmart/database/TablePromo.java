package com.ish.SaphireTransmart.database;



import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by ACER on 02/11/2016.
 */

    @DatabaseTable(tableName = "promo")
    public class TablePromo implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String kode_office, product_type, competitor, activity,area_activity,unixId, tgl_input, photo,keterangan, periodeawal, periodeakhir,userid;

    @DatabaseField
    private int flag;

    public TablePromo() {}

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getProduct_type() {
        return product_type;
    }

    public String getTgl_input() {
        return tgl_input;
    }

    public void setTgl_input(String tgl_input) {
        this.tgl_input = tgl_input;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

    public String getPeriodeawal() {
        return periodeawal;
    }

    public void setPeriodeawal(String periodeawal) {
        this.periodeawal = periodeawal;
    }
    public String getPeriodeakhir() {
        return periodeakhir;
    }

    public void setPeriodeakhir(String periodeakhir) {
        this.periodeakhir = periodeakhir;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setUnixId(String unixId) {
        this.unixId = unixId;
    }

    public String getUnixId() {
        return unixId;
    }

    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getKode_office() {
        return kode_office;
    }

    public void setCompetitor(String competitor) {
        this.competitor = competitor;
    }

    public String getCompetitor() {
        return competitor;
    }





    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }


    public String getActivity() {
        return activity;
    }
    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getArea_activity() {
        return area_activity;
    }
    public void setArea_activity(String area_activity) {
        this.area_activity = area_activity;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getKeterangan() {
        return keterangan;
    }

}
