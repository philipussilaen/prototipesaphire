package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 21/09/16.
 */
@DatabaseTable(tableName = "status_feed")
public class TableStatusFeed implements Serializable {

    @DatabaseField( )
    private int id;

    @DatabaseField
    private String status_feed;


    public TableStatusFeed() {}

    public TableStatusFeed(int id, String status_feed) {}

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setStatus_feed(String status_feed) {
        this.status_feed = status_feed;
    }

    public String getStatus_feed() {
        return status_feed;
    }



}
