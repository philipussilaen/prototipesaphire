package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableMCompetitor;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 21/09/16.
 */
public class TableMCompetitorAdapter {

    static private TableMCompetitorAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableMCompetitorAdapter(ctx);
        }
    }

    static public TableMCompetitorAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableMCompetitorAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableMCompetitor> getAllData() {
        List<TableMCompetitor> tblsatu = null;
        try {
            tblsatu = getHelper().getTableMCompetitor()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }



    /**
     * Get Data By ID
     * @return
     */
    public List<TableMCompetitor> getDatabyCondition(String condition, Object param) {
        List<TableMCompetitor> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableMCompetitor();
            QueryBuilder<TableMCompetitor, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMCompetitor, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableMCompetitor tbl, int id, String competitor,String kategori) {
        try {
            tbl.setId(id);
            tbl.setCompetitor(competitor);
            tbl.setKategori(kategori);

            getHelper().getTableMCompetitor().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMCompetitor.class);
            UpdateBuilder<TableMCompetitor, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableMCompetitor> tbl = null;
        try {
            tbl = getHelper().getTableMCompetitor().queryForAll();
            getHelper().getTableMCompetitor().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, int id ) {
        try {
            getHelper().getTableMCompetitor().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableMCompetitor.class);
            DeleteBuilder<TableMCompetitor, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public List<TableMCompetitor> getAllDatabyLup() {
        List<TableMCompetitor> tblsatu = null;
        try {
            tblsatu = getHelper().getTableMCompetitor()
                    .queryBuilder().orderBy("lup",
                            false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }
}
