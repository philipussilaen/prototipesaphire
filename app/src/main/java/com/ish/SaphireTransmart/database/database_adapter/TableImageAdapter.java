package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableImage;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 14/09/16.
 */
public class TableImageAdapter {

    static private TableImageAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableImageAdapter(ctx);
        }
    }

    static public TableImageAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableImageAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableImage> getAllData() {
        List<TableImage> tblsatu = null;
        try {
            tblsatu = getHelper().getTableImageDAO()
                    .queryBuilder().orderBy("id_image",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableImage> getDatabyCondition(String condition, Object param) {
        List<TableImage> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableImageDAO();
            QueryBuilder<TableImage, Integer> queryBuilder = dao.queryBuilder();
            Where<TableImage, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id_image", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableImage tbl, String id, String id_image, String id_absen, String image, String name, String m_path,
                           String userid, int index, int total, String type, String date, int flag) {
        try {
            tbl.setId(id);
            tbl.setId_image(id_image);
            tbl.setImage(image);
            tbl.setName(name);
            tbl.setM_path(m_path);
            tbl.setUserid(userid);
            tbl.setIndex(index);
            tbl.setTotal(total);
            tbl.setType(type);
            tbl.setDate(date);
            tbl.setFlag(flag);
            tbl.setId_absen(id_absen);

            getHelper().getTableImageDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableImage.class);
            UpdateBuilder<TableImage, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableImage> tbl = null;
        try {
            tbl = getHelper().getTableImageDAO().queryForAll();
            getHelper().getTableImageDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, String id) {
        try {
            getHelper().getTableImageDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableImage.class);
            DeleteBuilder<TableImage, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
