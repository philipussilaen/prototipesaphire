package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableTransOutGimmick;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 14/10/16.
 */
public class TableTransOGimmickAdapter {

    static private TableTransOGimmickAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableTransOGimmickAdapter(ctx);
        }
    }

    static public TableTransOGimmickAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableTransOGimmickAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableTransOutGimmick> getAllData() {
        List<TableTransOutGimmick> tblsatu = null;
        try {
            tblsatu = getHelper().getTableTransOGimmickDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableTransOutGimmick> getDatabyCondition(String condition, Object param) {
        List<TableTransOutGimmick> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableTransOGimmickDAO();
            QueryBuilder<TableTransOutGimmick, String> queryBuilder = dao.queryBuilder();
            Where<TableTransOutGimmick, String> where = queryBuilder.where();
            if (condition.equalsIgnoreCase("date")) {
                String[] parts = String.valueOf(param).split("&");
                String part1 = parts[0]; // 004
                String part2 = parts[1]; // 034556

                where.like(condition, part1).and().eq("kode_office", part2);

            } else {
                where.eq(condition, param);
            }
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableTransOutGimmick tbl, String id, String id_gimmick, String userid,
                           String nama, int jumlah, int flag, String image1, String image2, String date,
                           String office) {
        try {
            tbl.setId(id);
            tbl.setUserid(userid);
            tbl.setId_gimmick(id_gimmick);
            tbl.setFlag(flag);
            tbl.setJumlah(jumlah);
            tbl.setNama(nama);
            tbl.setImage1(image1);
            tbl.setImage2(image2);
            tbl.setDate(date);
            tbl.setKode_office(office);

            getHelper().getTableTransOGimmickDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTransOutGimmick.class);
            UpdateBuilder<TableTransOutGimmick, String> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableTransOutGimmick> tbl = null;
        try {
            tbl = getHelper().getTableTransOGimmickDAO().queryForAll();
            getHelper().getTableTransOGimmickDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, String id) {
        try {
            getHelper().getTableTransOGimmickDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTransOutGimmick.class);
            DeleteBuilder<TableTransOutGimmick, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
