package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableKategori;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 21/09/16.
 */
public class TableKategoriAdapter {

    static private TableKategoriAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableKategoriAdapter(ctx);
        }
    }

    static public TableKategoriAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableKategoriAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableKategori> getAllData() {
        List<TableKategori> tblsatu = null;
        try {
            tblsatu = getHelper().getTableKategoriDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    public List<TableKategori> getAllDataKokita() {
        List<TableKategori> tblsatu = null;
        try {
            tblsatu = getHelper().getTableKategoriDAO()
                    .queryBuilder().where().eq("channel", "KOKITA")
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    public List<TableKategori> getAllDataMrp() {
        List<TableKategori> tblsatu = null;
        try {
            tblsatu = getHelper().getTableKategoriDAO()
                    .queryBuilder().where().eq("channel", "MR.P")
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableKategori> getDatabyCondition(String condition, Object param) {
        List<TableKategori> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableKategoriDAO();
            QueryBuilder<TableKategori, Integer> queryBuilder = dao.queryBuilder();
            Where<TableKategori, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableKategori tbl, String product_category, String upd, String lup, String channel) {
        try {
            tbl.setProduct_category(product_category);
            tbl.setUpd(upd);
            tbl.setLup(lup);
            tbl.setChannel(channel);
            getHelper().getTableKategoriDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableKategori.class);
            UpdateBuilder<TableKategori, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableKategori> tbl = null;
        try {
            tbl = getHelper().getTableKategoriDAO().queryForAll();
            getHelper().getTableKategoriDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, String product_category) {
        try {
            getHelper().getTableKategoriDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableKategori.class);
            DeleteBuilder<TableKategori, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("product_category", product_category);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public List<TableKategori> getAllDatabyLup() {
        List<TableKategori> tblsatu = null;
        try {
            tblsatu = getHelper().getTableKategoriDAO()
                    .queryBuilder().orderBy("lup",
                            false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }
}
