package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableInsertion;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 16/10/16.
 */
public class TableInsertionAdapter {

    static private TableInsertionAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableInsertionAdapter(ctx);
        }
    }

    static public TableInsertionAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableInsertionAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableInsertion> getAllData() {
        List<TableInsertion> tblsatu = null;
        try {
            tblsatu = getHelper().getTableInsertionDAO()
                    .queryBuilder().orderBy("nama_product",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableInsertion> getDatabyCondition(String condition, Object param) {
        List<TableInsertion> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableInsertionDAO();
            QueryBuilder<TableInsertion, Integer> queryBuilder = dao.queryBuilder();
            Where<TableInsertion, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableInsertion tbl, String id, String kode_office, String product_category,
                           String id_product, String name, int stock, int actual, int flag) {
        try {
            tbl.setId(id);
            tbl.setId_product(id_product);
            tbl.setKode_office(kode_office);
            tbl.setProduct_category(product_category);
            tbl.setStock(stock);
            tbl.setActual(actual);
            tbl.setFlag(flag);
            tbl.setNama_product(name);

            getHelper().getTableInsertionDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableInsertion.class);
            UpdateBuilder<TableInsertion, String> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableInsertion> tbl = null;
        try {
            tbl = getHelper().getTableInsertionDAO().queryForAll();
            getHelper().getTableInsertionDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

