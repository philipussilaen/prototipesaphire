package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 21/09/16.
 */
@DatabaseTable(tableName = "trans_educationplan")
public class TableTransEducationPlan implements Serializable {



    @DatabaseField
    private String id,userid,tgl_edu, kode_office, tgl_input,pembicara,waktu_awal,waktu_akhir,judul,lup,notes;

    @DatabaseField
    private int  flag,jml_peserta,flagApprove;

    public TableTransEducationPlan() {}

    public void setTgl_input(String tgl_input) {
        this.tgl_input = tgl_input;
    }

    public String getTgl_input() {
        return tgl_input;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getJudul() {
        return judul;
    }

    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getKode_office() {
        return kode_office;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setTgl_edu(String tgl_edu) {
        this.tgl_edu = tgl_edu;
    }



    public String getTgl_edu() {
        return tgl_edu;
    }

    public void setWaktu_awal(String waktu_awal) {
        this.waktu_awal = waktu_awal;
    }



    public String getWaktu_awal() {
        return waktu_awal;
    }
    public void setWaktu_akhir(String waktu_akhir) {
        this.waktu_akhir = waktu_akhir;
    }



    public String getWaktu_akhir() {
        return waktu_akhir;
    }

    public void setPembicara(String pembicara) {
        this.pembicara = pembicara;
    }



    public String getPembicara() {
        return pembicara;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }




    public void setUserid(String userid) {
        this.userid = userid;
    }



    public String getUserid() {
        return userid;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }



    public String getNotes() {
        return notes;
    }



    public void setJml_peserta(int jml_peserta) {
        this.jml_peserta = jml_peserta;
    }

    public int getJml_peserta() {
        return jml_peserta;
    }
    public void setFlagApprove(int flagApprove) {
        this.flagApprove = flagApprove;
    }

    public int getFlagApprove() {
        return flagApprove;
    }

    public String getLup() {
        return lup;
    }

    public void setLup(String lup) {
        this.lup = lup;
    }
}
