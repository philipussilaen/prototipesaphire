package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by ACER on 02/11/2016.
 */

    @DatabaseTable(tableName = "trans_sell_out")
    public class TableTansSellOut implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String userid, tgl_input, kode_office, id_product,  unixId, photo, nama,address,phone;

    @DatabaseField
    private int total_jual, flag, qty,instalation;

    public TableTansSellOut() {}

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getId_product() {
        return id_product;
    }

    public String getTgl_input() {
        return tgl_input;
    }

    public void setTgl_input(String tgl_input) {
        this.tgl_input = tgl_input;
    }


    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setInstalation(int instalation) {
        this.instalation = instalation;
    }

    public int getInstalation() {
        return instalation;
    }

    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getKode_office() {
        return kode_office;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }


    public void setTotal_jual(int total_jual) {
        this.total_jual = total_jual;
    }

    public int getTotal_jual() {
        return total_jual;
    }
    public void setId(int id) {
        this.id = id;
    }




    public void setPhoto(String photo) {
        this.photo = photo;
    }



    public String getPhoto() {
        return photo;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getId() {
        return id;
    }




    public String getUserid() {
        return userid;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }


//    public void setClockin(String clockin) {
//        this.clockin = clockin;
//    }
//
//    public String getClockin() {
//        return clockin;
//    }

    public void setUnixId(String unixId) {
        this.unixId = unixId;
    }

    public String getUnixId() {
        return unixId;
    }
}
