package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableJadwalAbsenMobile;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 23/10/16.
 */
public class TableJadwalAbsenMobileAdapter {
    static private TableJadwalAbsenMobileAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableJadwalAbsenMobileAdapter(ctx);
        }
    }

    static public TableJadwalAbsenMobileAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableJadwalAbsenMobileAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableJadwalAbsenMobile> getAllData() {
        List<TableJadwalAbsenMobile> tblsatu = null;
        try {
            tblsatu = getHelper().getTableJadwalAbsenMobileDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableJadwalAbsenMobile> getDatabyCondition(List<String> condition, List<Object>  param) {
        List<TableJadwalAbsenMobile> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();
        List<String> conditions = condition;
        List<Object> params = param;

        int count = 0;
        try {
            dao = getHelper().getTableJadwalAbsenMobileDAO();
            QueryBuilder<TableJadwalAbsenMobile, Integer> queryBuilder = dao.queryBuilder();
            Where<TableJadwalAbsenMobile, Integer> where = queryBuilder.where();
            //where.eq(conditions.get(1), params.get(1));
            /*for (int i=0; i<params.size();i++) {
                String a = conditions.get(i);
                Object b = params.get(i);
                String c;
                where.eq(conditions.get(i), params.get(i));

            }
            if (params.size()>1) {
                where.and();
            }*/
            where.eq(conditions.get(0), params.get(0))
                    .and().eq(conditions.get(1), params.get(1));
            //where.eq(condition, param);
            //where.and();

            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableJadwalAbsenMobile tbl, int id, String date, String salesman, String office,
                           String roster, String upd, String lup) {
        try {
            tbl.setId(id);
            tbl.setDate(date);
            tbl.setSalesman(salesman);
            tbl.setRoster(roster);
            tbl.setUpd(upd);
            tbl.setLup(lup);
            tbl.setOffice(office);

            getHelper().getTableJadwalAbsenMobileDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableJadwalAbsenMobile.class);
            UpdateBuilder<TableJadwalAbsenMobile, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableJadwalAbsenMobile> tbl = null;
        try {
            tbl = getHelper().getTableJadwalAbsenMobileDAO().queryForAll();
            getHelper().getTableJadwalAbsenMobileDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, int id) {
        try {
            getHelper().getTableJadwalAbsenMobileDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableJadwalAbsenMobile.class);
            DeleteBuilder<TableJadwalAbsenMobile, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public List<TableJadwalAbsenMobile> getAllDatabyLup() {
        List<TableJadwalAbsenMobile> tblsatu = null;
        try {
            tblsatu = getHelper().getTableJadwalAbsenMobileDAO()
                    .queryBuilder().orderBy("lup",
                            false)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }
}
