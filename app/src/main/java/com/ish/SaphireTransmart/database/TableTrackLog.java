package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 29/09/16.
 */
@DatabaseTable(tableName = "m_tracklog")
public class TableTrackLog implements Serializable {

    @DatabaseField(id = true)
    private String trackid;

    @DatabaseField
    private String userid, date, lng, lat;

    @DatabaseField
    private int flag;

    public TableTrackLog() {}

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLat() {
        return lat;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLng() {
        return lng;
    }

    public void setTrackid(String trackid) {
        this.trackid = trackid;
    }

    public String getTrackid() {
        return trackid;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }
}
