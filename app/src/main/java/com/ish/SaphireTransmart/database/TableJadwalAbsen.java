package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by ISH NB on 30/08/2016.
 */
@DatabaseTable(tableName = "jadwal_absen")
public class TableJadwalAbsen {

    @DatabaseField(id = true)
    private int id;

    @DatabaseField
    private String date, salesman, office, roster, upd, lup;

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setLup(String lup) {
        this.lup = lup;
    }

    public String getLup() {
        return lup;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getOffice() {
        return office;
    }

    public void setRoster(String roster) {
        this.roster = roster;
    }

    public String getRoster() {
        return roster;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public String getUpd() {
        return upd;
    }
}
