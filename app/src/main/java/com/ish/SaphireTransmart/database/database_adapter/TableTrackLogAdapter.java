package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableTrackLog;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;
/**
 * Created by adminmc on 29/09/16.
 */
public class TableTrackLogAdapter {

    static private TableTrackLogAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableTrackLogAdapter(ctx);
        }
    }

    static public TableTrackLogAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableTrackLogAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableTrackLog> getAllData() {
        List<TableTrackLog> tblsatu = null;
        try {
            tblsatu = getHelper().getTableTrackLogDAO()
                    .queryBuilder().orderBy("trackid",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableTrackLog> getDatabyCondition(String condition, Object param) {
        List<TableTrackLog> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableTrackLogDAO();
            QueryBuilder<TableTrackLog, Integer> queryBuilder = dao.queryBuilder();
            Where<TableTrackLog, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("trackid", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableTrackLog tbl, String tracklogId, String userid, String date, String lng,
                           String lat, int flag) {
        try {
            tbl.setTrackid(tracklogId);
            tbl.setUserid(userid);
            tbl.setDate(date);
            tbl.setLat(lat);
            tbl.setLng(lng);
            tbl.setFlag(flag);

            getHelper().getTableTrackLogDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTrackLog.class);
            UpdateBuilder<TableTrackLog, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableTrackLog> tbl = null;
        try {
            tbl = getHelper().getTableTrackLogDAO().queryForAll();
            getHelper().getTableTrackLogDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(Context context, int flag) {
        try {
            getHelper().getTableTrackLogDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTrackLog.class);
            DeleteBuilder<TableTrackLog, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("flag", flag);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
