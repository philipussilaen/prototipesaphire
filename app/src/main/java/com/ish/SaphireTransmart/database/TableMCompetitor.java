package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 21/09/16.
 */
@DatabaseTable(tableName = "m_competitor")
public class TableMCompetitor implements Serializable {

    @DatabaseField
    private int id;

    @DatabaseField
    private String competitor,kategori;


    public TableMCompetitor() {}

    public TableMCompetitor(int id, String type_damage) {}

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setCompetitor(String competitor) {
        this.competitor = competitor;
    }

    public String getCompetitor() {
        return competitor;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getKategori() {
        return kategori;
    }



}
