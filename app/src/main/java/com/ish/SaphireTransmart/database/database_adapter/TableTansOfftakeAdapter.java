package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableTansOfftake;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 21/09/16.
 */
public class TableTansOfftakeAdapter {

    static private TableTansOfftakeAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableTansOfftakeAdapter(ctx);
        }
    }

    static public TableTansOfftakeAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableTansOfftakeAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableTansOfftake> getAllData() {
        List<TableTansOfftake> tblsatu = null;
        try {
            tblsatu = getHelper().getTableTransOfftakeDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableTansOfftake> getDatabyCondition(String condition, Object param) {
        List<TableTansOfftake> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {

            dao = getHelper().getTableTransOfftakeDAO();
            QueryBuilder<TableTansOfftake, Integer> queryBuilder = dao.queryBuilder();
            Where<TableTansOfftake, Integer> where = queryBuilder.where();
            if (condition.equalsIgnoreCase("clockin")) {
                String[] parts = String.valueOf(param).split("&");
                String part1 = parts[0]; // 004
                String part2 = parts[1]; // 034556

                where.like(condition, part1).and().eq("office", part2);

            } else {
                where.eq(condition, param);
            }

            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableTansOfftake tbl, String userid, String time, String office,
                           String product_category, String model_name, String ket_status, String
                           userid_approval, String ip_address, String mc_address,
                           int gram, int qty, int flag, String clockin, String unixId) {
        try {
            //tbl.setId(id);
            tbl.setProduct_category(product_category);
            tbl.setModel_name(model_name);
            tbl.setGram(gram);
            tbl.setUserid(userid);
            tbl.setTime(time);
            tbl.setOffice(office);
            tbl.setKet_status(ket_status);
            tbl.setUserid_approval(userid_approval);
            tbl.setIp_address(ip_address);
            tbl.setMc_address(mc_address);
            tbl.setQty(qty);
            tbl.setFlag(flag);
            tbl.setClockin(clockin);
            tbl.setUnixId(unixId);

            getHelper().getTableTransOfftakeDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTansOfftake.class);
            UpdateBuilder<TableTansOfftake, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableTansOfftake> tbl = null;
        try {
            tbl = getHelper().getTableTransOfftakeDAO().queryForAll();
            getHelper().getTableTransOfftakeDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, int id) {
        try {
            getHelper().getTableImageDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTansOfftake.class);
            DeleteBuilder<TableTansOfftake, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
