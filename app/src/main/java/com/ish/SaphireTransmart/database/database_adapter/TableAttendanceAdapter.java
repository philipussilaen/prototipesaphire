package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableAttendance;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by admin on 8/30/2016.
 */
public class TableAttendanceAdapter {

    static private TableAttendanceAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableAttendanceAdapter(ctx);
        }
    }

    static public TableAttendanceAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableAttendanceAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableAttendance> getAllData() {
        List<TableAttendance> tblsatu = null;
        try {
            tblsatu = getHelper().getTableAttendanceDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableAttendance> getDatabyCondition(String condition, Object param) {
        List<TableAttendance> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableAttendanceDAO();
            QueryBuilder<TableAttendance, Integer> queryBuilder = dao.queryBuilder();
            Where<TableAttendance, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableAttendance tbl, String id, String userid, String tanggal, String waktu, String status,
                           String lng, String lat, String office, String jenis, String photo_status,
                           String photo_name, String photo_path, String ip_address, String mc_address, String imei, int flag) {
        try {
            tbl.setId(id);
            tbl.setUserid(userid);
            tbl.setTanggal(tanggal);
            tbl.setWaktu(waktu);
            tbl.setLat(lat);
            tbl.setLng(lng);
            tbl.setStatus(status);
            tbl.setOffice(office);
            tbl.setJenis(jenis);
            tbl.setPhoto_status(photo_status);
            tbl.setPhoto_name(photo_name);
            tbl.setPhoto_path(photo_path);
            tbl.setIp_address(ip_address);
            tbl.setMc_address(mc_address);
            tbl.setImei(imei);
            tbl.setFlag(flag);
            getHelper().getTableAttendanceDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableAttendance.class);
            UpdateBuilder<TableAttendance, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableAttendance> tbl = null;
        try {
            tbl = getHelper().getTableAttendanceDAO().queryForAll();
            getHelper().getTableAttendanceDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long countByCondition(Context context, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableAttendance.class);
            QueryBuilder<TableAttendance, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.setCountOf(true);
            queryBuilder.setWhere(queryBuilder.where().eq(condition, param));
            Long usuarios = dao.countOf(queryBuilder.prepare());
            return usuarios;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableAttendance> getDatabyConditionArray(List<String> condition, List<Object>  param) {
        List<TableAttendance> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();
        List<String> conditions = condition;
        List<Object> params = param;

        int count = 0;
        try {

            dao = getHelper().getTableAttendanceDAO();
            QueryBuilder<TableAttendance, Integer> queryBuilder = dao.queryBuilder();
            Where<TableAttendance, Integer> where = queryBuilder.where();
            //where.eq(conditions.get(1), params.get(1));
            /*for (int i=0; i<params.size();i++) {
                String a = conditions.get(i);
                Object b = params.get(i);
                String c;
                where.eq(conditions.get(i), params.get(i));

            }
            if (params.size()>1) {
                where.and();
            }*/
            where.eq(conditions.get(0), params.get(0)).and().eq(conditions.get(1), params.get(1))
                    .and().eq(conditions.get(2), params.get(2))
                    .and().eq(conditions.get(3), params.get(3));
            //where.eq(condition, param);
            //where.and();
            String query="";
            for (int i=0;i<params.size();i++) {
                query = query+conditions.get(i);
            }

            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }


    public  List<TableAttendance> getLastDatabyConditionArray(List<String> condition, List<Object>  param) {
        List<TableAttendance> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();
        List<String> conditions = condition;
        List<Object> params = param;

        int count = 0;
        try {

            dao = getHelper().getTableAttendanceDAO();
            QueryBuilder<TableAttendance, Integer> queryBuilder = dao.queryBuilder();
            Where<TableAttendance, Integer> where = queryBuilder.where();
            //where.eq(conditions.get(1), params.get(1));
            /*for (int i=0; i<params.size();i++) {
                String a = conditions.get(i);
                Object b = params.get(i);
                String c;
                where.eq(conditions.get(i), params.get(i));

            }
            if (params.size()>1) {
                where.and();
            }*/
            where.eq(conditions.get(0), params.get(0)).and().eq(conditions.get(1), params.get(1))
                    .and().eq(conditions.get(2), params.get(2))
                    .and().eq(conditions.get(3), params.get(3));
            //where.eq(condition, param);
            //where.and();
            String query="";
            for (int i=0;i<params.size();i++) {
                query = query+conditions.get(i);
                Log.e("query", query);

                Log.e("params", params.get(i).toString());
            }

            queryBuilder.orderBy("id", true);
           // queryBuilder.limit(1);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }
    public void delete(Context context, String id) {
        try {
            getHelper().getTableAttendanceDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableAttendance.class);
            DeleteBuilder<TableAttendance, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
