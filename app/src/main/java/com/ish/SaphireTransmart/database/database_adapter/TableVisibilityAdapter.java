package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableVisibility;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 26/09/16.
 */
public class TableVisibilityAdapter {

    static private TableVisibilityAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableVisibilityAdapter(ctx);
        }
    }

    static public TableVisibilityAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableVisibilityAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableVisibility> getAllData() {
        List<TableVisibility> tblsatu = null;
        try {
            tblsatu = getHelper().getTableVisibilityDAO()
                    .queryBuilder().orderBy("visibility_id",
                            true)
                    .query();
            Log.d("TableVisibility ",  tblsatu.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableVisibility> getDatabyCondition(String condition, Object param) {
        List<TableVisibility> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableVisibilityDAO();
            QueryBuilder<TableVisibility, Integer> queryBuilder = dao.queryBuilder();
            Where<TableVisibility, Integer> where = queryBuilder.where();
            if (condition.equalsIgnoreCase("clockin")) {
                String[] parts = String.valueOf(param).split("&");
                String part1 = parts[0]; // 004
                String part2 = parts[1]; // 034556

                where.like(condition, part1).and().eq("office", part2);

            } else {
                where.eq(condition, param);
            }

            queryBuilder.orderBy("visibility_id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableVisibility tbl, String id, String userid, String kategori, String tipe,
                           String clockin, int isClean, int isRightPosition, int isGoodCondition,
                           int isTopShelf, int isFullShelf, int flag, String office, String image1, String image2) {
        try {
            tbl.setVisibility_id(id);
            tbl.setUserid(userid);
            tbl.setKategori(kategori);
            tbl.setTipe(tipe);
            tbl.setIsClean(isClean);
            tbl.setIsGoodCondition(isGoodCondition);
            tbl.setIsRightPosition(isRightPosition);
            tbl.setIsSheflFull(isFullShelf);
            tbl.setIsTopShelf(isTopShelf);
            tbl.setFlag(flag);
            tbl.setClockin(clockin);
            tbl.setOffice(office);
            tbl.setImage1(image1);
            tbl.setImage2(image2);
            getHelper().getTableVisibilityDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableVisibility.class);
            UpdateBuilder<TableVisibility, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableVisibility> tbl = null;
        try {
            tbl = getHelper().getTableVisibilityDAO().queryForAll();
            getHelper().getTableVisibilityDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(Context context, String flag) {
        try {
            getHelper().getTableVisibilityDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableVisibility.class);
            DeleteBuilder<TableVisibility, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("visibility_id", flag);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
