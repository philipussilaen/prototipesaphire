package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 21/09/16.
 */
@DatabaseTable(tableName = "kategori")
public class TableKategori implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String product_category, upd, lup, channel;


    public TableKategori() {}

    public TableKategori(int id, String product_category, String upd, String lup, String channel) {}

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getChannel() {
        return channel;
    }


    public void setProduct_category(String product_category) {
        this.product_category = product_category;
    }

    public String getProduct_category() {
        return product_category;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public String getUpd() {
        return upd;
    }

    public void setLup(String lup) {
        this.lup = lup;
    }

    public String getLup() {
        return lup;
    }

}
