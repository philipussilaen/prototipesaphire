package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableTransGimmick;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 13/10/16.
 */
public class TableTransGimmickAdapter {

    static private TableTransGimmickAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableTransGimmickAdapter(ctx);
        }
    }

    static public TableTransGimmickAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableTransGimmickAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableTransGimmick> getAllData() {
        List<TableTransGimmick> tblsatu = null;
        try {
            tblsatu = getHelper().getTableTransGimmickDAO()
                    .queryBuilder().orderBy("kategori_gimmick",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }
    public List<TableTransGimmick> getDatabyCondition(String condition, Object param) {
        List<TableTransGimmick> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableTransGimmickDAO();
            QueryBuilder<TableTransGimmick, String> queryBuilder = dao.queryBuilder();
            Where<TableTransGimmick, String> where = queryBuilder.where();
            if (condition.equalsIgnoreCase("kode_office")) {
//                String[] parts = String.valueOf(param).split("&");
//                String part1 = parts[0]; // 004
//                String part2 = parts[1]; // 034556

                where.like(condition, param).and().gt("outstanding", 0);

            } else {
                where.eq(condition, param);
            }

           // where.eq(condition, param);
            queryBuilder.orderBy("kategori_gimmick", true);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }
    public List<TableTransGimmick> getDatabyConditionDistinct(String condition, Object param, String coloumn) {
        List<TableTransGimmick> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableTransGimmickDAO();
            QueryBuilder<TableTransGimmick, String> queryBuilder = dao.queryBuilder().distinct().selectColumns(coloumn);
            Where<TableTransGimmick, String> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */

    public List<TableTransGimmick> getDatabyConditionAndADate(String condition, Object param, String date) {
        List<TableTransGimmick> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableTransGimmickDAO();
            QueryBuilder<TableTransGimmick, String> queryBuilder = dao.queryBuilder();
            Where<TableTransGimmick, String> where = queryBuilder.where();
            where.eq(condition, param).and().eq("bulan", date);
            queryBuilder.orderBy("kategori_gimmick", true);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }


    /**
     * Insert Data
     */
    public void insertData(TableTransGimmick tbl, String id, String kode_office, String kategori_gimmick,
                           String jenis, String bulan, String upd, String lup,
                           int jumlah, int outstanding, int trans) {
        try {
            tbl.setId(id);
            tbl.setKode_office(kode_office);
            tbl.setKategori_gimmick(kategori_gimmick);
            tbl.setJenis(jenis);
            tbl.setBulan(bulan);
            tbl.setUpd(upd);
            tbl.setLup(lup);
            tbl.setJumlah(jumlah);
            tbl.setOutstanding(outstanding);
            tbl.setTrans(trans);

            getHelper().getTableTransGimmickDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTransGimmick.class);
            UpdateBuilder<TableTransGimmick, String> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableTransGimmick> tbl = null;
        try {
            tbl = getHelper().getTableTransGimmickDAO().queryForAll();
            getHelper().getTableTransGimmickDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, String id) {
        try {
            getHelper().getTableTransGimmickDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTransGimmick.class);
            DeleteBuilder<TableTransGimmick, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long countByCondition(Context context, String condition, String condition2,
                                 Object param, Object param2) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTransGimmick.class);
            QueryBuilder<TableTransGimmick, Integer> queryBuilder = dao.queryBuilder();
            queryBuilder.setCountOf(true);
            queryBuilder.setWhere(queryBuilder.where().eq(condition, param).and().eq(condition2, param2));
            Long usuarios = dao.countOf(queryBuilder.prepare());
            return usuarios;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
