package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableJadwalAbsen;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by ISH NB on 30/08/2016.
 */
public class TableJadwalAbsenAdapter {

    static private TableJadwalAbsenAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableJadwalAbsenAdapter(ctx);
        }
    }

    static public TableJadwalAbsenAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableJadwalAbsenAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableJadwalAbsen> getAllData() {
        List<TableJadwalAbsen> tblsatu = null;
        try {
            tblsatu = getHelper().getTableJadwalAbsenDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public  List<TableJadwalAbsen> getDatabyCondition(List<String> condition, List<Object>  param) {
        List<TableJadwalAbsen> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();
        List<String> conditions = condition;
        List<Object> params = param;

        int count = 0;
        try {
            dao = getHelper().getTableJadwalAbsenDAO();
            QueryBuilder<TableJadwalAbsen, Integer> queryBuilder = dao.queryBuilder();
            Where<TableJadwalAbsen, Integer> where = queryBuilder.where();
            //where.eq(conditions.get(1), params.get(1));
            /*for (int i=0; i<params.size();i++) {
                String a = conditions.get(i);
                Object b = params.get(i);
                String c;
                where.eq(conditions.get(i), params.get(i));

            }
            if (params.size()>1) {
                where.and();
            }*/
            where.eq(conditions.get(0), params.get(0))
                    .and().eq(conditions.get(1), params.get(1))
                    .and().eq(conditions.get(2), params.get(2));
            //where.eq(condition, param);
            //where.and();

            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableJadwalAbsen tbl, int id, String date, String salesman, String office,
                           String roster, String upd, String lup) {
        try {
            tbl.setId(id);
            tbl.setDate(date);
            tbl.setSalesman(salesman);
            tbl.setRoster(roster);
            tbl.setUpd(upd);
            tbl.setLup(lup);
            tbl.setOffice(office);

            getHelper().getTableJadwalAbsenDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableJadwalAbsen.class);
            UpdateBuilder<TableJadwalAbsen, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableJadwalAbsen> tbl = null;
        try {
            tbl = getHelper().getTableJadwalAbsenDAO().queryForAll();
            getHelper().getTableJadwalAbsenDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, int id) {
        try {
            getHelper().getTableJadwalAbsenDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableJadwalAbsen.class);
            DeleteBuilder<TableJadwalAbsen, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
