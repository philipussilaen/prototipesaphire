package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 21/09/16.
 */
@DatabaseTable(tableName = "type_damage")
public class TableTypeDamage implements Serializable {

    @DatabaseField
    private int id;

    @DatabaseField
    private String type_damage,kategori;


    public TableTypeDamage() {}

    public TableTypeDamage(int id, String type_damage) {}

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setType_damage(String type_damage) {
        this.type_damage = type_damage;
    }

    public String getType_damage() {
        return type_damage;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getKategori() {
        return kategori;
    }



}
