package com.ish.SaphireTransmart.database;



import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by ACER on 02/11/2016.
 */

    @DatabaseTable(tableName = "competitor")
    public class TableCompetitor implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String perner, nama, kode_office, product, keterangan, unixId, periodeawal,periodeakhir, tgl_input, photo,photo2,photo3,photo4,photo5,competitor;

    @DatabaseField
    private int normalprice,flag,harga,promoprice;

    public TableCompetitor() {}

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProduct() {
        return product;
    }

    public String getTgl_input() {
        return tgl_input;
    }

    public void setTgl_input(String tgl_input) {
        this.tgl_input = tgl_input;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto2(String photo2) {
        this.photo2 = photo2;
    }

    public String getPhoto2() {
        return photo2;
    }

    public void setPhoto3(String photo3) {
        this.photo3 = photo3;
    }

    public String getPhoto3() {
        return photo3;
    }

    public void setPhoto4(String photo4) {
        this.photo4 = photo4;
    }

    public String getPhoto4() {
        return photo4;
    }


    public void setPhoto5(String photo5) {
        this.photo5 = photo5;
    }

    public String getPhoto5() {
        return photo5;
    }

    public String getPeriodeawal() {
        return periodeawal;
    }

    public void setPeriodeawal(String periodeawal) {
        this.periodeawal = periodeawal;
    }
    public String getPeriodeakhir() {
        return periodeakhir;
    }

    public void setPeriodeakhir(String periodeakhir) {
        this.periodeakhir = periodeakhir;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setUnixId(String unixId) {
        this.unixId = unixId;
    }

    public String getUnixId() {
        return unixId;
    }

    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getKode_office() {
        return kode_office;
    }

    public void setCompetitor(String competitor) {
        this.competitor = competitor;
    }

    public String getCompetitor() {
        return competitor;
    }

    public void setNormalPrice(int normalprice) {
        this.normalprice = normalprice;
    }

    public int getNormalPrice() {
        return normalprice;
    }

    public void setPromoPrice(int promoprice) {
        this.promoprice = promoprice;
    }

    public int getPromoPrice() {
        return promoprice;
    }



    public void setPerner(String perner) {
        this.perner = perner;
    }



    public String getPerner() {
        return perner;
    }



    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getHarga() {
        return harga;
    }
    public void setHarga(int harga) {
        this.harga = harga;
    }



    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }


    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getKeterangan() {
        return keterangan;
    }


}
