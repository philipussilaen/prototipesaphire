package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;

import com.ish.SaphireTransmart.database.TableDetailing;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 11/10/16.
 */
public class TableDetailingAdapter {

    static private TableDetailingAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableDetailingAdapter(ctx);
        }
    }

    static public TableDetailingAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableDetailingAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableDetailing> getAllData() {
        List<TableDetailing> tblsatu = null;
        try {
            tblsatu = getHelper().getTableDetailingDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    public List<TableDetailing> getAllDataDistinct() {
        List<TableDetailing> tblsatu = null;
        try {
            tblsatu = getHelper().getTableDetailingDAO()
                    .queryBuilder().distinct().selectColumns("id").query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableDetailing> getDatabyCondition(String condition, Object param) {
        List<TableDetailing> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableDetailingDAO();
            QueryBuilder<TableDetailing, Integer> queryBuilder = dao.queryBuilder();
            Where<TableDetailing, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableDetailing tbl, String id, String userid,
                           String nama, String value, int flag, String kode_office, String clockin) {
        try {
            tbl.setId(id);
            tbl.setUserid(userid);
            tbl.setNama(nama);
            tbl.setValue(value);
            tbl.setKode_office(kode_office);
            tbl.setFlag(flag);
            tbl.setClockin(clockin);
            getHelper().getTableDetailingDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    * Update by Condition
    */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableDetailing.class);
            UpdateBuilder<TableDetailing, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableDetailing> tbl = null;
        try {
            tbl = getHelper().getTableDetailingDAO().queryForAll();
            getHelper().getTableDetailingDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, String id) {
        try {
            getHelper().getTableDetailingDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableDetailing.class);
            DeleteBuilder<TableDetailing, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
