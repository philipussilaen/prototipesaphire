package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableTransEducation;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by adminmc on 21/09/16.
 */
public class TableTransEducationAdapter {

    static private TableTransEducationAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableTransEducationAdapter(ctx);
        }
    }

    static public TableTransEducationAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableTransEducationAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableTransEducation> getAllData() {
        List<TableTransEducation> tblsatu = null;
        try {
            tblsatu = getHelper().getTableTransEducationDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableTransEducation> getDatabyCondition(String condition, Object param) {
        List<TableTransEducation> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableTransEducationDAO();
            QueryBuilder<TableTransEducation, Integer> queryBuilder = dao.queryBuilder();
            Where<TableTransEducation, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    //id, ideduplanpilih,clockin,userId, utils.getCurrentDateandTimeSec(),varnextplan,varresult,image1,image2,image3);
//} catch (Exception e) {
    public void insertData(TableTransEducation tbl, String id, String id_eduplan, String clockin, String userid, String tgl_input,
                           String next_plan, String result, String image1, String image2, String image3, int flag) {
        try {
            tbl.setId(id);
            tbl.setId_eduplan(id_eduplan);
            tbl.setClockin(clockin);

           //tbl.setProduct_category(id_sales);
            tbl.setUserid(userid);
            tbl.setTgl_input(tgl_input);
            tbl.setNext_plan(next_plan);
            tbl.setResult(result);
            tbl.setImage1(image1);
            tbl.setImage2(image2);
            tbl.setImage3(image3);

            tbl.setFlag(flag);


            Log.d("save data ", tbl.toString());
            getHelper().getTableTransEducationDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTransEducation.class);
            UpdateBuilder<TableTransEducation, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableTransEducation> tbl = null;
        try {
            tbl = getHelper().getTableTransEducationDAO().queryForAll();
            getHelper().getTableTransEducationDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, String id) {
        try {
            getHelper().getTableTransEducationDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTransEducation.class);
            DeleteBuilder<TableTransEducation, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
