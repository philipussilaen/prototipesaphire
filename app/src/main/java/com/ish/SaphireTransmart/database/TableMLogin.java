package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by admin on 8/29/2016.
 */
@DatabaseTable(tableName = "m_login")
public class TableMLogin {
    @DatabaseField(id = true)
    private String userid;

    @DatabaseField
    private String username, password, userlevel, userstatus, jenis_absen, area, perner, xarea,
    kd_area, klien_layanan, kd_layanan, sub_area, kd_sub_area, jabatan, skill_layanan, kd_skill_layanan,
    fail_login, ket, upd, region,teritory;

    public void setArea(String area) {
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public void setFail_login(String fail_login) {
        this.fail_login = fail_login;
    }

    public String getFail_login() {
        return fail_login;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJenis_absen(String jenis_absen) {
        this.jenis_absen = jenis_absen;
    }

    public String getJenis_absen() {
        return jenis_absen;
    }

    public void setKd_area(String kd_area) {
        this.kd_area = kd_area;
    }

    public String getKd_area() {
        return kd_area;
    }

    public void setKd_layanan(String kd_layanan) {
        this.kd_layanan = kd_layanan;
    }

    public String getKd_layanan() {
        return kd_layanan;
    }

    public void setKd_skill_layanan(String kd_skill_layanan) {
        this.kd_skill_layanan = kd_skill_layanan;
    }

    public String getKd_skill_layanan() {
        return kd_skill_layanan;
    }

    public void setKd_sub_area(String kd_sub_area) {
        this.kd_sub_area = kd_sub_area;
    }

    public String getKd_sub_area() {
        return kd_sub_area;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public String getKet() {
        return ket;
    }

    public void setKlien_layanan(String klien_layanan) {
        this.klien_layanan = klien_layanan;
    }

    public String getKlien_layanan() {
        return klien_layanan;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPerner(String perner) {
        this.perner = perner;
    }

    public String getPerner() {
        return perner;
    }

    public void setSkill_layanan(String skill_layanan) {
        this.skill_layanan = skill_layanan;
    }

    public String getSkill_layanan() {
        return skill_layanan;
    }

    public void setSub_area(String sub_area) {
        this.sub_area = sub_area;
    }

    public String getSub_area() {
        return sub_area;
    }

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public String getUpd() {
        return upd;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserlevel(String userlevel) {
        this.userlevel = userlevel;
    }

    public String getUserlevel() {
        return userlevel;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUserstatus(String userstatus) {
        this.userstatus = userstatus;
    }

    public String getUserstatus() {
        return userstatus;
    }

    public void setXarea(String xarea) {
        this.xarea = xarea;
    }

    public String getXarea() {
        return xarea;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setTeritory(String teritory) {
        this.teritory = teritory;
    }

    public String getTeritory() {
        return teritory;
    }
}
