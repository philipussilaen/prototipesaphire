package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 13/10/16.
 */
@DatabaseTable(tableName = "trans_gimmick")
public class TableTransGimmick implements Serializable {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String kode_office, kategori_gimmick, jenis, bulan, upd, lup;

    @DatabaseField
    private int jumlah, outstanding, trans;

    public TableTransGimmick() {}

    public void setUpd(String upd) {
        this.upd = upd;
    }

    public String getUpd() {
        return upd;
    }

    public void setLup(String lup) {
        this.lup = lup;
    }

    public String getLup() {
        return lup;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBulan() {
        return bulan;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getId() {
        return id;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public String getJenis() {
        return jenis;
    }

    public void setKategori_gimmick(String kategori_gimmick) {
        this.kategori_gimmick = kategori_gimmick;
    }

    public String getKategori_gimmick() {
        return kategori_gimmick;
    }

    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getKode_office() {
        return kode_office;
    }

    public void setOutstanding(int outstanding) {
        this.outstanding = outstanding;
    }

    public int getOutstanding() {
        return outstanding;
    }

    public void setTrans(int trans) {
        this.trans = trans;
    }

    public int getTrans() {
        return trans;
    }

}
