package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 11/10/16.
 */
@DatabaseTable(tableName = "detailing")
public class TableDetailing implements Serializable {

    @DatabaseField(id = true)
    private String id;

    @DatabaseField
    private String userid, nama, value,kode_office,clockin;

    @DatabaseField
    private int flag;

    public TableDetailing() {}

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getKode_office() {
        return kode_office;
    }
    public String getUserid() {
        return userid;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    public void setClockin(String clockin) {
        this.clockin = clockin;
    }

    public String getClockin() {
        return clockin;
    }
}

