package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableTansSellOut;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by ACER on 02/11/2016.
 */
public class TableTansSellOutAdapter {

    static private TableTansSellOutAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableTansSellOutAdapter(ctx);
        }
    }

    static public TableTansSellOutAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableTansSellOutAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableTansSellOut> getAllData() {
        List<TableTansSellOut> tblsatu = null;
        try {
            tblsatu = getHelper().getTableTransSellOutDAO()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableTansSellOut> getDatabyCondition(String condition, Object param) {
        List<TableTansSellOut> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableTransSellOutDAO();
            QueryBuilder<TableTansSellOut, Integer> queryBuilder = dao.queryBuilder();
            Where<TableTansSellOut, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableTansSellOut tbl, String userid, String tgl_input, String kode_office,
                           String id_product,
                           int total_jual, int qty, int flag, String unixId, String photo, String nama,String address, String phone, int instalation) {
        try {
            //tbl.setId(id);
            tbl.setUserid(userid);
            tbl.setTgl_input(tgl_input);
            tbl.setKode_office(kode_office);
            //tbl.setKategori_sellout(kategori_sellout);
            tbl.setId_product(id_product);
            tbl.setQty(qty);
            tbl.setTotal_jual(total_jual);
            tbl.setFlag(flag);
//            tbl.setClockin(clockin);
            tbl.setUnixId(unixId);
            tbl.setPhoto(photo);
            tbl.setNama(nama);
            tbl.setAddress(address);
            tbl.setPhone(phone);
            tbl.setInstalation(instalation);

            Log.d("save data ", tbl.toString());
            getHelper().getTableTransSellOutDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTansSellOut.class);
            UpdateBuilder<TableTansSellOut, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableTansSellOut> tbl = null;
        try {
            tbl = getHelper().getTableTransSellOutDAO().queryForAll();
            getHelper().getTableTransSellOutDAO().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void delete(Context context, int id) {
        try {
            getHelper().getTableTransSellOutDAO().deleteBuilder();
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableTansSellOut.class);
            DeleteBuilder<TableTansSellOut, String> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
