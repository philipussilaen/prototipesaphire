package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TablePromo;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by ACER on 02/11/2016.
 */
public class TablePromoAdapter {

    static private TablePromoAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TablePromoAdapter(ctx);
        }
    }

    static public TablePromoAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TablePromoAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TablePromo> getAllData() {
        List<TablePromo> tblsatu = null;
        try {
            tblsatu = getHelper().getTablePromo()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TablePromo> getDatabyCondition(String condition, Object param) {
        List<TablePromo> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTablePromo();
            QueryBuilder<TablePromo, Integer> queryBuilder = dao.queryBuilder();
            Where<TablePromo, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TablePromo tbl, String kode_office,
                           String product_type,String competitor,
                           String activity,
                           String area_activity, int flag,
                           String unixId, String tgl_input, String photo,String keterangan,
                           String periodeawal,String periodeakhir,String userid
                          ) {
        try {
            tbl.setKode_office(kode_office);
            tbl.setProduct_type(product_type);
            tbl.setCompetitor(competitor);
            tbl.setActivity(activity);
            tbl.setArea_activity(area_activity);
            tbl.setFlag(flag);
            tbl.setUnixId(unixId);
            tbl.setTgl_input(tgl_input);
            tbl.setPhoto(photo);
            tbl.setKeterangan(keterangan);
            tbl.setPeriodeawal(periodeawal);
            tbl.setPeriodeakhir(periodeakhir);
            tbl.setUserid(userid);

            Log.d("save data ", tbl.toString());
            getHelper().getTablePromo().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TablePromo.class);
            UpdateBuilder<TablePromo, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TablePromo> tbl = null;
        try {
            tbl = getHelper().getTablePromo().queryForAll();
            getHelper().getTablePromo().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
