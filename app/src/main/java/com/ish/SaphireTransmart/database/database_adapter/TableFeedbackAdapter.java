package com.ish.SaphireTransmart.database.database_adapter;

import android.content.Context;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableFeedback;
import com.ish.SaphireTransmart.utils.DatabaseManager;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

/**
 * Created by ACER on 02/11/2016.
 */
public class TableFeedbackAdapter {

    static private TableFeedbackAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableFeedbackAdapter(ctx);
        }
    }

    static public TableFeedbackAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableFeedbackAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableFeedback> getAllData() {
        List<TableFeedback> tblsatu = null;
        try {
            tblsatu = getHelper().getTableFeedback()
                    .queryBuilder().orderBy("id",
                            true)
                    .query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     * @return
     */
    public List<TableFeedback> getDatabyCondition(String condition, Object param) {
        List<TableFeedback> tblsatu = null;
        //QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

        int count = 0;
        try {
            dao = getHelper().getTableFeedback();
            QueryBuilder<TableFeedback, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFeedback, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            queryBuilder.orderBy("id", false);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Insert Data
     */
    public void insertData(TableFeedback tbl, String kode_office,
                           String product_type,
                           String customer_name,
                           String phone,String tgl_input,int flag,
                           String unixId, String feedback, String status,String userid
                          ) {
        try {
            //tbl.setId(id);

            tbl.setKode_office(kode_office);

            //tbl.setKategori_sellout(kategori_sellout);
            tbl.setProduct_type(product_type);
            tbl.setCustomer_name(customer_name);
            tbl.setPhone(phone);
            tbl.setTgl_input(tgl_input);
            tbl.setFlag(flag);
            tbl.setUnixId(unixId);
            tbl.setFeedback(feedback);
            tbl.setStatus(status);
            tbl.setUserid(userid);


            Log.d("save data ", tbl.toString());
            getHelper().getTableFeedback().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update by Condition
     */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFeedback.class);
            UpdateBuilder<TableFeedback, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        List<TableFeedback> tbl = null;
        try {
            tbl = getHelper().getTableFeedback().queryForAll();
            getHelper().getTableFeedback().delete(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
