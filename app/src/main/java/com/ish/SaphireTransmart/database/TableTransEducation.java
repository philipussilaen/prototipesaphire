package com.ish.SaphireTransmart.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by adminmc on 21/09/16.
 */
@DatabaseTable(tableName = "trans_education")
public class TableTransEducation implements Serializable {




    @DatabaseField
    private String id,id_eduplan,userid,next_plan, kode_office, tgl_input,result,clockin,image1,image2,image3;

    @DatabaseField
    private int  flag;

    public TableTransEducation() {}

    public void setTgl_input(String tgl_input) {
        this.tgl_input = tgl_input;
    }

    public String getTgl_input() {
        return tgl_input;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getKode_office() {
        return kode_office;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setNext_plan(String next_plan) {
        this.next_plan = next_plan;
    }



    public String getNext_plan() {
        return next_plan;
    }


    public void setId_eduplan(String id_eduplan) {
        this.id_eduplan = id_eduplan;
    }
    public String getId_eduplan() {
        return id_eduplan;
    }
    public void setClockin(String clockin) {
        this.clockin = clockin;
    }
    public String getClockin() {
        return clockin;
    }
    public void setImage1(String image1) {
        this.image1 = image1;
    }
    public String getImage1() {
        return image1;
    }
    public void setImage2(String image2) {
        this.image2 = image2;
    }
    public String getImage2() {
        return image2;
    }
    public void setImage3(String image3) {
        this.image3 = image3;
    }
    public String getImage3() {
        return image3;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getId() {
        return id;
    }


    public void setUserid(String userid) {
        this.userid = userid;
    }



    public String getUserid() {
        return userid;
    }



}
