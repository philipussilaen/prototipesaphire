package com.ish.SaphireTransmart.database;



import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by ACER on 02/11/2016.
 */

    @DatabaseTable(tableName = "damagegood")
    public class TableDamageGood implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String kode_office, product_type, serial_number, unixId, type_damage,keterangan,photo,photo2,photo3, tgl_input, status, userid;

    @DatabaseField
    private int qty,flag;

    public TableDamageGood() {}

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getProduct_type() {
        return product_type;
    }

    public String getTgl_input() {
        return tgl_input;
    }

    public void setTgl_input(String tgl_input) {
        this.tgl_input = tgl_input;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto2(String photo2) {
        this.photo2 = photo2;
    }

    public String getPhoto2() {
        return photo2;
    }

    public void setPhoto3(String photo3) {
        this.photo3 = photo3;
    }

    public String getPhoto3() {
        return photo3;
    }

    public String getPeriodeawal() {
        return type_damage;
    }

    public void setType_damage(String type_damage) {
        this.type_damage = type_damage;
    }
    public String getType_damage() {
        return type_damage;
    }



    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getFlag() {
        return flag;
    }

    public void setUnixId(String unixId) {
        this.unixId = unixId;
    }

    public String getUnixId() {
        return unixId;
    }

    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getKode_office() {
        return kode_office;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public int getQty() {
        return qty;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getKeterangan() {
        return keterangan;
    }


}
