package com.ish.SaphireTransmart.main;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.database.TableAppVersion;
import com.ish.SaphireTransmart.database.TableJadwalAbsen;
import com.ish.SaphireTransmart.database.TableJadwalAbsenMobile;
import com.ish.SaphireTransmart.database.TableJudul;
import com.ish.SaphireTransmart.database.TableKategori;
import com.ish.SaphireTransmart.database.TableMCompetitor;
import com.ish.SaphireTransmart.database.TableMLogin;
import com.ish.SaphireTransmart.database.TableMOffice;
import com.ish.SaphireTransmart.database.TablePembicara;
import com.ish.SaphireTransmart.database.TableProduk;
import com.ish.SaphireTransmart.database.TableStatusDamage;
import com.ish.SaphireTransmart.database.TableStatusFeed;
import com.ish.SaphireTransmart.database.TableTransEducationPlan;
import com.ish.SaphireTransmart.database.TableTypeDamage;
import com.ish.SaphireTransmart.database.database_adapter.TableJadwalAbsenAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableJadwalAbsenMobileAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableJudulAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableKategoriAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableMCompetitorAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableMLoginAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableMOfficeAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TablePembicaraAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableProdukAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableStatusDamageAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableStatusFeedAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableTransEducationPlanAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableTypeDamageAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableVersionAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.utils.ConnectionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hari H on 7/28/2016.
 */
public class SplashScreenActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 1500;
    private String TAG = SplashScreenActivity.class.getSimpleName();
    private Context ctx;
    private TableMOfficeAdapter mOfficeAdapter;
    private TableJadwalAbsenMobileAdapter mJadwalmobAdapter;
    private TableTransEducationPlanAdapter tEducationplanAdapter;
    String lupMOffice,lupjadwalmobile,lupEduplan;

    protected SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        fetchMOffice();
        fetchAppVersion();

        fetchJadwalAbsen();
        fetchMLogin();
        displaySplashScreen();
        fetchKategori();
        fetchType();
        fetchStatusDamage();
        fetchStatusFeed();
        fetchPembicara();
        fetchTransEducationPlan();

        fetchJudul();
        fetchCompetitor();
        fetchProduct();


        // displaySplashScreen();

        fetchJadwalAbsenMobile();


//        TableMLoginAdapter dbAdapter = new TableMLoginAdapter(SplashScreenActivity.this);
//        dbAdapter.deleteAll();
//        dbAdapter.insertData(new TableMLogin(), "1", "nganu", "nganu", "1",
//                "coba", "jenis", "area", "nganu", "nganu", "nagju", "asas", "a", "asa",
//                "aa", "a", "as",  "asas", "asa", "as",  "asas", "asas");
//
//        TableProdukAdapter adapter = new TableProdukAdapter(SplashScreenActivity.this);
//        adapter.deleteAll();
        /*adapter.insertData(new TableProduk(), "1", "Ensure", "Ensure NG Chocolate", 400,
                "GT", "API", "99119", "http://pharmacycentral.com.au/media/catalog/product/e/n/ensure-powder-chocolate-900g-pow-003.jpg", 0);

        adapter.    insertData(new TableProduk(), "2", "Isomil", "Isomil Adv", 400,
                "GT", "API", "103724", "http://www.rajasusu.com/image/cache/data/susu%20batita%201-3%20tahun/isomil%20advance-800x800.jpg", 0);

        adapter.insertData(new TableProduk(), "3", "Pediasure", "Pediasure Complete Chocolate", 850,
                "GT", "API", "221263", "http://static.abbottnutrition.com/cms-prod/abbottnutrition.com/img/PediaSureWeb.png", 0);

        adapter.insertData(new TableProduk(), "4", "Pediasure", "Pediasure Complete Chocolate", 400,
                "GT", "API", "99119", "http://www.mims.com/resources/drugs/Malaysia/packshot/Pediasure%20Complete6001PPS0.JPG", 0);

        adapter.insertData(new TableProduk(), "5", "Similac", "Similac Adv 1", 400,
                "GT", "API", "103724", "https://img.orami.co.id/media/catalog/product/cache/1/image/265x/0dc2d03fe217f8c83829496872af24a0/S/U/SUSU-SIMI-001A-1.jpg", 0);

        adapter.insertData(new TableProduk(), "6", "Similac", "Similac Neosure", 850,
                "GT", "API", "221263", "https://i5.walmartimages.com/asr/cceae3a2-cb6b-4217-8e6b-ffdcffe62d2e_1.d7d6bd6423087adb4015b264dcb114d7.jpeg", 0);*/


    }

    /**
     * Method for display Splash screen
     */
    private void displaySplashScreen() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    Intent i = new Intent(SplashScreenActivity.this, MultiplePermissionsActivity.class);
                    startActivity(i);
                    finish();
                }else{
                Intent i = new Intent(SplashScreenActivity.this, MainMenu.class);
                startActivity(i);
                finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }


    private void fetchKategori() {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                ConnectionManager.MASTER_M_KATEGORI, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    TableKategoriAdapter dbAdapter = new TableKategoriAdapter(SplashScreenActivity.this);
                    // dbAdapter.deleteAll();
                    // check for error flag
                    if (obj.getBoolean("error") == false) {

                        JSONArray jsonArray = obj.getJSONArray("list_kategori");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject versionObj = (JSONObject) jsonArray.get(i);
//                            String id = versionObj.getString("id");
                            String product_category = versionObj.getString("product_category");
                            String upd = versionObj.getString("upd");
                            String lup = versionObj.getString("lup");
                            String channel = versionObj.getString("channel");

                            ctx = getApplicationContext();
                            dbAdapter.delete(SplashScreenActivity.this,product_category);

                            dbAdapter.insertData(new TableKategori(),  product_category, upd, lup, channel);

                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }


    private void fetchType() {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                ConnectionManager.MASTER_M_TYPEDAMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    TableTypeDamageAdapter dbAdapter = new TableTypeDamageAdapter(SplashScreenActivity.this);
                    // dbAdapter.deleteAll();
                    // check for error flag
                    if (obj.getBoolean("error") == false) {

                        JSONArray jsonArray = obj.getJSONArray("list_type");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject versionObj = (JSONObject) jsonArray.get(i);
                            int id = versionObj.getInt("id");
                            String type_damage = versionObj.getString("type_damage");
                            String kategori = versionObj.getString("kategori");


                            ctx = getApplicationContext();
                            dbAdapter.delete(SplashScreenActivity.this,id);

                            dbAdapter.insertData(new TableTypeDamage(),  id,type_damage,kategori);

                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void fetchJudul() {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                ConnectionManager.MASTER_M_JUDUL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    TableJudulAdapter dbAdapter = new TableJudulAdapter(SplashScreenActivity.this);
                    // dbAdapter.deleteAll();
                    // check for error flag
                    if (obj.getBoolean("error") == false) {

                        JSONArray jsonArray = obj.getJSONArray("judul");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject versionObj = (JSONObject) jsonArray.get(i);
                            String id = versionObj.getString("id");
                            String judul = versionObj.getString("judul");

                            //String image = versionObj.getString("image");

                            ctx = getApplicationContext();
                            dbAdapter.delete(SplashScreenActivity.this,id);

                            dbAdapter.insertData(new TableJudul(), id, judul);

                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void fetchPembicara() {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                ConnectionManager.MASTER_M_PEMBICARA, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    TablePembicaraAdapter dbAdapter = new TablePembicaraAdapter(SplashScreenActivity.this);
                    // dbAdapter.deleteAll();
                    // check for error flag
                    if (obj.getBoolean("error") == false) {

                        JSONArray jsonArray = obj.getJSONArray("pembicara");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject versionObj = (JSONObject) jsonArray.get(i);
                            String id = versionObj.getString("id");
                            String pembicara = versionObj.getString("pembicara");


                            ctx = getApplicationContext();
                            dbAdapter.delete(SplashScreenActivity.this,id);

                            dbAdapter.insertData(new TablePembicara(), id, pembicara);

                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void fetchTransEducationPlan() {
        tEducationplanAdapter = new TableTransEducationPlanAdapter(this);
        List<TableTransEducationPlan> listEduPlan = tEducationplanAdapter.getAllDatabyLup();

        for (int i=0; i<listEduPlan.size(); i++) {
            TableTransEducationPlan item = listEduPlan.get(i);
            lupEduplan = item.getLup();
            //  locationIdArray.add(item.getKode_office());
            break;
        }
        Log.e(TAG, "lup fetchTransEducationPlan: " +lupEduplan );

        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.EDUCATIONPLANGET_ENTRY, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);



                try {
                    JSONObject obj = new JSONObject(response);
                    TableTransEducationPlanAdapter dbAdapter = new TableTransEducationPlanAdapter(SplashScreenActivity.this);
                    //  dbAdapter.deleteAll();

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        JSONArray jsonArray = obj.getJSONArray("data_educationplan");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject versionObj = (JSONObject) jsonArray.get(i);
                            String id = versionObj.getString("id_eduplan");
                            String userid = versionObj.getString("userid");
                            String kode_office = versionObj.getString("kode_office");
                            String tgl_input = versionObj.getString("tgl_input");
                            String tgl_edu = versionObj.getString("tgl_edu");
                            String waktu_awal = versionObj.getString("waktu_awal");
                            String waktu_akhir = versionObj.getString("waktu_akhir");
                            String judul = versionObj.getString("judul");
                            String pembicara = versionObj.getString("pembicara");
                            String notes = versionObj.getString("notes");
                            int jml_peserta = versionObj.getInt("jml_peserta");
                            int flag = versionObj.getInt("flag");
                            int flag_approval = versionObj.getInt("flag_approval");
                            String lup = versionObj.getString("lup");
                            //versionObj.getInt
                            Log.e("isitabelEDUPLAN",   id);
                            ctx = getApplicationContext();
                            dbAdapter.delete(SplashScreenActivity.this,id);
                            //dbAdapter.delete(ctx,id);
                            dbAdapter.insertData(new TableTransEducationPlan(), id, userid, kode_office, tgl_input,
                                    tgl_edu, waktu_awal, waktu_akhir, judul, pembicara, jml_peserta, flag, flag_approval,lup,notes);
                            //     break;
                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        })
        {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (lupEduplan!=null) {
                    params.put("luptranseducationplan", lupEduplan);
                };

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        // MyApplication.getInstance().addToRequestQueue(strReq);

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void fetchCompetitor() {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                ConnectionManager.MASTER_M_COMPETITOR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    TableMCompetitorAdapter dbAdapter = new TableMCompetitorAdapter(SplashScreenActivity.this);
                    // dbAdapter.deleteAll();
                    // check for error flag
                    if (obj.getBoolean("error") == false) {

                        JSONArray jsonArray = obj.getJSONArray("list_competitor");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject versionObj = (JSONObject) jsonArray.get(i);
                            int id = versionObj.getInt("id");
                            String competitor = versionObj.getString("competitor");
                            String kategori = versionObj.getString("kategori");


                            ctx = getApplicationContext();
                            dbAdapter.delete(SplashScreenActivity.this,id);

                            dbAdapter.insertData(new TableMCompetitor(), id, competitor,kategori);

                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void fetchStatusDamage() {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                ConnectionManager.MASTER_M_STATUSDAMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    TableStatusDamageAdapter dbAdapter = new TableStatusDamageAdapter(SplashScreenActivity.this);
                    // dbAdapter.deleteAll();
                    // check for error flag
                    if (obj.getBoolean("error") == false) {

                        JSONArray jsonArray = obj.getJSONArray("list_stdm");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject versionObj = (JSONObject) jsonArray.get(i);
                            int id = versionObj.getInt("id");
                            String status = versionObj.getString("status");
//                            String kategori = versionObj.getString("kategori");


                            ctx = getApplicationContext();
                            dbAdapter.delete(SplashScreenActivity.this,id);

                            dbAdapter.insertData(new TableStatusDamage(), id, status);

                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }


    private void fetchStatusFeed() {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                ConnectionManager.MASTER_M_STATUSFEED, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    TableStatusFeedAdapter dbAdapter = new TableStatusFeedAdapter(SplashScreenActivity.this);
                    // dbAdapter.deleteAll();
                    // check for error flag
                    if (obj.getBoolean("error") == false) {

                        JSONArray jsonArray = obj.getJSONArray("list_feed");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject versionObj = (JSONObject) jsonArray.get(i);
                            int id = versionObj.getInt("id");
                            String status = versionObj.getString("status");
//                            String kategori = versionObj.getString("kategori");


                            ctx = getApplicationContext();
                            dbAdapter.delete(SplashScreenActivity.this,id);

                            dbAdapter.insertData(new TableStatusFeed(), id, status);

                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    /**
     * Get data master
     */
    private void fetchJadwalAbsen() {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                ConnectionManager.MASTER_ABSEN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    TableJadwalAbsenAdapter dbAdapter = new TableJadwalAbsenAdapter(SplashScreenActivity.this);
                 //   dbAdapter.deleteAll();
                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        JSONArray jsonArray = obj.getJSONArray("jadwal_absen");
                        for (int i = 0;     i < jsonArray.length(); i++) {
                            JSONObject jdwlObj = (JSONObject) jsonArray.get(i);
                            TableJadwalAbsen ja = new TableJadwalAbsen();
                            int id = jdwlObj.getInt("id");
                            String date = jdwlObj.getString("date");
                            String salesman = jdwlObj.getString("salesman");
                            String office = jdwlObj.getString("office");
                            String roster = jdwlObj.getString("roster");
                            String upd = jdwlObj.getString("upd");
                            String lup = jdwlObj.getString("lup");

                            ctx = getApplicationContext();
                            dbAdapter.delete(SplashScreenActivity.this,id);
                            dbAdapter.insertData(new TableJadwalAbsen(), id, date, salesman, office,
                                    roster, upd, lup);

                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void fetchAppVersion() {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                ConnectionManager.MASTER_APP_VERSION, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    TableVersionAdapter dbAdapter = new TableVersionAdapter(SplashScreenActivity.this);
                    dbAdapter.deleteAll();
                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        JSONArray jsonArray = obj.getJSONArray("app_version");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject versionObj = (JSONObject) jsonArray.get(i);
                            String id = versionObj.getString("app_version_code");
                            String name = versionObj.getString("app_version_name");
                            String desc = versionObj.getString("app_desc");
                            String url = versionObj.getString("url_encrypt64");
                            String kd_user = versionObj.getString("kd_user");
                            String tgl_upload = versionObj.getString("tgl_upload");

                            dbAdapter.insertData(new TableAppVersion(), id, name, desc, url,
                                    kd_user, tgl_upload);

                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void fetchMOffice() {
        mOfficeAdapter = new TableMOfficeAdapter(this);
        List<TableMOffice> listOffice = mOfficeAdapter.getAllDatabyLup();

        for (int i=0; i<listOffice.size(); i++) {
            TableMOffice item = listOffice.get(i);
            lupMOffice = item.getLup();
          //  locationIdArray.add(item.getKode_office());
            break;
        }
        Log.e(TAG, "lup fetchMOffice: " +lupMOffice );

        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.MASTER_M_OFFICE, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);



                try {
                    JSONObject obj = new JSONObject(response);
                    TableMOfficeAdapter dbAdapter = new TableMOfficeAdapter(SplashScreenActivity.this);
                  //  dbAdapter.deleteAll();
                 //   SQLiteStatement insert = mDb.compileStatement(sql);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        JSONArray jsonArray = obj.getJSONArray("m_office");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject versionObj = (JSONObject) jsonArray.get(i);
                            int id = versionObj.getInt("id");
                            String office = versionObj.getString("office");
                            String kode_office = versionObj.getString("kode_office");
                            String channel = versionObj.getString("channel");
                            String jenis = versionObj.getString("jenis");
                            String alamat = versionObj.getString("alamat");
                            String kota = versionObj.getString("kota");
                            String propinsi = versionObj.getString("propinsi");
                            String pic = versionObj.getString("pic");
                            String pic_phone = versionObj.getString("pic_phone");
                            String map_lat = versionObj.getString("map_lat");
                            String map_lng = versionObj.getString("map_lng");
                            String upd = versionObj.getString("upd");
                            String lup = versionObj.getString("lup");
                            String ket = versionObj.getString("ket");
                            String region = versionObj.getString("region");
                            String rek = versionObj.getString("rek");
                            String bank = versionObj.getString("bank");
                            String rental = versionObj.getString("rental");
                            String teritory = versionObj.getString("teritory");
                            Log.e("isitabeloffice",   Integer.toString(id));
                            ctx = getApplicationContext();
                            dbAdapter.delete(SplashScreenActivity.this,id);
                            //dbAdapter.delete(ctx,id);
                            dbAdapter.insertData(new TableMOffice(), id, office, kode_office, channel,
                                    jenis, alamat, kota, propinsi, pic, pic_phone, map_lat, map_lng,
                                    upd, lup, ket, region, rek, bank, rental,teritory);
                       //     break;
                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        })
        {

            @Override
            protected Map<String, String> getParams() {
            Map<String, String> params = new HashMap<>();
            if (lupMOffice!=null) {
                params.put("lupmoffice", lupMOffice);


            };

            Log.e(TAG, "params: " + params.toString());
            return params;
        }
        };

        //Adding request to request queue
       // MyApplication.getInstance().addToRequestQueue(strReq);

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void fetchProduct() {
        StringRequest strReq = new StringRequest(Request.Method.GET,
                ConnectionManager.MASTER_M_PRODUK, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    TableProdukAdapter dbAdapter = new TableProdukAdapter(SplashScreenActivity.this);
                    // dbAdapter.deleteAll();
                    // check for error flag
                    if (obj.getBoolean("error") == false) {

                        JSONArray jsonArray = obj.getJSONArray("produk");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject versionObj = (JSONObject) jsonArray.get(i);
                            String id = versionObj.getString("id");
                            String product_category = versionObj.getString("product_category");
                            String detail_category = versionObj.getString("detail_category");
                            String model_name = versionObj.getString("model_name");
                            int gram = versionObj.getInt("gram");
                            String channel = versionObj.getString("channel");
                            String entity = versionObj.getString("entity");
                            String price = versionObj.getString("price");
                            String region = versionObj.getString("region");
                            //String image = versionObj.getString("image");

                            ctx = getApplicationContext();
                            dbAdapter.delete(SplashScreenActivity.this,id);

                            dbAdapter.insertData(new TableProduk(), id, product_category, model_name, gram,
                                    channel, entity, price, "", 0,region,detail_category);

                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }
    private void fetchMLogin() {

        StringRequest strReq = new StringRequest(Request.Method.GET,
                ConnectionManager.MASTER_M_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    TableMLoginAdapter dbAdapter = new TableMLoginAdapter(SplashScreenActivity.this);
                   // dbAdapter.deleteAll();
                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        JSONArray jsonArray = obj.getJSONArray("m_login");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject loginObj = (JSONObject) jsonArray.get(i);
                            String userid = loginObj.getString("userid");
                            String username = loginObj.getString("username");
                            String pass = loginObj.getString("password");
                            String level = loginObj.getString("userlevel");
                            String jenis = loginObj.getString("jenis_absen");
                            String status = loginObj.getString("userstatus");
                            String area = loginObj.getString("area");
                            String perner = loginObj.getString("perner");
                            String xarea = loginObj.getString("xarea");
                            String kd_area = loginObj.getString("kd_area");
                            String klien_layanan = loginObj.getString("klien_layanan");
                            String kd_layanan = loginObj.getString("kd_layanan");
                            String sub_area = loginObj.getString("sub_area");
                            String kd_sub_area = loginObj.getString("kd_sub_area");
                            String jabatan = loginObj.getString("jabatan");

                            String skill_layanan = loginObj.getString("skill_layanan");
                            String kd_skill_layanan = loginObj.getString("kd_skill_layanan");
                            String fail_login = loginObj.getString("fail_login");
                            String ket = loginObj.getString("ket");
                            String upd = loginObj.getString("upd");
                            String region = loginObj.getString("region");
                            String teritory = loginObj.getString("teritory");
                            ctx = getApplicationContext();
                            //dbAdapter.delete(ctx,userid);
                            dbAdapter.delete(SplashScreenActivity.this,userid);
                            dbAdapter.insertData(new TableMLogin(), userid, username, pass, level,
                                    status, jenis, area, perner, xarea, kd_area, klien_layanan, kd_layanan, sub_area,
                                    kd_sub_area, upd, jabatan,  skill_layanan, kd_skill_layanan, fail_login,  ket, region,teritory);

                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }


    private void fetchJadwalAbsenMobile() {
        mJadwalmobAdapter = new TableJadwalAbsenMobileAdapter(this);
        List<TableJadwalAbsenMobile> listjadwalm = mJadwalmobAdapter.getAllDatabyLup();

        for (int i=0; i<listjadwalm.size(); i++) {
            TableJadwalAbsenMobile item = listjadwalm.get(i);
            lupjadwalmobile = item.getLup();
            //  locationIdArray.add(item.getKode_office());
            break;
        }
        Log.e(TAG, "lupjadwalmobile: " +lupjadwalmobile );
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.MASTER_ABSEN_MOBILE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    TableJadwalAbsenMobileAdapter dbAdapter = new TableJadwalAbsenMobileAdapter(SplashScreenActivity.this);
                    //   dbAdapter.deleteAll();
                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        JSONArray jsonArray = obj.getJSONArray("jadwal_absen_mobile");
                        for (int i = 0;     i < jsonArray.length(); i++) {
                            JSONObject jdwlObj = (JSONObject) jsonArray.get(i);
                            TableJadwalAbsen ja = new TableJadwalAbsen();
                            int id = jdwlObj.getInt("id");
                            String date = jdwlObj.getString("date");
                            String salesman = jdwlObj.getString("salesman");
                            String office = jdwlObj.getString("office");
                            String roster = jdwlObj.getString("roster");
                            String upd = jdwlObj.getString("upd");
                            String lup = jdwlObj.getString("lup");

                            ctx = getApplicationContext();
                            dbAdapter.delete(SplashScreenActivity.this,id);
                            dbAdapter.insertData(new TableJadwalAbsenMobile(), id, date, salesman, office,
                                    roster, upd, lup);
                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        })
        {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (lupjadwalmobile!=null) {
                    params.put("lupjadwalmobile", lupjadwalmobile);


                };

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        // MyApplication.getInstance().addToRequestQueue(strReq);

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }
//    @Override
//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
//
//        MultiDex.install(this);
//    }
}
