package com.ish.SaphireTransmart.main.menu;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.adapter.JudulListAdapter;
import com.ish.SaphireTransmart.database.TableImage;
import com.ish.SaphireTransmart.database.TableMOffice;
import com.ish.SaphireTransmart.database.TableTransEducation;
import com.ish.SaphireTransmart.database.TableTransEducationPlan;
import com.ish.SaphireTransmart.database.database_adapter.TableImageAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableMOfficeAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableTransEducationAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableTransEducationPlanAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.main.MainMenuActivity;
import com.ish.SaphireTransmart.utils.AlbumStorageDirFactory;
import com.ish.SaphireTransmart.utils.Base64;
import com.ish.SaphireTransmart.utils.BaseAlbumDirFactory;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;
import com.ish.SaphireTransmart.utils.listitem_object.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

/**
 * Created by adminmc on 27/09/16.
 */
public class Education extends Fragment {
    private String TAG = Education.class.getSimpleName();
    private ViewGroup root;
    private List<Stock> list = new ArrayList<Stock>();
//    private InsertingAdapter adapter;
    private RecyclerView recyclerView;
   // private TextView location;
    private ArrayAdapter<String> spinnerLocation;
    //private ArrayList<String> locationList, locationIdArray;
    private android.app.DatePickerDialog DatePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TableMOfficeAdapter mOfficeAdapter;
    private Button btnSave;
    private Utility utils;
    private TableTransEducationAdapter transEducationAdapter;
    private TableTransEducationPlanAdapter transEducationPlanAdapter;
    private TextView cdate, location, timein, locationIn, timeOut, locationOut;
    private EditText EdNextPlan, EdResult;
    private TableTransEducationPlan item;
    private List<TableMOffice> listOffice;
    private List<TableTransEducationPlan> listEducationPLan;
    private ArrayList<String> locationList, JudulList,EduplanList;
    private Double lng=0.0, lat=0.0;
  //  private TableMOffice item;
    public Education() {
        utils = new Utility(getActivity());
    }
    private String locationId, userId,locationName,datetimenow,datetimeedu,varnextplan,varresult,image1,image2,image3,clockin;
    private static final int TAKE_PICTURE_EDU1 = 121;
    private static final int TAKE_PICTURE_EDU2 = 122;
    private static final int TAKE_PICTURE_EDU3 = 123;
    private File f;
    private String picturePath = "", picturePath2 = "",picturePath3 = "",judulpilih,ideduplanpilih;
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private ImageView ibPhotoEdu1,ibPhotoEdu2,ibPhotoEdu3 ;
    ProgressDialog progressDialog;
    private String[] judulArrayList, tipeArrayList;
    private ArrayAdapter<String> spinnerJudul, spinnerTipe;
    private TextView judulspin;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_education, container, false);
       // prepareData();
        mAlbumStorageDirFactory = new BaseAlbumDirFactory();

        utils = new Utility(getActivity());
        utils.setIMEI();
        utils.setGeoLocation();
        userId = MyApplication.getInstance().getPrefManager().getUser().getId();
        judulArrayList = getActivity().getResources().getStringArray(R.array.judul_edulist);
        spinnerJudul = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item,
                judulArrayList);
        initView(root);
        return root;

    }
    private void initView(ViewGroup v) {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        ibPhotoEdu1 = (ImageView) v.findViewById(R.id.ib_edu1);
        ibPhotoEdu2 = (ImageView) v.findViewById(R.id.ib_edu2);
        ibPhotoEdu3 = (ImageView) v.findViewById(R.id.ib_edu3);

        ibPhotoEdu1.setOnClickListener(ibPhotoEdu1Listener);
        ibPhotoEdu2.setOnClickListener(ibPhotoEdu2Listener);
        ibPhotoEdu3.setOnClickListener(ibPhotoEdu3Listener);

        EdNextPlan = (EditText) v.findViewById(R.id.input_nextplanedu);
//        location = (TextView) v.findViewById(R.id.txt_vlokasiedu);
//        location.setOnClickListener(locationListener);
        EdResult = (EditText) v.findViewById(R.id.input_resultedu);
        btnSave = (Button) v.findViewById(R.id.btn_saveedu);
        btnSave.setOnClickListener(saveListener);
        EdNextPlan.setOnClickListener(nextplanlistener);
        setDateTimeField();

        judulspin = (TextView) v.findViewById(R.id.txt_vjuduledu);

        judulspin.setOnClickListener(judulListener);
        judulspin.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_search, 0, 0, 0);

        JudulList = new ArrayList<String>();
        EduplanList = new ArrayList<String>();

        String object = utils.getClockIn(getActivity());
        try {
            if (object!=null && !object.equalsIgnoreCase("")) {
                JSONObject obj = new JSONObject(object);
             //   location.setText(obj.getString("office"));
                clockin = obj.getString("time");
                btnSave.setEnabled(true);
            } else {
                Toast.makeText(getActivity(), "Silahkan lakukan clockin terlebih dahulu", Toast.LENGTH_SHORT).show();
                btnSave.setEnabled(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            btnSave.setEnabled(false);
        }
       // userId = MyApplication.getInstance().getPrefManager().getUser().getId();
      //  transEducationPlanAdapter = new TableTransEducationPlanAdapter(getActivity());
       // List<TableTransEducationPlan> transEducationPlans = transEducationPlanAdapter.getAllData();
        TableTransEducationPlanAdapter transEducationPlan = new TableTransEducationPlanAdapter(getActivity());
        try {
          //  listEducationPLan = transEducationPlan.getAllData();
            listEducationPLan = transEducationPlan.getDatabyCondition("flagApprove", 2);
            Log.d("list eduplan", listEducationPLan.toString());
            for (int i=0; i<listEducationPLan.size(); i++) {
                item = listEducationPLan.get(i);
            JudulList.add(item.getJudul());
            EduplanList.add(item.getId());
            Log.d("list eduplan", item.getJudul() + "-" + item.getId());
        }
        } catch (Exception e) {
            e.printStackTrace();
        }

        spinnerJudul = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, JudulList);


    }

    private View.OnClickListener judulListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            LayoutInflater li = LayoutInflater.from(getActivity());
            View view = li.inflate(R.layout.m_judul_listview, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    getActivity());
            alertDialogBuilder.setView(view);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            ListView judulListView = (ListView) view.findViewById(R.id.judul_list);
            EditText txtjudul = (EditText) view.findViewById(R.id.search_judul);

            final JudulListAdapter adapter = new JudulListAdapter(getActivity(), JudulList);
            judulListView.setAdapter(adapter);

            txtjudul.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    System.out.println("Text ["+s+"]");

                    adapter.getFilter().filter(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


            judulListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        judulspin.setText(adapter.getFilterData().get(position));
//                        TableTransEducationPlanAdapter mOfficeAdapter = new TableMOfficeAdapter(getActivity());
//                        String[] separated = adapter.getFilterData().get(position).split(" => ");
//                        List<TableMOffice> listdata = mOfficeAdapter.getDatabyCondition("kode_office", separated[2]);

                         judulpilih = adapter.getFilterData().get(position);
                         ideduplanpilih = EduplanList.get(position).toString();
                        Log.d("locationid1", judulpilih+"-"+ideduplanpilih);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    alertDialog.dismiss();
                }
            });

            alertDialog.show();

        }
    };
    private View.OnClickListener nextplanlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DatePickerDialog.show();

        }
    };

    View.OnClickListener ibPhotoEdu1Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            takeImage(TAKE_PICTURE_EDU1);
        }
    };
    View.OnClickListener ibPhotoEdu2Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            takeImage(TAKE_PICTURE_EDU2);
        }
    };
    View.OnClickListener ibPhotoEdu3Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            takeImage(TAKE_PICTURE_EDU3);
        }
    };
    public void takeImage(int code) {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            f = null;
            f = createImageFile(code);
            if (code == TAKE_PICTURE_EDU1) {
                picturePath = f.getAbsolutePath();
            }else  if (code == TAKE_PICTURE_EDU2) {
                picturePath2 = f.getAbsolutePath();
            } else if (code == TAKE_PICTURE_EDU3) {
                picturePath3 = f.getAbsolutePath();
            }

            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            startActivityForResult(takePictureIntent, code);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private File createImageFile(int code) throws IOException {
        String imageFileName = "";
        switch (code) {
            case TAKE_PICTURE_EDU1:
                imageFileName = generateImageName() + "_edu1";
                break;
            case TAKE_PICTURE_EDU2:
                imageFileName = generateImageName() + "_edu2";
                break;
            case TAKE_PICTURE_EDU3:
                imageFileName = generateImageName() + "_edu3";
                break;

        }

        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
        return imageF;
    }

    private String generateImageName() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.toString();

        return sb.toString();
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("Saphire");
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (TAKE_PICTURE_EDU1):
                if (resultCode == Activity.RESULT_OK) {
                    final File file = new File(picturePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
                    if (bitmap!=null) {
                        ibPhotoEdu1.setImageBitmap(bitmap);
                    }
                    galleryAddPic(picturePath);
                }
                break;
            case (TAKE_PICTURE_EDU2):
                if (resultCode == Activity.RESULT_OK) {
                    final File file = new File(picturePath2);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath2, options);
                    if (bitmap!=null) {
                        ibPhotoEdu2.setImageBitmap(bitmap);
                    }
                    galleryAddPic(picturePath2);
                }
                break;
            case (TAKE_PICTURE_EDU3):
                if (resultCode == Activity.RESULT_OK) {
                    final File file = new File(picturePath3);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath3, options);
                    if (bitmap!=null) {
                        ibPhotoEdu3.setImageBitmap(bitmap);
                    }
                    galleryAddPic(picturePath3);
                }
                break;


        }
    }

    private void galleryAddPic(String path) {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);

    }


    private String generateIdIndex(int i) {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append(userId);
        sb.append("_");
        sb.append("education");
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));
        sb.append(String.valueOf(i));

        sb.toString();
        return sb.toString();
    }


    private void chunkImage(String image, String id) {
        try {
            TableImageAdapter imageAdapter = new TableImageAdapter(getActivity());

            String userid= MyApplication.getInstance().getPrefManager().getUser().getId();
            final File file = new File(image);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
          //  Bitmap bitmap = BitmapFactory.decodeFile(image, options);
            String filePath = Utility.compressImage(image,file.getName());

            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            byte[] data = bos.toByteArray();
            String file1 = Base64.encodeBytes(data);
            String md5 = utils.md5(file.getName());

            int limit = 10000;
            int start = 0;
            String h = "";
            String f = "";
            int tot = (int) Math.ceil((double) file1.length() / (double) limit);

            /*imageAdapter.insertData(new TableImage(), md5, file1, file.getName(), image, userid,
                    1, 1, "attendance", utils.getCurrentDateandTime(), 0);*/
            for (int i = 0; i < tot; i++) {
                if (limit < file1.length()) {
                    h = file1.substring(start, limit);
                    start = limit;
                    limit = limit + 10000;
                } else {
                    limit = file1.length();
                    h = file1.substring(start, limit);
                    f = h;
                }

                imageAdapter.insertData(new TableImage(),  generateIdIndex(i), md5, id, h, file.getName(), image, userid,
                        i+1, tot, "education", utils.getCurrentDateandTime(), 0);
            }

            List<TableImage> listImage = imageAdapter.getDatabyCondition("flag", 0);
            for (int i =0; i<listImage.size(); i++) {
                TableImage itemImage = listImage.get(i);
                uploadImage(itemImage, getActivity());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadImage(final TableImage item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.UPLOAD_IMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableImageAdapter adapter = new TableImageAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("id", String.valueOf(item.getId()));
                    params.put("image", item.getImage());
                    params.put("name", item.getName());
                    params.put("id_image", item.getId_image());
                    params.put("m_path", item.getM_path());
                    params.put("userid", item.getUserid());
                    params.put("index", String.valueOf(item.getIndex()));
                    params.put("total", String.valueOf(item.getTotal()));
                    params.put("type", item.getType());
                    params.put("id_absen", item.getId_absen());


                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }
    
    private void setDateTimeField() {
         // EdNextPlan.setOnClickListener(this);
        EdNextPlan = (EditText) root.findViewById(R.id.input_nextplanedu);
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog = new DatePickerDialog(getActivity(), new android.app.DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                Calendar cal = Calendar.getInstance(TimeZone.getDefault());
                newDate.set(year, monthOfYear, dayOfMonth);
                // DatePickerDialog.getDatePicker().setMinDate(newDate.getTime()-(newDate.getTime()%(24*60*60*1000)));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                datetimenow = sdf.format(cal.getTime()).toString();
                datetimeedu = sdf.format(newDate.getTime()).toString();

                try {

                    Date datepick = sdf.parse(datetimeedu);
                    Log.i(TAG, "response date " + datetimeedu);
                    Date datenow = sdf.parse(datetimenow);
                    Log.i(TAG, "response date " + datetimenow);

                    if(datepick.equals(datenow)){
                        System.out.println("Cannot Pick Date = today");
                        Toast.makeText(getActivity(), "Cannot Pick Date = today ", Toast.LENGTH_LONG).show();

                        EdNextPlan.setText("");
                    }
//                    else if(datepick.before(datenow) || datepick.equals(datenow)){
                    else{
                        long timeOne = datenow.getTime();
                        long timeTwo = datepick.getTime();
                        long oneDay = 1000 * 60 * 60 * 24;
                        long delta = (timeTwo - timeOne) / oneDay;
                        System.out.println(delta);
                        if (delta <=-1) {
                            //  return "dateTwo is " + delta + " days after dateOne";
                            Toast.makeText(getActivity(), " Cannot Pick Date  < today ! ", Toast.LENGTH_LONG).show();
                            System.out.println("Cannot Pick Date  < 1 day");
                            EdNextPlan.setText("");
                        }

                        else {
                            //delta *= -1;
                            EdNextPlan.setText(dateFormatter.format(newDate.getTime()));
                            //  return "dateTwo is " + delta + " days before dateOne";
                        }


                   }


                }catch(ParseException ex){
                    ex.printStackTrace();
                }

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


    }

    private View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (judulspin.getText().toString()==null || judulspin.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Silahkan lakukan pilih Judul terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }
            else if (EdNextPlan.getText().toString()==null || EdNextPlan.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Silahkan Lengkapi seluruh Form terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }
            else if (EdResult.getText().toString()==null || EdResult.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Silahkan Lengkapi seluruh Form terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }
            else if (EdNextPlan.getText().toString()==null || EdNextPlan.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Silahkan Lengkapi seluruh Form terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }
            else if (ibPhotoEdu1.getDrawable() ==null || ibPhotoEdu2.getDrawable() ==null || ibPhotoEdu3.getDrawable() ==null  ) {
                Toast.makeText(getActivity(), "Silahkan Lengkapi Photo terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }else {
                image1 = new File(picturePath).getName();
                image1 = new File(picturePath).getName();
                image2 = new File(picturePath2).getName();
                image3 = new File(picturePath3).getName();

                String id = generateId();
                //List<TableProduk> stockList = adapter.retrieveData();
                transEducationAdapter = new TableTransEducationAdapter(getActivity());
                varnextplan =  datetimeedu;
                varresult = EdResult.getText().toString();
                try {

                    Log.d("save data ", id+"-"+userId+"-"+ideduplanpilih+"-"+clockin+"-"+userId+"-"+
                            utils.getCurrentDateandTimeSec()+"-"+varnextplan+"-"+varresult+"-"+
                            image1+"-"+image2+"-"+image3+"-"+utils.getCurrentDateandTimeSec()+"-"+generateId().toString());
                    transEducationAdapter.insertData(new TableTransEducation(), id, ideduplanpilih,clockin,userId, utils.getCurrentDateandTimeSec(),varnextplan,varresult,image1,image2,image3,0);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                Toast.makeText(getActivity(), "Data berhasil disimpan", Toast.LENGTH_SHORT).show();

                List<TableTransEducation> list = transEducationAdapter.getDatabyCondition("flag", 0);
                for (int i=0; i<list.size(); i++) {
                    TableTransEducation item = list.get(i);
                    if (item!=null) {
                        sendData(item, getActivity());
                    }
                }
                if (picturePath!=null && picturePath.length()>1)
                    chunkImage(picturePath, id);
                if (picturePath2!=null && picturePath2.length()>1)
                    chunkImage(picturePath2, id);
                if (picturePath3!=null && picturePath3.length()>1)
                    chunkImage(picturePath3, id);

                Fragment education = new Education();
                ((MainMenuActivity)getActivity()).initView(education, "Education");

            }
        }
    };
    private void sendData(final TableTransEducation item,final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.EDUCATION_ENTRY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableTransEducationAdapter adapter = new TableTransEducationAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error2: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                if (item!=null) {
                    params.put("id", item.getId());
                    params.put("id_eduplan", item.getId_eduplan());
                    params.put("clockin", item.getClockin());
                    params.put("userid", item.getUserid());

                    params.put("tgl_input", item.getTgl_input());
                    params.put("next_plan", item.getNext_plan());
                    params.put("result", item.getResult());
                    params.put("image1", item.getImage1());
                    params.put("image2", item.getImage2());
                    params.put("image3", item.getImage3());
                    params.put("flag", String.valueOf(item.getFlag()));


                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private String generateId() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append("education");
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.append(String.valueOf(a));
        sb.toString();

        return sb.toString();
    }
}
