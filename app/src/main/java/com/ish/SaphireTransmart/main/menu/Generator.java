package com.ish.SaphireTransmart.main.menu;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.database.TableDetailing;
import com.ish.SaphireTransmart.database.database_adapter.TableDetailingAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableProdukKategoriAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.main.MainMenu;
import com.ish.SaphireTransmart.utils.AlbumStorageDirFactory;
import com.ish.SaphireTransmart.utils.BaseAlbumDirFactory;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

//import com.journeyapps.barcodescanner.BarcodeEncoder;

/**
 * Created by INFOMEDIA on 11/04/2017.
 */

public class Generator extends Fragment {

    private String TAG = Detailing.class.getSimpleName();
    private ViewGroup root;
    private ImageView ibRentArc, ibRentArc2, closer;
    private Button save;
    private static final int TAKE_PICTURE_RENT_ARC = 121;
    private static final int TAKE_PICTURE_RENT_ARC2 = 122;
    private File f;
    private String picturePath = "", picturePath2 = "", name,kd_area,level,userId, clockin;
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private Utility utils;
    private ProgressDialog progressDialog;
    private ImageButton ibGift;
    private EditText txtName;
    private TableProdukKategoriAdapter produkAdapter;
    private LinearLayout mLayoutProduk;
    private ArrayList<String> list = new ArrayList<>();
    private TableDetailingAdapter detailingAdapter;
    private EditText location,locationEdname;
    private TextView TxtPicPath1,TxtPicPath2;
    EditText text;
    Button gen_btn;
    ImageView image;
    String text2Qr;
    private String locationId,locationName;
    public Generator() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_generator, container,false);
        utils = new Utility(getActivity());
        mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        userId = MyApplication.getInstance().getPrefManager().getUser().getId();
        level = MyApplication.getInstance().getPrefManager().getUser().getLevel();
        kd_area = MyApplication.getInstance().getPrefManager().getUser().getKdArea();
        name = MyApplication.getInstance().getPrefManager().getUser().getName();
        detailingAdapter = new TableDetailingAdapter(getActivity());
        utils = new Utility(getActivity());
        initView(root);
        return root;
    }

    private void initView(ViewGroup v) {
        ibRentArc = (ImageView) v.findViewById(R.id.ib_rent_arc);

        locationEdname  = (EditText) root.findViewById(R.id.txtLocationName);


        text = (EditText) root.findViewById(R.id.text);
        gen_btn = (Button) root.findViewById(R.id.gen_btn);
        image = (ImageView) root.findViewById(R.id.image);

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try{
            BitMatrix bitMatrix = multiFormatWriter.encode(userId +" => "+kd_area+" => "+name, BarcodeFormat.QR_CODE,200,200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            image.setImageBitmap(bitmap);
        }
        catch (WriterException e){
            e.printStackTrace();
        }
//        gen_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                text2Qr = text.getText().toString().trim();
//                MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
//                try{
//                    BitMatrix bitMatrix = multiFormatWriter.encode(userId +" => "+kd_area, BarcodeFormat.QR_CODE,200,200);
//                    BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
//                    Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
//                    image.setImageBitmap(bitmap);
//                }
//                catch (WriterException e){
//                    e.printStackTrace();
//                }
//            }
//        });


//        save = (Button) v.findViewById(R.id.btn_save);
//        save.setOnClickListener(saveListener);

    }

//    View.OnClickListener saveListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            AlertConfrim();
//        }
//    };

    private String generateIdIndex(int i) {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append(userId);
        sb.append("_");
        sb.append("detail");
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));
        sb.append(String.valueOf(i));

        sb.toString();
        return sb.toString();
    }

    private String generateId() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append("dtl");
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.append(String.valueOf(a));
        sb.toString();

        return sb.toString();
    }

    private void sendData(final TableDetailing item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.DETAILING_ENTRY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableDetailingAdapter adapter = new TableDetailingAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("id", item.getId());
                    params.put("userid", item.getUserid());
                    params.put("name", item.getNama());
                    params.put("value", item.getValue());
                    params.put("kode_office", item.getKode_office());
                    params.put("clockin", item.getClockin());
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }


    public void SaveData(){
        if (txtName.getText().toString().length()<1) {
            Toast.makeText(getActivity(), "Silahkan isikan nama", Toast.LENGTH_SHORT).show();
        } else if (picturePath2==null || picturePath2.length()<1){
            Toast.makeText(getActivity(), "Silahkan ambil gambar", Toast.LENGTH_SHORT).show();
        } else {
            String value = "";
            for (int j=0; j<list.size();j++) {
                if (j==0) {
                    value = list.get(j);
                } else {
                    value = value+", "+list.get(j);
                }
            }
            String id = generateId();

            detailingAdapter.insertData(new TableDetailing(), id, userId,
                    txtName.getText().toString(), value, 0,locationId,clockin);

            Toast.makeText(getActivity(), "Data berhasil tersimpan", Toast.LENGTH_SHORT).show();

//            List<TableDetailing> list = detailingAdapter.getDatabyCondition("flag", 0);
//            for (int n=0; n<list.size();n++) {
//                TableDetailing item = list.get(n);
//                sendData(item, getActivity());
//            }

            Fragment detail = new Generator();
            ((MainMenu)getActivity()).initView(detail, "Detailing");

        }
    }
    private void AlertConfrim(){
        //Context context;
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
        //String url_dlfile = varurl;
        // set title
        alertDialogBuilder.setTitle("Konfirmasi");

        // set dialog message
        alertDialogBuilder
                .setMessage("Apakah Anda Yakin ?")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
//                        SaveData();
                        // if this button is clicked, close
                        // current activity
//                        if (isi_menu.equals("savesellout")) {
//                            saveTransSellOutOffline();
//                        } else if (isi_menu.equals("saveofftake")) {
//                            saveTransOffTakeOffline();
//                        }
//                        else if (isi_menu.equals("saveofftake_m")) {
//                            saveTransOffTake_MOffline();
//                        }
//                        else if (isi_menu.equals("saveofftake_b")) {
//                            saveTransOffTake_BOffline();
//                        }
                        //id=1;
                        //   return id;
                        //MainActivity.this.finish();
                        //MainActivity.this.finish();
                    }
                }) .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, close
                // current activity

                //MainActivity.this.finish();
                //MainActivity.this.finish();
            }
        });



        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

}
