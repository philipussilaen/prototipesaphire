package com.ish.SaphireTransmart.main.menu;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.database.TableInsertion;
import com.ish.SaphireTransmart.database.TableMOffice;
import com.ish.SaphireTransmart.database.TableProduk;
import com.ish.SaphireTransmart.database.TableTansOfftake;
import com.ish.SaphireTransmart.database.database_adapter.TableInsertionAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableMOfficeAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableProdukAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableTansOfftakeAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.main.MainMenu;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Hari on 20/09/16.
 */
public class MonthlyOfftake extends Fragment {

    private String TAG = MonthlyOfftake.class.getSimpleName();
    private ViewGroup root;
    private List<TableProduk> list = new ArrayList<TableProduk>();
//    private MonthlyOfftakeAdapter adapter;
    private RecyclerView recyclerView;
    private EditText location,locationEdname;
    private ArrayAdapter<String> spinnerLocation;
    private ArrayList<String> locationList, locationIdArray;
    private String locationId,locationName, userId, clockin="";
    private TableMOfficeAdapter mOfficeAdapter;
    private Button btnSave;
    private Utility utils;
    private TableTansOfftakeAdapter transOffAdapter;
    private LinearLayout mLayoutStock;
    private TableInsertionAdapter insertionAdapter;

    public MonthlyOfftake() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.frag_monthly_offtake, container, false);
        utils = new Utility(getActivity());
        //utils.setClockIn(getActivity(), new AttendanceModel("asas", utils.getCurrentDate(), utils.getCurrentTime(), "in", "stay", "GIANT FATMAWATI"));

        prepareData();

        //recyclerView = (RecyclerView) root.findViewById(R.id.tableList);
        //adapter = new MonthlyOfftakeAdapter(list, getActivity());
        //RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        //recyclerView.setLayoutManager(mLayoutManager);
       // recyclerView.setItemAnimator(new DefaultItemAnimator());
        //recyclerView.setAdapter(adapter);

        mLayoutStock = (LinearLayout) root.findViewById(R.id.list_stock);

        btnSave = (Button) root.findViewById(R.id.save);
        btnSave.setOnClickListener(saveListener);

        location = (EditText) root.findViewById(R.id.txtLocation);
        locationEdname  = (EditText) root.findViewById(R.id.txtLocationName);
        //utils.clearClockin();
        String object = utils.getClockIn(getActivity());
        try {
            if (object!=null && !object.equalsIgnoreCase("")) {
                JSONObject obj = new JSONObject(object);
                TableMOfficeAdapter mOfficeAdapter = new TableMOfficeAdapter(getActivity());
                List<TableMOffice> listdata = mOfficeAdapter.getDatabyCondition("kode_office", obj.getString("office"));
              //  Log.d("locationid1", separated[2].toString());
                // int a = listdata.size();
                if (listdata!=null) {
                    TableMOffice item = listdata.get(listdata.size()-1);
                    locationId = item.getKode_office();
                    locationName=item.getOffice();
                    Log.d("locationid2", locationName.toString());

                }
                Log.d("locationid3",obj.getString("office"));
                location.setText(obj.getString("office"));
                locationEdname.setText(locationName);
                clockin = obj.getString("date")+ " " +obj.getString("time");
                btnSave.setEnabled(true);
            } else {
                Toast.makeText(getActivity(), "Silahkan lakukan clockin terlebih dahulu", Toast.LENGTH_SHORT).show();
                btnSave.setEnabled(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            btnSave.setEnabled(false);
        }

        //location.setOnClickListener(locationListener);

        locationList = new ArrayList<String>();
        locationIdArray = new ArrayList<String>();

        userId = MyApplication.getInstance().getPrefManager().getUser().getId();
        mOfficeAdapter = new TableMOfficeAdapter(getActivity());
        List<TableMOffice> listOffice = mOfficeAdapter.getAllData();
        for (int i=0; i<listOffice.size(); i++) {
            TableMOffice item = listOffice.get(i);
            locationList.add(item.getOffice());
            locationIdArray.add(item.getKode_office());
        }

        spinnerLocation = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, locationList);
        System.out.print(list.size());
        for (int i=0; i<list.size();i++) {
            addStockList(list, i);
        }

        return root;
    }

    private View.OnClickListener locationListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Lokasi")
                    .setAdapter(spinnerLocation,
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    location.setText(locationList.get(which).toString());
                                    locationId = locationIdArray.get(which);
                                    dialog.dismiss();
                                }
                            }).create().show();
        }
    };

    private View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            AlertConfrim();
        }
    };



    private void prepareData() {
        TableProdukAdapter productAdapter = new TableProdukAdapter(getActivity());
        list = productAdapter.getAllData();
        System.out.print(list);
    }

    private void addStockList(List<TableProduk> list, final int position) {
        try {
            if (list!=null) {
                final TableProduk item = list.get(position);

                final LinearLayout Mainlinear = new LinearLayout(getActivity());
                Mainlinear.setBackgroundColor(getResources().getColor(R.color.white));
                Mainlinear.setOrientation(LinearLayout.VERTICAL);

                LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                pLinearPackage.setMargins(0, 0, 0, 0);
                Mainlinear.setLayoutParams(pLinearPackage);
                ((LinearLayout) mLayoutStock).addView(Mainlinear);

                //Liner Title
                LinearLayout.LayoutParams pLineTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                LinearLayout linearTitle = new LinearLayout(getActivity());
                linearTitle.setLayoutParams(pLineTitle);
                linearTitle.setOrientation(LinearLayout.HORIZONTAL);
                linearTitle.setPadding(10, 0, 10, 0);
                ((LinearLayout) Mainlinear).addView(linearTitle);

                LinearLayout.LayoutParams pTxtTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                pTxtTitle.setMargins(0,25, 0,0);
                final TextView txtTitle = new TextView(getActivity());
                txtTitle.setLayoutParams(pTxtTitle);
                txtTitle.setText(item.getModel_name());
                txtTitle.setTypeface(null, Typeface.BOLD);
                //txtTitle.setTag(picturePath);
                txtTitle.setTextSize(16);
                txtTitle.setFreezesText(true);
                txtTitle.setPadding(5, 25, 0, 0);
                ((LinearLayout) linearTitle).addView(txtTitle);

                //Liner Title
                LinearLayout.LayoutParams pLineTitleProduct = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                LinearLayout linearTitleProduct = new LinearLayout(getActivity());
                linearTitleProduct.setLayoutParams(pLineTitleProduct);
                linearTitleProduct.setOrientation(LinearLayout.HORIZONTAL);
                linearTitleProduct.setPadding(10, 0, 10, 0);
                ((LinearLayout) Mainlinear).addView(linearTitleProduct);

                LinearLayout.LayoutParams pTxtTitleProduct = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                final TextView txtTitleProduct = new TextView(getActivity());
                txtTitleProduct.setLayoutParams(pTxtTitleProduct);
                txtTitleProduct.setText(item.getProduct_category());
                //txtTitle.setTag(picturePath);
                txtTitleProduct.setVisibility(View.GONE);
                txtTitleProduct.setTextSize(14);
                txtTitleProduct.setFreezesText(true);
                txtTitleProduct.setPadding(5, 5, 0, 0);
                ((LinearLayout) linearTitleProduct).addView(txtTitleProduct);

                //Liner Title
                LinearLayout.LayoutParams pLineProduct = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                pLineProduct.setMargins(0,5,0,0);
                LinearLayout linearProduct = new LinearLayout(getActivity());
                linearProduct.setLayoutParams(pLineProduct);
                linearProduct.setOrientation(LinearLayout.HORIZONTAL);
                linearProduct.setPadding(10, 0, 10, 0);
                txtTitleProduct.setVisibility(View.GONE);
                ((LinearLayout) Mainlinear).addView(linearProduct);




                LinearLayout.LayoutParams pTxtUOM = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
                final TextView txtUOM = new TextView(getActivity());
                txtUOM.setLayoutParams(pTxtUOM);
             //   txtUOM.setText(item.getEntity());
                txtUOM.setText("Stok :");
                //txtTitle.setTag(picturePath);
                txtUOM.setTextSize(14);
                txtUOM.setFreezesText(true);
                txtUOM.setPadding(5, 25, 0, 0);
                ((LinearLayout) linearProduct).addView(txtUOM);

                LinearLayout.LayoutParams pEdtUOM = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
                if (position==list.size()-1) {
                    pEdtUOM.setMargins(0,0,0,500);
                }

                final EditText edtUOM = new EditText(getActivity());
                edtUOM.setLayoutParams(pEdtUOM);
                edtUOM.setInputType(InputType.TYPE_CLASS_NUMBER);
                edtUOM.setLines(1);
                edtUOM.setHint("0");

                //edtUOM.setText("Counterpain");
                //txtTitle.setTag(picturePath);
                //edtUOM.setTextSize(14);

                edtUOM.setGravity(Gravity.CENTER);
                edtUOM.setFreezesText(true);
                edtUOM.setPadding(15, 15, 15, 20);
              //  edtUOM.setPa
                edtUOM.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.toString()==null || s.toString().length()<1) {
                            item.setQty(0);
                           // Toast.makeText(getActivity(), "posisi="+position+"  qty="+item.getQty(), Toast.LENGTH_SHORT).show();
                        } else {
                            item.setQty(Integer.parseInt(s.toString()));
                           // Toast.makeText(getActivity(), "posisi="+position+"  qty="+item.getQty(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                ((LinearLayout) linearProduct).addView(edtUOM);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendData(final TableTansOfftake item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.STOCK_ENTRY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableTansOfftakeAdapter adapter = new TableTansOfftakeAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "unixId", obj.getString("unixId"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("userid", item.getUserid());
                    params.put("waktu", item.getTime());
                    params.put("office", item.getOffice());
                    params.put("product_category", item.getProduct_category());
                    params.put("model_name", item.getModel_name());
                    params.put("gram", String.valueOf(item.getGram()));
                    params.put("qty", String.valueOf(item.getQty()));
                    params.put("flag", String.valueOf(item.getFlag()));
                    params.put("ket_status", item.getKet_status());
                    params.put("userid_approval", item.getUserid_approval());
                    params.put("ip_address", item.getIp_address());
                    params.put("mc_address", item.getMc_address());
                    params.put("clockin", item.getClockin());
                    params.put("unixId", item.getUnixId());

                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private String generateId() {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append("IS_");
        sb.append(MyApplication.getInstance().getPrefManager().getUser().getId());
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));

        sb.toString();
        return sb.toString();
    }
    private String generateInsertionId(int pos) {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append("Insrt_");
        sb.append(MyApplication.getInstance().getPrefManager().getUser().getId());
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));
        sb.append(String.valueOf(pos));

        sb.toString();
        return sb.toString();
    }
    public void SaveData(){
        if (location.getText().toString()==null || location.getText().toString().equalsIgnoreCase("")
                || clockin==null || clockin.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Silahkan lakukan clockin terlebih dahulu!", Toast.LENGTH_SHORT).show();
        } else {
            //List<TableProduk> stockList = adapter.retrieveData();
            transOffAdapter = new TableTansOfftakeAdapter(getActivity());
            insertionAdapter = new TableInsertionAdapter(getActivity());
            insertionAdapter.deleteAll();
            for (int i=0; i<list.size(); i++) {
                try {
                    TableProduk stock = list.get(i);
                    String nama = stock.getModel_name();
                    String id_produk = stock.getId();
                    //int gram = stock.getGram();
                    //String image = stock.getUrl();
                    int qty = stock.getQty();
                    String product_category = stock.getProduct_category();
                    //String clockin = utils.getClockIn(getActivity());
                    if (qty > 0) {
                        transOffAdapter.insertData(new TableTansOfftake(), userId, utils.getCurrentDateandTime(),
                                locationId, product_category, nama, "0", userId, utils.getMobileIP(),
                                utils.getMobileIP(), 0, qty, 0, clockin, generateId());
                        insertionAdapter.insertData(new TableInsertion(), generateInsertionId(i), locationId, product_category,
                                id_produk, nama, qty, 0, 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Toast.makeText(getActivity(), "Data berhasil disimpan", Toast.LENGTH_SHORT).show();

            List<TableTansOfftake> list = transOffAdapter.getDatabyCondition("flag", 0);
            for (int i=0; i<list.size(); i++) {
                TableTansOfftake item = list.get(i);
                if (item!=null) {
                    sendData(item, getActivity());
                }
            }
            //initView(root);
            Fragment stock = new MonthlyOfftake();
            ((MainMenu)getActivity()).initView(stock, "Input Stock");

        }
    }
    private void AlertConfrim(){
        //Context context;
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
        //String url_dlfile = varurl;
        // set title
        alertDialogBuilder.setTitle("Konfirmasi");

        // set dialog message
        alertDialogBuilder
                .setMessage("Apakah Anda Yakin ?")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SaveData();
                        // if this button is clicked, close
                        // current activity
//                        if (isi_menu.equals("savesellout")) {
//                            saveTransSellOutOffline();
//                        } else if (isi_menu.equals("saveofftake")) {
//                            saveTransOffTakeOffline();
//                        }
//                        else if (isi_menu.equals("saveofftake_m")) {
//                            saveTransOffTake_MOffline();
//                        }
//                        else if (isi_menu.equals("saveofftake_b")) {
//                            saveTransOffTake_BOffline();
//                        }
                        //id=1;
                        //   return id;
                        //MainActivity.this.finish();
                        //MainActivity.this.finish();
                    }
                }) .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, close
                // current activity

                //MainActivity.this.finish();
                //MainActivity.this.finish();
            }
        });



        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }
}
