package com.ish.SaphireTransmart.main;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.adapter.MainMenuListviewAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.main.menu.AttandanceMobile;
import com.ish.SaphireTransmart.main.menu.AttendanceStay;
import com.ish.SaphireTransmart.main.menu.FAQ;
import com.ish.SaphireTransmart.main.menu.Home;
import com.ish.SaphireTransmart.main.menu.UserGuide;
import com.ish.SaphireTransmart.service.TrackLogService;
import com.ish.SaphireTransmart.utils.GlobalVarClass;
import com.ish.SaphireTransmart.utils.Utility;
import com.ish.SaphireTransmart.utils.listitem_object.MainMenuListItem;

import java.io.File;
import java.util.ArrayList;
/**
 * Created by Hari H on 7/28/2016.
 */
public class MainMenuActivity extends AppCompatActivity {

    private Toolbar toolbar;
    protected DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private LinearLayout leftLayout;
    private ListView mainMenuListview;
    private ArrayList<MainMenuListItem> menuItemsList;
    private MainMenuListviewAdapter mainMenuListviewAdapter;
    private TextView username, usercode;
    private ImageView userProfile;
    private boolean isMainActivity = true;
    private String userJnsabsen;
    private String userLevel;
    private Utility utils;
    private Activity activity;
    private static final int PERMISSION_REQUEST_CODE = 1;

    private static final String[] INITIAL_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_CONTACTS
    };
    private static final String[] CAMERA_PERMS={
            Manifest.permission.CAMERA
    };
    private static final String[] CONTACTS_PERMS={
            Manifest.permission.READ_CONTACTS
    };
    private static final String[] LOCATION_PERMS={
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final String[] INTERNAL_PERMS={
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private static final int INITIAL_REQUEST=1337;
    private static final int CAMERA_REQUEST=INITIAL_REQUEST+1;
    private static final int CONTACTS_REQUEST=INITIAL_REQUEST+2;
    private static final int LOCATION_REQUEST=INITIAL_REQUEST+3;
    static final Integer WRITE_EXST = 0x3;
    static final Integer READ_EXST = 0x4;
    private static final int REQUEST_EXTERNAL_STORAGE = INITIAL_REQUEST+4;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public String PicturePathMn,LocationIdMn;

    private static final int TAKE_PICTURE_RENT_ARC = 121;
    private static final int TAKE_PICTURE_RENT_ARC2 = 122;
    private String picturePath = "", picturePath2 = "";
    private TextView kategori, tipe,TxtPicPath1,TxtPicPath2;
    private ImageView ibRentArc, ibRentArc2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();

        }
        final GlobalVarClass globalVariable = new GlobalVarClass();
        launchMainMenuActivity();
        //Set name and email in global/application context
//        globalVariable.setName(listItemText);
//        globalVariable.setID(listItemId);
        if (MyApplication.getInstance().getPrefManager().getUser() == null) {
            launchLoginActivity();
        }else {
         //   DeleteService   Delservice = new DeleteService(this);
        //    DeleteService
//             userJnsabsen = MyApplication.getInstance().getPrefManager().getUser().getJenis_absen();
//             userLevel = MyApplication.getInstance().getPrefManager().getUser().getLevel();

            // if (!canAccessCamera() ) {
              //  requestPermissions(INTERNAL_PERMS, PERMISSION_REQUEST_CODE);
          //  }
          //  requestPermissions(INTERNAL_PERMS, PERMISSION_REQUEST_CODE);

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (!Settings.System.canWrite(this)) {
//                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                            Manifest.permission.READ_EXTERNAL_STORAGE}, 2909);
//                } else {
//                    // continue with your code
//                }
//                if (!canAccessLocation() || !canAccessContacts()) {
//                    requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
//                }
//                if (!canWriteExternal()) {
//                    requestPermissions(PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
//                }
//            } else {
//                // continue with your code
//            }
            //  ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);

            utils = new Utility(this);
            utils.setGeoLocation();
            setContentView(R.layout.activity_main_menu);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            leftLayout = (LinearLayout) findViewById(R.id.left_layout);
            mainMenuListview = (ListView) findViewById(R.id.main_menu_listView1);
            setSupportActionBar(toolbar);
            drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawerLayout.setDrawerListener(actionBarDrawerToggle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            actionBarDrawerToggle.syncState();
            prepareMainMenuListItem();
          //  stopService(new Intent(MainMenuActivity.this, TrackLogService.class));

            ViewGroup mainMenuListviewHeader = (ViewGroup) getLayoutInflater().inflate(R.layout.header, mainMenuListview, false);
            username = (TextView) mainMenuListviewHeader.findViewById(R.id.user_name);
            usercode = (TextView) mainMenuListviewHeader.findViewById(R.id.user_code);
            userProfile = (ImageView) mainMenuListviewHeader.findViewById(R.id.user_profile);
            setHeaderData();

            mainMenuListview.addHeaderView(mainMenuListviewHeader, null, false);
            mainMenuListviewAdapter = new MainMenuListviewAdapter(MainMenuActivity.this, menuItemsList);
            mainMenuListview.setAdapter(mainMenuListviewAdapter);
            mainMenuListview.setOnItemClickListener(menuListListener);

            if (savedInstanceState == null) {
                isMainActivity = true;
                initView(new Home(), getResources().getString(R.string.app_name));
            }
//            FragmentManager fragmentManager = getFragmentManager();
//            fragmentManager.beginTransaction()
//                    .replace(R.id.content_framemain, new FragPresensiMobile3())
//                    .commit();
        }

    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//
//        switch (requestCode) {
////            case PERMISSION_REQUEST_CODE:
//////
////                askForPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,WRITE_EXST);
////                break;
//
////            case CAMERA_REQUEST:
////                if (canAccessCamera()) {
////                    doCameraThing();
////                }
////                else {
////                  //  bzzzt();
////                    Log.d("onRequestPermissions", "failed ");
////
////                }
////                break;
////
////            case CONTACTS_REQUEST:
////                if (canAccessContacts()) {
////                    doContactsThing();
////                }
////                else {
////                    bzzzt();
////                }
////                break;
////
////            case LOCATION_REQUEST:
////                if (canAccessLocation()) {
////                    doLocationThing();
////                }
////                else {
////                    bzzzt();
////                }
////                break;
////             case REQUEST_EXTERNAL_STORAGE:
////                if (canWriteExternal()) {
////                  // Toast.makeText(this, "Bisa", Toast.LENGTH_SHORT).show();
////                } else {
////                  //  Toast.makeText(this, "Hari Ganteng", Toast.LENGTH_SHORT).show();
////                    startInstalledAppDetailsActivity(this);
////                }
//        }
//
//
//
//        // other 'case' lines to check for other
//        // permissions this app might request
//    }
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        //Log.i(TAG, "onResume: " + this);
//        createLocationRequest();
//        // super.onResume();
//        // locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, this);
//        if (locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) != null) {
//            locationuser = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//            Log.d("NETWORK_PROVIDER", locationuser.toString());
//        } else if (locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
//            //locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) == null ){
//            locationuser = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//            Log.d("GPS_PROVIDER", locationuser.toString());
//
//        }
        //<7>

    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        // stopLocationUpdates();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        //  super.onSaveInstanceState(outState);

        super.onSaveInstanceState(outState);
     //   Log.i(TAG, "onSaveInstanceState");
    }
    @Override
    protected void onStart() {
        super.onStart();
//        createLocationRequest();
//        googleApiClient.connect();


    }

    @Override
    protected void onStop() {
        super.onStop();
      // googleApiClient.disconnect();
    }

    private void prepareMainMenuListItem() {
        menuItemsList = new ArrayList<MainMenuListItem>();
        menuItemsList.add(new MainMenuListItem(-1, "Home", R.drawable.home));
        menuItemsList.add(new MainMenuListItem(13, "To Do List", R.drawable.todolist));


        //menuItemsList.add(new MainMenuListItem(1, "Catalog", R.drawable.ic_catalog));
        //  menuItemsList.add(new MainMenuListItem(2, "Chat and Broadcast", R.drawable.ic_chat));
//        if (Integer.parseInt(userLevel) <= 4){
//            menuItemsList.add(new MainMenuListItem(3, "Presensi Stay", R.drawable.stay));
//          menuItemsList.add(new MainMenuListItem(4, "Presensi Mobile", R.drawable.mobile));
//         }else  if (Integer.parseInt(userLevel) >= 5){
//            if (userJnsabsen.equals("onsite")) {
//                menuItemsList.add(new MainMenuListItem(3, "Presensi Stay", R.drawable.stay));
//            }else   if (userJnsabsen.equals("mobile")){
//                menuItemsList.add(new MainMenuListItem(4, "Presensi Mobile", R.drawable.mobile));
//            }
//
//        }

        menuItemsList.add(new MainMenuListItem(3, "Presensi Stay", R.drawable.stay));
        menuItemsList.add(new MainMenuListItem(4, "Presensi Mobile", R.drawable.mobile));
       // menuItemsList.add(new MainMenuListItem(5, "Signature", R.drawable.ic_sig));

        menuItemsList.add(new MainMenuListItem(7, "Input Stock", R.drawable.ic_stockcolor));
        menuItemsList.add(new MainMenuListItem(6, "Visibility", R.drawable.visibility));
        menuItemsList.add(new MainMenuListItem(8, "Detailing", R.drawable.detailing));
        menuItemsList.add(new MainMenuListItem(9, "Gimmick", R.drawable.gimmick));
        menuItemsList.add(new MainMenuListItem(10, "Inserting", R.drawable.insert));
        menuItemsList.add(new MainMenuListItem(11, "Other Information", R.drawable.place));
        menuItemsList.add(new MainMenuListItem(12, "Data Outlet", R.drawable.outletdata));

        menuItemsList.add(new MainMenuListItem(14, "Overview", R.drawable.overview));
//        menuItemsList.add(new MainMenuListItem(15, "FAQ", R.drawable.faq));
//        menuItemsList.add(new MainMenuListItem(16, "User Guide", R.drawable.userguide));
//        menuItemsList.add(new MainMenuListItem(10, "Inserting", R.drawable.ic_pin));
        menuItemsList.add(new MainMenuListItem(0, "", R.drawable.ic_lock));
        menuItemsList.add(new MainMenuListItem(-2, "Logout", R.drawable.power));
    }

    private AdapterView.OnItemClickListener menuListListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            FragmentManager fragmentManager = getFragmentManager();
            Bundle bundle = new Bundle();
        //    Bundle bundle = new Bundle();
            MainMenuListItem item = (MainMenuListItem) mainMenuListviewAdapter.getItem(position - 1);
            switch (item.getId()) {
                case -1:
                    isMainActivity = false;
                    Fragment home = new Home();
                    initView(home, "Home");
                    break;
                case -2:
                    set_logout();
                    drawerLayout.closeDrawer(leftLayout);
                    break;

                case 3:
                    isMainActivity = false;
                    Fragment stay = new AttendanceStay();
                    initView(stay, "Presensi Stay");
                    break;
                case 4:
                  //  isMainActivity = false;
                    Fragment mobile = new AttandanceMobile();
                    initView(mobile, "Presensi Mobile");
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.content_framemain, new FragPresensiMobile3())
//                            .commit();
                    break;


                case 15:
                    isMainActivity = false;
                    Fragment faq = new FAQ();
                    initView(faq, "FAQ");
                    break;
                case 16:
                    isMainActivity = false;
                    Fragment userguide = new UserGuide();
                    initView(userguide, "UserGuide");
                    break;

            }
        }
    };

    /**
     * Method for set fragment view
     */
    public void initView(Fragment fragment, String title) {
     //   FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentManager fragmentManager = getFragmentManager();
        Bundle bundle = new Bundle();
    //    Bundle bundle = new Bundle();
        fragment.setArguments(bundle);

        //fragmentManager.popBackStack();
       // fragmentManager.beginTransaction().replace(R.id.content_framemain, fragment).commit();
        setTitle(title);
        drawerLayout.closeDrawer(leftLayout);
    }

    private  void setHeaderData() {
        try {
            username.setText(MyApplication.getInstance().getPrefManager().getUser().getName());
            usercode.setText(MyApplication.getInstance().getPrefManager().getUser().getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method confirm for logout
     */
    private void set_logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainMenuActivity.this);
        builder.setMessage(getResources().getString(R.string.ask_logout_app));
        builder.setTitle(getResources().getString(R.string.text_logout));
        builder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //new LogoutTask().execute();
                        stopService(new Intent(MainMenuActivity.this, TrackLogService.class));
                        MyApplication.getInstance().logout();
                        utils.clearClockin();
                        finish();


                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
        alert.show();

    }

    public void backButtonHandler() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainMenuActivity.this);
        builder.setMessage(getResources().getString(R.string.ask_exit_app));
        builder.setTitle(getResources().getString(R.string.exit_app));
        builder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MainMenuActivity.this.finish();
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
        alert.show();
    }

    @Override
    public void onBackPressed() {
        if (isMainActivity) {
            backButtonHandler();
        } else {
            initView(new Home(), getResources().getString(R.string.app_name));
            isMainActivity = true;
        }
    }

    private void launchLoginActivity() {
        Intent intent = new Intent(MainMenuActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
    private void launchMainMenuActivity() {
        Intent intent = new Intent(MainMenuActivity.this, MainMenu.class);
       // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivityResult", "onActivityResult " + PicturePathMn);

//        Uri selectedImage = data.getData();
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
         //    fragment = getSupportFragmentManager().findFragmentById(android.R.id.content);

            Log.d("onActivityResult", "onActivityResult "+requestCode+" - "+resultCode);
            fragment.onActivityResult(requestCode, resultCode, data);
          //  fragment.getChildFragmentManager();

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

//    private boolean canAccessLocation() {
//        return(hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
//    }
//
//    private boolean canAccessCamera() {
//        return(hasPermission(Manifest.permission.CAMERA));
//    }
//
//    private boolean canAccessContacts() {
//        return(hasPermission(Manifest.permission.READ_CONTACTS));
//    }
//    private boolean canAccessStorage() {
//        return(hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
//    }
//    private boolean canWriteExternal() {
//        return(hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE));
//    }
//    private boolean hasPermission(String perm) {
//      //  return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
//    }

    private void bzzzt() {
      //  Toast.makeText(this, R.string.toast_bzzzt, Toast.LENGTH_LONG).show();
    }

    private void doCameraThing() {
     //   Toast.makeText(this, R.string.toast_camera, Toast.LENGTH_SHORT).show();
    }

    private void doContactsThing() {
      //  Toast.makeText(this, R.string.toast_contacts, Toast.LENGTH_SHORT).show();
    }

    private void doLocationThing() {
      //  Toast.makeText(this, R.string.toast_location, Toast.LENGTH_SHORT).show();
    }
    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }
    private void galleryAddPic(String path) {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

}
