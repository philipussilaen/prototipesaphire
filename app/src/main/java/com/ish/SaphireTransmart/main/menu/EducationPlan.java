package com.ish.SaphireTransmart.main.menu;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.adapter.JudulListAdapter;
import com.ish.SaphireTransmart.adapter.OfficeListAdapter;
import com.ish.SaphireTransmart.adapter.PembicaraListAdapter;
import com.ish.SaphireTransmart.database.TableJudul;
import com.ish.SaphireTransmart.database.TableMOffice;
import com.ish.SaphireTransmart.database.TablePembicara;
import com.ish.SaphireTransmart.database.TableTransEducationPlan;
import com.ish.SaphireTransmart.database.database_adapter.TableJudulAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableMOfficeAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TablePembicaraAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableTransEducationPlanAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.main.MainMenuActivity;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;
import com.ish.SaphireTransmart.utils.listitem_object.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

/**
 * Created by adminmc on 27/09/16.
 */
public class EducationPlan extends Fragment {
    private String TAG = EducationPlan.class.getSimpleName();
    private ViewGroup root;
    private List<Stock> list = new ArrayList<Stock>();
//    private InsertingAdapter adapter;
    private RecyclerView recyclerView;
   // private TextView location;
    private ArrayAdapter<String> spinnerLocation;
    private ArrayAdapter<String> spinnerJudul;
    private ArrayAdapter<String> spinnerPembicara;
    //private ArrayList<String> locationList, locationIdArray;
    private android.app.DatePickerDialog DatePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TableMOfficeAdapter mOfficeAdapter;
    private Button btnSave;
    private Utility utils;
    private TableTransEducationPlanAdapter transEducationPlanAdapter;
    private TextView cdate, location, timein, locationIn, timeOut, locationOut,EdJudul, EdPembicara;
    private EditText EdTglEdu, EdJmlpeserta,EdResult,EdNotes;
    private TimePicker TpWaktu1, TpWaktu2;
    private List<TableMOffice> listOffice;
    private List<TableJudul> listJudul;
    private List<TablePembicara> listPembicara;
        private ArrayList<String> locationList, officeIdList,judulList,judulIdList,pembicaraList,pembicaraIdList;
    private Double lng=0.0, lat=0.0;
    private TableMOffice item;
    private TableJudul itemJudul;
    private TablePembicara itemPembicara;
    public EducationPlan() {
        utils = new Utility(getActivity());
    }
    private String locationId,judulId,pembicaraId, userId,locationName,datetimenow,datetimeedu,vartgledu,varresult;
    private String varjudul, varpembicara,varjmpeserta,varwaktu1,varwaktu2,varnotes;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_educationplan, container, false);
       // prepareData();
        utils = new Utility(getActivity());
        utils.setIMEI();
        utils.setGeoLocation();
        userId = MyApplication.getInstance().getPrefManager().getUser().getId();
        initView(root);
        return root;

    }
    private void initView(ViewGroup v) {
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        EdTglEdu = (EditText) v.findViewById(R.id.input_tgleduplan);
        location = (TextView) v.findViewById(R.id.txt_vlokasieduplan);

        location.setOnClickListener(locationListener);

//        EdResult = (EditText) v.findViewById(R.id.input_resultedu);

        btnSave = (Button) v.findViewById(R.id.btn_saveeduplan);
        btnSave.setOnClickListener(saveListener);

        EdJudul = (TextView) v.findViewById(R.id.input_juduleduplan);
        EdJudul.setOnClickListener(judulListener);
        EdPembicara = (TextView) v.findViewById(R.id.input_pembicaraeduplan);
        EdPembicara.setOnClickListener(pembicaraListener);
        EdJmlpeserta = (EditText) v.findViewById(R.id.input_jmlpesertaleduplan);
        EdNotes= (EditText) v.findViewById(R.id.input_detaileduplan);

        TpWaktu1 = (TimePicker) v.findViewById(R.id.input_waktu1eduplan);
        TpWaktu2 = (TimePicker) v.findViewById(R.id.input_waktu2eduplan);


        EdTglEdu.setOnClickListener(tgleduplanlistener);
        setDateTimeField();
        locationList = new ArrayList<String>();
        officeIdList = new ArrayList<String>();
        judulList = new ArrayList<String>();
        judulIdList = new ArrayList<String>();
        pembicaraList = new ArrayList<String>();
        pembicaraIdList = new ArrayList<String>();
        location.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_search, 0, 0, 0);
//
       //   mAttAdapter = new TableAttendanceAdapter(getActivity());
        TableMOfficeAdapter mOfficeAdapter = new TableMOfficeAdapter(getActivity());
        String region = MyApplication.getInstance().getPrefManager().getUser().getRegion();
        try {

            listOffice = mOfficeAdapter.getDatabyCondition("pic", userId);

            for (int i=0; i<listOffice.size(); i++) {
                item = listOffice.get(i);
                locationList.add(item.getOffice()+" => "+item.getAlamat()+" => "+item.getKode_office());
                officeIdList.add(item.getKode_office());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            listOffice = mOfficeAdapter.getDatabyCondition("region", 0);

            for (int i=0; i<listOffice.size(); i++) {
                item = listOffice.get(i);
                locationList.add(item.getOffice()+" => "+item.getAlamat()+" => "+item.getKode_office());
                officeIdList.add(item.getKode_office());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        TableJudulAdapter mJudulAdapter = new TableJudulAdapter(getActivity());
//        String region = MyApplication.getInstance().getPrefManager().getUser().getRegion();
        try {

            listJudul = mJudulAdapter.getAllData();

            for (int i=0; i<listJudul.size(); i++) {
                itemJudul = listJudul.get(i);
                judulList.add(itemJudul.getJudul());
                judulIdList.add(itemJudul.getJudul());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        TablePembicaraAdapter mPembicaraAdapter = new TablePembicaraAdapter(getActivity());
//        String region = MyApplication.getInstance().getPrefManager().getUser().getRegion();
        try {

            listPembicara = mPembicaraAdapter.getAllData();

            for (int i=0; i<listJudul.size(); i++) {
                itemPembicara = listPembicara.get(i);
                pembicaraList.add(itemPembicara.getPembicara());
                pembicaraIdList.add(itemPembicara.getPembicara());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        spinnerLocation = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, locationList);
        spinnerJudul = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, judulList);
        spinnerPembicara = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, pembicaraList);

    }

    private View.OnClickListener judulListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            LayoutInflater li = LayoutInflater.from(getActivity());
            View view = li.inflate(R.layout.m_office_listview, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    getActivity());
            alertDialogBuilder.setView(view);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            ListView judulListView = (ListView) view.findViewById(R.id.office_list);
            EditText txtOffice = (EditText) view.findViewById(R.id.search_office);

            final JudulListAdapter adapter = new JudulListAdapter(getActivity(), judulList);
            judulListView.setAdapter(adapter);

            txtOffice.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    System.out.println("Text ["+s+"]");

                    adapter.getFilter().filter(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


            judulListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        EdJudul.setText(adapter.getFilterData().get(position));
                        TableJudulAdapter mJudulAdapter = new TableJudulAdapter(getActivity());
                        String[] separated = adapter.getFilterData().get(position).split(" => ");
                        List<TableJudul> listdata = mJudulAdapter.getAllData();
//                        Log.d("locationid1", separated[2].toString());
                        // int a = listdata.size();
                        if (listdata!=null) {

                            TableJudul item = listdata.get(listdata.size()-1);
                            judulId = item.getId();
//                            locationName=item.getOffice();
//                            Log.d("locationid2", locationId.toString());
//                            if (item.getMap_lat()!=null && !item.getMap_lat().equalsIgnoreCase("null")) {
//
//                                lat = Double.parseDouble(item.getMap_lat());
//                            }
//
//                            if (item.getMap_lng()!=null && !item.getMap_lng().equalsIgnoreCase("null")) {
//                                lng = Double.parseDouble(item.getMap_lng());
//                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    alertDialog.dismiss();
                }
            });

            alertDialog.show();


            /*new AlertDialog.Builder(getActivity())
                    .setTitle("Lokasi")
                    .setAdapter(spinnerLocation,
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    location.setText(locationList.get(which).toString());
                                    officeId = officeIdList.get(which).toString();
                                    item = listOffice.get(which);
                                    if (item!=null) {
                                        if (item.getMap_lat()!=null && !item.getMap_lat().equalsIgnoreCase("null")) {

                                            lat = Double.parseDouble(item.getMap_lat());
                                        }

                                        if (item.getMap_lng()!=null && !item.getMap_lng().equalsIgnoreCase("null")) {
                                            lng = Double.parseDouble(item.getMap_lng());
                                        }

                                    }
                                    dialog.dismiss();
                                }
                            }).create().show();*/
        }
    };

    private View.OnClickListener pembicaraListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            LayoutInflater li = LayoutInflater.from(getActivity());
            View view = li.inflate(R.layout.m_office_listview, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    getActivity());
            alertDialogBuilder.setView(view);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            ListView pembicaraListView = (ListView) view.findViewById(R.id.office_list);
            EditText txtOffice = (EditText) view.findViewById(R.id.search_office);

            final PembicaraListAdapter pembicaraadapter = new PembicaraListAdapter(getActivity(), pembicaraList);
            pembicaraListView.setAdapter(pembicaraadapter);

            txtOffice.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    System.out.println("Text ["+s+"]");

                    pembicaraadapter.getFilter().filter(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


            pembicaraListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        EdPembicara.setText(pembicaraadapter.getFilterData().get(position));
                        TablePembicaraAdapter mPembicaraAdapter = new TablePembicaraAdapter(getActivity());
                        String[] separated = pembicaraadapter.getFilterData().get(position).split(" => ");
                        List<TablePembicara> listdata = mPembicaraAdapter.getAllData();
//                        Log.d("locationid1", separated[2].toString());
                        // int a = listdata.size();
                        if (listdata!=null) {

                            TablePembicara itemPembicara = listdata.get(listdata.size()-1);
                            pembicaraId = itemPembicara.getId();
//                            locationName=item.getOffice();
//                            Log.d("locationid2", locationId.toString());
//                            if (item.getMap_lat()!=null && !item.getMap_lat().equalsIgnoreCase("null")) {
//
//                                lat = Double.parseDouble(item.getMap_lat());
//                            }
//
//                            if (item.getMap_lng()!=null && !item.getMap_lng().equalsIgnoreCase("null")) {
//                                lng = Double.parseDouble(item.getMap_lng());
//                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    alertDialog.dismiss();
                }
            });

            alertDialog.show();


            /*new AlertDialog.Builder(getActivity())
                    .setTitle("Lokasi")
                    .setAdapter(spinnerLocation,
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    location.setText(locationList.get(which).toString());
                                    officeId = officeIdList.get(which).toString();
                                    item = listOffice.get(which);
                                    if (item!=null) {
                                        if (item.getMap_lat()!=null && !item.getMap_lat().equalsIgnoreCase("null")) {

                                            lat = Double.parseDouble(item.getMap_lat());
                                        }

                                        if (item.getMap_lng()!=null && !item.getMap_lng().equalsIgnoreCase("null")) {
                                            lng = Double.parseDouble(item.getMap_lng());
                                        }

                                    }
                                    dialog.dismiss();
                                }
                            }).create().show();*/
        }
    };


    private View.OnClickListener locationListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            LayoutInflater li = LayoutInflater.from(getActivity());
            View view = li.inflate(R.layout.m_office_listview, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    getActivity());
            alertDialogBuilder.setView(view);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            ListView officeListView = (ListView) view.findViewById(R.id.office_list);
            EditText txtOffice = (EditText) view.findViewById(R.id.search_office);

            final OfficeListAdapter adapter = new OfficeListAdapter(getActivity(), locationList);
            officeListView.setAdapter(adapter);

            txtOffice.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    System.out.println("Text ["+s+"]");

                    adapter.getFilter().filter(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


            officeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        location.setText(adapter.getFilterData().get(position));
                        TableMOfficeAdapter mOfficeAdapter = new TableMOfficeAdapter(getActivity());
                        String[] separated = adapter.getFilterData().get(position).split(" => ");
                        List<TableMOffice> listdata = mOfficeAdapter.getDatabyCondition("kode_office", separated[2]);
                        Log.d("locationid1", separated[2].toString());
                        // int a = listdata.size();
                        if (listdata!=null) {

                            TableMOffice item = listdata.get(listdata.size()-1);
                            locationId = item.getKode_office();
                            locationName=item.getOffice();
                            Log.d("locationid2", locationId.toString());
                            if (item.getMap_lat()!=null && !item.getMap_lat().equalsIgnoreCase("null")) {

                                lat = Double.parseDouble(item.getMap_lat());
                            }

                            if (item.getMap_lng()!=null && !item.getMap_lng().equalsIgnoreCase("null")) {
                                lng = Double.parseDouble(item.getMap_lng());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    alertDialog.dismiss();
                }
            });

            alertDialog.show();


            /*new AlertDialog.Builder(getActivity())
                    .setTitle("Lokasi")
                    .setAdapter(spinnerLocation,
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    location.setText(locationList.get(which).toString());
                                    officeId = officeIdList.get(which).toString();
                                    item = listOffice.get(which);
                                    if (item!=null) {
                                        if (item.getMap_lat()!=null && !item.getMap_lat().equalsIgnoreCase("null")) {

                                            lat = Double.parseDouble(item.getMap_lat());
                                        }

                                        if (item.getMap_lng()!=null && !item.getMap_lng().equalsIgnoreCase("null")) {
                                            lng = Double.parseDouble(item.getMap_lng());
                                        }

                                    }
                                    dialog.dismiss();
                                }
                            }).create().show();*/
        }
    };
    private View.OnClickListener tgleduplanlistener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DatePickerDialog.show();

        }
    };
    private void setDateTimeField() {
         // EdTglEdu.setOnClickListener(this);
        EdTglEdu = (EditText) root.findViewById(R.id.input_tgleduplan);
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog = new DatePickerDialog(getActivity(), new android.app.DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                Calendar cal = Calendar.getInstance(TimeZone.getDefault());
                newDate.set(year, monthOfYear, dayOfMonth);
                // DatePickerDialog.getDatePicker().setMinDate(newDate.getTime()-(newDate.getTime()%(24*60*60*1000)));
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                datetimenow = sdf.format(cal.getTime()).toString();
                datetimeedu = sdf.format(newDate.getTime()).toString();

                try {

                    Date datepick = sdf.parse(datetimeedu);
                    Log.i(TAG, "response date " + datetimeedu);
                    Date datenow = sdf.parse(datetimenow);
                    Log.i(TAG, "response date " + datetimenow);

//                    if(datepick.equals(datenow)){
//                        System.out.println("Cannot Pick Date = today");
//                        Toast.makeText(getActivity(), "Cannot Pick Date = today ", Toast.LENGTH_LONG).show();
//
//                        EdTglEdu.setText("");
//                    }
////                    else if(datepick.before(datenow) || datepick.equals(datenow)){
//                    else{
                        long timeOne = datenow.getTime();
                        long timeTwo = datepick.getTime();
                        long oneDay = 1000 * 60 * 60 * 24;
                        long delta = (timeTwo - timeOne) / oneDay;
                        System.out.println(delta);
                        if (delta <=-1) {
                            //  return "dateTwo is " + delta + " days after dateOne";
                            Toast.makeText(getActivity(), " Cannot Pick Date  < today ! ", Toast.LENGTH_LONG).show();
                            System.out.println("Cannot Pick Date  < 1 day");
                            EdTglEdu.setText("");
                        }

                        else {
                            //delta *= -1;
                            EdTglEdu.setText(dateFormatter.format(newDate.getTime()));
                            //  return "dateTwo is " + delta + " days before dateOne";
                        }


                  // }


                }catch(ParseException ex){
                    ex.printStackTrace();
                }

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


    }

    private View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (location.getText().toString()==null || location.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Silahkan lakukan pilih Account terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }
            else if (EdPembicara.getText().toString()==null || EdPembicara.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Silahkan Lengkapi seluruh Form terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }
            else if (EdPembicara.getText().toString()==null || EdPembicara.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Silahkan Lengkapi seluruh Form terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }
            else if (EdJmlpeserta.getText().toString()==null || EdJmlpeserta.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Silahkan Lengkapi seluruh Form terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }
            else if (EdTglEdu.getText().toString()==null || EdTglEdu.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Silahkan Lengkapi seluruh Form terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }
            else if (locationId.equals("0") &&  (EdNotes.getText().toString()==null || EdNotes.getText().toString().equalsIgnoreCase(""))) {
                Toast.makeText(getActivity(), "Silahkan Lengkapi Detail Note terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }
            else {
                //List<TableProduk> stockList = adapter.retrieveData();
                transEducationPlanAdapter = new TableTransEducationPlanAdapter(getActivity());
                vartgledu =  datetimeedu;
//                varjudul = EdJudul.getText().toString();
//                varpembicara = EdPembicara.getText().toString();
                varjmpeserta = EdJmlpeserta.getText().toString();
                varnotes = EdNotes.getText().toString();
                varwaktu1 = TpWaktu1.getCurrentHour() + ":" + TpWaktu2.getCurrentMinute();
                varwaktu2 =  TpWaktu1.getCurrentHour() + ":" + TpWaktu2.getCurrentMinute();
//                varresult = EdResult.getText().toString();
//                varresult = EdResult.getText().toString();
                String id = generateId();

                try {

                    Log.d("save data ", userId+"-"+locationId+"-"+varjudul+"-"+vartgledu+"-"+utils.getCurrentDateandTimeSec()+"-"+generateId().toString()+"-"+varpembicara
                            +"-"+varjmpeserta+"-"+varwaktu1+"-"+varwaktu2);
                    transEducationPlanAdapter.insertData(new TableTransEducationPlan(), id,userId, locationId, utils.getCurrentDateandTimeSec(),vartgledu,varwaktu1,
                            varwaktu2,judulId,pembicaraId, Integer.parseInt(varjmpeserta),0,0, utils.getCurrentDateandTimeSec(),varnotes);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                Toast.makeText(getActivity(), "Data berhasil disimpan", Toast.LENGTH_SHORT).show();

                List<TableTransEducationPlan> list = transEducationPlanAdapter.getDatabyCondition("flag", 0);
                for (int i=0; i<list.size(); i++) {
                    TableTransEducationPlan item = list.get(i);
                    if (item!=null) {
                        sendData(item, getActivity());
                    }
                }
                Fragment education = new EducationPlan();
                ((MainMenuActivity)getActivity()).initView(education, "Education Plan");

            }
        }
    };
    private void sendData(final TableTransEducationPlan item,final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.EDUCATIONPLAN_ENTRY
//                "http://192.168.88.7/saphireapi.ish-solutions.com/saphire_3mdental/v1/index.php/educationplan/entry"
                , new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableTransEducationPlanAdapter adapter = new TableTransEducationPlanAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error2: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                if (item!=null) {
                    params.put("id", item.getId());
                    params.put("userid", item.getUserid());
                    params.put("kode_office", item.getKode_office());
                    params.put("tgl_input", item.getTgl_input());
                    params.put("tgl_edu", item.getTgl_edu());
                    params.put("waktu_awal", item.getWaktu_awal());
                    params.put("waktu_akhir", item.getWaktu_akhir());
                    params.put("judul", item.getJudul());
                    params.put("pembicara", item.getPembicara());
                    params.put("jml_peserta", String.valueOf(item.getJml_peserta()));
                    params.put("notes", item.getNotes());
                    params.put("flag", String.valueOf(item.getFlag()));
                    params.put("flag_approval", String.valueOf(item.getFlagApprove()));

                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private String generateId() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append("eduplan");
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.append(String.valueOf(a));
        sb.toString();

        return sb.toString();
    }
    private String generateIdIndex(int i) {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append(userId);
        sb.append("_");
        sb.append("sales");
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));
        sb.append(String.valueOf(i));

        sb.toString();
        return sb.toString();
    }
}
