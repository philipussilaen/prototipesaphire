package com.ish.SaphireTransmart.main.menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.adapter.OfficeListAdapter;
import com.ish.SaphireTransmart.database.TableAttendance;
import com.ish.SaphireTransmart.database.TableImage;
import com.ish.SaphireTransmart.database.TableMOffice;
import com.ish.SaphireTransmart.database.database_adapter.TableAttendanceAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableImageAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableMOfficeAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.main.MainMenu;
import com.ish.SaphireTransmart.utils.AlbumStorageDirFactory;
import com.ish.SaphireTransmart.utils.Base64;
import com.ish.SaphireTransmart.utils.BaseAlbumDirFactory;
import com.ish.SaphireTransmart.utils.CameraActivity;
import com.ish.SaphireTransmart.utils.ConnectionDetector;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;
import com.ish.SaphireTransmart.utils.listitem_object.AttendanceModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by hari on 04/09/16.
 */

public class AttandanceMobile extends android.support.v4.app.Fragment {
    private String TAG = AttandanceMobile.class.getSimpleName();
    private ViewGroup root;
    private Button clockin, clockout, btnmaps;
    private TextView cdate, location, timein, locationIn, timeOut, locationOut,TxtPicPath;
    private ImageView imgIn, imgOut;
    public File f;
    public String picturePath, picturePathOut, userId, locationId, locationName,imageFileName;

    public Uri imageUri;
    private Double lng=0.0, lat=0.0;
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private static final int TAKE_PICTURE_CLOCKIN = 1;
    private static final int TAKE_PICTURE_CLOCKOUT = 2;
    private ArrayList<String> locationList, officeIdList;
    private ArrayAdapter<String> spinnerLocation;
    private Utility utils;
    private CameraActivity CamActiv;
    private MainMenu MnM;
    private ConnectionDetector cd;
    private Boolean isInternetPresent = false;


    private boolean isTaskRunning = false;
    private ArrayList<String> keys, parameters;
    private TableMOffice item;
    private TableAttendanceAdapter mAttAdapter;
    private String oficeClockin="";
    private List<TableMOffice> listOffice;

    public AttandanceMobile() {
        super();

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //  CameraActivity activity = (CameraActivity)getActivity();
//        Uri selectedImage = data.getData();
//
//        Log.d("onActivityResult", "onActivityResult "+requestCode+" - "+resultCode+" - "+selectedImage.toString());
        Log.d(TAG, "onActivityResult " + requestCode);

        switch (requestCode) {
            case (TAKE_PICTURE_CLOCKIN):
                if (resultCode == Activity.RESULT_OK) {
                    {

                        if(locationId == null || locationId.equals("")) {
                            location = (TextView) root.findViewById(R.id.txtLocation);
                            String[] separated = location.getText().toString().split(" => ");

                            locationId =separated[2].toString();
                            Log.d("locationid1", separated[2].toString());

                            //   List<TableMOffice> listdata = mOfficeAdapter.getDatabyCondition("kode_office", separated[2]);
                        }
                        Log.d(TAG, "locationId "+locationId);

                        if(picturePath == null || picturePath.equals("")) {
                            TxtPicPath = (TextView) root.findViewById(R.id.txtPictpath);


//                            Uri selectedImage = data.getData();
////
                            Log.e("TxtPicPath", TxtPicPath.getText().toString());
                            picturePath = TxtPicPath.getText().toString();
                        }

                        if(picturePath == null || picturePath.equals("")) {
                            Toast.makeText(getActivity(), "Gagal menyimpan Photo,.. Silahkan Cek Memory dan Kondisi HP Anda...", Toast.LENGTH_SHORT).show();



                        }else{

                            Log.d(TAG, "picturePath "+picturePath);

                            final File file = new File(picturePath);
//                        BitmapFactory.Options options = new BitmapFactory.Options();
//                        options.inSampleSize = 8;
//                        Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);

                            String filePath = Utility.compressImage(picturePath,file.getName());
                            Bitmap bitmap = BitmapFactory.decodeFile(filePath);

                            if (bitmap!=null) {
                                imgIn.setImageBitmap(bitmap);
                            }

                            galleryAddPic(picturePath);
                            String time = utils.getCurrentTime();
                            String id_absen = generateId();
                            String date = utils.getCurrentDate();

                            TableAttendanceAdapter dbAttendance = new TableAttendanceAdapter(getActivity());
                            dbAttendance.insertData(new TableAttendance(), id_absen, userId, date,
                                    time, "in", utils.getLongitude(), utils.getLatitude(),
                                    locationId, "mobile", "0", file.getName(), picturePath, utils.getMobileIP(),
                                    utils.getMobileIP(), utils.getIMEI(), 0);

                            utils.setClockIn(getActivity(), new AttendanceModel(userId, date, time, "in", "mobile", locationId,locationId));
                            //String b = utils.getClockIn();

                            locationIn.setText(locationName);
                            clockin.setVisibility(View.GONE);
                            clockin.setEnabled(false);
                            clockout.setVisibility(View.VISIBLE);
                            clockout.setEnabled(true);
                            oficeClockin = locationId;
                            Toast.makeText(getActivity(), "Data berhasil tersimpan", Toast.LENGTH_SHORT).show();


                            List<TableAttendance> list = mAttAdapter.getDatabyCondition(TableAttendance.FLAG, 0);
                            for (int i=0; i<list.size(); i++) {
                                TableAttendance item = list.get(i);
                                if (item!=null) {
                                    sendData(item, getActivity());
                                    //   uploadImage2(item, getActivity());
                                }
                            }

//                            List<TableAttendance> list2 = mAttAdapter.getDatabyCondition(TableAttendance.PHOTOSTATUS, 0);
//                            for (int i=0; i<list2.size(); i++) {
//                                TableAttendance item = list2.get(i);
//                                if (item!=null) {
//                                    //sendData(item, getActivity());
//                                    //  uploadImage2(item, getActivity());
//                                }
//                            }

                            chunkImage(picturePath, id_absen);


                        }

                        //  Log.d(TAG, "imageFileName "+picturePath);
//
//                        //  uploadImage2(list,getActivity());
                    }
                }
                break;

            case (TAKE_PICTURE_CLOCKOUT):
                if (resultCode == Activity.RESULT_OK) {
                    {

                        if(locationId == null || locationId.equals("")) {
                            location = (TextView) root.findViewById(R.id.txtLocation);
                            String[] separated = location.getText().toString().split(" => ");

                            locationId =separated[2].toString();
                            Log.d("locationid1", separated[2].toString());

                            //   List<TableMOffice> listdata = mOfficeAdapter.getDatabyCondition("kode_office", separated[2]);
                        }
                        Log.d(TAG, "locationId "+locationId);
                        //  Log.d(TAG, "imageFileName "+selectedImage1);
                        //   Log.d(TAG, "imageFileName "+picturePath);
                        //  mCurrentPhotoPath = getPath(selectedImage);
                        if(picturePathOut == null || picturePathOut.equals("")) {
                            TxtPicPath = (TextView) root.findViewById(R.id.txtPictpath);


//                            Uri selectedImage = data.getData();
////
                            Log.e("TxtPicPath", TxtPicPath.getText().toString());
                            picturePathOut = TxtPicPath.getText().toString();
                        }

//                        if( data != null) {
//
//                        }

                        if(picturePathOut == null || picturePathOut.equals("")) {
                            Toast.makeText(getActivity(), "Gagal menyimpan Photo,.. Silahkan Cek Memory dan Kondisi HP Anda...", Toast.LENGTH_SHORT).show();



                        }else {
                            //  Log.d(TAG, "imageFileName "+picturePath);
                            Log.d(TAG, "picturePathOut "+picturePathOut);

                            //listPathIdentitas = new ArrayList<PathListItem>();
                            //setPic(bitmapIdentity, TAKE_PICTURE_INSTALASI);
                            final File file = new File(picturePathOut);
//                        BitmapFactory.Options options = new BitmapFactory.Options();
//                        options.inSampleKSize = 8;
//                        Bitmap bitmap = BitmapFactory.decodeFile(picturePathOut, options);
                            String filePath = Utility.compressImage(picturePathOut,file.getName());
                            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                            if (bitmap!=null) {
                                imgOut.setImageBitmap(bitmap);
                            }
                            galleryAddPic(picturePathOut);
                            String id_absen = generateId();
                            TableAttendanceAdapter dbAttendance = new TableAttendanceAdapter(getActivity());
                            dbAttendance.insertData(new TableAttendance(), id_absen, userId, utils.getCurrentDate(),
                                    utils.getCurrentTime(), "out",utils.getLongitude(), utils.getLatitude(),
                                    locationId, "mobile", "0", file.getName(), picturePathOut,
                                    utils.getMobileIP(), utils.getMobileIP(), utils.getIMEI(), 0);
                            locationOut.setText(locationName);
                            clockin.setVisibility(View.GONE);
                            clockin.setEnabled(false);
                            clockout.setVisibility(View.VISIBLE);
                            clockout.setEnabled(false);
                            utils.clearClockin();
                            Toast.makeText(getActivity(), "Data berhasil tersimpan", Toast.LENGTH_SHORT).show();


                            //chunkImage(picturePathOut);
                            List<TableAttendance> list = mAttAdapter.getDatabyCondition(TableAttendance.FLAG, 0);
                            for (int i=0; i<list.size(); i++) {
                                TableAttendance item = list.get(i);
                                if (item!=null) {
                                    sendData(item, getActivity());
                                }
                            }
//                            List<TableAttendance> list2 = mAttAdapter.getDatabyCondition(TableAttendance.PHOTOSTATUS, 0);
//                            for (int i=0; i<list2.size(); i++) {
//                                TableAttendance item = list2.get(i);
//                                if (item!=null) {
//                                    //sendData(item, getActivity());
//                                    // uploadImage2(item, getActivity());
//                                }
//                            }

                            chunkImage(picturePathOut, id_absen);

                            //  initView(root);

                            Fragment mobile = new AttandanceMobile();

                            ((MainMenu)getActivity()).initView(mobile, "Presensi Mobile");

                        }



                        //   MainMenu.initView(new Home(), getResources().getString(R.string.app_name));
                        /*TableImageAdapter tableImageAdapter = new TableImageAdapter(getActivity());
                        List<TableImage> lisImage = tableImageAdapter.getDatabyCondition("flag", 0);
                        for (int i=0; i<lisImage.size(); i++) {
                            TableImage item = lisImage.get(i);
                            if (item!=null) {
                                //sendData(item, getActivity());
                            }
                        }*/
//
                    }
                }
                break;
        }
    }

    //    public static AttandanceMobile newInstance(int sectionNumber) {
//        AttandanceMobile fragment = new AttandanceMobile();
//        Bundle args = new Bundle();
//        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
//        fragment.setArguments(args);
//        return fragment;
//    }
    //@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_att_mobile, container, false);
        mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        CamActiv = new CameraActivity(getActivity());
        utils = new Utility(getActivity());
        utils.setIMEI();
        utils.setGeoLocation();
        userId = MyApplication.getInstance().getPrefManager().getUser().getId();
        initView(root);
        return root;
    }

    private void initView(ViewGroup v) {
        clockin = (Button) v.findViewById(R.id.btn_in);
        clockout = (Button) v.findViewById(R.id.btn_out);
        btnmaps = (Button) v.findViewById(R.id.btn_map);
        cdate = (TextView) v.findViewById(R.id.txtDate);
        location = (TextView) v.findViewById(R.id.txtLocation);
        timein = (TextView) v.findViewById(R.id.time_in);
        timeOut = (TextView) v.findViewById(R.id.time_out);
        locationIn = (TextView) v.findViewById(R.id.location_in);
        locationOut = (TextView) v.findViewById(R.id.location_out);
        imgIn = (ImageView) v.findViewById(R.id.image_in);
        imgOut = (ImageView) v.findViewById(R.id.image_out);
        TxtPicPath = (TextView) v.findViewById(R.id.txtPictpath);
        location.setOnClickListener(locationListener);
        clockin.setOnClickListener(clockInListener);
        clockout.setOnClickListener(clockoutListener);
        btnmaps.setOnClickListener(mapsListener);
        location.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_search, 0, 0, 0);
        locationList = new ArrayList<String>();
        officeIdList = new ArrayList<String>();
        mAttAdapter = new TableAttendanceAdapter(getActivity());
        TableMOfficeAdapter mOfficeAdapter = new TableMOfficeAdapter(getActivity());
        String region = MyApplication.getInstance().getPrefManager().getUser().getRegion();
        Log.d(TAG, "region "+region);
        Log.d(TAG, "PicturePathMn "+  ((MainMenu)getActivity()).PicturePathMn);
        try {
            //    listOffice = mOfficeAdapter.getDatabyCondition("region", region);
            listOffice = mOfficeAdapter.getAllData();
            Log.d(TAG, "office1 " + listOffice.get(0).getKode_office());
            for (int i=0; i<listOffice.size(); i++) {
                item = listOffice.get(i);
                locationList.add(item.getOffice()+" => "+item.getAlamat()+" => "+item.getKode_office());
                officeIdList.add(item.getKode_office());
                Log.d(TAG, "office2 " + item.getOffice());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



        spinnerLocation = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, locationList);

        setCalendar();
        checkClockin();
        //   checkClockout();
    }

    private void checkClockin() {
        List<String> conditions = new ArrayList<>();
        List<Object> params = new ArrayList<>();
        List<Object> paramsOut = new ArrayList<>();
        conditions.add("userid");
        conditions.add("tanggal");
        conditions.add("status");
        conditions.add("jenis");

        params.add(userId);
        params.add(utils.getCurrentDate());
        //  params.add(utils.getCurrentDate());
        params.add("in");
        params.add("mobile");

        paramsOut.add(userId);
        paramsOut.add(utils.getCurrentDate());
        paramsOut.add("out");
        paramsOut.add("mobile");

        Log.e("condition", conditions.toString());
        Log.e("params", params.toString());
        mAttAdapter = new TableAttendanceAdapter(getActivity());

        List<TableAttendance> list = mAttAdapter.getLastDatabyConditionArray(conditions, params);
        List<TableAttendance> listOut = mAttAdapter.getLastDatabyConditionArray(conditions, paramsOut);

        if (list!=null && listOut!=null && list.size()>listOut.size()) {
            for (int i=0 ; i<list.size();i++) {
            /*  String a = list.get(i).getId();
                String b = list.get(i).getOffice();*/
                oficeClockin = list.get(list.size()-1).getOffice();

                Log.d("oficeClockin", oficeClockin.toString());
                timein.setText(list.get(list.size()-1).getWaktu());

                locationIn.setText(list.get(list.size()-1).getOffice());
                TableMOfficeAdapter mOfficeAdapter = new TableMOfficeAdapter(getActivity());

                List < TableMOffice > listdata = mOfficeAdapter.getDatabyCondition("kode_office", list.get(list.size()-1).getOffice());
                //  Log.d("locationid1", separated[2].toString());
                // int a = listdata.size();
                if (listdata!=null) {
                    TableMOffice item = listdata.get(listdata.size()-1);
                    locationIn.setText(item.getOffice());
                    location.setText(item.getOffice()+" => "+item.getAlamat()+" => "+item.getKode_office());
                    location.setEnabled(true);
                    locationId = item.getKode_office();
                    // locationName=item.getOffice();
                    Log.d("locationid2", item.getOffice());
                    if (item.getMap_lat()!=null && !item.getMap_lat().equalsIgnoreCase("null")) {

                        lat = Double.parseDouble(item.getMap_lat());
                    }

                    if (item.getMap_lng()!=null && !item.getMap_lng().equalsIgnoreCase("null")) {
                        lng = Double.parseDouble(item.getMap_lng());
                    }

                }
                //    Log.d("locationid3", obj.getString("office"));

                clockin.setVisibility(View.GONE);
                clockin.setEnabled(false);
                clockout.setEnabled(true);
                clockout.setVisibility(View.VISIBLE);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap bitmap = BitmapFactory.decodeFile(list.get(list.size()-1).getPhoto_path(), options);
                Log.d(TAG, "checkClockin "+list.get(list.size()-1).getPhoto_path());

                //    String filePath = Utility.compressImage(list.get(list.size() - 1).getPhoto_path(),list.get(list.size() - 1).getPhoto_name());
                //     Bitmap bitmap = BitmapFactory.decodeFile(filePath);

                // Bitmap bitmapin = BitmapFactory.decodeFile(filePath);
                //    Bitmap bitmapinscale = Bitmap.createScaledBitmap(bitmapin, 100, 100, true);

                if (bitmap!=null) {
                    imgIn.setImageBitmap(bitmap);
                }
            }
        }  else {
            //  fetchAttendance("in");
        }
    }

    private void checkClockout() {
        List<String> conditions = new ArrayList<>();
        List<Object> params = new ArrayList<>();
        conditions.add("userid");
        conditions.add("tanggal");
        conditions.add("status");
        conditions.add("jenis");

        params.add(MyApplication.getInstance().getPrefManager().getUser().getId());
        params.add(utils.getCurrentDate());
        params.add("out");
        params.add("mobile");

        /*List<TableAttendance> list = mAttAdapter.getLastDatabyConditionArray(conditions, params);
        for (int i=0 ; i<list.size();i++) {
        }*/

    }

    /**
     * Listener
     */
    private View.OnClickListener clockInListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                Log.d("koordinat1",utils.getGPSLat().toString());
                Log.d("koordinat2",utils.getGPSLng().toString());
                if (utils.isGpsEnable()) {
                    if (location.getText().toString() == null || location.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Silahkan pilih Lokasi!", Toast.LENGTH_SHORT).show();
                        btnmaps.setVisibility(View.GONE);
                    } else if ((lng!=null && lng == 0.0) || (lat!=null && lat==0.0)) {
                        Toast.makeText(getActivity(), "Lokasi tidak tersedia!", Toast.LENGTH_SHORT).show();
                        btnmaps.setVisibility(View.GONE);
                    }else if(utils.getGPSLat()==null || utils.getGPSLat()== 0.0 || utils.getGPSLng()==null
                            || utils.getGPSLng()==0.0) {

                        Toast.makeText(getActivity(), "Lokasi Tidak Akurat , Mohon lakukan pencarian di maps terhadap lokasi!", Toast.LENGTH_SHORT).show();
                        btnmaps.setVisibility(View.VISIBLE);
                    } else {
                        btnmaps.setVisibility(View.GONE);
//                        Location loc1 = new Location("");
//                        loc1.setLatitude(utils.getGPSLat());
//                        loc1.setLongitude(utils.getGPSLng());
//
//                        Location loc2 = new Location("");
//                        loc2.setLatitude(lat);
//                        loc2.setLongitude(lng);
//                        int distance = utils.distances(loc1, loc2);

//                        if (distance > 500) {
//                            Toast.makeText(getActivity(), "Anda terlalu jauh dari lokasi kantor.", Toast.LENGTH_SHORT).show();
//                        } else {
//                        Intent intent = new Intent(getActivity(), TrackLogService.class);
//                        getActivity().startService(intent);
                        timein.setText(utils.getCurrentTime());
                        takeImage(TAKE_PICTURE_CLOCKIN);
                    }
                    // }
                } else {
                    utils.showGpsTrackerAlert();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private View.OnClickListener clockoutListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            /*List<String> conditions = new ArrayList<>();
            List<Object> params = new ArrayList<>();*/
            try {
                if (utils.isGpsEnable()) {
                    if (location.getText().toString() == null || location.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Silahkan pilih lokasi!", Toast.LENGTH_SHORT).show();
                        btnmaps.setVisibility(View.GONE);
                    } else if ((lng!=null && lng == 0.0) || (lat!=null && lat==0.0)) {
                        Toast.makeText(getActivity(), "Lokasi tidak tersedia!", Toast.LENGTH_SHORT).show();
                        btnmaps.setVisibility(View.GONE);
                    } else if (oficeClockin!=null && !oficeClockin.equalsIgnoreCase(locationId)){
                        Log.d("oficeClockin",oficeClockin.toString());
                        Log.d("locationId",locationId.toString());
                        Toast.makeText(getActivity(), "Lokasi tidak sama.", Toast.LENGTH_SHORT).show();
                        btnmaps.setVisibility(View.GONE);
                    } else if(utils.getGPSLat()==null || utils.getGPSLat()==0.0 || utils.getGPSLng()==null
                            || utils.getGPSLng()==0.0) {

                        Toast.makeText(getActivity(), "Mohon lakukan pencarian terhadap lokasi!", Toast.LENGTH_SHORT).show();
                        btnmaps.setVisibility(View.VISIBLE);
                    } else {
                        btnmaps.setVisibility(View.GONE);
//                        Location loc1 = new Location("");
//                        loc1.setLatitude(utils.getGPSLat());
//                        loc1.setLongitude(utils.getGPSLng());
//
//                        Location loc2 = new Location("");
//                        loc1.setLatitude(lat);
//                        loc1.setLongitude(lng);
//                        int distance = utils.distances(loc1, loc2);

//                        if (distance == 500) {
//                            Toast.makeText(getActivity(), "Anda terlalu jauh dari lokasi kantor.", Toast.LENGTH_SHORT).show();
//                        } else {
                        //  getActivity().stopService(new Intent(getActivity(), TrackLogService.class));
                        //  Log.d(TAG, "onClick cekout");
                        timeOut.setText(utils.getCurrentTime());

                        takeImage(TAKE_PICTURE_CLOCKOUT);
                    }
//                    }
                } else {
                    utils.showGpsTrackerAlert();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    };

    private View.OnClickListener mapsListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String url = "http://maps.google.com/";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
    };

    private View.OnClickListener locationListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            LayoutInflater li = LayoutInflater.from(getActivity());
            View view = li.inflate(R.layout.m_office_listview, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    getActivity());
            alertDialogBuilder.setView(view);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            ListView officeListView = (ListView) view.findViewById(R.id.office_list);
            EditText txtOffice = (EditText) view.findViewById(R.id.search_office);

            final OfficeListAdapter adapter = new OfficeListAdapter(getActivity(), locationList);
            officeListView.setAdapter(adapter);

            txtOffice.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    System.out.println("Text ["+s+"]");

                    adapter.getFilter().filter(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


            officeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        location.setText(adapter.getFilterData().get(position));
                        TableMOfficeAdapter mOfficeAdapter = new TableMOfficeAdapter(getActivity());
                        String[] separated = adapter.getFilterData().get(position).split(" => ");
                        List <TableMOffice> listdata = mOfficeAdapter.getDatabyCondition("kode_office", separated[2]);
                        Log.d("locationid1", separated[2].toString());


                        ((MainMenu)getActivity()).LocationIdMn = separated[2].toString() ;
                        // int a = listdata.size();
                        if (listdata!=null) {
                            TableMOffice item = listdata.get(listdata.size()-1);
                            locationId = item.getKode_office();
                            locationName=item.getOffice();
                            Log.d("locationid2", locationId.toString());
                            if (item.getMap_lat()!=null && !item.getMap_lat().equalsIgnoreCase("null")) {

                                lat = Double.parseDouble(item.getMap_lat());
                            }

                            if (item.getMap_lng()!=null && !item.getMap_lng().equalsIgnoreCase("null")) {
                                lng = Double.parseDouble(item.getMap_lng());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    alertDialog.dismiss();
                }
            });

            alertDialog.show();


            /*new AlertDialog.Builder(getActivity())
                    .setTitle("Lokasi")
                    .setAdapter(spinnerLocation,
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    location.setText(locationList.get(which).toString());
                                    officeId = officeIdList.get(which).toString();
                                    item = listOffice.get(which);
                                    if (item!=null) {
                                        if (item.getMap_lat()!=null && !item.getMap_lat().equalsIgnoreCase("null")) {

                                            lat = Double.parseDouble(item.getMap_lat());
                                        }

                                        if (item.getMap_lng()!=null && !item.getMap_lng().equalsIgnoreCase("null")) {
                                            lng = Double.parseDouble(item.getMap_lng());
                                        }

                                    }
                                    dialog.dismiss();
                                }
                            }).create().show();*/
        }
    };

    /**
     * Method for Take Picture From Camera
     */
    public void takeImage(int code) {
        //
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        f = null;
        // CameraActivity activity = (CameraActivity)getActivity();
        try {
            f = createImageFile(code);
        } catch (IOException ex) {
            // Error occurred while creating the File
            ex.printStackTrace();
        }
        if (f != null) {
            Uri fileUri = Uri.fromFile(f);
            CamActiv.setCapturedImageURI(fileUri);
            CamActiv.setCurrentPhotoPath(fileUri.getPath());
            location = (TextView) root.findViewById(R.id.txtLocation);
            TxtPicPath = (TextView) root.findViewById(R.id.txtPictpath);
            // String[] separated = location.getText().toString().split(" => ");
            // startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            if (code == TAKE_PICTURE_CLOCKIN) {
                picturePath = f.getAbsolutePath();
                Log.d(TAG, "takeImage "+picturePath);
                ((MainMenu)getActivity()).PicturePathMn = picturePath ;
            } else {
                picturePathOut = f.getAbsolutePath();
                Log.d(TAG, "takeImage "+picturePathOut);
                ((MainMenu)getActivity()).PicturePathMn = picturePathOut ;
            }


            TxtPicPath.setText(f.getAbsoluteFile().toString());
            //     TxtPicPath.setText(f.getAbsolutePath());
            Log.d(TAG, "PicturePathMn2 " + ((MainMenu) getActivity()).PicturePathMn);
            Log.d(TAG, "activity "+CamActiv.getCurrentPhotoPath());
            Log.d(TAG, "Uri.fromFile(f) "+Uri.fromFile(f));
            Log.d(TAG, "f.getAbsoluteFile() "+f.getAbsoluteFile());
            // takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            //  imageUri = Uri.fromFile(f);
            File storageDir = new File(Uri.fromFile(f).getPath());
//                if (storageDir != null) {
//                    if (!storageDir.mkdirs()) {
//                        if (!storageDir.exists()) {
//                            Log.d("CameraSample", "failed to create directory");
//                            return null;
//                        }
//                    }
            if (!storageDir.exists()) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        f.getAbsoluteFile());
                Log.d(TAG, "takePictureIntent " + f.getAbsoluteFile());

            }else{
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(f));
                Log.d(TAG, "takePictureIntent " + Uri.fromFile(f));
            }
//
//                    if(Uri.fromFile(f) != null){
//
//                    }else{
//
//                    }

            startActivityForResult(takePictureIntent, code);
        }

    }

    /**
     * Create Image File
     *
     * @return
     * @throws IOException
     */
    protected File createImageFile(int code) throws IOException {
        imageFileName = "";
        switch (code) {
            case TAKE_PICTURE_CLOCKIN:
                imageFileName = generateImageName() + "_clockin";
                break;
            case TAKE_PICTURE_CLOCKOUT:
                imageFileName = generateImageName() + "_clockout";
                break;
        }
        File image;
        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
        Log.d(TAG, "createImageFile "+imageF.getAbsolutePath());
        //  image = new File(albumF + "/" + imageFileName + ".jpg");

        return imageF;
    }

    /**
     * image name generator
     */
    private String generateImageName() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.toString();

        return sb.toString();
    }

    /**
     * Create Folder Image
     *
     * @return
     */
    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("Saphire");
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }


    private void galleryAddPic(String path) {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private void setCalendar() {
        String days="", month, year, date;
        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_WEEK);
        switch (day) {
            case Calendar.SUNDAY:
                // Current day is Sunday
                //days.setText("Minggu");
                days = "Minggu";
                break;
            case Calendar.MONDAY:
                // Current day is Monday
                //days.setText("Senin");
                days = "Senin";
                break;
            case Calendar.TUESDAY:
                //days.setText("Selasa");
                days = "Selasa";
                break;
            case Calendar.WEDNESDAY:
                //days.setText("Rabu");
                days = "Rabu";
                break;
            case Calendar.THURSDAY:
                //days.setText("Kamis");
                days = "Kamis";
                break;
            case Calendar.FRIDAY:
                //days.setText("Jum'at");
                days = "Jum'at";
                break;
            case Calendar.SATURDAY:
                //days.setText("Sabtu");
                days = "Sabtu";
                break;
        }
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");

        month = month_date.format(c.getTime());
        date = String.valueOf(c.get(Calendar.DATE));
        year = String.valueOf(c.get(Calendar.YEAR));
        StringBuilder sb = new StringBuilder();
        sb.append(days);
        sb.append(" , ");
        sb.append(date);
        sb.append(" ");
        sb.append(month);
        sb.append(" ");
        sb.append(year);
        cdate.setText(sb.toString());
    }

    private String generateId() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.append(String.valueOf(a));
        sb.toString();

        return sb.toString();
    }

    private String generateIdIndex(int i) {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append(userId);
        sb.append("_");
        sb.append("attd");
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));
        sb.append(String.valueOf(i));

        sb.toString();
        return sb.toString();
    }

    private void sendData(final TableAttendance item,final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.ATTENDANCE_ENTRY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableAttendanceAdapter adapter = new TableAttendanceAdapter(context);
                        adapter.updatePartial(context, TableAttendance.FLAG, 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("id_a", item.getId());
                    params.put("user_id", item.getUserid());
                    params.put("date", item.getTanggal());
                    params.put("waktu", item.getWaktu());
                    params.put("status", item.getStatus());
                    params.put("lng", item.getLng());
                    params.put("lat", item.getLat());
                    params.put("office", item.getOffice());
                    params.put("jenis", item.getJenis());
                    params.put("photo_status", item.getPhoto_status());
                    params.put("photo_name", item.getPhoto_name());
                    params.put("ip_address", item.getIp_address());
                    params.put("mc_address", item.getMc_address());
                    params.put("imei", item.getImei());

                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void uploadImage(final TableImage item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.UPLOAD_IMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableImageAdapter adapter = new TableImageAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                if (item!=null) {
                    params.put("id", String.valueOf(item.getId()));
                    params.put("image", item.getImage());
                    params.put("name", item.getName());
                    params.put("id_image", item.getId_image());
                    params.put("m_path", item.getM_path());
                    params.put("userid", item.getUserid());
                    params.put("index", String.valueOf(item.getIndex()));
                    params.put("total", String.valueOf(item.getTotal()));
                    params.put("type", item.getType());
                    params.put("id_absen", item.getId_absen());
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void chunkImage(String image, String idAbsen) {
        try {
            TableImageAdapter imageAdapter = new TableImageAdapter(getActivity());

            String userid= MyApplication.getInstance().getPrefManager().getUser().getId();
            final File file = new File(image);

            //  String filePath = Utility.compressImage(image,file.getName());

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;

            String filePath = Utility.compressImage(image,file.getName());

            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);

            byte[] data = bos.toByteArray();

            String file1 = Base64.encodeBytes(data);
            String md5 = utils.md5(file.getName());

            int limit = 10000;
            int start = 0;
            String h = "";
            String f = "";
            int tot = (int) Math.ceil((double) file1.length() / (double) limit);

            /*imageAdapter.insertData(new TableImage(), md5, file1, file.getName(), image, userid,
                    1, 1, "attendance", utils.getCurrentDateandTime(), 0);*/
            for (int i = 0; i < tot; i++) {
                if (limit < file1.length()) {
                    h = file1.substring(start, limit);
                    start = limit;
                    limit = limit + 10000;
                } else {
                    limit = file1.length();
                    h = file1.substring(start, limit);
                    f = h;
                }

                imageAdapter.insertData(new TableImage(),  generateIdIndex(i), md5, idAbsen, h, file.getName(), image, userid,
                        i+1, tot, "attendance", utils.getCurrentDateandTime(), 0);
                //url = ConnectionManager.CM_URL_DTD_CHUNK;
                //imei = dbManager.getIMEI();
                //response = ConnectionManager.postChunk(url, imei, String.valueOf(tot), pictureName, md5, String.valueOf(i), h, getApplicationContext());
                //Log.d("Log", "Berhasil kirim " + pictureName + "index ke " + String.valueOf(i) + " dari " + String.valueOf(tot));
            }

            /*if (h == f) {
                TablePathImageAdapter tablePathImage = new TablePathImageAdapter(getApplicationContext()).open();
                tablePathImage.delete(TablePathImage.KEY_IMAGE_NAME, pictureName);
                tablePathImage.close();
                Log.d("Log", "Path Image: " + pictureName + "di hapus");
            }*/

            List<TableImage> listImage = imageAdapter.getDatabyCondition("flag", 0);
//            isInternetPresent = cd.isConnectingToInternet();
//            if(isInternetPresent) {
                for (int i = 0; i < listImage.size(); i++) {
                    TableImage itemImage = listImage.get(i);

                    uploadImage(itemImage, getActivity());
                }
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadImage2(final TableAttendance item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.UPLOAD_IMAGE2, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableAttendanceAdapter adapter = new TableAttendanceAdapter(context);
                        adapter.updatePartial(context, "photo_status", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                String filePath = Utility.compressImage(item.getPhoto_path(),item.getPhoto_name());
                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                String uploadImage = getStringImage(bitmap);
                Log.d(TAG, "getParams " + filePath);
                Log.d(TAG, "getParams " + uploadImage);

                Map<String, String> params = new HashMap<>();

                if (item!=null) {
                    params.put("myFile", uploadImage);
                    params.put("id", String.valueOf(item.getId()));
//                    params.put("image", item.getImage());
//                    params.put("name", item.getName());
//                    params.put("id_image", item.getId_image());
//                    params.put("m_path", item.getM_path());
//                    params.put("userid", item.getUserid());
//                    params.put("index", String.valueOf(item.getIndex()));
//                    params.put("total", String.valueOf(item.getTotal()));
//                    params.put("type", item.getType());
//                    params.put("id_absen", item.getId_absen());

                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }
    private void fetchAttendance(final String status) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.ATTENDANCE_MOBILE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        //listAttendanceModel.clear();
                        JSONArray jsonArray = obj.getJSONArray("attendance");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject absenObj = (JSONObject) jsonArray.get(i);

                            String office = absenObj.getString("office");
                            String jenis = absenObj.getString("jenis");
                            String tanggal = absenObj.getString("tanggal");
                            String userid = absenObj.getString("userid");
                            String waktu = absenObj.getString("waktu");
                            String status = absenObj.getString("status");

                            if (status.equalsIgnoreCase("in")) {
                                timein.setText(waktu);
                                locationIn.setText(office);
                                clockin.setVisibility(View.GONE);
                                clockin.setEnabled(false);
                                clockout.setVisibility(View.VISIBLE);
                                oficeClockin = office;

                            } else {
                                clockin.setVisibility(View.GONE);
                                clockout.setEnabled(false);
                                clockout.setVisibility(View.VISIBLE);
                                timeOut.setText(waktu);
                                locationOut.setText(office);
                            }
                        }

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("userid", MyApplication.getInstance().getPrefManager().getUser().getId());
                params.put("date", utils.getCurrentDate());
                params.put("jenis", "mobile");
                params.put("status", status);

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeBytes(imageBytes);
        return encodedImage;
    }

    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

}
