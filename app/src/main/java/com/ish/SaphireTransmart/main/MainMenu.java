package com.ish.SaphireTransmart.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.adapter.MainMenuListviewAdapter;
import com.ish.SaphireTransmart.database.TableAttendance;
import com.ish.SaphireTransmart.database.TableImage;
import com.ish.SaphireTransmart.database.TableMOffice;
import com.ish.SaphireTransmart.database.database_adapter.TableAttendanceAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableImageAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableMOfficeAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.main.menu.AttandanceMobile;
import com.ish.SaphireTransmart.main.menu.AttendanceStay;
import com.ish.SaphireTransmart.main.menu.Competitor;
import com.ish.SaphireTransmart.main.menu.DamageGood;
import com.ish.SaphireTransmart.main.menu.Detailing;
import com.ish.SaphireTransmart.main.menu.Display;
import com.ish.SaphireTransmart.main.menu.Education;
import com.ish.SaphireTransmart.main.menu.EducationPlan;
import com.ish.SaphireTransmart.main.menu.Feedback;
import com.ish.SaphireTransmart.main.menu.Generator;
import com.ish.SaphireTransmart.main.menu.Gimmick;
import com.ish.SaphireTransmart.main.menu.Home;
import com.ish.SaphireTransmart.main.menu.Inserting;
import com.ish.SaphireTransmart.main.menu.MonthlyOfftake;
import com.ish.SaphireTransmart.main.menu.Promo;
import com.ish.SaphireTransmart.main.menu.Reader;
import com.ish.SaphireTransmart.main.menu.SellOut;
import com.ish.SaphireTransmart.main.menu.Signature;
import com.ish.SaphireTransmart.main.menu.Visibility;
import com.ish.SaphireTransmart.service.TrackLogService;
import com.ish.SaphireTransmart.utils.Utility;
import com.ish.SaphireTransmart.utils.listitem_object.AttendanceModel;
import com.ish.SaphireTransmart.utils.listitem_object.MainMenuListItem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

//import com.ish.SaphireTransmart.main.menu.SellOut;

//import abbott.opengl.Matrix;

public class MainMenu extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,NavigationView.OnNavigationItemSelectedListener ,LocationListener {
    Location locationuser;
    private String TAG = MainMenu.class.getSimpleName();


    // Session Manager Class
   // SessionManager session;
    LocationManager locationManager;
    public String PicturePathMn,LocationIdMn;

    private Toolbar toolbar;
    protected DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private LinearLayout leftLayout;
    private ListView mainMenuListview;
    private ArrayList<MainMenuListItem> menuItemsList;
    private MainMenuListviewAdapter mainMenuListviewAdapter;
    private TextView username, usercode;
    private boolean isMainActivity = true;
    private Utility utils;
    private ImageView userProfile;
    private  String userId,locationId,level;
    private TableMOffice item;
    private TableAttendanceAdapter mAttAdapter;
    private String oficeClockin="";
    private List<TableMOffice> listOffice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {




        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        try {
            PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (MyApplication.getInstance().getPrefManager().getUser() == null) {
            launchLoginActivity();
        }else {
//            toolbar = (Toolbar) findViewById(R.id.toolbar);
//            setSupportActionBar(toolbar);
            userId = MyApplication.getInstance().getPrefManager().getUser().getId();

            level = MyApplication.getInstance().getPrefManager().getUser().getLevel();


            //String newString;
            if (savedInstanceState == null) {
                Bundle extras = getIntent().getExtras();

            }

            utils = new Utility(this);
            checkClockin();
            utils = new Utility(this);
            utils.setGeoLocation();
            checkClockin();
            final long timeInterval = 60000;
            final Handler handler = new Handler();

            Runnable runnable = new Runnable() {
                // private long time = 0;
                @Override
                public void run() {
                    while (true) {
                        // ------- code for task to run
                        //  System.out.println("Hello !!");
                        utils.TrackLog(userId);

                        try {
                            Thread.sleep(timeInterval);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            if (utils.getCurrentDateNumber() == "14" || utils.getCurrentDateNumber() == "28" ){
                DeleteTask();
            }


            toolbar = (Toolbar) findViewById(R.id.toolbar);
            drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            leftLayout = (LinearLayout) findViewById(R.id.left_layout);
            mainMenuListview = (ListView) findViewById(R.id.main_menu_listView1);
            setSupportActionBar(toolbar);
            drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
            actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
            drawerLayout.setDrawerListener(actionBarDrawerToggle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            actionBarDrawerToggle.syncState();
            prepareMainMenuListItem();
            //  stopService(new Intent(MainMenu.this, TrackLogService.class));

            ViewGroup mainMenuListviewHeader = (ViewGroup) getLayoutInflater().inflate(R.layout.header, mainMenuListview, false);
            username = (TextView) mainMenuListviewHeader.findViewById(R.id.user_name);
            usercode = (TextView) mainMenuListviewHeader.findViewById(R.id.user_code);
            userProfile = (ImageView) mainMenuListviewHeader.findViewById(R.id.user_profile);
            setHeaderData();

            mainMenuListview.addHeaderView(mainMenuListviewHeader, null, false);
            mainMenuListviewAdapter = new MainMenuListviewAdapter(MainMenu.this, menuItemsList);
            mainMenuListview.setAdapter(mainMenuListviewAdapter);
            mainMenuListview.setOnItemClickListener(menuListListener);

            if (savedInstanceState == null) {
                isMainActivity = true;
                initView(new Home(), getResources().getString(R.string.app_name));
            }
        }


    }


    @Override
    protected void onStart() {
        super.onStart();
      //  createLocationRequest();
//        googleApiClient.connect();


    }

    @Override
    protected void onStop() {
        super.onStop();
      //  googleApiClient.disconnect();
    }

//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {
        // cekLokasi_GPS();
        //lock_lokasi();
      //  createLocationRequest();

    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    /**
     * when connection failed
     *
     * @param connectionResult
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    public void onLocationChanged(Location location) {
//        Log.d("tag", "locupdate");
//        locationuser = location;
//        mCurrentLocation = location;
//        userLat = location.getLatitude();
//        userLong = location.getLongitude();
//        Log.i("lokasi changed  :", Double.toString(userLat) + "" + Double.toString(userLong));
//        Log.d("onLocationChanged 2", location.getLongitude() + "-" + location.getLatitude());
//        // Called when a new location is found by the network location provider.
//        //	makeUseOfNewLocation(location);
//        String message = String.format(
//                "New Location 2 \n Longitude: %1$s \n Latitude: %2$s",
//                location.getLongitude(), location.getLatitude()
//        );
//        // cekLokasi_GPS();
//        //  lock_lokasi();
//        // mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());


    }

    @Override
    public void onProviderEnabled(String provider) {
        //    Log.d(TAG, provider + " provider enabled");
    }

    @Override
    public void onProviderDisabled(String provider) {
        ///  Log.d(TAG, provider + " provider enabled");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @SuppressWarnings("StatementWithEmptyBody")

    //nav menu
    @Override
    public boolean onNavigationItemSelected(MenuItem  item) {
        // Handle navigation view item clicks here.
//        FragmentManager fragmentManager = getFragmentManager();
//        Bundle bundle = new Bundle();
//
//        fragmentManager.beginTransaction()
//                .replace(R.id.flContent, new Visibility())
//                .commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //popup click

    private void prepareMainMenuListItem() {
        menuItemsList = new ArrayList<MainMenuListItem>();
        menuItemsList.add(new MainMenuListItem(-1, "Home", R.drawable.home));
       //menuItemsList.add(new MainMenuListItem(13, "To Do List", R.drawable.todolist));


        //menuItemsList.add(new MainMenuListItem(1, "Catalog", R.drawable.ic_catalog));
        //  menuItemsList.add(new MainMenuListItem(2, "Chat and Broadcast", R.drawable.ic_chat));
//        if (Integer.parseInt(userLevel) <= 4){
//            menuItemsList.add(new MainMenuListItem(3, "Presensi Stay", R.drawable.stay));
//          menuItemsList.add(new MainMenuListItem(4, "Presensi Mobile", R.drawable.mobile));
//         }else  if (Integer.parseInt(userLevel) >= 5){
//            if (userJnsabsen.equals("onsite")) {
//                menuItemsList.add(new MainMenuListItem(3, "Presensi Stay", R.drawable.stay));
//            }else   if (userJnsabsen.equals("mobile")){
//                menuItemsList.add(new MainMenuListItem(4, "Presensi Mobile", R.drawable.mobile));
//            }
//
//        }

        menuItemsList.add(new MainMenuListItem(3, "Presensi Stay", R.drawable.stay));
        menuItemsList.add(new MainMenuListItem(4, "Presensi Mobile", R.drawable.mobile));


//        menuItemsList.add(new MainMenuListItem(7, "Input Stock", R.drawable.ic_stockcolor));
//        menuItemsList.add(new MainMenuListItem(8, "Detailing", R.drawable.detailing));
//        menuItemsList.add(new MainMenuListItem(9, "Gimmick", R.drawable.gimmick));
//        menuItemsList.add(new MainMenuListItem(10, "Inserting", R.drawable.insert));
//        menuItemsList.add(new MainMenuListItem(11, "Other Information", R.drawable.place));
//        menuItemsList.add(new MainMenuListItem(12, "Data Outlet", R.drawable.outletdata));
        menuItemsList.add(new MainMenuListItem(5, "Competitor", R.drawable.competitor));
        menuItemsList.add(new MainMenuListItem(6, "Damage Good", R.drawable.damage));
        menuItemsList.add(new MainMenuListItem(7, "Feedback", R.drawable.feedback));
        menuItemsList.add(new MainMenuListItem(8, "Promo", R.drawable.promotion));
        menuItemsList.add(new MainMenuListItem(9, "Sell Out", R.drawable.ic_stockcolor));
        menuItemsList.add(new MainMenuListItem(10, "Visibility", R.drawable.visibility));


        menuItemsList.add(new MainMenuListItem(11, "Signature", R.drawable.ic_sig));
        menuItemsList.add(new MainMenuListItem(12, "Detailing", R.drawable.detailing));
        menuItemsList.add(new MainMenuListItem(13, "Gimmick", R.drawable.gimmick));
        menuItemsList.add(new MainMenuListItem(14, "Input Stock", R.drawable.ic_stockcolor));
        menuItemsList.add(new MainMenuListItem(15, "Inserting", R.drawable.insert));
        menuItemsList.add(new MainMenuListItem(16, "Education", R.drawable.edukasi));
        menuItemsList.add(new MainMenuListItem(17, "Education Plan", R.drawable.edukasi));
        if(level.equalsIgnoreCase("6")) {
            menuItemsList.add(new MainMenuListItem(18, "Generator", R.drawable.ic_action_gamepad));
        }
        if(level.equalsIgnoreCase("5")) {
            menuItemsList.add(new MainMenuListItem(19, "Reader", R.drawable.ic_action_gamepad));
        }
        menuItemsList.add(new MainMenuListItem(20, "Display Items", R.drawable.ic_display));

//        menuItemsList.add(new MainMenuListItem(14, "Overview", R.drawable.overview));
//        menuItemsList.add(new MainMenuListItem(15, "FAQ", R.drawable.faq));
//        menuItemsList.add(new MainMenuListItem(16, "User Guide", R.drawable.userguide));
//        menuItemsList.add(new MainMenuListItem(10, "Inserting", R.drawable.ic_pin));
        menuItemsList.add(new MainMenuListItem(0, "", R.drawable.ic_lock));
        menuItemsList.add(new MainMenuListItem(-2, "Logout", R.drawable.power));
    }

    private AdapterView.OnItemClickListener menuListListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Bundle bundle = new Bundle();
            MainMenuListItem item = (MainMenuListItem) mainMenuListviewAdapter.getItem(position - 1);
            switch (item.getId()) {
                case -1:
                    isMainActivity = false;
                    Fragment home = new Home();
                    initView(home, "Home");
                    break;
                case -2:
                    set_logout();
                    drawerLayout.closeDrawer(leftLayout);
                    break;
////                case 1:
////                    isMainActivity = false;
////                    Fragment catalog = new Catalog();
////                    initView(catalog, "Catalog");
////                    break;
//                case 2:
//                    isMainActivity = false;
//                    Fragment chat = new ChatMessage();
//                    initView(chat, "Chat and Broadcast");
//                    break;
                case 3:
                    isMainActivity = false;
                    Fragment stay = new AttendanceStay();
                    initView(stay, "Presensi Stay");
                    break;
                case 4:
                    //  isMainActivity = false;
                    Fragment mobile = new AttandanceMobile();
                     initView(mobile, "Presensi Mobile");
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.content_framemain, new FragPresensiMobile3())
//                            .commit();
                    break;
                case 5:
                    isMainActivity = false;
                    Fragment competitor = new Competitor();
                    initView(competitor, "Competitor");
                    break;
                case 6:
                    isMainActivity = false;
                    Fragment damagegood = new DamageGood();
                    initView(damagegood, "Damage Good");
                    break;
                case 7:
                    isMainActivity = false;
                    Fragment feedback = new Feedback();
                    initView(feedback, "Feedback");
                    break;
                case 8:
                    isMainActivity = false;
                    Fragment promo = new Promo();
                    initView(promo, "Promo");
                    break;
                case 9:
                    isMainActivity = false;
                    Fragment sellout = new SellOut();
                    initView(sellout, "Sell Out");
                    break;
                case 10:
                    String object = utils.getClockIn(view.getContext());
                    if (object!=null && !object.equalsIgnoreCase("")) {
                        isMainActivity = false;

                        Fragment disp = new Visibility();
                        initView(disp, "Visibility");
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_framemain, new Visibility())
//                                .commit();
                    } else {
                        Toast.makeText(view.getContext(), "Silahkan lakukan clockin terlebih dahulu", Toast.LENGTH_SHORT).show();
                        isMainActivity = false;
                        mobile = new AttandanceMobile();
                        initView(mobile, "Presensi Mobile");
                    }
                    break;

                case 11:
                    isMainActivity = false;
                    Fragment sig = new Signature();
                    initView(sig, "Signature");
                    break;
//

//
                case 12:
                    object = utils.getClockIn(view.getContext());
                    if (object!=null && !object.equalsIgnoreCase("")) {
                        isMainActivity = false;
                        Fragment detailing = new Detailing();
                        initView(detailing, "Detailing");
                    } else {
                        Toast.makeText(view.getContext(), "Silahkan lakukan clockin terlebih dahulu", Toast.LENGTH_SHORT).show();
                        isMainActivity = false;
                        mobile = new AttandanceMobile();
                        initView(mobile, "Presensi Mobile");
                    }

                    break;
//
                case 13:
                    object = utils.getClockIn(view.getContext());
                    if (object!=null && !object.equalsIgnoreCase("")) {
                        isMainActivity = false;
                        Fragment gimmick = new Gimmick();
                        initView(gimmick, "Gimmick");
                    } else {
                        Toast.makeText(view.getContext(), "Silahkan lakukan clockin terlebih dahulu", Toast.LENGTH_SHORT).show();
                        isMainActivity = false;
                        mobile = new AttandanceMobile();
                        initView(mobile, "Presensi Mobile");
                    }

                    break;
                case 14:

                    object = utils.getClockIn(view.getContext());

                    if (object!=null && !object.equalsIgnoreCase("")) {
                        isMainActivity = false;
                        Fragment stock = new MonthlyOfftake();
                        initView(stock, "Input Stock");
                    } else {
                        Toast.makeText(view.getContext(), "Silahkan lakukan clockin terlebih dahulu", Toast.LENGTH_SHORT).show();
                        isMainActivity = false;
                        mobile = new AttandanceMobile();
                        initView(mobile, "Presensi Mobile");
                    }

                    break;
                case 15:
                    object = utils.getClockIn(view.getContext());
                    if (object!=null && !object.equalsIgnoreCase("")) {
                        isMainActivity = false;
                        Fragment inserting = new Inserting();
                        initView(inserting, "Inserting");
                    } else {
                        Toast.makeText(view.getContext(), "Silahkan lakukan clockin terlebih dahulu", Toast.LENGTH_SHORT).show();
                        isMainActivity = false;
                        mobile = new AttandanceMobile();
                        initView(mobile, "Presensi Mobile");
                    }

                    break;
                case 16:
                    isMainActivity = false;
                    Fragment education = new Education();
                    initView(education, "Education");
                    break;
                case 17:
                    isMainActivity = false;
                    Fragment educationplan = new EducationPlan();
                    initView(educationplan, "Education Plan");
                    break;
                case 18:
                    isMainActivity = false;
                    Fragment generator = new Generator();
                    initView(generator, "Generator");
                    break;

                case 19:
                    isMainActivity = false;
                    Fragment reader = new Reader();
                    initView(reader, "Reader");
                    break;

                case 20:
                    isMainActivity = false;
                    Fragment display = new Display();
                    initView(display, "Display Items");
                    break;
//                case 11:
//                    object = utils.getClockIn(view.getContext());
//                    if (object!=null && !object.equalsIgnoreCase("")) {
//                        isMainActivity = false;
//                        Fragment otherinfo = new OtherInfo();
//                        initView(otherinfo, "Other Info");
//                    } else {
//                        Toast.makeText(view.getContext(), "Silahkan lakukan clockin terlebih dahulu", Toast.LENGTH_SHORT).show();
//                        isMainActivity = false;
//                        mobile = new AttandanceMobile();
//                        initView(mobile, "Presensi Mobile");
//                    }
//
//                    break;
//                case 12:
//                    object = utils.getClockIn(view.getContext());
//                    if (object!=null && !object.equalsIgnoreCase("")) {
//                        isMainActivity = false;
//                        Fragment updateOutlet = new FormUpdateDataOutlet();
//                        initView(updateOutlet, "Data Outlet");
//                    } else {
//                        Toast.makeText(view.getContext(), "Silahkan lakukan clockin terlebih dahulu", Toast.LENGTH_SHORT).show();
//                        isMainActivity = false;
//                        mobile = new AttandanceMobile();
//                        initView(mobile, "Presensi Mobile");
//                    }
//
//                    break;
//
//                case 13:
//
//                    isMainActivity = false;
//                    Fragment todolist = new ToDoList();
//                    initView(todolist, "To Do List");
//                    break;
//
//                case 14:
//                    isMainActivity = false;
//                    Fragment overview = new Overview();
//                    initView(overview, "Overview");
//                    break;
//                case 15:
//                    isMainActivity = false;
//                    Fragment faq = new FAQ();
//                    initView(faq, "FAQ");
//                    break;
//                case 16:
//                    isMainActivity = false;
//                    Fragment userguide = new UserGuide();
//                    initView(userguide, "UserGuide");
//                    break;

            }
        }
    };

    /**
     * Method for set fragment view
     */
    public void initView(Fragment fragment, String title) {
           FragmentManager fragmentManager = getSupportFragmentManager();
  //      FragmentManager fragmentManager = getFragmentManager();
        Bundle bundle = new Bundle();
        //    Bundle bundle = new Bundle();
        fragment.setArguments(bundle);

        fragmentManager.popBackStack();
         fragmentManager.beginTransaction().replace(R.id.content_framemain, fragment).commit();
        setTitle(title);
        drawerLayout.closeDrawer(leftLayout);
    }

    private  void setHeaderData() {
        try {
            username.setText(MyApplication.getInstance().getPrefManager().getUser().getName());
            usercode.setText(MyApplication.getInstance().getPrefManager().getUser().getId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void set_logout() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MainMenu.this);
        builder.setMessage(getResources().getString(R.string.ask_logout_app));
        builder.setTitle(getResources().getString(R.string.text_logout));
        builder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //new LogoutTask().execute();
                        stopService(new Intent(MainMenu.this, TrackLogService.class));
                        MyApplication.getInstance().logout();
                        utils.clearClockin();
                        finish();


                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
        alert.show();

    }

    public void backButtonHandler() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(MainMenu.this);
        builder.setMessage(getResources().getString(R.string.ask_exit_app));
        builder.setTitle(getResources().getString(R.string.exit_app));
        builder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MainMenu.this.finish();
                    }
                });
        builder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.ic_menu_close_clear_cancel);
        alert.show();
    }

    @Override
    public void onBackPressed() {
        if (isMainActivity) {
            backButtonHandler();
        } else {
            initView(new Home(), getResources().getString(R.string.app_name));
            isMainActivity = true;
        }
    }
    private void launchLoginActivity() {
        Intent intent = new Intent(MainMenu.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
    private void checkClockin() {
        List<String> conditions = new ArrayList<>();
        List<Object> params = new ArrayList<>();
        List<Object> paramsOut = new ArrayList<>();
        conditions.add("userid");
        conditions.add("tanggal");
        conditions.add("status");
        conditions.add("jenis");

        params.add(userId);
        params.add(utils.getCurrentDate());
        //  params.add(utils.getCurrentDate());
        params.add("in");
        params.add("mobile");

        paramsOut.add(userId);
        paramsOut.add(utils.getCurrentDate());
        paramsOut.add("out");
        paramsOut.add("mobile");

        Log.e("condition", conditions.toString());
        Log.e("params", params.toString());
        mAttAdapter = new TableAttendanceAdapter(this);

        List<TableAttendance> list = mAttAdapter.getLastDatabyConditionArray(conditions, params);
        List<TableAttendance> listOut = mAttAdapter.getLastDatabyConditionArray(conditions, paramsOut);

        if (list!=null && listOut!=null && list.size()>listOut.size()) {
            for (int i=0 ; i<list.size();i++) {
            /*  String a = list.get(i).getId();
                String b = list.get(i).getOffice();*/
                oficeClockin = list.get(list.size()-1).getOffice();

                Log.d("oficeClockin", oficeClockin.toString());

                TableMOfficeAdapter mOfficeAdapter = new TableMOfficeAdapter(this);

                List <TableMOffice> listdata = mOfficeAdapter.getDatabyCondition("kode_office", list.get(list.size()-1).getOffice());
                //  Log.d("locationid1", separated[2].toString());
                // int a = listdata.size();
                if (listdata!=null) {
                    TableMOffice item = listdata.get(listdata.size()-1);

                    locationId = item.getKode_office();
                    // locationName=item.getOffice();
                    Log.d("locationid2", item.getOffice());


                }
                //    Log.d("locationid3", obj.getString("office"));
                utils.setClockIn(this, new AttendanceModel(userId, list.get(list.size()-1).getTanggal(), list.get(list.size()-1).getWaktu(), "in", "mobile", locationId,locationId));


            }
        }  else {
            //  fetchAttendance("in");
        }
    }

    private void DeleteTask()
    {
        Log.d(TAG, "DeleteTask ");
            //toastHandler.sendEmptyMessage(0);
            try {
//                TableDetailingAdapter detailingAdapter = new TableDetailingAdapter(this);
//                List<TableDetailing> listDetailing = detailingAdapter.getAllData();
//                for (int i=0;i<listDetailing.size();i++) {
//                    TableDetailing item = listDetailing.get(i);
//                    if (getWeeks(item.getClockin(), utils.getCurrentDate()) >= 14) {
//                        detailingAdapter.delete(this, item.getId());
//                    }
//                }

                TableAttendanceAdapter adapter = new TableAttendanceAdapter(this);
                List<TableAttendance> listAttendance = adapter.getAllData();
                for (int i=0;i<listAttendance.size();i++) {
                    TableAttendance item = listAttendance.get(i);
                    if (getWeeks(item.getTanggal(), utils.getCurrentDate()) >= 14) {
                        adapter.delete(this, item.getId());
                    }
                }

                TableImageAdapter imageAdapter = new TableImageAdapter(this);
                List<TableImage> listImage = imageAdapter.getAllData();
                for (int i=0;i<listImage.size();i++) {
                    TableImage item = listImage.get(i);
                    if (getWeeks(item.getDate(), utils.getCurrentDateandTime()) >= 14) {
                        imageAdapter.delete(this, item.getId());
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        
    }
    public static int getWeeks(String aa, String bb) {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date a, b;
        int workDays = 0;

        try {
            a = df.parse(aa);
            b = df.parse(bb);

            Calendar startCal = Calendar.getInstance();
            startCal.setTime(a);

            Calendar endCal = Calendar.getInstance();
            endCal.setTime(b);

            //Return 0 if start and end are the same
            if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
                return 0;
            }

            if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
                startCal.setTime(a);
                endCal.setTime(b);
            }

            do {
                //excluding start date
                startCal.add(Calendar.DAY_OF_MONTH, 1);
                if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                    ++workDays;
                }
            } while (startCal.getTimeInMillis() < endCal.getTimeInMillis()); //excluding end date

        } catch (Exception e) {
            e.printStackTrace();
        }
        return workDays;
    }
}
