package com.ish.SaphireTransmart.main.menu;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.database.TableDetailing;
import com.ish.SaphireTransmart.database.TableImage;
import com.ish.SaphireTransmart.database.TableMOffice;
import com.ish.SaphireTransmart.database.TableProdukKategori;
import com.ish.SaphireTransmart.database.database_adapter.TableDetailingAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableImageAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableMOfficeAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableProdukKategoriAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.main.MainMenu;
import com.ish.SaphireTransmart.utils.AlbumStorageDirFactory;
import com.ish.SaphireTransmart.utils.Base64;
import com.ish.SaphireTransmart.utils.BaseAlbumDirFactory;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by adminmc on 26/09/16.
 */
public class Detailing extends Fragment {

    private String TAG = Detailing.class.getSimpleName();
    private ViewGroup root;
    private ImageView ibRentArc, ibRentArc2, closer;
    private Button save;
    private static final int TAKE_PICTURE_RENT_ARC = 121;
    private static final int TAKE_PICTURE_RENT_ARC2 = 122;
    private File f;
    private String picturePath = "", picturePath2 = "", userId, clockin;
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private Utility utils;
    private ProgressDialog progressDialog;
    private ImageButton ibGift;
    private EditText txtName;
    private TableProdukKategoriAdapter produkAdapter;
    private LinearLayout mLayoutProduk;
    private ArrayList<String> list = new ArrayList<>();
    private TableDetailingAdapter detailingAdapter;
    private EditText location,locationEdname;
    private TextView TxtPicPath1,TxtPicPath2;

    private String locationId,locationName;
    public Detailing() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_detailing, container,false);
        utils = new Utility(getActivity());
        mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        userId = MyApplication.getInstance().getPrefManager().getUser().getId();
        detailingAdapter = new TableDetailingAdapter(getActivity());
        utils = new Utility(getActivity());
        initView(root);
        return root;
    }

    private void initView(ViewGroup v) {
        ibRentArc = (ImageView) v.findViewById(R.id.ib_rent_arc);
        ibRentArc2 = (ImageView) v.findViewById(R.id.ib_rent_arc2);
        ibGift = (ImageButton) v.findViewById(R.id.gift);
        location = (EditText) root.findViewById(R.id.txtLocation);
        locationEdname  = (EditText) root.findViewById(R.id.txtLocationName);

        ibRentArc.setOnClickListener(ibRentArcListener);
        ibRentArc2.setOnClickListener(ibRentArcListener2);
        TxtPicPath1 = (TextView) root.findViewById(R.id.txtPictpath1);
        TxtPicPath2 = (TextView) root.findViewById(R.id.txtPictpath2);
        //ibGift.setOnClickListener(ibListener);

        txtName = (EditText) v.findViewById(R.id.nama);
        txtName.requestFocus();

        mLayoutProduk = (LinearLayout) v.findViewById(R.id.list_produk);

        save = (Button) v.findViewById(R.id.btn_save);
        save.setOnClickListener(saveListener);

        setCheckProduct();
        String object = utils.getClockIn(getActivity());
        try {
            if (object!=null && !object.equalsIgnoreCase("")) {
                JSONObject obj = new JSONObject(object);

                TableMOfficeAdapter mOfficeAdapter = new TableMOfficeAdapter(getActivity());
                List<TableMOffice> listdata = mOfficeAdapter.getDatabyCondition("kode_office", obj.getString("office"));
                //  Log.d("locationid1", separated[2].toString());
                // int a = listdata.size();
                clockin = obj.getString("date")+ " " +obj.getString("time");
                if (listdata!=null) {
                    TableMOffice item = listdata.get(listdata.size()-1);
                    locationId = item.getKode_office();
                    locationName=item.getOffice();
                    Log.d("locationid2", locationName.toString());

                }
                Log.d("locationid3",obj.getString("office"));
                location.setText(obj.getString("office"));
                locationEdname.setText(locationName);
                clockin = obj.getString("date")+ " " +obj.getString("time");
                save.setEnabled(true);
            } else {
                Toast.makeText(getActivity(), "Silahkan lakukan clockin terlebih dahulu", Toast.LENGTH_SHORT).show();
                save.setEnabled(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            save.setEnabled(false);
        }
    }

    View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertConfrim();
        }
    };

    View.OnClickListener ibRentArcListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            takeImage(TAKE_PICTURE_RENT_ARC);
        }
    };

    View.OnClickListener ibRentArcListener2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            takeImage(TAKE_PICTURE_RENT_ARC2);
        }
    };

    /**
     * Method for Take Picture From Camera
     */
    public void takeImage(int code) {
        TxtPicPath1 = (TextView) root.findViewById(R.id.txtPictpath1);
        TxtPicPath2 = (TextView) root.findViewById(R.id.txtPictpath2);
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            f = null;
            f = createImageFile(code);
            if (code == TAKE_PICTURE_RENT_ARC) {
                picturePath = f.getAbsolutePath();
                TxtPicPath1.setText(f.getAbsoluteFile().toString());
                Log.d(TAG, "f.getAbsoluteFile() "+f.getAbsoluteFile());

            } else {
                picturePath2 = f.getAbsolutePath();
                TxtPicPath2.setText(f.getAbsoluteFile().toString());
                Log.d(TAG, "f.getAbsoluteFile() "+f.getAbsoluteFile());

            }

            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            startActivityForResult(takePictureIntent, code);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create Image File
     *
     * @return
     * @throws IOException
     */
    private File createImageFile(int code) throws IOException {
        String imageFileName = "";
        switch (code) {
            case TAKE_PICTURE_RENT_ARC:
                imageFileName = generateImageName() + "_detailing1";
                break;
            case TAKE_PICTURE_RENT_ARC2:
                imageFileName = generateImageName() + "_detailing2";
                break;
        }

        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
        return imageF;
    }


    private void setCheckProduct() {
        produkAdapter = new TableProdukKategoriAdapter(getActivity());
        List<TableProdukKategori> listProduk = produkAdapter.getAllData();//produkAdapter.getDatabyCondition("is_detailing", 1);

        for (int i =0; i<listProduk.size(); i++) {
            final int j = i;
            final TableProdukKategori item = listProduk.get(i);

            final LinearLayout Mainlinear = new LinearLayout(getActivity());
            Mainlinear.setBackgroundColor(getResources().getColor(R.color.white));
            Mainlinear.setOrientation(LinearLayout.VERTICAL);

            LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            pLinearPackage.setMargins(0, 0, 0, 0);
            Mainlinear.setLayoutParams(pLinearPackage);
            ((LinearLayout) mLayoutProduk).addView(Mainlinear);

            //Liner Title
            LinearLayout.LayoutParams pLineTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            LinearLayout linearTitle = new LinearLayout(getActivity());
            linearTitle.setLayoutParams(pLineTitle);
            linearTitle.setOrientation(LinearLayout.HORIZONTAL);
            linearTitle.setPadding(10, 0, 10, 0);
            ((LinearLayout) Mainlinear).addView(linearTitle);

            LinearLayout.LayoutParams pChkBox = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.2f);
            pChkBox.setMargins(0,25, 0,0);
            final CheckBox chk = new CheckBox(getActivity());
            chk.setLayoutParams(pChkBox);
            chk.setText(item.getName());
            chk.setPadding(5, 25, 0, 0);
            chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (chk.isChecked()) {
                        list.add(item.getId());
                    } else {
                        list.remove(list.indexOf(item.getId()));
                    }

                    // Toast.makeText(getActivity(), "size="+ list.size(), Toast.LENGTH_SHORT).show();
                }
            });

            LinearLayout.LayoutParams pGiftBox = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.8f);
            pGiftBox.setMargins(0,25, 0,0);
            final ImageButton imageButton = new ImageButton(getActivity());
            imageButton.setImageResource(R.drawable.gimmick);
            imageButton.setLayoutParams(pGiftBox);
            imageButton.setBackgroundColor(Color.parseColor("#ffffff"));

            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.gift_popup);
                    dialog.getWindow().setBackgroundDrawable(
                            new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = dialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    closer        = (ImageView) dialog.findViewById(R.id.imageView_close);
                    ImageView view = (ImageView) dialog.findViewById(R.id.image);
                    final ProgressBar progressBar = (ProgressBar) dialog.findViewById(R.id.progress);
                    String image = item.getImage();
                    Log.d("image", image);
                    if (image==null)
                        image="";

                    Glide.with(getActivity())
                            .load(image)
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    progressBar.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .thumbnail(0.5f)
                            .crossFade()
                                    //.error(R.drawable.no_image)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(view);

                    closer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
            });

            /*final TextView txtTitle = new TextView(getActivity());
            txtTitle.setLayoutParams(pTxtTitle);
            txtTitle.setText(item.getModel_name());
            txtTitle.setTypeface(null, Typeface.BOLD);
            //txtTitle.setTag(picturePath);
            txtTitle.setTextSize(16);
            txtTitle.setFreezesText(true);
            txtTitle.setPadding(5, 25, 0, 0);*/
            ((LinearLayout) linearTitle).addView(chk);
            ((LinearLayout) linearTitle).addView(imageButton);
        }
    }

    /**
     * image name generator
     */
    private String generateImageName() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.toString();

        return sb.toString();
    }

    /**
     * Create Folder Image
     *
     * @return
     */
    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("Saphire");
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case (TAKE_PICTURE_RENT_ARC):
                if (resultCode == Activity.RESULT_OK) {
                    if(picturePath == null || picturePath.equals("")) {
                        TxtPicPath1 = (TextView) root.findViewById(R.id.txtPictpath1);
                        Log.e("TxtPicPath1", TxtPicPath1.getText().toString());
                        picturePath = TxtPicPath1.getText().toString();
                    }
                    final File file = new File(picturePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
                    if (bitmap!=null) {
                        ibRentArc.setImageBitmap(bitmap);
                    }
                    galleryAddPic(picturePath);
                }
                break;
            case (TAKE_PICTURE_RENT_ARC2):
                if (resultCode == Activity.RESULT_OK) {
                    if(picturePath2 == null || picturePath2.equals("")) {
                        TxtPicPath2 = (TextView) root.findViewById(R.id.txtPictpath2);
                        Log.e("TxtPicPath2", TxtPicPath2.getText().toString());
                        picturePath2 = TxtPicPath2.getText().toString();
                    }
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath2, options);
                    if (bitmap!=null) {
                        ibRentArc2.setImageBitmap(bitmap);
                    }
                    galleryAddPic(picturePath2);
                }
                break;

        }
    }

    private void galleryAddPic(String path) {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private String generateIdIndex(int i) {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append(userId);
        sb.append("_");
        sb.append("detail");
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));
        sb.append(String.valueOf(i));

        sb.toString();
        return sb.toString();
    }

    private String generateId() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append("dtl");
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.append(String.valueOf(a));
        sb.toString();

        return sb.toString();
    }

    private void sendData(final TableDetailing item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.DETAILING_ENTRY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableDetailingAdapter adapter = new TableDetailingAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("id", item.getId());
                    params.put("userid", item.getUserid());
                    params.put("name", item.getNama());
                    params.put("value", item.getValue());
                    params.put("kode_office", item.getKode_office());
                    params.put("clockin", item.getClockin());
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void chunkImage(String image, String id) {
        try {
            TableImageAdapter imageAdapter = new TableImageAdapter(getActivity());

            String userid= MyApplication.getInstance().getPrefManager().getUser().getId();
            final File file = new File(image);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
           // Bitmap bitmap = BitmapFactory.decodeFile(image, options);
            String filePath = Utility.compressImage(image,file.getName());

            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            byte[] data = bos.toByteArray();
            String file1 = Base64.encodeBytes(data);
            String md5 = utils.md5(file.getName());

            int limit = 10000;
            int start = 0;
            String h = "";
            String f = "";
            int tot = (int) Math.ceil((double) file1.length() / (double) limit);

            /*imageAdapter.insertData(new TableImage(), md5, file1, file.getName(), image, userid,
                    1, 1, "attendance", utils.getCurrentDateandTime(), 0);*/
            for (int i = 0; i < tot; i++) {
                if (limit < file1.length()) {
                    h = file1.substring(start, limit);
                    start = limit;
                    limit = limit + 10000;
                } else {
                    limit = file1.length();
                    h = file1.substring(start, limit);
                    f = h;
                }

                imageAdapter.insertData(new TableImage(),  generateIdIndex(i), md5, id, h, file.getName(), image, userid,
                        i+1, tot, "detailing", utils.getCurrentDateandTime(), 0);
            }

            List<TableImage> listImage = imageAdapter.getDatabyCondition("flag", 0);
            for (int i =0; i<listImage.size(); i++) {
                TableImage itemImage = listImage.get(i);
                uploadImage(itemImage, getActivity());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadImage(final TableImage item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.UPLOAD_IMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableImageAdapter adapter = new TableImageAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("id", String.valueOf(item.getId()));
                    params.put("image", item.getImage());
                    params.put("name", item.getName());
                    params.put("id_image", item.getId_image());
                    params.put("m_path", item.getM_path());
                    params.put("userid", item.getUserid());
                    params.put("index", String.valueOf(item.getIndex()));
                    params.put("total", String.valueOf(item.getTotal()));
                    params.put("type", item.getType());
                    params.put("id_absen", item.getId_absen());
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);

    }
    public void SaveData(){
        if (txtName.getText().toString().length()<1) {
            Toast.makeText(getActivity(), "Silahkan isikan nama", Toast.LENGTH_SHORT).show();
        } else if (picturePath2==null || picturePath2.length()<1){
            Toast.makeText(getActivity(), "Silahkan ambil gambar", Toast.LENGTH_SHORT).show();
        } else {
            String value = "";
            for (int j=0; j<list.size();j++) {
                if (j==0) {
                    value = list.get(j);
                } else {
                    value = value+", "+list.get(j);
                }
            }
            String id = generateId();

            detailingAdapter.insertData(new TableDetailing(), id, userId,
                    txtName.getText().toString(), value, 0,locationId,clockin);

            Toast.makeText(getActivity(), "Data berhasil tersimpan", Toast.LENGTH_SHORT).show();

            List<TableDetailing> list = detailingAdapter.getDatabyCondition("flag", 0);
            for (int n=0; n<list.size();n++) {
                TableDetailing item = list.get(n);
                sendData(item, getActivity());
            }

            if (picturePath2!=null && picturePath2.length()>1)
                chunkImage(picturePath2, id);


            if (picturePath!=null && picturePath.length()>1){
                chunkImage(picturePath, id);
            }

            Fragment detail = new Detailing();
            ((MainMenu)getActivity()).initView(detail, "Detailing");

        }
    }
    private void AlertConfrim(){
        //Context context;
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
        //String url_dlfile = varurl;
        // set title
        alertDialogBuilder.setTitle("Konfirmasi");

        // set dialog message
        alertDialogBuilder
                .setMessage("Apakah Anda Yakin ?")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SaveData();
                        // if this button is clicked, close
                        // current activity
//                        if (isi_menu.equals("savesellout")) {
//                            saveTransSellOutOffline();
//                        } else if (isi_menu.equals("saveofftake")) {
//                            saveTransOffTakeOffline();
//                        }
//                        else if (isi_menu.equals("saveofftake_m")) {
//                            saveTransOffTake_MOffline();
//                        }
//                        else if (isi_menu.equals("saveofftake_b")) {
//                            saveTransOffTake_BOffline();
//                        }
                        //id=1;
                        //   return id;
                        //MainActivity.this.finish();
                        //MainActivity.this.finish();
                    }
                }) .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, close
                // current activity

                //MainActivity.this.finish();
                //MainActivity.this.finish();
            }
        });



        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

}
