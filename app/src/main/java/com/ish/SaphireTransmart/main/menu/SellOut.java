package com.ish.SaphireTransmart.main.menu;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.adapter.OfficeListAdapter;
import com.ish.SaphireTransmart.database.TableImage;
import com.ish.SaphireTransmart.database.TableKategori;
import com.ish.SaphireTransmart.database.TableMOffice;
import com.ish.SaphireTransmart.database.TableProduk;
import com.ish.SaphireTransmart.database.TableTansSellOut;
import com.ish.SaphireTransmart.database.database_adapter.TableAttendanceAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableImageAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableKategoriAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableMOfficeAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableProdukAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableTansSellOutAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.main.MainMenu;
import com.ish.SaphireTransmart.utils.AlbumStorageDirFactory;
import com.ish.SaphireTransmart.utils.Base64;
import com.ish.SaphireTransmart.utils.BaseAlbumDirFactory;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

/**
 * Created by ACER on 02/11/2016.
 */
public class SellOut extends Fragment {
    private String TAG = SellOut.class.getSimpleName();
//    private InsertingAdapter adapter;
    private List<TableProduk> list = new ArrayList<TableProduk>();
    private List<TableTansSellOut> list2 = new ArrayList<TableTansSellOut>();
    private List<TableKategori> list4 = new ArrayList<TableKategori>();
    private Integer total_jual;
    //    private List<TableSellOut> list = new ArrayList<TableSellOut>();
//    private List<Stock> list = new ArrayList<Stock>();
    private ViewGroup root;
    private File f;
    EditText editText1,edtUOM2,edtUOM3,edtUOM4;
    SharedPreferences pref;
    //    private EditText location;
//    private String clockin="";
//    private ArrayList<String> list2 = new ArrayList<>();
    private Button save;
    private static final int TAKE_PICTURE_PSellout = 120;
    private TextView kategori, cdate, location;
    private String[] kategoriArrayList, list3;
    private ArrayAdapter<String> spinnerKategori;
    private Utility utils;
    ProgressDialog progressDialog;
    private String picturePath = "",imageSellout, name="";
    private TableTansSellOutAdapter transSellOutAdapter;
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private String locationId,locationName,channel,region, userId, clockin="";
    private ImageView ibRentSellOut;
    private LinearLayout mLayoutStock;
    private EditText EdTotSellout;
    private Integer TotalJual;
    private List<String> kategoriList;
    private TableKategoriAdapter kategoriAdapter;
    private ArrayList<String> locationList, officeIdList;
    private TableAttendanceAdapter mAttAdapter;
    private List<TableMOffice> listOffice;
    private TableMOffice item;
    private ArrayAdapter<String> spinnerLocation;
    private Double lng=0.0, lat=0.0;
    int qty,price,Jml_harga;
    int[] id;
    TextView txttotal;
    String namaa,phonea,addressa;
    private RadioGroup radio_reminder;
    private RadioButton radioReminder;

//    private List<TableProduk> list = new ArrayList<TableProduk>();

    public SellOut() {
        utils = new Utility(getActivity());
    }

    @Nullable


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.sellout, container, false);
        utils = new Utility(getActivity());
        utils.setIMEI();
        userId = MyApplication.getInstance().getPrefManager().getUser().getId();

        initView(root);
//        prepareData();
        prepareDataKategori();
        radio_reminder=(RadioGroup) root.findViewById(R.id.radio_reminder);



        mAlbumStorageDirFactory = new BaseAlbumDirFactory();
//        recyclerView = (RecyclerView) root.findViewById(R.id.tableListSellout);
//        adapter = new InsertingAdapter(list, getActivity());
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(adapter);

        mLayoutStock = (LinearLayout) root.findViewById(R.id.list_stocksellout);

//        mLayoutStock = (LinearLayout) root.findViewById(R.id.tableListSellout);

//        kategoriArrayList = getActivity().getResources().getStringArray(R.array.kategori_sellout_list);
//        spinnerKategori = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item,
//                kategoriArrayList);






        return  root;
    }


//
//    private Map<String,ArrayList<String>> getDataSpinner(){
//        ArrayList<String> alSpinnerKategori = new ArrayList<>();
//        ArrayList<String> alSpinnerId = new ArrayList<>();
//        ArrayList<String> alSpinnerJumlahBarang = new ArrayList<>();
//        Map<String, ArrayList<String>> dataSpinner = new HashMap<>();
//
//        /*
//
//
//         */
//        dataSpinner.put("arrayKategori", alSpinnerKategori);
//        dataSpinner.put("arrayId", alSpinnerId);
//        dataSpinner.put("arrayJumlahBarang", alSpinnerJumlahBarang);
//
//
//        return dataSpinner;
//
//    }

    private void initView(ViewGroup v) {
//        final Map<String,ArrayList<String>> isiSpinner = getDataSpinner();


        ibRentSellOut = (ImageView) v.findViewById(R.id.ib_rent_sellout);
        edtUOM2 = (EditText) v.findViewById(R.id.editText2);
        edtUOM3 = (EditText) v.findViewById(R.id.editText3);
        edtUOM4 = (EditText) v.findViewById(R.id.editText4);
        ibRentSellOut.setOnClickListener(ibRentArcListener);

        location = (TextView) v.findViewById(R.id.txtLocationSellout);

        editText1 = (EditText) v.findViewById(R.id.editText1);

        editText1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog.OnDateSetListener dpd = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {

                        int s=monthOfYear+1;
                        String days = "";

                        if(s == 1){
                            days = "Januari";
                        }else if(s == 2){
                            days = "Februari";
                        }else if(s == 3){
                            days = "Maret";
                        }else if(s == 4){
                            days = "April";
                        }else if(s == 5){
                            days = "Mei";
                        }else if(s == 6){
                            days = "Juni";
                        }else if(s == 7){
                            days = "Juli";
                        }else if(s == 8){
                            days = "Agustus";
                        }else if(s == 9){
                            days = "September";
                        }else if(s == 10){
                            days = "Oktober";
                        }else if(s == 11){
                            days = "November";
                        }else if(s == 12){
                            days = "Desember";
                        }else {
                            days ="";
                        }
                        String a = year+"-"+s+"-"+10;

                        String input_date = dayOfMonth+"/"+s+"/"+year;
                        SimpleDateFormat format1=new SimpleDateFormat("dd/MM/yyyy");
                        Date dt1= null;
                        try {
                            dt1 = format1.parse(input_date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        DateFormat format2=new SimpleDateFormat("EEEE");
                        String finalDay =format2.format(dt1);
                        String namedays ="";
                        if(finalDay.equals("Sunday")){
                            namedays = "Minggu";
                        }else if(finalDay.equals("Monday")){
                            namedays = "Senin";
                        }else if(finalDay.equals("Tuesday")){
                            namedays = "Selasa";
                        }else if(finalDay.equals("Wednesday")){
                            namedays = "Rabu";
                        }else if(finalDay.equals("Thursday")){
                            namedays = "Kamis";
                        }else if(finalDay.equals("Friday")){
                            namedays = "Jumat";
                        }else if(finalDay.equals("Saturday")){
                            namedays = "Sabtu";
                        }

                        String b = days+" "+year;
                        pref = getActivity().getSharedPreferences("dataz", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("date",a);
                        editor.commit();

                        editText1.setText(""+b);
                    }
                };

                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog d = new DatePickerDialog(getActivity(), dpd, mYear ,mMonth, mDay);
                d.show();
            }
        });

//        EdTotSellout = (EditText) v.findViewById(R.id.input_prices);

        kategori = (TextView) v.findViewById(R.id.txt_kategori_sellout_arc);
//        tipe = (TextView) v.findViewById(R.id.txt_type_rent_arc);

        kategori.setOnClickListener(kategoriListener);

        location.setOnClickListener(locationListener);



        kategori.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                kategori.setText("");

                kategori.setClickable(true);

            }
        });

        save = (Button) root.findViewById(R.id.save);
        save.setOnClickListener(saveListener);





        locationList = new ArrayList<String>();
        officeIdList = new ArrayList<String>();
        mAttAdapter = new TableAttendanceAdapter(getActivity());
        location.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_search, 0, 0, 0);

        TableMOfficeAdapter mOfficeAdapter = new TableMOfficeAdapter(getActivity());
        // String region = MyApplication.getInstance().getPrefManager().getUser().getRegion();
        try {
            listOffice = mOfficeAdapter.getAllData();

            for (int i=0; i<listOffice.size(); i++) {
                item = listOffice.get(i);
                locationList.add(item.getOffice()+" => "+item.getAlamat()+" => "+item.getKode_office());
                officeIdList.add(item.getKode_office());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            listOffice = mOfficeAdapter.getDatabyCondition("kode_office", 0);

            for (int i=0; i<listOffice.size(); i++) {
                item = listOffice.get(i);
                locationList.add(item.getOffice()+" => "+item.getAlamat()+" => "+item.getKode_office());
                officeIdList.add(item.getKode_office());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        spinnerLocation = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, locationList);

//        setCalendar();



    }
    View.OnClickListener kategoriListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//            if ((channel == "" && channel == null) || (region == "" && region == null)) {
//                Toast.makeText(getActivity(), "Silahkan pilih lokasi terlebih dahulu", Toast.LENGTH_LONG).show();
//            }
            if (location.getText().toString() == null || location.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Silahkan pilih Lokasi!", Toast.LENGTH_SHORT).show();
                    }
            else{
            new AlertDialog.Builder(getActivity())
                    .setTitle("Kategori")
                    .setAdapter(spinnerKategori,
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    //Log.d("MSProfile", "genderArrayList=" + genderArrayList[which].toString());
                                    kategori.setText(kategoriList.get(which).toString());


                                    prepareData(kategoriList.get(which).toString(), channel, region);
                                    (mLayoutStock).removeAllViews();
                                    for (int i = 0; i < list.size(); i++) {
                                        addStockList(list, i);
                                        Log.d(TAG, "onCreateView" + list.get(i).getProduct_category());
                                    }
                                    dialog.dismiss();
                                }
                            }).create().show();
        }
        }

    };



//    View.OnClickListener tipeListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            new AlertDialog.Builder(getActivity())
//                    .setTitle("Tipe")
//                    .setAdapter(spinnerTipe,
//                            new DialogInterface.OnClickListener() {
//
//                                public void onClick(DialogInterface dialog,
//                                                    int which) {
//                                    //Log.d("MSProfile", "genderArrayList=" + genderArrayList[which].toString());
//                                    tipe.setText(tipeArrayList[which].toString());
//                                    dialog.dismiss();
//                                }
//                            }).create().show();
//        }
//    };

    View.OnClickListener ibRentArcListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            takeImage(TAKE_PICTURE_PSellout);
        }
    };
    //
//
    public void takeImage(int code) {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            f = null;
            f = createImageFile(code);
            if (code == TAKE_PICTURE_PSellout) {
                picturePath = f.getAbsolutePath();
            } else {
//                picturePath2 = f.getAbsolutePath();
            }

            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            startActivityForResult(takePictureIntent, code);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //
    private File createImageFile(int code) throws IOException {
        String imageFileName = "";
        switch (code) {
            case TAKE_PICTURE_PSellout:
                imageFileName = generateImageName() + "_sellout";
                break;
//            case TAKE_PICTURE_RENT_ARC2:
//                imageFileName = generateImageName() + "_rent_arc2";
//                break;
        }

        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
        return imageF;
    }

    /**
     * image name generator
     */
    private String generateImageName() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.toString();

        return sb.toString();
    }
    //
//    /**
//     * Create Folder Image
//     *
//     * @return
//     */
    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("Saphire");
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (TAKE_PICTURE_PSellout):
                if (resultCode == Activity.RESULT_OK) {
                    final File file = new File(picturePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 5;
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
                    if (bitmap!=null) {
                        ibRentSellOut.setImageBitmap(bitmap);
                    }
                    galleryAddPic(picturePath);
                }
                break;
        }
    }

    private void galleryAddPic(String path) {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private String generateIdIndex(int i) {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append(userId);
        sb.append("_");
        sb.append("Sell Out");
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));
        sb.append(String.valueOf(i));

        sb.toString();
        return sb.toString();
    }

    private String generateId() {

//        long unix = System.currentTimeMillis() / 1000L;
//        Random r = new Random();
//        int a = r.nextInt(80 - 65) + 65;
//        StringBuilder sb = new StringBuilder();
//        sb.append("IS_");
//        sb.append(MyApplication.getInstance().getPrefManager().getUser().getId());
//        sb.append("_");
//        sb.append(unix);
//        sb.append(String.valueOf(a));
//
//        sb.toString();
//        return sb.toString();
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
//        sb.append("visbl");
//        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.append(String.valueOf(a));
        sb.toString();

        return sb.toString();
    }

    private void chunkImage(String image, String id) {
        try {
            TableImageAdapter imageAdapter = new TableImageAdapter(getActivity());

            String userid= MyApplication.getInstance().getPrefManager().getUser().getId();
            final File file = new File(image);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
            Bitmap bitmap = BitmapFactory.decodeFile(image, options);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            byte[] data = bos.toByteArray();
            String file1 = Base64.encodeBytes(data);
            String md5 = utils.md5(file.getName());

            int limit = 10000;
            int start = 0;
            String h = "";
            String f = "";
            int tot = (int) Math.ceil((double) file1.length() / (double) limit);

            /*imageAdapter.insertData(new TableImage(), md5, file1, file.getName(), image, userid,
                    1, 1, "attendance", utils.getCurrentDateandTime(), 0);*/
            for (int i = 0; i < tot; i++) {
                if (limit < file1.length()) {
                    h = file1.substring(start, limit);
                    start = limit;
                    limit = limit + 10000;
                } else {
                    limit = file1.length();
                    h = file1.substring(start, limit);
                    f = h;
                }

                imageAdapter.insertData(new TableImage(),  generateIdIndex(i), md5, id, h, file.getName(), image, userid,
                        i+1, tot, "Sell Out", utils.getCurrentDateandTime(), 0);
            }

            List<TableImage> listImage = imageAdapter.getDatabyCondition("flag", 0);
            for (int i =0; i<listImage.size(); i++) {
                TableImage itemImage = listImage.get(i);
                uploadImage(itemImage, getActivity());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadImage(final TableImage item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.UPLOAD_IMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableImageAdapter adapter = new TableImageAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        })


        {
            //
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("id", String.valueOf(item.getId()));
                    params.put("image", item.getImage());
                    params.put("name", item.getName());
                    params.put("id_image", item.getId_image());
                    params.put("m_path", item.getM_path());
                    params.put("userId", item.getUserid());
                    params.put("index", String.valueOf(item.getIndex()));
                    params.put("total", String.valueOf(item.getTotal()));
                    params.put("type", item.getType());
                    params.put("id_absen", item.getId_absen());
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

//        Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }


    private View.OnClickListener locationListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            LayoutInflater li = LayoutInflater.from(getActivity());
            View view = li.inflate(R.layout.m_office_listview, null);
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    getActivity());
            alertDialogBuilder.setView(view);
            final AlertDialog alertDialog = alertDialogBuilder.create();
            ListView officeListView = (ListView) view.findViewById(R.id.office_list);
            EditText txtOffice = (EditText) view.findViewById(R.id.search_office);

            final OfficeListAdapter adapter = new OfficeListAdapter(getActivity(), locationList);
            officeListView.setAdapter(adapter);

            txtOffice.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    System.out.println("Text ["+s+"]");

                    adapter.getFilter().filter(s.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });


            officeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    try {
                        location.setText(adapter.getFilterData().get(position));

                        TableMOfficeAdapter mOfficeAdapter = new TableMOfficeAdapter(getActivity());
                        String[] separated = adapter.getFilterData().get(position).split(" => ");
                        List< TableMOffice > listdata = mOfficeAdapter.getDatabyCondition("kode_office", separated[2]);
                        Log.d("locationid1", separated[2].toString());
                        // int a = listdata.size();

                        if (listdata!=null) {
                            TableMOffice item = listdata.get(listdata.size()-1);
                            locationId = item.getKode_office();
                            channel = item.getChannel();
                            region = item.getRegion();
                            locationName=item.getOffice();
                            Log.d("locationid2", locationId.toString());


                            if (item.getMap_lat()!=null && !item.getMap_lat().equalsIgnoreCase("null")) {

                                lat = Double.parseDouble(item.getMap_lat());
                            }

                            if (item.getMap_lng()!=null && !item.getMap_lng().equalsIgnoreCase("null")) {
                                lng = Double.parseDouble(item.getMap_lng());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    alertDialog.dismiss();
                }
            });

            alertDialog.show();


            /*new AlertDialog.Builder(getActivity())
                    .setTitle("Lokasi")
                    .setAdapter(spinnerLocation,
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    location.setText(locationList.get(which).toString());
                                    officeId = officeIdList.get(which).toString();
                                    item = listOffice.get(which);
                                    if (item!=null) {
                                        if (item.getMap_lat()!=null && !item.getMap_lat().equalsIgnoreCase("null")) {

                                            lat = Double.parseDouble(item.getMap_lat());
                                        }

                                        if (item.getMap_lng()!=null && !item.getMap_lng().equalsIgnoreCase("null")) {
                                            lng = Double.parseDouble(item.getMap_lng());
                                        }

                                    }
                                    dialog.dismiss();
                                }
                            }).create().show();*/
        }
    };









    View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            if (location.getText().toString()==null || location.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(getActivity(), "Silahkan lakukan pilih Account terlebih dahulu!", Toast.LENGTH_SHORT).show();
            }

//            if (ibRentSellOut.getDrawable() == null || clockin==null || clockin.equalsIgnoreCase("") ) {
//                Toast.makeText(getActivity(), "Silahkan Lengkapi Photo terlebih dahulu!", Toast.LENGTH_SHORT).show();
//            }
            else {
                //List<TableProduk> stockList = adapter.retrieveData();
                int cek = 0;
                transSellOutAdapter = new TableTansSellOutAdapter(getActivity());
                int selectedReminderID = radio_reminder.getCheckedRadioButtonId();

                radioReminder = (RadioButton) root.findViewById(selectedReminderID);
                if (radioReminder.getText().equals("Yes")) {
                    cek = 1;
                } else {
                    cek = 0;
                }
                namaa = edtUOM2.getText().toString();
                addressa = edtUOM3.getText().toString();
                phonea = edtUOM4.getText().toString();
//                total_jual = Integer.parseInt(edtUOM2.toString().replaceAll("\\.", ""));
//                TotalJual =  Integer.parseInt(EdTotSales.getText().toString().replaceAll("\\.",""));
//                total_jual =  Integer.parseInt(edtUOM2.getText().toString().replaceAll("\\.",""));
                if (picturePath!=null && picturePath.length()>1)
                    imageSellout = new File(picturePath).getName();

                String id = generateId();


                for (int i=0; i<list.size(); i++) {
                    try {
                        TableProduk stock = list.get(i);
                        String id_product = stock.getId();
//                        TableTansSellOut stock = list.get(i);
//                        String kategori_sellout = stock.getKategori_sellout();
                        //int gram = stock.getGram();
                        //String image = stock.getUr();
                        int total_jual = stock.getTotal_jual();
                        int qty = stock.getQty();
                        pref = getActivity().getSharedPreferences("dataz", Context.MODE_PRIVATE);

                        final String date = pref.getString("date","");
//                        int flag = stock.getFlag();
//                        String id_product = stock.getProduct_category();
                        //String clockin = utils.getClockIn(getActivity());}
                        Log.d("save data ", userId + "-" + utils.getCurrentDateandTime() + "-" + locationId + "-" + id_product + "-" + total_jual + "-" + qty + "-" + clockin);
                        if (qty != 0) {
                            sendData(null, getActivity());
                            transSellOutAdapter.insertData(new TableTansSellOut(), userId, date+" "+utils.getCurrentTime(),
                                    locationId, id_product, total_jual, qty, 0,
                                     id, imageSellout,namaa,addressa,phonea,cek);

                            if (picturePath != null && picturePath.length() > 1)
                                chunkImage(picturePath, id);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                Toast.makeText(getActivity(), "Data berhasil disimpan", Toast.LENGTH_SHORT).show();

                List<TableTansSellOut> list = transSellOutAdapter.getDatabyCondition("flag", 0);
                for (int i=0; i<list.size(); i++) {
                    TableTansSellOut item = list.get(i);
                    if (item!=null) {
                        sendData(item, getActivity());
                    }
                }
                Fragment Sellout = new SellOut();

                ((MainMenu)getActivity()).initView(Sellout, "Sell Out");

            }
//            if (progressDialog!=null){
//                progressDialog.dismiss();
//            }
//
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Proses kirim data...");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//
//            if (picturePath.length()>1) {
//                chunkImage(picturePath);
//            }
        }
    };

    private void prepareData(String s,String channel,String region) {


//        kategori.setClickable(true);
////        Log.d(TAG, "prepareData " + varProduct_category);
//        if (list!= null) {
//            TableProdukAdapter ProdukAdapter = new TableProdukAdapter((getActivity()));
//            list = ProdukAdapter.getAllData();
//            Log.d(TAG, "prepareData" + list.get(0).getProduct_category());
//        }
//        else if (kategori.getText().toString().equalsIgnoreCase("Klinik")) {
//            kategoriArrayList = getActivity().getResources().getStringArray(R.array.kategori_sellout_list);
//            TableKategoriAdapter KategoriAdapter = new TableKategoriAdapter((getActivity()));
//            list3 = KategoriAdapter.getAllDatabyLup();
//        }
//        else if (kategori.getText().toString().equalsIgnoreCase("Rumah Sakit")) {
//            kategoriArrayList = getActivity().getResources().getStringArray(R.array.kategori_sellout_list);
//            TableKategoriAdapter KategoriAdapter = new TableKategoriAdapter((getActivity()));
//            list3 = KategoriAdapter.getAllDatabyLup();
//        }
//        else if (kategori.getText().toString().equalsIgnoreCase("Univ")) {
//            kategoriArrayList = getActivity().getResources().getStringArray(R.array.kategori_sellout_list);
//            TableKategoriAdapter KategoriAdapter = new TableKategoriAdapter((getActivity()));
//            list3 = KategoriAdapter.getAllData();
//
//
//        }
        list.clear();
        TableProdukAdapter ProdukAdapter = new TableProdukAdapter((getActivity()));
//        list = ProdukAdapter.getDatabyCondition("product_category",s);
        list = ProdukAdapter.getDatabyConditionz("product_category",s,"channel",channel,"region_prod",region);

//                Log.d(TAG, "prepareData" + list.get(0).getProduct_category());




    }


    private void prepareDataKategori() {

        TableKategoriAdapter KategoriAdapter = new TableKategoriAdapter((getActivity()));
        list4 = KategoriAdapter.getAllData();
//        Log.d(TAG, "prepareDataKategori" + list4.get(0).getProduct_category());



        kategoriList = new ArrayList<String>();
        kategoriAdapter = new TableKategoriAdapter(getActivity());

//        String region = MyApplication.getInstance().getPrefManager().getUser().getRegion();
        try {
            list4 = kategoriAdapter.getAllData();

            for (int i=0; i<list4.size(); i++) {
                TableKategori item = list4.get(i);
                kategoriList.add(item.getProduct_category());

//                locationIdArray.add(item.getKode_office());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        spinnerKategori = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, kategoriList);


//        mLayoutStock = (LinearLayout) root.findViewById(R.id.list_stocksellout);

//        LayoutInflater li = LayoutInflater.from(getActivity());
//        View view = li.inflate(R.layout.fragment_sellout, null);
//        ListView kategoriListView = (ListView) view.findViewById(R.id.tableListSellout);
//
//        kategoriArrayList = getActivity().getResources().getStringArray(R.array.kategori_sellout_list);
//        spinnerKategori = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, list4);
//        spinnerKategori = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item,
//                list4);


//        kategoriListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                try {
////                    location.setText(adapter.getFilterData().get(position));
//
//                    TableKategoriAdapter KategoriAdapter = new TableKategoriAdapter(getActivity());
////                    list4 = KategoriAdapter.getAllData();
//                    List<TableKategori> listdata = KategoriAdapter.getAllData();
//                    // int a = listdata.size();
//                    if (listdata!=null) {
//                        TableKategori item = listdata.get(listdata.size()-1);
//
//                        String name = item.getProduct_category();
//
////                        if (item.getMap_lat()!=null && !item.getMap_lat().equalsIgnoreCase("null")) {
////
////                            lat = Double.parseDouble(item.getMap_lat());
////                        }
//
////                        if (item.getMap_lng()!=null && !item.getMap_lng().equalsIgnoreCase("null")) {
////                            lng = Double.parseDouble(item.getMap_lng());
////                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
////                alertDialog.dismiss();
//            }
//        });

//        kategori = (TextView) v.findViewById(R.id.txt_kategori_sellout_arc);

//                kategori.setText("");
//                kategori.setClickable(true);

//        Log.d(TAG, "prepareData " + varProduct_category);
//        if (kategori.getText().toString().equalsIgnoreCase("Klinik")) {
//            kategoriArrayList = getActivity().getResources().getStringArray(R.array.kategori_sellout_list);
////            TableKategoriAdapter KategoriAdapter = new TableKategoriAdapter((getActivity()));
//            list3 = KategoriAdapter.getAllDatabyLup();
//        }
//        else if (kategori.getText().toString().equalsIgnoreCase("Rumah Sakit")) {
//            kategoriArrayList = getActivity().getResources().getStringArray(R.array.kategori_sellout_list);
////            TableKategoriAdapter KategoriAdapter = new TableKategoriAdapter((getActivity()));
//            list3 = KategoriAdapter.getAllDatabyLup();
//        }
//        else if (kategori.getText().toString().equalsIgnoreCase("Univ")) {
//            kategoriArrayList = getActivity().getResources().getStringArray(R.array.kategori_sellout_list);
////            TableKategoriAdapter KategoriAdapter = new TableKategoriAdapter((getActivity()));
//            list3 = KategoriAdapter.getAllData();
//        }
//                {
//                    kategori.setText("");
//                    kategori.setClickable(false);
//                }


    }

    private void addStockList(List<TableProduk> list, final int position) {
        try {
            if (list!=null) {

//                list.clear();
                final TableProduk item = list.get(position);


                final LinearLayout Mainlinear = new LinearLayout(getActivity());
                Mainlinear.setBackgroundColor(getResources().getColor(R.color.white));
                Mainlinear.setOrientation(LinearLayout.VERTICAL);

                LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                pLinearPackage.setMargins(0, 0, 0, 0);
                Mainlinear.setLayoutParams(pLinearPackage);
                ((LinearLayout) mLayoutStock).addView(Mainlinear);


                //Liner Title
                LinearLayout.LayoutParams pLineTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                LinearLayout linearTitle = new LinearLayout(getActivity());
                linearTitle.setLayoutParams(pLineTitle);
                linearTitle.setOrientation(LinearLayout.HORIZONTAL);
                linearTitle.setPadding(10, 0, 10, 0);
                ((LinearLayout) Mainlinear).addView(linearTitle);

                LinearLayout.LayoutParams pTxtTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                pTxtTitle.setMargins(0,25, 0,0);
                final TextView txtTitle = new TextView(getActivity());
                txtTitle.setLayoutParams(pTxtTitle);
                txtTitle.setText(item.getDetail_category()+" => "+item.getModel_name());
                txtTitle.setTypeface(null, Typeface.BOLD);
                //txtTitle.setTag(picturePath);
                txtTitle.setTextSize(16);
                txtTitle.setFreezesText(true);
                txtTitle.setPadding(5, 25, 0, 0);
                ((LinearLayout) linearTitle).addView(txtTitle);

                //Liner Title
                LinearLayout.LayoutParams pLineTitleProduct = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                LinearLayout linearTitleProduct = new LinearLayout(getActivity());
                linearTitleProduct.setLayoutParams(pLineTitleProduct);
                linearTitleProduct.setOrientation(LinearLayout.HORIZONTAL);
                linearTitleProduct.setPadding(10, 0, 10, 0);
                ((LinearLayout) Mainlinear).addView(linearTitleProduct);

                LinearLayout.LayoutParams pTxtTitleProduct = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                final TextView txtTitleProduct = new TextView(getActivity());
                txtTitleProduct.setLayoutParams(pTxtTitleProduct);
                txtTitleProduct.setText(item.getProduct_category());
                //txtTitle.setTag(picturePath);
                txtTitleProduct.setVisibility(View.GONE);
                txtTitleProduct.setTextSize(14);
                txtTitleProduct.setFreezesText(true);
                txtTitleProduct.setPadding(5, 5, 0, 0);
                ((LinearLayout) linearTitleProduct).addView(txtTitleProduct);

                //Liner Title

                LinearLayout.LayoutParams pLinePrice = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                pLinePrice.setMargins(0, 5, 0, 0);
                LinearLayout linearPrice = new LinearLayout(getActivity());
                linearPrice.setLayoutParams(pLinePrice);
                linearPrice.setOrientation(LinearLayout.HORIZONTAL);
                linearPrice.setPadding(10, 0, 10, 0);
                txtTitleProduct.setVisibility(View.GONE);
                ((LinearLayout) Mainlinear).addView(linearPrice);


                LinearLayout.LayoutParams pLineProduct = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                pLineProduct.setMargins(0, 5, 0, 0);
                LinearLayout linearProduct = new LinearLayout(getActivity());
                linearProduct.setLayoutParams(pLineProduct);
                linearProduct.setOrientation(LinearLayout.HORIZONTAL);
                linearProduct.setPadding(10, 0, 10, 0);
                txtTitleProduct.setVisibility(View.GONE);
                ((LinearLayout) Mainlinear).addView(linearProduct);



                LinearLayout.LayoutParams lprice = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
                final TextView titlePrice = new TextView(getActivity());
                titlePrice.setLayoutParams(lprice);
                //   txtUOM.setText(item.getEntity());
                titlePrice.setText("Price");
                //txtTitle.setTag(picturePath);
                //   txtUOM.setTextAlignment();
                titlePrice.setTextColor(Color.BLACK);
                titlePrice.setTextSize(14);
                titlePrice.setFreezesText(true);
                titlePrice.setPadding(5, 25, 0, 0);
                ((LinearLayout) linearPrice).addView(titlePrice);

                LinearLayout.LayoutParams isiPrice = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
                final TextView txtPrice = new TextView(getActivity());
                txtPrice.setLayoutParams(isiPrice);
                //   txtUOM.setText(item.getEntity());
                int pricez = Integer.parseInt(item.getPrice());

                String s = NumberFormat.getIntegerInstance(Locale.GERMAN).format(pricez);

                txtPrice.setText("Rp " +  s+",00");

                //txtTitle.setTag(picturePath);
                //   txtUOM.setTextAlignment();
                txtPrice.setTextColor(Color.BLACK);
                txtPrice.setTextSize(14);
                txtPrice.setFreezesText(true);
                txtPrice.setPadding(5, 25, 0, 0);
                ((LinearLayout) linearPrice).addView(txtPrice);



                LinearLayout.LayoutParams pTxtUOM = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
                final TextView txtUOM = new TextView(getActivity());
                txtUOM.setLayoutParams(pTxtUOM);
                //   txtUOM.setText(item.getEntity());
                txtUOM.setText("Qty");
                //txtTitle.setTag(picturePath);
                //   txtUOM.setTextAlignment();
                txtUOM.setTextColor(Color.BLACK);
                txtUOM.setTextSize(14);
                txtUOM.setFreezesText(true);
                txtUOM.setPadding(5, 25, 0, 0);
                ((LinearLayout) linearProduct).addView(txtUOM);



                LinearLayout.LayoutParams pEdtUOM = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
//                if (position==list.size()-1) {
//                    pEdtUOM.setMargins(0,0,0,500);
//                }

                final EditText edtUOM = new EditText(getActivity());
                final EditText edtUOM2 = new EditText(getActivity());
                edtUOM.setLayoutParams(pEdtUOM);
                edtUOM.setInputType(InputType.TYPE_CLASS_NUMBER);
                edtUOM.setLines(1);
                edtUOM.setHint("0");

                edtUOM.setGravity(Gravity.CENTER);
                edtUOM.setFreezesText(true);
                edtUOM.setPadding(15, 20, 15, 20);
                //  edtUOM.setPad
                edtUOM.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.toString() == null || s.toString().length() < 1) {
                            item.setQty(0);


                            // Toast.makeText(getActivity(), "posisi="+position+"  qty="+item.getQty(), Toast.LENGTH_SHORT).show();
                        } else {
                            item.setQty(Integer.parseInt(s.toString()));
                            int jml = Integer.parseInt(s.toString());
                            int harga = Integer.parseInt(item.getPrice());
                            item.setQty(Integer.parseInt(s.toString()));

                            Jml_harga = jml * harga;
                            Log.d(TAG, "onTextChanged " + Integer.toString(Jml_harga));
                            edtUOM2.setText(Integer.toString(Jml_harga));
                            // Toast.makeText(getActivity(), "posisi="+position+"  qty="+item.getQty(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void afterTextChanged(Editable s) {


                    }
                });
                ((LinearLayout) linearProduct).addView(edtUOM);


//                LinearLayout.LayoutParams pLineTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//
                //Liner Title
                LinearLayout.LayoutParams pLinepenjualan = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                pLinepenjualan.setMargins(0, 5, 0, 0);
                LinearLayout Linepenjualan = new LinearLayout(getActivity());
                Linepenjualan.setLayoutParams(pLinepenjualan);
                Linepenjualan.setOrientation(LinearLayout.HORIZONTAL);
                Linepenjualan.setPadding(10, 0, 10, 0);
                txtTitleProduct.setVisibility(View.GONE);
                //    linearProduct.setTextAlignment();
                ((LinearLayout) Mainlinear).addView(Linepenjualan);

                LinearLayout.LayoutParams pTxtUOM2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
                final TextView txtUOM2 = new TextView(getActivity());
                txtUOM2.setLayoutParams(pTxtUOM2);

                txtUOM2.setTextColor(Color.BLACK);
                txtUOM2.setText("Total penjualan  ");

//
                txtUOM2.setTextSize(14);
//                txtUOM2.set
                txtUOM2.setFreezesText(true);
                txtUOM2.setPadding(5, 25, 0, 0);
                ((LinearLayout) Linepenjualan).addView(txtUOM2);


//

                LinearLayout.LayoutParams pEdtUOM2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
//                if (position==list.size()-1) {
//                    pEdtUOM2.setMargins(0,0,0,500);
//                }

                if (position==list.size()-1) {
                    pEdtUOM2.setMargins(0,0,0,200);
                }

//

                edtUOM2.setLayoutParams(pEdtUOM2);
                edtUOM2.setInputType(InputType.TYPE_CLASS_NUMBER);
                edtUOM2.setHint("0");
//                edtUOM2.setLines(1);
//                edtUOM2.setLineSpacing(0,2);
//                edtUOM2.setLines(2);

                edtUOM2.setFreezesText(true);


//                LinearLayout.LayoutParams pLineTitleCategory1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                LinearLayout linearTitleCategory1 = new LinearLayout(getActivity());
//                linearTitleCategory1.setLayoutParams(pLineTitleCategory1);
//                linearTitleCategory1.setOrientation(LinearLayout.VERTICAL);
//                linearTitleCategory1.setPadding(10, 10, 10, 10);
//                ((LinearLayout) Mainlinear).addView(linearTitleCategory1);

                //edtUOM.setText("Counterpain");
                //txtTitle.setTag(picturePath);
                //edtUOM.setTextSize(14);

                edtUOM2.setGravity(Gravity.CENTER);
//                edtUOM2.setFreezesText(true);
                edtUOM2.setPadding(15, 20, 15, 20);
                edtUOM2.addTextChangedListener(new TextWatcher()

                {
                    final String current = "";

                    @Override
                    public void beforeTextChanged(CharSequence b, int start, int count, int after) {
                        if
//                                (b.toString() == null || b.toString().length() < 1)
                                (!b.toString().equals(current)) {

                            item.setTotal_jual(Integer.parseInt(b.toString().replaceAll("[$,.]", "")));
//                            item.setTotal_jual(0);
//                            item.setTotal_jual(Integer.parseInt(b.toString()));

                            edtUOM2.removeTextChangedListener(this);

                            String cleanString = b.toString().replaceAll("[$,.]", "");

                            double parsed = Double.parseDouble(cleanString);
                            String formatted = NumberFormat.getInstance(Locale.GERMANY).format((parsed));

                            // current = formatted;
                            edtUOM2.setText(formatted);
                            edtUOM2.setSelection(formatted.length());

                            edtUOM2.addTextChangedListener(this);


                        }
                    }

                    @Override
                    public void onTextChanged(CharSequence b, int arg1, int arg2, int arg3) {
                        if
//                                (b.toString() == null || b.toString().length() < 1)
                                (!b.toString().equals(current)) {

                            item.setTotal_jual(Integer.parseInt(b.toString().replaceAll("[$,.]", "")));
//                            item.setTotal_jual(0);
//                            item.setTotal_jual(Integer.parseInt(b.toString()));

                            edtUOM2.removeTextChangedListener(this);

                            String cleanString = b.toString().replaceAll("[$,.]", "");

                            double parsed = Double.parseDouble(cleanString);
                            String formatted = NumberFormat.getInstance(Locale.GERMANY).format((parsed));

                            // current = formatted;
                            edtUOM2.setText(formatted);
                            edtUOM2.setSelection(formatted.length());

                            edtUOM2.addTextChangedListener(this);


                        }
//

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });


                // ((LinearLayout) linearProduct).addView(edtUOM);
                ((LinearLayout) Linepenjualan).addView(edtUOM2);
//
//                // ((LinearLayout) linearProduct).addView(edtUOM);
//                ((LinearLayout) Linepenjualan).addView(edtUOM2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendData(final TableTansSellOut item,final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.SELLOUT_ENTRY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableTansSellOutAdapter adapter = new TableTansSellOutAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "unixId", obj.getString("unixId"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("userid", item.getUserid());
                    params.put("tgl_input", item.getTgl_input());
                    params.put("kode_office", item.getKode_office());
//                    params.put("kategori_sellout", item.getKategori_sellout());
                    params.put("id_product", item.getId_product());
                    params.put("total_jual", String.valueOf(item.getTotal_jual()));
                    params.put("qty", String.valueOf(item.getQty()));
                    params.put("flag", String.valueOf(item.getFlag()));
//                    params.put("clockin", item.getClockin());
                    params.put("unixId", item.getUnixId());
                    params.put("name", item.getNama());
                    params.put("address", item.getAddress());
                    params.put("phone", item.getPhone());
                    params.put("instalation", String.valueOf(item.getInstalation()));
                    if(item.getPhoto() == null || item.getPhoto() == ""){
                        params.put("photo", "kosong");
                    }else{
                        params.put("photo", item.getPhoto());
                    }

                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }




//    private void sendData(final TableTansSellOut item,final Context context) {
//        StringRequest strReq = new StringRequest(Request.Method.POST,
//                ConnectionManager.SELLOUT_ENTRY, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                Log.e(TAG, "response: " + response);
//
//                try {
//                    JSONObject obj = new JSONObject(response);
//
//                    // check for error flag
//                    if (obj.getBoolean("error") == false) {
//                        TableTansSellOutAdapter adapter = new TableTansSellOutAdapter(context);
//                        adapter.updatePartial(context, "flag", 1, "unixId", obj.getString("unixId"));
//
//                    } else {
//                        // error in fetching chat rooms
//                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
//                    }
//
//                } catch (JSONException e) {
//                    Log.e(TAG, "json parsing error: " + e.getMessage());
//                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                NetworkResponse networkResponse = error.networkResponse;
//                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
//                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<>();
//                if (item!=null) {
////                    String a = item.getUserid();
//                    params.put("userid", item.getUserid());
//                    params.put("tgl_input", item.getTgl_input());
//                    params.put("kode_office", item.getKode_office());
////                    params.put("kategori_sellout", item.getKategori_sellout());
//                    params.put("id_product", item.getId_product());
//                    params.put("total_jual", String.valueOf(item.getTotal_jual()));
//                    params.put("qty", String.valueOf(item.getQty()));
//                    params.put("flag", String.valueOf(item.getFlag()));
//                    params.put("clockin", item.getClockin());
//                    params.put("unixId", item.getUnixId());
//                    params.put("photo", item.getPhoto());
//
//                }
//
//                Log.e(TAG, "params: " + params.toString());
//                return params;
//
//
//
//            }
//
////            private Map<String, String> params(Map<String, String> map){
////                Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
////                while (it.hasNext()) {
////                    Map.Entry<String, String> pairs = (Map.Entry<String, String>)it.next();
////                    if(pairs.getValue()==null){
////                        map.put(pairs.getKey(), "");
////                    }
////                }
////                return map;
////
////
////            }
//
//        };
//
//        //Adding request to request queue
//        MyApplication.getInstance().addToRequestQueue(strReq);
//    }

//    private void setCalendar() {
//        String days="", month, year, date;
//        Calendar c = Calendar.getInstance();
//        int day = c.get(Calendar.DAY_OF_WEEK);
//        switch (day) {
//            case Calendar.SUNDAY:
//                // Current day is Sunday
//                //days.setText("Minggu");
//                days = "Minggu";
//                break;
//            case Calendar.MONDAY:
//                // Current day is Monday
//                //days.setText("Senin");
//                days = "Senin";
//                break;
//            case Calendar.TUESDAY:
//                //days.setText("Selasa");
//                days = "Selasa";
//                break;
//            case Calendar.WEDNESDAY:
//                //days.setText("Rabu");
//                days = "Rabu";
//                break;
//            case Calendar.THURSDAY:
//                //days.setText("Kamis");
//                days = "Kamis";
//                break;
//            case Calendar.FRIDAY:
//                //days.setText("Jum'at");
//                days = "Jum'at";
//                break;
//            case Calendar.SATURDAY:
//                //days.setText("Sabtu");
//                days = "Sabtu";
//                break;
//        }
//        SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
//
//        month = month_date.format(c.getTime());
//        date = String.valueOf(c.get(Calendar.DATE));
//        year = String.valueOf(c.get(Calendar.YEAR));
//        StringBuilder sb = new StringBuilder();
//        sb.append(days);
//        sb.append(" , ");
//        sb.append(date);
//        sb.append(" ");
//        sb.append(month);
//        sb.append(" ");
//        sb.append(year);
//        cdate.setText(sb.toString());
//    }



}
