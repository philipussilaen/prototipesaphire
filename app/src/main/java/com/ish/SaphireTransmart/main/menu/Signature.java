package com.ish.SaphireTransmart.main.menu;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.database.TableMSIgnature;
import com.ish.SaphireTransmart.database.database_adapter.TableMSignatureAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;
import com.ish.SaphireTransmart.widget.SignatureView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

//import com.ish.saphiretaisho3.R;
//import com.ish.saphiretaisho3.database.TableMSIgnature;
//import com.ish.saphiretaisho3.database.database_adapter.TableMSignatureAdapter;
//import com.ish.saphiretaisho3.gcm.app.MyApplication;
//import com.ish.saphiretaisho3.utils.ConnectionManager;
//import com.ish.saphiretaisho3.utils.Utility;
//import com.ish.saphiretaisho3.widget.SignatureView;

/**
 * Created by hari on 8/30/2016.
 */
public class Signature extends Fragment {
    private String TAG = Signature.class.getSimpleName();
    private ViewGroup root;
    private ImageView prev, closer, sign;
    private SignatureView signatureView;
    private String _fileName, imagePath;
    private Bitmap bitmap;
    private Button ok, save, clear;
    private TextView imageName;
    private Utility utility;

    public Signature() {
        utility = new Utility(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root    = (ViewGroup) inflater.inflate(R.layout.fragment_signature, container, false);
        prev        = (ImageView) root.findViewById(R.id.imagepreview);
        sign        = (ImageView) root.findViewById(R.id.signature);
        imageName   = (TextView) root.findViewById(R.id.sig_pic);
        save        = (Button)   root.findViewById(R.id.save);
        sign.setOnClickListener(ibListener);
        save.setOnClickListener(saveListener);

        return root;
    }

    private View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (imagePath==null || imagePath.length()<=0) {
                Toast.makeText(getActivity(), "Silahkan tanda tangan!", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    String image = utility.getStringImage(bitmap);
                    TableMSignatureAdapter adapter = new TableMSignatureAdapter(getActivity());
                    adapter.insertData(new TableMSIgnature(), generateId(), imagePath, _fileName, image,  0);

                    Toast.makeText(getActivity(), "Data tersimpan..", Toast.LENGTH_SHORT)
                            .show();
                    List<TableMSIgnature> list = adapter.getDatabyCondition("status", 0);
                    for (int i=0; i<list.size(); i++) {
                        TableMSIgnature item = list.get(i);
                        if (item!=null) {
                            //sendData(item, getActivity());
                            uploadImage(item, getActivity());
                        }
                    }
                } catch ( Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private View.OnClickListener ibListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.activity_sign);
            dialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            closer        = (ImageView) dialog.findViewById(R.id.imageView_close);
            clear         = (Button) dialog.findViewById(R.id.clear);
            ok            = (Button) dialog.findViewById(R.id.ok);
            signatureView = (SignatureView) dialog.findViewById(R.id.signature);
            closer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            clear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    signatureView.clear();
                }
            });

            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (signatureView.isClear()) {
                        Toast.makeText(getActivity(), "Silahkan tanda tangan!", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        StringBuilder fileName = new StringBuilder();
                        long unix = System.currentTimeMillis() / 1000L;
                        Random r = new Random();
                        int a = r.nextInt(80 - 65) + 65;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date now = new Date();
                        String strDate = sdf.format(now);
                        String yy  = strDate.substring(2, 4);
                        String mm  = strDate.substring(5, 7);
                        String dd  = strDate.substring(8, 10);
                        if (!Environment.getExternalStorageState().equals(
                                Environment.MEDIA_MOUNTED)) {
                            Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_SHORT)
                                    .show();
                        } else {
                            fileName.append("Signature_");
                            fileName.append(MyApplication.getInstance().getPrefManager().getUser().getId());
                            fileName.append("_");
                            fileName.append(unix);
                            fileName.append(String.valueOf(a));
                            fileName.append(".jpg");
                            _fileName   = fileName.toString();
                            String path = Environment.getExternalStorageDirectory()+ File.separator + "Pictures";
                            signatureView.exportFile(path, _fileName);
                            dialog.dismiss();
                            Toast.makeText(getActivity(), "Gambar tersimpan", Toast.LENGTH_SHORT).show();
                            imageName.setText(_fileName);
                            imagePath = path+ File.separator+fileName.toString();
                            decodeFile(imagePath);

                        }
                    }
                }
            });
            dialog.show();
        }
    };

    public void decodeFile(final String filePath) {
        // Decode ukuran gambar
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);
        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        // Decode with inSampleSize
        final BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;

        bitmap = BitmapFactory.decodeFile(filePath, o2);
        prev.setImageBitmap(bitmap);
    }

    private void uploadImage(final TableMSIgnature item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.UPLOAD_SIGNATURE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableMSignatureAdapter adapter = new TableMSignatureAdapter(context);
                        adapter.updatePartial(context, "status", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("image", item.getImage());
                    params.put("name", item.getName());
                    params.put("id_image", item.getId());
                    params.put("m_path", item.getPath());
                    params.put("userid", MyApplication.getInstance().getPrefManager().getUser().getId());
                    params.put("index", "1");
                    params.put("total", "1");
                    params.put("type", "Signature");
                    params.put("id_absen", "");
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private String generateId() {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append("SIGN_");
        sb.append(MyApplication.getInstance().getPrefManager().getUser().getId());
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));

        sb.toString();
        return sb.toString();
    }
}
