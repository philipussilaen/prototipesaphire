package com.ish.SaphireTransmart.main.menu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.database.TableQr;
import com.ish.SaphireTransmart.database.database_adapter.TableDetailingAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableProdukKategoriAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableQrAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.main.MainMenu;
import com.ish.SaphireTransmart.utils.AlbumStorageDirFactory;
import com.ish.SaphireTransmart.utils.BaseAlbumDirFactory;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

/**
 * Created by INFOMEDIA on 11/04/2017.
 */

public class Reader extends Fragment {

    private String TAG = Detailing.class.getSimpleName();
    private ViewGroup root;
    private ImageView ibRentArc, ibRentArc2, closer;
    private Button save;
    private static final int TAKE_PICTURE_RENT_ARC = 121;
    private static final int TAKE_PICTURE_RENT_ARC2 = 122;
    private File f;
    private String picturePath = "", picturePath2 = "", kd_area,level,userId, clockin;
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private Utility utils;
    private ProgressDialog progressDialog;
    private ImageButton ibGift;
    private EditText txtName;
    private TableProdukKategoriAdapter produkAdapter;
    private LinearLayout mLayoutProduk;
    private ArrayList<String> list = new ArrayList<>();
    private TableDetailingAdapter detailingAdapter;
    private TableQrAdapter qrAdapter;
    private EditText total,location,locationEdname;
    private TextView namaa,id,TxtPicPath1,TxtPicPath2,judul;
    private Button scan_btn;
    private String idz,name,kode,locationId,locationName;
    public Reader() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_reader, container,false);
        utils = new Utility(getActivity());
        mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        userId = MyApplication.getInstance().getPrefManager().getUser().getId();
        level = MyApplication.getInstance().getPrefManager().getUser().getLevel();
        kd_area = MyApplication.getInstance().getPrefManager().getUser().getKdArea();

//        detailingAdapter = new TableDetailingAdapter(getActivity());
        qrAdapter = new TableQrAdapter(getActivity());
        utils = new Utility(getActivity());
        initView(root);
        return root;
    }

    private void initView(ViewGroup v) {
//        ibRentArc = (ImageView) v.findViewById(R.id.ib_rent_arc);
//
//        locationEdname  = (EditText) root.findViewById(R.id.txtLocationName);


        scan_btn = (Button) root.findViewById(R.id.scan_btn);
        id = (TextView) root.findViewById(R.id.id2);
        namaa = (TextView) root.findViewById(R.id.id);
        judul = (TextView) root.findViewById(R.id.judul);
        SpannableString content = new SpannableString("QRCode Scanner");
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        judul.setText(content);
        total = (EditText) root.findViewById(R.id.editText2);

        total.addTextChangedListener(new TextWatcher()

        {
            final String current = "";

            @Override
            public void beforeTextChanged(CharSequence b, int start, int count, int after) {
//                if
////                                (b.toString() == null || b.toString().length() < 1)
//                (!b.toString().equals(current)) {
//                    total.removeTextChangedListener(this);
//                    String cleanString = b.toString().replaceAll("[Rp,.]", "");
//                    double parsed = Double.parseDouble(cleanString);
//                    String formatted = NumberFormat.getInstance(Locale.GERMANY).format((parsed));
//                    // current = formatted;
//                    total.setText(formatted);
//                    total.setSelection(formatted.length());
//                    total.addTextChangedListener(this);
//                }
            }

            @Override
            public void onTextChanged(CharSequence b, int arg1, int arg2, int arg3) {
                if
                (!b.toString().equals(current)) {
                    total.removeTextChangedListener(this);
                    String cleanString = b.toString().replaceAll("[Rp,.]", "");
                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getInstance(Locale.GERMANY).format((parsed));
                    // current = formatted;
                    total.setText(formatted);
                    total.setSelection(formatted.length());
                    total.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        final Activity activity = getActivity();

        scan_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                IntentIntegrator.forSupportFragment(Reader.this).initiateScan();

//                Fragment reader = new Reader();
//                IntentIntegrator integrator = new IntentIntegrator(reader.getActivity());
////                IntentIntegrator integrator = new IntentIntegrator(getActivity());
//                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
//                integrator.setPrompt("Scan");
//                integrator.setCameraId(0);
//                integrator.setBeepEnabled(false);
//                integrator.setBarcodeImageEnabled(false);
//                integrator.initiateScan();
            }
        });


        save = (Button) v.findViewById(R.id.btn_save);
        save.setOnClickListener(saveListener);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            String barcode = result.getContents();
            String barcodez = result.getFormatName();

            String[] separated = barcode.split(" => ");
            idz = separated[0];
            kode = separated[1];
            name = separated[2];
            namaa.setText(name);
            id.setText(idz);
//        id.setText(idz);
//        Toast.makeText(getActivity(), barcode, Toast.LENGTH_SHORT).show();
//        Toast.makeText(getActivity(), barcodez, Toast.LENGTH_SHORT).show();
        }

    }

    View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            try {
                AlertConfrim();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    private String generateIdIndex(int i) {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append(userId);
        sb.append("_");
        sb.append("detail");
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));
        sb.append(String.valueOf(i));

        sb.toString();
        return sb.toString();
    }

    private String generateId() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append("qr");
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.append(String.valueOf(a));
        sb.toString();

        return sb.toString();
    }

    private void sendData(final TableQr item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.QR_ENTRY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableQrAdapter adapter = new TableQrAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "unixId", obj.getString("unixId"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("userid_pelanggan", item.getUserid_pelanggan());
                    params.put("userid_kasir", item.getUserid_kasir());
                    params.put("total_belanja", String.valueOf(item.getTotal_belanja()));
                    params.put("kode_office", item.getKode_office());
                    params.put("unixId", item.getUnixId());
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }


    public void SaveData(){
        if (total==null ||total.getText().toString().length()<1) {
            Toast.makeText(getActivity(), "Silahkan isikan total belanja", Toast.LENGTH_SHORT).show();
        }  else {

            String unix = generateId();
            int totalbelanja = Integer.parseInt(total.getText().toString().replaceAll("[Rp,.]", ""));

            qrAdapter.insertData(new TableQr(), idz, userId,
                     kd_area, totalbelanja, 0,unix);

            List<TableQr> list = qrAdapter.getDatabyCondition("flag", 0);
            for (int i=0; i<list.size(); i++) {
                TableQr item = list.get(i);
                if (item!=null) {
                    sendData(item, getActivity());
                }
            }

            Toast.makeText(getActivity(), "Data berhasil tersimpan", Toast.LENGTH_SHORT).show();

//            List<TableDetailing> list = detailingAdapter.getDatabyCondition("flag", 0);
//            for (int n=0; n<list.size();n++) {
//                TableDetailing item = list.get(n);
//                sendData(item, getActivity());
//            }

            Fragment reader = new Reader();
            ((MainMenu)getActivity()).initView(reader, "Reader");

        }
    }
    private void AlertConfrim(){
        //Context context;
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
        //String url_dlfile = varurl;
        // set title
        alertDialogBuilder.setTitle("Konfirmasi");

        // set dialog message
        alertDialogBuilder
                .setMessage("Apakah Anda Yakin ?")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SaveData();
                        // if this button is clicked, close
                        // current activity
//                        if (isi_menu.equals("savesellout")) {
//                            saveTransSellOutOffline();
//                        } else if (isi_menu.equals("saveofftake")) {
//                            saveTransOffTakeOffline();
//                        }
//                        else if (isi_menu.equals("saveofftake_m")) {
//                            saveTransOffTake_MOffline();
//                        }
//                        else if (isi_menu.equals("saveofftake_b")) {
//                            saveTransOffTake_BOffline();
//                        }
                        //id=1;
                        //   return id;
                        //MainActivity.this.finish();
                        //MainActivity.this.finish();
                    }
                }) .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, close
                // current activity

                //MainActivity.this.finish();
                //MainActivity.this.finish();
            }
        });



        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

}
