package com.ish.SaphireTransmart.main;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.database.TableMLogin;
import com.ish.SaphireTransmart.database.database_adapter.TableMLoginAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;
import com.ish.SaphireTransmart.utils.listitem_object.User;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hari H on 7/28/2016.
 */
public class LoginActivity extends AppCompatActivity {

    private String TAG = LoginActivity.class.getSimpleName(), name, password, message;
    private Button login;
    private EditText username, pass;
    private boolean isTaskRunning = false;
    private ArrayList<String> keys, parameters;
    private TableMLoginAdapter loginAdapter;
    private Utility utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Check for login session. It user is already logged in
         * redirect him to main activity
         * */
        if (MyApplication.getInstance().getPrefManager().getUser() != null) {
            startActivity(new Intent(this, MainMenu.class));
            finish();
        }
        utils = new Utility(this);
       // utils.setIMEI();
        utils.setGeoLocation();

        setContentView(R.layout.activity_login);
        login       = (Button) findViewById(R.id.btn_login);
        username    = (EditText) findViewById(R.id.username);
        pass        = (EditText) findViewById(R.id.pass);
        login.setOnClickListener(loginListener);
    }

    /**
     * Listener
     */
    View.OnClickListener loginListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            name = username.getText().toString();
            password = pass.getText().toString();

            if (name.equalsIgnoreCase("")|| password.equalsIgnoreCase("")) {
                Toast.makeText(getApplicationContext(), "Silahkan masukan username dan password", Toast.LENGTH_SHORT).show();
            } else {

                loginAdapter = new TableMLoginAdapter(LoginActivity.this);
                //loginAdapter.getDatabyCondition("username", name);
                List<TableMLogin> list = loginAdapter.getDatabyUser("userid", "password", name, password);
                if (list!=null && list.size()>0) {
                    User user = new User(list.get(0).getUserid(),
                            list.get(0).getUsername(),
                            list.get(0).getUserlevel(),
                            list.get(0).getJabatan(),
                            list.get(0).getArea(),
                            list.get(0).getKlien_layanan(),
                            list.get(0).getRegion(),
                            list.get(0).getTeritory(),
                            list.get(0).getKd_area()
                    );

                    // storing user in shared preferences
                    MyApplication.getInstance().getPrefManager().storeUser(user);

                    // start main activity
                    startActivity(new Intent(getApplicationContext(), MainMenu.class));
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Username dan password salah.", Toast.LENGTH_SHORT).show();
                }
                //new LoginTask().execute();

                /*StringRequest strReq = new StringRequest(Request.Method.POST,
                        ConnectionManager.LOGIN, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "response: " + response);

                        try {
                            JSONObject obj = new JSONObject(response);

                            // check for error flag
                            if (obj.getBoolean("error") == false) {
                                // user successfully logged in

                                JSONObject userObj = obj.getJSONObject("user");
                                User user = new User(userObj.getString("user_id"),
                                        userObj.getString("name"),
                                        userObj.getString("email"));

                                // storing user in shared preferences
                                MyApplication.getInstance().getPrefManager().storeUser(user);

                                // start main activity
                                startActivity(new Intent(getApplicationContext(), MainMenu.class));
                                finish();

                            } else {
                                // login error - simply toast the message
                                Toast.makeText(getApplicationContext(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Log.e(TAG, "json parsing error: " + e.getMessage());
                            Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                        Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("name", name);
                        params.put("email", password);

                        Log.e(TAG, "params: " + params.toString());
                        return params;
                    }
                };

                //Adding request to request queue
                MyApplication.getInstance().addToRequestQueue(strReq);*/
            }
        }
    };

    private class LoginTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (isTaskRunning) {
                return;
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                isTaskRunning = true;
                keys = new ArrayList<String>();
                parameters = new ArrayList<String>();

                //set value key
                keys.add("userid");
                keys.add("password");

                //set params
                parameters.add(name);
                parameters.add(password);

                String url = ConnectionManager.LOGIN;

                String response = ConnectionManager.requestPostData(url, keys, parameters,
                        getApplicationContext());

                JSONObject jsonObject = new JSONObject(response.toString());
                if (jsonObject.getBoolean("error")){
                    message = jsonObject.getString("message");
                } else {
                    JSONObject object = new JSONObject(jsonObject.getString("user"));
                    User user = new User(object.getString("userid"),
                            object.getString("username"),
                            object.getString("userlevel"),
                            object.getString("jabatan"),
                            object.getString("area"),
                            object.getString("klien_layanan"),
                            object.getString("region"),
                            object.getString("teritory"),
                            object.getString("kd_area")
                    );

                    // storing user in shared preferences
                    MyApplication.getInstance().getPrefManager().storeUser(user);

                    // start main activity
                    startActivity(new Intent(getApplicationContext(), MainMenu.class));
                    finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            isTaskRunning = false;
        }
    }
}
