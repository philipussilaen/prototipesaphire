package com.ish.SaphireTransmart.main.menu;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.database.TableImage;
import com.ish.SaphireTransmart.database.TableInsertion;
import com.ish.SaphireTransmart.database.TableMOffice;
import com.ish.SaphireTransmart.database.TableProduk;
import com.ish.SaphireTransmart.database.TableProdukKategori;
import com.ish.SaphireTransmart.database.TableTransInsertion;
import com.ish.SaphireTransmart.database.database_adapter.TableImageAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableInsertionAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableMOfficeAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableProdukAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableTransInsertionAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.main.MainMenu;
import com.ish.SaphireTransmart.utils.AlbumStorageDirFactory;
import com.ish.SaphireTransmart.utils.Base64;
import com.ish.SaphireTransmart.utils.BaseAlbumDirFactory;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by adminmc on 27/09/16.
 */
public class Inserting extends Fragment {
    private String TAG = Inserting.class.getSimpleName();
    private ViewGroup root;
    private List<TableInsertion> list = new ArrayList<TableInsertion>();
    private List<TableProdukKategori> list2 = new ArrayList<TableProdukKategori>();
    private List<TableProduk> list3 = new ArrayList<TableProduk>();

//    private InsertingAdapter adapter;
    private RecyclerView recyclerView;
    private TextView location;
    private ArrayAdapter<String> spinnerLocation;
    private ArrayList<String> locationList, locationIdArray;
    private String locationId, userId, clockin="";
    private TableMOfficeAdapter mOfficeAdapter;
    private Button btnSave;
    private Utility utils;
    private TableTransInsertionAdapter transOffAdapter;
    private TableInsertionAdapter insertionAdapter;
    private TableTransInsertionAdapter transInsertionAdapter;
    private TableProduk produkAdapter;
    private TableProdukAdapter mprodukAdapter;
    private LinearLayout mLayoutStock;


    private static final int TAKE_PICTURE_RENT_ARC = 121;
    private static final int TAKE_PICTURE_RENT_ARC2 = 122;
    private File f;
    private String picturePath = "", picturePath2 = "", image1="", image2="",locationName;
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private ImageView ibRentArc, ibRentArc2, closer;
    private int jmlrow,jmldata;
    private TextView TxtPicPath1,TxtPicPath2;


    public Inserting() {
        utils = new Utility(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_inserting, container, false);
        mAlbumStorageDirFactory = new BaseAlbumDirFactory();

        insertionAdapter = new TableInsertionAdapter(getActivity());
        transInsertionAdapter = new TableTransInsertionAdapter(getActivity());

        mprodukAdapter = new TableProdukAdapter(getActivity());
       // transInsertionAdapter = new TableTransInsertionAdapter(getActivity());
        userId = MyApplication.getInstance().getPrefManager().getUser().getId();

        mLayoutStock = (LinearLayout) root.findViewById(R.id.list_stock);

        location = (TextView) root.findViewById(R.id.txt_location);
        ibRentArc = (ImageView) root.findViewById(R.id.ib_insert1);
        ibRentArc2 = (ImageView) root.findViewById(R.id.ib_insert2);

        btnSave = (Button) root.findViewById(R.id.save);
        btnSave.setOnClickListener(saveListener);

        ibRentArc.setOnClickListener(ibRentArcListener);
        ibRentArc2.setOnClickListener(ibRentArcListener2);
        TxtPicPath1 = (TextView) root.findViewById(R.id.txtPictpath1);
        TxtPicPath2 = (TextView) root.findViewById(R.id.txtPictpath2);
        //prepareData();

        /*recyclerView = (RecyclerView) root.findViewById(R.id.tableList);
        adapter = new InsertingAdapter(list, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);*/
        String object = utils.getClockIn(getActivity());
        try {
            if (object!=null && !object.equalsIgnoreCase("")) {
                JSONObject obj = new JSONObject(object);
                TableMOfficeAdapter mOfficeAdapter = new TableMOfficeAdapter(getActivity());
                List<TableMOffice> listdata = mOfficeAdapter.getDatabyCondition("kode_office", obj.getString("office"));
                //  Log.d("locationid1", separated[2].toString());
                // int a = listdata.size();
                locationId = obj.getString("kode_office");

                clockin = obj.getString("date")+ " " +obj.getString("time");

                if (listdata!=null) {
                    TableMOffice item = listdata.get(listdata.size()-1);

                    locationId = item.getKode_office();
                    locationName=item.getOffice();

                    Log.d("locationid2", locationName.toString());

                }

                Log.d("locationid3",obj.getString("office"));
                location.setText(obj.getString("office"));
                location.setText(locationName);
                clockin = obj.getString("date")+ " " +obj.getString("time");
                btnSave.setEnabled(true);
            } else {
                Toast.makeText(getActivity(), "Silahkan lakukan clockin terlebih dahulu", Toast.LENGTH_SHORT).show();
                btnSave.setEnabled(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            btnSave.setEnabled(false);
        }
        list = insertionAdapter.getAllData();
        jmlrow =1;
        jmldata =0;
        for (int i=0; i<list.size();i++) {
            final TableInsertion item3 = list.get(i);
            Log.d("item3 ", item3.getNama_product());
            //  list.clear();

            // for (int i=0; i<list.size();i++) {
            //                Log.d("item1 ", list.get(i2).getNama_product().toString());

            list3 = mprodukAdapter.getDatabyCondition("isinsertion", 1);

            for (int i2=0; i2<list3.size();i2++) {
                final TableProduk item2 = list3.get(i2);
                Log.d("item2 ", item2.getModel_name());
                    if(item3.getNama_product().equals(item2.getModel_name())) {
                        addStockList(list, i);
                        jmlrow++;
                        jmldata++;
                    }

               }
        }


        return  root;
    }

    View.OnClickListener ibRentArcListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            takeImage(TAKE_PICTURE_RENT_ARC);
        }
    };

    View.OnClickListener ibRentArcListener2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            takeImage(TAKE_PICTURE_RENT_ARC2);
        }
    };

    private View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          AlertConfrim();
        }
    };

    private void addStockList(List<TableInsertion> list, final int position) {
        try {
            if (list!=null) {
                final TableInsertion item = list.get(position);
                Log.d("item ", item.getNama_product());
                final LinearLayout Mainlinear = new LinearLayout(getActivity());
                Mainlinear.setBackgroundColor(getResources().getColor(R.color.white));
                Mainlinear.setOrientation(LinearLayout.VERTICAL);

                LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                pLinearPackage.setMargins(0, 0, 0, 50);
                Mainlinear.setLayoutParams(pLinearPackage);
                ((LinearLayout) mLayoutStock).addView(Mainlinear);

                //Liner Title
                LinearLayout.LayoutParams pLineTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                LinearLayout linearTitle = new LinearLayout(getActivity());
                linearTitle.setLayoutParams(pLineTitle);
                linearTitle.setOrientation(LinearLayout.HORIZONTAL);
                linearTitle.setPadding(10, 0, 10, 0);
                ((LinearLayout) Mainlinear).addView(linearTitle);

                LinearLayout.LayoutParams pTxtTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                pTxtTitle.setMargins(0,25, 0,0);
                final TextView txtTitle = new TextView(getActivity());
                txtTitle.setLayoutParams(pTxtTitle);
                txtTitle.setText(item.getNama_product());
                txtTitle.setTypeface(null, Typeface.BOLD);
                //txtTitle.setTag(picturePath);
                txtTitle.setTextSize(16);
                txtTitle.setFreezesText(true);
                txtTitle.setPadding(5, 25, 0, 0);
                ((LinearLayout) linearTitle).addView(txtTitle);

                //Liner Title
                LinearLayout.LayoutParams pLineTitleProduct = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                LinearLayout linearTitleProduct = new LinearLayout(getActivity());
                linearTitleProduct.setLayoutParams(pLineTitleProduct);
                linearTitleProduct.setOrientation(LinearLayout.HORIZONTAL);
                linearTitleProduct.setPadding(10, 0, 10, 0);
                ((LinearLayout) Mainlinear).addView(linearTitleProduct);

                LinearLayout.LayoutParams pTxtTitleProduct = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
                final TextView txtTitleProduct = new TextView(getActivity());
                txtTitleProduct.setLayoutParams(pTxtTitleProduct);
                txtTitleProduct.setText("Stock");
                //txtTitle.setTag(picturePath);
                txtTitleProduct.setTextColor(Color.BLACK);
                txtTitleProduct.setTextSize(14);
                txtTitleProduct.setFreezesText(true);
                txtTitleProduct.setPadding(11, 11, 11, 11);
                ((LinearLayout) linearTitleProduct).addView(txtTitleProduct);

                LinearLayout.LayoutParams pEdtOutstanding = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);

                final TextView edtOutstanding = new TextView(getActivity());
                edtOutstanding.setLayoutParams(pEdtOutstanding);
                edtOutstanding.setInputType(InputType.TYPE_CLASS_NUMBER);
                edtOutstanding.setText(String.valueOf(item.getStock()));
                //edtUOM.setText("Counterpain");
                //txtTitle.setTag(picturePath);
                //edtUOM.setTextSize(14);
                edtOutstanding.setTextColor(Color.BLACK);
                edtOutstanding.setGravity(Gravity.CENTER);
                edtOutstanding.setFreezesText(true);
                edtOutstanding.setPadding(11, 11, 11, 11);
                if( position == 0 ){
                    edtOutstanding.requestFocus();
                }
                ((LinearLayout) linearTitleProduct).addView(edtOutstanding);

                //Liner Title
                LinearLayout.LayoutParams pLineProduct = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                pLineProduct.setMargins(0,5,0,0);
                LinearLayout linearProduct = new LinearLayout(getActivity());
                linearProduct.setLayoutParams(pLineProduct);
                linearProduct.setOrientation(LinearLayout.HORIZONTAL);
                linearProduct.setPadding(10, 0, 10, 0);
                ((LinearLayout) Mainlinear).addView(linearProduct);

                LinearLayout.LayoutParams pTxtUOM = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
                final TextView txtUOM = new TextView(getActivity());
                txtUOM.setLayoutParams(pTxtUOM);
                txtUOM.setText("Qty");
                //txtTitle.setTag(picturePath);
              //  txtUOM.setTextColor(Color.BLACK);
                txtUOM.setTextSize(14);
                txtUOM.setFreezesText(true);
                txtUOM.setPadding(11, 11, 11, 11);
                ((LinearLayout) linearProduct).addView(txtUOM);

                LinearLayout.LayoutParams pEdtUOM = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
              //  if (position == jmlrow-1) {
                   // pEdtUOM.setMargins(0,0,0,100);
              //  }

                final EditText edtQty = new EditText(getActivity());
                edtQty.setLayoutParams(pEdtUOM);
                edtQty.setInputType(InputType.TYPE_CLASS_NUMBER);
                edtQty.setLines(1);
                edtQty.setHint("0");
                //edtUOM.setText("Counterpain");
                //txtTitle.setTag(picturePath);
                //edtUOM.setTextSize(14);

                edtQty.setGravity(Gravity.CENTER);
                edtQty.setFreezesText(true);
                edtQty.setPadding(15, 15, 15, 10);
                edtQty.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.toString() == null || s.toString().length() < 1) {
                            //jml = 0;
                            item.setActual(0);
                            // Toast.makeText(getActivity(), "posisi="+position+"  qty="+item.getTrans(), Toast.LENGTH_SHORT).show();
                        } else {
                            int jml = Integer.parseInt(s.toString());
                            if (jml > item.getStock()) {
                                Toast.makeText(getActivity(), "Stock tidak cukup", Toast.LENGTH_SHORT).show();
                                btnSave.setEnabled(false);
                            } else {
                                item.setActual(Integer.parseInt(s.toString()));
                                btnSave.setEnabled(true);
                            }

                            //Toast.makeText(getActivity(), "posisi="+position+"  qty="+item.getTrans(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

                ((LinearLayout) linearProduct).addView(edtQty);
              //  jmldata++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendData(final TableTransInsertion item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.TRANS_INSERT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableTransInsertionAdapter adapter = new TableTransInsertionAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("id", item.getId());
                    params.put("userid", userId);
                    params.put("office", item.getKode_office());
                    params.put("product_category", item.getProduct_category());
                    params.put("product_id", item.getId_product());
                    params.put("actual", String.valueOf(item.getActual()));
                    params.put("stock", String.valueOf(item.getStock()));
                    params.put("clockin", item.getClockin());
                    params.put("image1", item.getImage1());
                    params.put("image2", item.getImage2());

                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }
    public void takeImage(int code) {
        TxtPicPath1 = (TextView) root.findViewById(R.id.txtPictpath1);
        TxtPicPath2 = (TextView) root.findViewById(R.id.txtPictpath2);
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            f = null;
            f = createImageFile(code);
            if (code == TAKE_PICTURE_RENT_ARC) {
                picturePath = f.getAbsolutePath();
                TxtPicPath1.setText(f.getAbsoluteFile().toString());
                Log.d(TAG, "f.getAbsoluteFile() "+f.getAbsoluteFile());

            } else {
                picturePath2 = f.getAbsolutePath();
                TxtPicPath2.setText(f.getAbsoluteFile().toString());
                Log.d(TAG, "f.getAbsoluteFile() "+f.getAbsoluteFile());

            }


            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            startActivityForResult(takePictureIntent, code);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create Image File
     *
     * @return
     * @throws IOException
     */
    private File createImageFile(int code) throws IOException {
        String imageFileName = "";
        switch (code) {
            case TAKE_PICTURE_RENT_ARC:
                imageFileName = generateImageName() + "_insert1";
                break;
            case TAKE_PICTURE_RENT_ARC2:
                imageFileName = generateImageName() + "_insert2";
                break;
        }

        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
        return imageF;
    }

    /**
     * image name generator
     */
    private String generateImageName() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.toString();

        return sb.toString();
    }

    /**
     * Create Folder Image
     *
     * @return
     */
    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("Saphire");
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (TAKE_PICTURE_RENT_ARC):
                if (resultCode == Activity.RESULT_OK) {
                    if(picturePath == null || picturePath.equals("")) {
                        TxtPicPath1 = (TextView) root.findViewById(R.id.txtPictpath1);
                        Log.e("TxtPicPath1", TxtPicPath1.getText().toString());
                        picturePath = TxtPicPath1.getText().toString();
                    }
                    final File file = new File(picturePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
                    if (bitmap!=null) {
                        ibRentArc.setImageBitmap(bitmap);
                    }
                    galleryAddPic(picturePath);
                }
                break;
            case (TAKE_PICTURE_RENT_ARC2):
                if (resultCode == Activity.RESULT_OK) {
                    if(picturePath2 == null || picturePath2.equals("")) {
                        TxtPicPath2 = (TextView) root.findViewById(R.id.txtPictpath2);
                        Log.e("TxtPicPath2", TxtPicPath2.getText().toString());
                        picturePath2 = TxtPicPath2.getText().toString();
                    }
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath2, options);
                    if (bitmap!=null) {
                        ibRentArc2.setImageBitmap(bitmap);
                    }
                    galleryAddPic(picturePath2);
                }
                break;

        }
    }

    private void galleryAddPic(String path) {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private String generateIdIndex(int i) {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append(userId);
        sb.append("_");
        sb.append("insert");
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));
        sb.append(String.valueOf(i));

        sb.toString();
        return sb.toString();
    }

    private String generateId() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append("insert");
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.append(String.valueOf(a));
        sb.toString();

        return sb.toString();
    }

    private void chunkImage(String image, String id) {
        try {
            TableImageAdapter imageAdapter = new TableImageAdapter(getActivity());

            String userid= MyApplication.getInstance().getPrefManager().getUser().getId();
            final File file = new File(image);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
        //    Bitmap bitmap = BitmapFactory.decodeFile(image, options);
            String filePath = Utility.compressImage(image,file.getName());

            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            byte[] data = bos.toByteArray();
            String file1 = Base64.encodeBytes(data);
            String md5 = utils.md5(file.getName());

            int limit = 10000;
            int start = 0;
            String h = "";
            String f = "";
            int tot = (int) Math.ceil((double) file1.length() / (double) limit);

            /*imageAdapter.insertData(new TableImage(), md5, file1, file.getName(), image, userid,
                    1, 1, "attendance", utils.getCurrentDateandTime(), 0);*/
            for (int i = 0; i < tot; i++) {
                if (limit < file1.length()) {
                    h = file1.substring(start, limit);
                    start = limit;
                    limit = limit + 10000;
                } else {
                    limit = file1.length();
                    h = file1.substring(start, limit);
                    f = h;
                }

                imageAdapter.insertData(new TableImage(),  generateIdIndex(i), md5, id, h, file.getName(), image, userid,
                        i+1, tot, "insert", utils.getCurrentDateandTime(), 0);
            }

            List<TableImage> listImage = imageAdapter.getDatabyCondition("flag", 0);
            for (int i =0; i<listImage.size(); i++) {
                TableImage itemImage = listImage.get(i);
                uploadImage(itemImage, getActivity());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void uploadImage(final TableImage item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.UPLOAD_IMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableImageAdapter adapter = new TableImageAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("id", String.valueOf(item.getId()));
                    params.put("image", item.getImage());
                    params.put("name", item.getName());
                    params.put("id_image", item.getId_image());
                    params.put("m_path", item.getM_path());
                    params.put("userid", item.getUserid());
                    params.put("index", String.valueOf(item.getIndex()));
                    params.put("total", String.valueOf(item.getTotal()));
                    params.put("type", item.getType());
                    params.put("id_absen", item.getId_absen());
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    public void SaveData(){
        if (location.getText().toString()==null || location.getText().toString().equalsIgnoreCase("")
                || clockin==null || clockin.equalsIgnoreCase("")) {
            Toast.makeText(getActivity(), "Silahkan lakukan clockin terlebih dahulu!", Toast.LENGTH_SHORT).show();
        }
        else if (ibRentArc.getDrawable() ==null  && ibRentArc2.getDrawable() ==null) {
            Toast.makeText(getActivity(), "Silahkan lakukan Foto terlebih dahulu!", Toast.LENGTH_SHORT).show();
        }else {
            if (picturePath!=null && picturePath.length()>1)
                image1 = new File(picturePath).getName();

            if (picturePath2!=null && picturePath2.length()>1)
                image2 = new File(picturePath2).getName();

            for (int i=0; i<list.size(); i++) {
                try {
                    String id = generateId();

                    TableInsertion item = list.get(i);

                    int qty = item.getStock();

                    if (qty > 0) {
                        transInsertionAdapter.insertData(new TableTransInsertion(), id, item.getKode_office(),
                                item.getProduct_category(), item.getId_product(), clockin, item.getStock(), item.getActual(), 0,image1,image2,userId);
                    }

                    if (i==0) {
                        if (picturePath!=null && picturePath.length()>1) {
                            chunkImage(picturePath, id);
                        }
                        if (picturePath2!=null && picturePath2.length()>1) {
                            chunkImage(picturePath2, id);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            Toast.makeText(getActivity(), "Data berhasil disimpan", Toast.LENGTH_SHORT).show();

            List<TableTransInsertion> list = transInsertionAdapter.getDatabyCondition("flag", 0);

            for (int i=0; i<list.size(); i++) {
                TableTransInsertion item = list.get(i);
                if (item!=null) {
                    sendData(item, getActivity());
                }
            }
            Fragment insert = new Inserting();
            ((MainMenu)getActivity()).initView(insert, "Inserting");
        }
    }
    private void AlertConfrim(){
        //Context context;
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
        //String url_dlfile = varurl;
        // set title
        alertDialogBuilder.setTitle("Konfirmasi");

        // set dialog message
        alertDialogBuilder
                .setMessage("Apakah Anda Yakin ?")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SaveData();
                        // if this button is clicked, close
                        // current activity
//                        if (isi_menu.equals("savesellout")) {
//                            saveTransSellOutOffline();
//                        } else if (isi_menu.equals("saveofftake")) {
//                            saveTransOffTakeOffline();
//                        }
//                        else if (isi_menu.equals("saveofftake_m")) {
//                            saveTransOffTake_MOffline();
//                        }
//                        else if (isi_menu.equals("saveofftake_b")) {
//                            saveTransOffTake_BOffline();
//                        }
                        //id=1;
                        //   return id;
                        //MainActivity.this.finish();
                        //MainActivity.this.finish();
                    }
                }) .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, close
                // current activity

                //MainActivity.this.finish();
                //MainActivity.this.finish();
            }
        });



        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

}
