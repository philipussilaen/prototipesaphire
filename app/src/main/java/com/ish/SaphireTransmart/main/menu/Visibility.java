package com.ish.SaphireTransmart.main.menu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ish.SaphireTransmart.R;
import com.ish.SaphireTransmart.database.TableImage;
import com.ish.SaphireTransmart.database.TableMOffice;
import com.ish.SaphireTransmart.database.TableVisibility;
import com.ish.SaphireTransmart.database.database_adapter.TableImageAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableMOfficeAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableVisibilityAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.main.MainMenu;
import com.ish.SaphireTransmart.utils.AlbumStorageDirFactory;
import com.ish.SaphireTransmart.utils.Base64;
import com.ish.SaphireTransmart.utils.BaseAlbumDirFactory;
import com.ish.SaphireTransmart.utils.ConnectionManager;
import com.ish.SaphireTransmart.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by adminmc on 26/09/16.
 */
public class Visibility extends Fragment {
    private String TAG = Visibility.class.getSimpleName();
    private ViewGroup root;
    private ImageView ibRentArc, ibRentArc2;
    private Button save;
    private static final int TAKE_PICTURE_RENT_ARC = 121;
    private static final int TAKE_PICTURE_RENT_ARC2 = 122;
    private File f;
    private String picturePath , picturePath2 , userId, clockin;
    private AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private Utility utils;
    ProgressDialog progressDialog;
    private TextView kategori, tipe,TxtPicPath1,TxtPicPath2;
    private String[] kategoriArrayList, tipeArrayList;
    private ArrayAdapter<String> spinnerKategori, spinnerTipe;
    private CheckBox chkBersih, chkPosisi, chkKondisi, chkTopShelf, chkFullShelf, chkGoodCondition;
    private TableVisibilityAdapter visibityAdapter;
    private String location,locationName,image1,image2;
    private EditText locationEdname;

//    public Visibility() {
//        super();
//    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       // super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (TAKE_PICTURE_RENT_ARC):
                if (resultCode == Activity.RESULT_OK) {
                    if (picturePath == null || picturePath.equals("")) {
                        TxtPicPath1 = (TextView) root.findViewById(R.id.txtPictpath1vis);
                        Log.e("TxtPicPath11", TxtPicPath1.getText().toString());
                        picturePath = TxtPicPath1.getText().toString();
                    }
                    final File file = new File(picturePath);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
                    if (bitmap != null) {
                        ibRentArc.setImageBitmap(bitmap);
                    }
                    galleryAddPic(picturePath);
                    //chunkImage(picturePath);
                }
                break;
            case (TAKE_PICTURE_RENT_ARC2):
                if (resultCode == Activity.RESULT_OK) {
                    if (resultCode == Activity.RESULT_OK) {
                        if (picturePath2 == null || picturePath2.equals("")) {
                            TxtPicPath2 = (TextView) root.findViewById(R.id.txtPictpath2vis);
                            Log.e("TxtPicPath11", TxtPicPath2.getText().toString());
                            picturePath2 = TxtPicPath2.getText().toString();
                        }
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 8;
                        Bitmap bitmap = BitmapFactory.decodeFile(picturePath2, options);
                        if (bitmap != null) {
                            ibRentArc2.setImageBitmap(bitmap);
                        }
                        galleryAddPic(picturePath2);
                        //chunkImage(picturePath2);
                    }
                    break;

                }

        }
//        Log.d(TAG, "onActivityResult " + requestCode);
//        TxtPicPath1 = (TextView) root.findViewById(R.id.txtPictpath1vis);
//        Log.e("TxtPicPath11", TxtPicPath1.getText().toString());
//    //    if (requestCode == TAKE_PICTURE_RENT_ARC && resultCode == Activity.RESULT_OK) {
//          //  if (resultCode == Activity.RESULT_OK) {
//
//                if(picturePath == null || picturePath.equals("")) {
//                    TxtPicPath1 = (TextView) root.findViewById(R.id.txtPictpath1vis);
//                    Log.e("TxtPicPath11", TxtPicPath1.getText().toString());
//                    picturePath = TxtPicPath1.getText().toString();
//                }
//
//
//
//                final File file = new File(picturePath);
//                    BitmapFactory.Options options = new BitmapFactory.Options();
//                    options.inSampleSize = 8;
//                    Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
//
////                String filePath = Utility.compressImage(picturePath,file.getName());
////                 Log.d(TAG, "onActivityResult3 "+filePath);
////                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
//
//                if (bitmap!=null) {
//                    ibRentArc.setImageBitmap(bitmap);
//                    Log.d(TAG, "onActivityResult4 "+ibRentArc.getDrawable().toString());
//                }
//                galleryAddPic(picturePath);
//                //chunkImage(picturePath);
          //  }
         //   break;
       // }
        //super.onActivityResult(requestCode, resultCode, data);
//       // else if (requestCode == 2) {
//            // data contains result
//            // Do some task
//            if(data != null) {
//                Uri selectedImage = data.getData();
//
//                Log.e("selectedImage", selectedImage.toString());
////				mCurrentPhotoPath = getPath(selectedImage);
////
////				MoveFile();
//            }
//            //   MoveFile(getPath(selectedImage),mCurrentPhotoPath));
//
//            //  Image_Selecting_Task(data);
//        }



    }
  //  @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup) inflater.inflate(R.layout.fragment_visiblity, container,false);
        mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        userId = MyApplication.getInstance().getPrefManager().getUser().getId();
        utils = new Utility(getActivity());

        visibityAdapter = new TableVisibilityAdapter(getActivity());

      //  visibityAdapter.deleteAll();
        kategoriArrayList = getActivity().getResources().getStringArray(R.array.kategori_vsibility_list);
        spinnerKategori = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item,
                kategoriArrayList);

        initView(root);

        return root;
    }

    private void initView(ViewGroup v) {
        ibRentArc = (ImageView) v.findViewById(R.id.ib_rent_arcvis);
        ibRentArc2 = (ImageView) v.findViewById(R.id.ib_rent_arc2vis);

        ibRentArc.setOnClickListener(ibRentArcListener);
        ibRentArc2.setOnClickListener(ibRentArcListener2);
        // location = (EditText) root.findViewById(R.id.txtLocation);
        locationEdname  = (EditText) v.findViewById(R.id.txtLocationName);
        TxtPicPath1 = (TextView) v.findViewById(R.id.txtPictpath1vis);
        TxtPicPath2 = (TextView) v.findViewById(R.id.txtPictpath2vis);
        Log.d("locationEdname1", locationEdname.getText().toString());
        chkBersih = (CheckBox) v.findViewById(R.id.chk_bersih_rent_arc);
        chkPosisi = (CheckBox) v.findViewById(R.id.chk_posisi_rent_arc);
        chkKondisi = (CheckBox) v.findViewById(R.id.chk_kondisi_rent_arc);
        chkTopShelf = (CheckBox) v.findViewById(R.id.chk_top_shelf);
        chkFullShelf = (CheckBox) v.findViewById(R.id.chk_full_shelf);
        chkGoodCondition = (CheckBox)  v.findViewById(R.id.chk_good_condition);

        kategori = (TextView) v.findViewById(R.id.txt_kategori_rent_arc);
        tipe = (TextView) v.findViewById(R.id.txt_type_rent_arc);

        kategori.setOnClickListener(kategoriListener);
        tipe.setOnClickListener(tipeListener);

        tipe.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (tipe.getText().toString().equalsIgnoreCase("TOP Shelfing")) {
                    chkGoodCondition.setVisibility(View.VISIBLE);
                    chkFullShelf.setVisibility(View.VISIBLE);
                    chkTopShelf.setVisibility(View.VISIBLE);

                    chkBersih.setChecked(false);
                    chkPosisi.setChecked(false);
                    chkKondisi.setChecked(false);

                    chkBersih.setVisibility(View.GONE);
                    chkPosisi.setVisibility(View.GONE);
                    chkKondisi.setVisibility(View.GONE);
                } else {
                    chkBersih.setVisibility(View.VISIBLE);
                    chkPosisi.setVisibility(View.VISIBLE);
                    chkKondisi.setVisibility(View.VISIBLE);

                    chkGoodCondition.setChecked(false);
                    chkFullShelf.setChecked(false);
                    chkTopShelf.setChecked(false);

                    chkGoodCondition.setVisibility(View.GONE);
                    chkFullShelf.setVisibility(View.GONE);
                    chkTopShelf.setVisibility(View.GONE);
                }
            }
        });

        kategori.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tipe.setText("");

                tipe.setClickable(true);
                if (kategori.getText().toString().equalsIgnoreCase("rental")){
                    tipeArrayList = getActivity().getResources().getStringArray(R.array.tipe_rental_list);

                    spinnerTipe = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item,
                            tipeArrayList);
                } else if (kategori.getText().toString().equalsIgnoreCase("non rental")) {
                    tipeArrayList = getActivity().getResources().getStringArray(R.array.tipe_nonrent_list);

                    spinnerTipe = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item,
                            tipeArrayList);
                } else if (kategori.getText().toString().equalsIgnoreCase("Kimia Farma Display")) {
                    tipeArrayList = getActivity().getResources().getStringArray(R.array.tipe_kimiafarma_list);

                    spinnerTipe = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item,
                            tipeArrayList);
                } else {
                    tipe.setText("Full Pict");
                    tipe.setClickable(false);

                    chkBersih.setChecked(false);
                    chkPosisi.setChecked(false);
                    chkKondisi.setChecked(false);

                    chkBersih.setVisibility(View.GONE);
                    chkPosisi.setVisibility(View.GONE);
                    chkKondisi.setVisibility(View.GONE);

                    chkGoodCondition.setChecked(false);
                    chkFullShelf.setChecked(false);
                    chkTopShelf.setChecked(false);

                    chkGoodCondition.setVisibility(View.GONE);
                    chkFullShelf.setVisibility(View.GONE);
                    chkTopShelf.setVisibility(View.GONE);
                }
            }
        });

        save = (Button) root.findViewById(R.id.btn_save);
        save.setOnClickListener(saveListener);

        String object = utils.getClockIn(getActivity());
        try {
            if (object!=null && !object.equalsIgnoreCase("")) {
                JSONObject obj = new JSONObject(object);
                TableMOfficeAdapter mOfficeAdapter = new TableMOfficeAdapter(getActivity());
                List<TableMOffice> listdata = mOfficeAdapter.getDatabyCondition("kode_office", obj.getString("office"));
                //  Log.d("locationid1", separated[2].toString());
                // int a = listdata.size();
                // locationId = obj.getString("kode_office");

                clockin = obj.getString("date")+ " " +obj.getString("time");

                if (listdata!=null) {
                    TableMOffice item = listdata.get(listdata.size()-1);

                    //    locationId = item.getKode_office();
                    locationName=item.getOffice();

                    Log.d("locationid2", locationName.toString());

                }

                Log.d("locationid3",obj.getString("office"));
                //   location.setText(obj.getString("office"));
                locationEdname.setText(locationName);
                //  clockin = obj.getString("date")+ " " +obj.getString("time");
                //   btnSave.setEnabled(true);
                clockin = obj.getString("date")+ " " +obj.getString("time");
                location = obj.getString("kode_office");
                // locationName = obj.getString("office");
                locationEdname.setText(locationName.toString());
                save.setEnabled(true);
            } else {
                Toast.makeText(getActivity(), "Silahkan lakukan clockin terlebih dahulu", Toast.LENGTH_SHORT).show();
                save.setEnabled(false);
            }

            Log.d("TxtPicPath1a", TxtPicPath1.getText().toString());
            Log.d("TxtPicPath2a", TxtPicPath2.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
            save.setEnabled(false);
        }
        Log.d("locationEdname", locationEdname.getText().toString());
//        Log.d("TxtPicPath1", TxtPicPath1.getText().toString());
//        Log.d("TxtPicPath2", TxtPicPath2.getText().toString());


//        picturePath = TxtPicPath1.getText().toString();
//        picturePath2 = TxtPicPath2.getText().toString();
//
//        if(picturePath != null && !picturePath.equals("")) {
//            final File file = new File(picturePath);
//           // Log.e("TxtPicPath1", TxtPicPath1.getText().toString());
//
////            BitmapFactory.Options options = new BitmapFactory.Options();
////            options.inSampleSize = 8;
////            Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
//            String filePath = Utility.compressImage(picturePath,file.getName());
//            Bitmap bitmap = BitmapFactory.decodeFile(filePath);
//
//            if (bitmap!=null) {
//                ibRentArc.setImageBitmap(bitmap);
//            }
//
//        }
//        if(picturePath2 != null && !picturePath2.equals("")) {
//
//
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inSampleSize = 8;
//            Bitmap bitmap = BitmapFactory.decodeFile(picturePath2, options);
//            if (bitmap!=null) {
//                ibRentArc2.setImageBitmap(bitmap);
//            }
//
//        }





    }


    View.OnClickListener kategoriListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Kategori")
                    .setAdapter(spinnerKategori,
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    //Log.d("MSProfile", "genderArrayList=" + genderArrayList[which].toString());
                                    kategori.setText(kategoriArrayList[which].toString());
                                    dialog.dismiss();
                                }
                            }).create().show();
        }
    };

    View.OnClickListener tipeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Tipe")
                    .setAdapter(spinnerTipe,
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    //Log.d("MSProfile", "genderArrayList=" + genderArrayList[which].toString());
                                    tipe.setText(tipeArrayList[which].toString());
                                    dialog.dismiss();
                                }
                            }).create().show();
        }
    };

    View.OnClickListener ibRentArcListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            takeImage(TAKE_PICTURE_RENT_ARC);
        }
    };

    View.OnClickListener ibRentArcListener2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            takeImage(TAKE_PICTURE_RENT_ARC2);
        }
    };

    View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            try {
               AlertConfrim();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    /**
     * Method for Take Picture From Camera
     */
    public void takeImage(int code) {
        try {
            TxtPicPath1 = (TextView) root.findViewById(R.id.txtPictpath1vis);
            TxtPicPath2 = (TextView) root.findViewById(R.id.txtPictpath2vis);

            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            f = null;
            f = createImageFile(code);
            if (code == TAKE_PICTURE_RENT_ARC) {
                picturePath = f.getAbsolutePath();

                TxtPicPath1.setText(f.getAbsoluteFile().toString());
                Log.d(TAG, "f.getAbsoluteFile() "+f.getAbsoluteFile());

            } else {
                picturePath2 = f.getAbsolutePath();
                TxtPicPath2.setText(f.getAbsoluteFile().toString());
                Log.d(TAG, "` "+f.getAbsoluteFile());

            }

            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));

            startActivityForResult(takePictureIntent, code);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create Image File
     *
     * @return
     * @throws IOException
     */
    private File createImageFile(int code) throws IOException {
        String imageFileName = "";
        switch (code) {
            case TAKE_PICTURE_RENT_ARC:
                imageFileName = generateImageName() + "_rent_arc";
                break;
            case TAKE_PICTURE_RENT_ARC2:
                imageFileName = generateImageName() + "_rent_arc2";
                break;
        }

        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
        return imageF;
    }


    /**
     * image name generator
     */
    private String generateImageName() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.toString();

        return sb.toString();
    }

    /**
     * Create Folder Image
     *
     * @return
     */
    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("Saphire");
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }



    private void galleryAddPic(String path) {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private String generateIdIndex(int i) {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append(userId);
        sb.append("_");
        sb.append("vsbl");
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));
        sb.append(String.valueOf(i));

        sb.toString();
        return sb.toString();
    }

    private String generateId() {
        String userid,yy, mm, dd, hh, ss, s;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now = new Date();
        String strDate = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);
        s = strDate.substring(17, 19);
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        userid = MyApplication.getInstance().getPrefManager().getUser().getId();

        StringBuilder sb = new StringBuilder();
        sb.append(userid);
        sb.append("_");
        sb.append("visbl");
        sb.append("_");
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);
        sb.append(s);
        sb.append(String.valueOf(a));
        sb.toString();  

        return sb.toString();
    }

    private void chunkImage(String image, String id) {
        try {
            TableImageAdapter imageAdapter = new TableImageAdapter(getActivity());

            String userid= MyApplication.getInstance().getPrefManager().getUser().getId();
            final File file = new File(image);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 1;
          //  Bitmap bitmap = BitmapFactory.decodeFile(image, options);
            String filePath = Utility.compressImage(image,file.getName());

            Bitmap bitmap = BitmapFactory.decodeFile(filePath, options);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            byte[] data = bos.toByteArray();
            String file1 = Base64.encodeBytes(data);
            String md5 = utils.md5(file.getName());

            int limit = 10000;
            int start = 0;
            String h = "";
            String f = "";
            int tot = (int) Math.ceil((double) file1.length() / (double) limit);

            /*imageAdapter.insertData(new TableImage(), md5, file1, file.getName(), image, userid,
                    1, 1, "attendance", utils.getCurrentDateandTime(), 0);*/
            for (int i = 0; i < tot; i++) {
                if (limit < file1.length()) {
                    h = file1.substring(start, limit);
                    start = limit;
                    limit = limit + 10000;
                } else {
                    limit = file1.length();
                    h = file1.substring(start, limit);
                    f = h;
                }

                imageAdapter.insertData(new TableImage(),  generateIdIndex(i), md5, id, h, file.getName(), image, userid,
                        i+1, tot, "visibility", utils.getCurrentDateandTime(), 0);
            }

            List<TableImage> listImage = imageAdapter.getDatabyCondition("flag", 0);
            for (int i =0; i<listImage.size(); i++) {
                TableImage itemImage = listImage.get(i);
                uploadImage(itemImage, getActivity());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadImage(final TableImage item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.UPLOAD_IMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableImageAdapter adapter = new TableImageAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("id", String.valueOf(item.getId()));
                    params.put("image", item.getImage());
                    params.put("name", item.getName());
                    params.put("id_image", item.getId_image());
                    params.put("m_path", item.getM_path());
                    params.put("userid", item.getUserid());
                    params.put("index", String.valueOf(item.getIndex()));
                    params.put("total", String.valueOf(item.getTotal()));
                    params.put("type", item.getType());
                    params.put("id_absen", item.getId_absen());
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void sendData(final TableVisibility item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.VISIBILITY_ENTRY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableVisibilityAdapter adapter = new TableVisibilityAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "visibility_id", obj.getString("visibility_id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("visibility_id", item.getVisibility_id());
                    params.put("userid", item.getUserid());
                    params.put("kategori", item.getKategori());
                    params.put("tipe", item.getTipe());
                    params.put("clockin", item.getClockin());
                    params.put("is_clean", String.valueOf(item.getIsClean()));
                    params.put("is_right_position", String.valueOf(item.getIsRightPosition()));
                    params.put("is_good_condition", String.valueOf(item.getIsGoodCondition()));
                    params.put("is_top_shelf", String.valueOf(item.getIsTopShelf()));
                    params.put("is_shelf_full", String.valueOf(item.getIsSheflFull()));
                    //   params.put("office", String.valueOf(item.getOffice()));
                    params.put("image1", item.getImage1());
                    params.put("image2", item.getImage2());
                    params.put("office", item.getOffice());
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }
    public void SaveData(){
        if (kategori==null || kategori.getText().toString().length()<1) {
            Toast.makeText(getActivity(), "Silahkan pilih kategori!", Toast.LENGTH_SHORT).show();
        } else if (!kategori.getText().toString().equalsIgnoreCase("Full Branding")
                &&(tipe==null || tipe.getText().toString().length()<1)) {
            Toast.makeText(getActivity(), "Silahkan pilih tipe!", Toast.LENGTH_SHORT).show();

        } else {
            int vChkkondisi=0, vChkFull=0, vChkTop=0, vChkClean=0, vChkPosisi=0;

            if (chkGoodCondition.isChecked() || chkKondisi.isChecked())
                vChkkondisi = 1;

            if (chkBersih.isChecked())
                vChkClean = 1;

            if (chkPosisi.isChecked())
                vChkPosisi = 1;

            if (chkTopShelf.isChecked())
                vChkTop = 1;

            if (chkFullShelf.isChecked())
                vChkFull = 1;

            String id = generateId();
            if (picturePath!=null && picturePath.length()>1) {
                image1 = new File(picturePath).getName();
            }

            if (picturePath2!=null && picturePath2.length()>1) {
                image2 = new File(picturePath2).getName();
            }

            Toast.makeText(getActivity(), "Data berhasil tersimpan", Toast.LENGTH_SHORT).show();

            visibityAdapter.insertData(new TableVisibility(), id, userId, kategori.getText().toString(),
                    tipe.getText().toString(), clockin, vChkClean, vChkPosisi, vChkkondisi,
                    vChkTop, vChkFull, 0, location,image1,image2);



            List<TableVisibility> list = visibityAdapter.getDatabyCondition("flag", 0);
            for (int i=0; i<list.size(); i++) {
                TableVisibility item = list.get(i);
                if (item!=null) {
                    sendData(item, getActivity());
                }
            }

            if (picturePath!=null && picturePath.length()>1){
                chunkImage(picturePath, id);
            }

            if (picturePath2!=null && picturePath2.length()>1) {
                chunkImage(picturePath2, id);
            }
            Fragment visibility = new Visibility();
           ((MainMenu)getActivity()).initView(visibility, "Visibility");

        }
    }
    private void AlertConfrim(){
        //Context context;
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());
        //String url_dlfile = varurl;
        // set title
        alertDialogBuilder.setTitle("Konfirmasi");

        // set dialog message
        alertDialogBuilder
                .setMessage("Apakah Anda Yakin ?")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SaveData();
                        // if this button is clicked, close
                        // current activity
//                        if (isi_menu.equals("savesellout")) {
//                            saveTransSellOutOffline();
//                        } else if (isi_menu.equals("saveofftake")) {
//                            saveTransOffTakeOffline();
//                        }
//                        else if (isi_menu.equals("saveofftake_m")) {
//                            saveTransOffTake_MOffline();
//                        }
//                        else if (isi_menu.equals("saveofftake_b")) {
//                            saveTransOffTake_BOffline();
//                        }
                        //id=1;
                        //   return id;
                        //MainActivity.this.finish();
                        //MainActivity.this.finish();
                    }
                }) .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // if this button is clicked, close
                // current activity

                //MainActivity.this.finish();
                //MainActivity.this.finish();
            }
        });



        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }
}
