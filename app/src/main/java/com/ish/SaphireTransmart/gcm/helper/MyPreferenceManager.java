package com.ish.SaphireTransmart.gcm.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.ish.SaphireTransmart.utils.listitem_object.User;

/**
 * Created by Lincoln on 07/01/16.
 */
public class MyPreferenceManager {

    private String TAG = MyPreferenceManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "ish_gcm";

    // All Shared Preferences Keys
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_USER_NAME = "user_name";
    private static final String KEY_USER_EMAIL = "user_email";
    private static final String KEY_USER_REGION = "user_region";
    private static final String KEY_USER_TERITORY = "user_teritory";
    private static final String KEY_USER_LEVEL = "user_level";
    private static final String KEY_USER_KDAREA = "user_kdarea";
//    private static final String KEY_USER_ = "user_region";
//    private static final String KEY_USER_REGION = "user_region";
//    private static final String KEY_USER_REGION = "user_region";
//    private static final String KEY_USER_REGION = "user_region";
//    private static final String KEY_USER_REGION = "user_region";
    private static final String KEY_NOTIFICATIONS = "notifications";

    // Constructor
    public MyPreferenceManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void storeUser(User user) {
        editor.putString(KEY_USER_ID, user.getId());
        editor.putString(KEY_USER_NAME, user.getName());
        editor.putString(KEY_USER_EMAIL, user.getEmail());
        editor.putString(KEY_USER_REGION, user.getRegion());
        editor.putString(KEY_USER_TERITORY, user.getTeritory());
        editor.putString(KEY_USER_LEVEL, user.getLevel());
        editor.putString(KEY_USER_KDAREA, user.getKdArea());
        editor.commit();

        Log.e(TAG, "User is stored in shared preferences. " + user.getName() + ", " + user.getEmail()+ ", " + user.getRegion()+ ", " + user.getTeritory());
    }

    public User getUser() {
        if (pref.getString(KEY_USER_ID, null) != null) {
            String id, name, email, region,teritory,level,kd_area;
            id = pref.getString(KEY_USER_ID, null);
            name = pref.getString(KEY_USER_NAME, null);
            email = pref.getString(KEY_USER_EMAIL, null);
            region = pref.getString(KEY_USER_REGION, null);
            teritory = pref.getString(KEY_USER_TERITORY, null);
            level = pref.getString(KEY_USER_LEVEL, null);
            kd_area = pref.getString(KEY_USER_KDAREA, null);

            User user = new User(id, name, email, region,teritory,level,kd_area);
            return user;
        }
        return null;
    }

    public void addNotification(String notification) {

        // get old notifications
        String oldNotifications = getNotifications();

        if (oldNotifications != null) {
            oldNotifications += "|" + notification;
        } else {
            oldNotifications = notification;
        }

        editor.putString(KEY_NOTIFICATIONS, oldNotifications);
        editor.commit();
    }

    public String getNotifications() {
        return pref.getString(KEY_NOTIFICATIONS, null);
    }

    public void clear() {
        editor.clear();
        editor.commit();
    }
}
