package com.ish.SaphireTransmart.utils;

/**
 * Created by Hari on 7/28/2016.
 */
public class Config {
    public static final String URL_HTTP_STRING = "http://";
    public static final String URL_HTTPS_STRING = "https://";

    /*
   |-----------------------------------------------------------------------------------------------
   | URL server for this application to transfer data.
   |-----------------------------------------------------------------------------------------------
   */
//    public static final String url_Server = "saphireapi.ish-solutions.com";//"saphireapi.ish-solutions.com"; yangini
    //  public static final String url_Server = "222.124.13.123:780/saphireapi.ish-solutions.com";//"saphireapi.ish-solutions.com";
//    public static final String url_Server = "192.168.43.229/saphireapi.ish-solutions.com";//
    public static final String url_Server = "192.168.43.184/saphireapi.ish-solutions.com";//
//    public static final String url_Server = "saphireacpi.ish-solutions.com";//
//    public static final String url_Server = "haier.ish-solutions.com";//
    /*
    |-----------------------------------------------------------------------------------------------
    | URL server local for this application to transfer data.
    |-----------------------------------------------------------------------------------------------
    */
    public static final String url_Server_local = "192.168.84.188";
    public static final String url_server_proxy = "";

    /*
    |-----------------------------------------------------------------------------------------------
    | Port server for this application to transfer data.
    |-----------------------------------------------------------------------------------------------
    */
    public static final String port_Server = "";
    public static final String port_Server_proxy = "";
    public static final String version = "3";

    /*
   |-----------------------------------------------------------------------------------------------
   | Key for json
   |-----------------------------------------------------------------------------------------------
   */
    public static final String KEY_JSON_PROFILE = "json";

    /*
   |-----------------------------------------------------------------------------------------------
   | Key for imei
   |-----------------------------------------------------------------------------------------------
   */
    public static final String KEY_IMEI = "imei";


    /*
   |-----------------------------------------------------------------------------------------------
   | Database name
   |-----------------------------------------------------------------------------------------------
   */
    public static final String DATABASE_NAME = "sss";

    /*
   |-----------------------------------------------------------------------------------------------
   | Database path
   |-----------------------------------------------------------------------------------------------
   */
    public static final String DATABASE_PATH = "/data/data/com.ish.SaphireTransmart/databases/";

    /*
    |-----------------------------------------------------------------------------------------------
    | Database version
    |-----------------------------------------------------------------------------------------------
    */
    //public static final int DATABASE_VERSION = 1;
    public static final int DATABASE_VERSION = 3;

    /*
  |-----------------------------------------------------------------------------------------------
  | Database delay time
  |-----------------------------------------------------------------------------------------------
  */
    public static final int DB_CLOSE_DELAY_TIME = 5000;

    /*
   |-----------------------------------------------------------------------------------------------
   | Key for json respon
   |-----------------------------------------------------------------------------------------------
   */
    public static final String KEY_STATUS = "status";
    public static final String KEY_DEVICE_ID = "device_id";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_MESSAGE = "message";

    /*
    |-----------------------------------------------------------------------------------------------
    | Method for append URL and URI as API
    |-----------------------------------------------------------------------------------------------
    */
    public static String makeUrlString(String uri) {
        StringBuilder url = new StringBuilder(URL_HTTP_STRING);
        url.append(url_Server);
        url.append("/");
        url.append(uri);
        return url.toString();
    }

    public static String makeUrlLocalString(String uri) {
        StringBuilder url = new StringBuilder(URL_HTTP_STRING);
        url.append(url_Server_local);
        url.append("/");
        url.append(uri);
        return url.toString();
    }


    /*
    |-----------------------------------------------------------------------------------------------
    | flag to identify whether to show single line or multi line test push notification tray
    |-----------------------------------------------------------------------------------------------
    */
    public static boolean appendNotificationMessages = true;

    /*
    |-----------------------------------------------------------------------------------------------
    | global topic to receive app wide push notifications
    |-----------------------------------------------------------------------------------------------
    */
    public static final String TOPIC_GLOBAL = "global";

    /*
    |-----------------------------------------------------------------------------------------------
    | broadcast receiver intent filters
    |-----------------------------------------------------------------------------------------------
    */
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    /*
    |-----------------------------------------------------------------------------------------------
    | type of push messages
    |-----------------------------------------------------------------------------------------------
    */
    public static final int PUSH_TYPE_CHATROOM = 1;
    public static final int PUSH_TYPE_USER = 2;

    /*
    |-----------------------------------------------------------------------------------------------
    | id to handle the notification in the notification try
    |-----------------------------------------------------------------------------------------------
    */
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

}
