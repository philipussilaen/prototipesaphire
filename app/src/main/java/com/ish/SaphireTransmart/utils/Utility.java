package com.ish.SaphireTransmart.utils;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Location;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ish.SaphireTransmart.database.TableAttendance;
import com.ish.SaphireTransmart.database.TableImage;
import com.ish.SaphireTransmart.database.TableJadwalAbsen;
import com.ish.SaphireTransmart.database.TableJadwalAbsenMobile;
import com.ish.SaphireTransmart.database.TableTrackLog;
import com.ish.SaphireTransmart.database.database_adapter.TableAttendanceAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableImageAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableJadwalAbsenAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableJadwalAbsenMobileAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableTrackLogAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;
import com.ish.SaphireTransmart.utils.listitem_object.AttendanceModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

/**
 * Created by Hari H on 7/28/2016.
 */
public class Utility {

    private static String TAG = Utility.class.getSimpleName();
    private String longitude, latitude, addressLine="",deviceID="";
    private Context context;
    private GPSTracker gpsTracker;
    public static String time="0000-00-00 00:00:00";
    public static String date="", onlineTime="";

    public Utility(Context context) {
        this.context = context;
    }

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TimePickerDialog timePickerDialog;

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;

    /*
    |-----------------------------------------------------------------------------------------------
    | Dialog for alert no internet connection
    |-----------------------------------------------------------------------------------------------
    */
    public void showErrorDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                new ContextThemeWrapper(context,
                        android.R.style.Theme_Dialog));
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("Close",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.setIcon(android.R.drawable.stat_sys_warning);
        alert.show();
    }


    /*
   |-----------------------------------------------------------------------------------------------
   | Dialog for alert request failed
   |-----------------------------------------------------------------------------------------------
   */
    public void showAlertDlg(Handler handler, String message, Context context) {
        final String strMessage = message;
        final Context mContext = context;
        handler.post(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        new ContextThemeWrapper(mContext,
                                android.R.style.Theme_Dialog));
                builder.setMessage(strMessage)
                        .setCancelable(false)
                        .setNegativeButton("close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.setIcon(android.R.drawable.stat_sys_warning);
                alertDialog.show();
            }
        });
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Set mobile IMEI
    |-----------------------------------------------------------------------------------------------
    */
    public void setIMEI() {
        try {
            final TelephonyManager tm =(TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            deviceID = tm.getDeviceId();
            SharedPreferences imeiPref = context.getSharedPreferences(Config.KEY_IMEI, Context.MODE_PRIVATE);
            SharedPreferences.Editor shareEditor = imeiPref.edit();
            shareEditor.putString("device_id", deviceID);
            shareEditor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get mobile IMEI
    |-----------------------------------------------------------------------------------------------
    */
    public String getIMEI() {
        try {
            SharedPreferences sessionpref = context.getSharedPreferences(Config.KEY_IMEI, Context.MODE_PRIVATE);
            deviceID = sessionpref.getString("device_id", null);
            Log.d("DatabaseManager", "imei= " + deviceID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deviceID;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Set session clockin
    |-----------------------------------------------------------------------------------------------
    */
    public void setClockIn(Context context, AttendanceModel model) {
        try {
            JSONObject obj = new JSONObject();
            obj.put("userid", model.getUserid());
            obj.put("date", model.getTanggal());
            obj.put("time", model.getWaktu());
            obj.put("status", model.getStatus());
            obj.put("jenis", model.getJenis());
            obj.put("office", model.getOffice());
            obj.put("kode_office", model.getKode_office());
            //   obj.put("officename", model.getOffice());

            SharedPreferences imeiPref = context.getSharedPreferences("attendance", Context.MODE_PRIVATE);
            SharedPreferences.Editor shareEditor = imeiPref.edit();
            shareEditor.putString("clockin", obj.toString());
            shareEditor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void clearClockin() {
        SharedPreferences imeiPref = context.getSharedPreferences("attendance", Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor = imeiPref.edit();
        shareEditor.clear();
        shareEditor.commit();
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get session clockin
    |-----------------------------------------------------------------------------------------------
    */
    public String getClockIn(Context context) {
        String time = "";
        try {
            SharedPreferences sessionpref = context.getSharedPreferences("attendance", Context.MODE_PRIVATE);
            time = sessionpref.getString("clockin", null);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    /*
   |-----------------------------------------------------------------------------------------------
   | Method for Location
   |-----------------------------------------------------------------------------------------------
   */
    public void setGeoLocation() {
        try {
            gpsTracker = new GPSTracker(context);
            if (gpsTracker.getIsGPSTrackingEnabled()) {
                latitude    = String.valueOf(gpsTracker.latitude);
                longitude   = String.valueOf(gpsTracker.longitude);
                addressLine = gpsTracker.getAddressLine(context);
            } else {
                gpsTracker.showSettingsAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isGpsEnable() {
        try {
            if (gpsTracker!=null && gpsTracker.getIsGPSTrackingEnabled()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void showGpsTrackerAlert() {
        try {
            gpsTracker.showSettingsAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getLongitude() {
        if (this.longitude != null) {
            return this.longitude;
        }
        return "";
    }

    public String getLatitude() {
        if (this.latitude != null) {
            return this.latitude;
        }
        return "";
    }

    public Double getGPSLat () {
        return gpsTracker.latitude;
    }

    public Double getGPSLng () {
        return gpsTracker.longitude;
    }

    public String getAddressLine() {
        if (this.addressLine != null) {
            return this.addressLine;
        }
        return "";
    }

    /*
   |-----------------------------------------------------------------------------------------------
   | Method for get mac address
   |-----------------------------------------------------------------------------------------------
   */


    /*
   |-----------------------------------------------------------------------------------------------
   | Method for get ip address
   |-----------------------------------------------------------------------------------------------
   */
    public static String getMobileIP() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface
                    .getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf
                        .getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ipaddress = inetAddress .getHostAddress().toString();
                        Log.e(TAG, "Exception in Get IP Address: " + ipaddress);
                        //ip_address = Integer.toString(ipaddress);
                        return ipaddress;

                    }
                }
            }

        } catch (SocketException ex) {
            Log.i(TAG, "Exception in Get IP Address: " + ex.toString());
        }
        return null;
    }

    /**
     * Method to get current date
     */

    public String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return  formattedDate;
    }
    public String getCurrentDateNumber(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd");
        String formattedDate = df.format(c.getTime());
        return  formattedDate;
    }

    public String getCurrentDateandTime(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String formattedDate = df.format(c.getTime());
        return  formattedDate;
    }

    public String getCurrentTime() {
        String locaTime ="";
        try {
            Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT+7:00"));
            Date currentLocalTime = c.getTime();
            DateFormat date = new SimpleDateFormat("HH:mm:ss");
            date.setTimeZone(TimeZone.getTimeZone("GMT+7:00"));
            locaTime = date.format(currentLocalTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return locaTime;
    }

    /**
     * Method for set time field
     */
    public void setTimeField(Context context, TimePickerDialog timePickerDialog, TextView textView) {
        final TextView time = textView;
        Calendar newCalendar = Calendar.getInstance();
        timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minuteOfDay) {
                time.setText(hourOfDay + ":" + minuteOfDay);
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

    /**
     * Method for set date field
     */
    public void setDateField(Context context, DatePickerDialog datePickerDialog, SimpleDateFormat dateFormatter,
                             TextView textView) {
        final TextView date = textView;
        dateFormatter = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        final SimpleDateFormat simpleDateFormat = dateFormatter;

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                date.setText(simpleDateFormat.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    /**
     * Part of Floating Button library
     */
    public static int dpToPx(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return Math.round(dp * scale);
    }

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static int distance(Location StartP, Location EndP) {
       /* Location loc1 = new Location("");
        loc1.setLatitude(lat1);
        loc1.setLongitude(lon1);

        Location loc2 = new Location("");
        loc2.setLatitude(lat2);
        loc2.setLongitude(lon2);*/

        float distanceInMeters = StartP.distanceTo(EndP);
        int dist = (int) distanceInMeters;

        return dist;

    }

    public static int distances(Location StartP, Location EndP) {
        double earthRadius = 6371000; //meters
        double lat1 = StartP.getLatitude();
        double lat2 = EndP.getLatitude();
        double lng1 = StartP.getLongitude();
        double lng2 = EndP.getLongitude();

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        int dist = (int) (earthRadius * c);
        return dist;
    }

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = Utility.getConnectivityStatus(context);
        String status = null;
        if (conn == Utility.TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == Utility.TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == Utility.TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }

    public String getStringImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageByte = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageByte, Base64.DEFAULT);
        return encodedImage;
    }

    public void setOnlineTime(String lat, String lon) {
        fetchTimeFromInternet(lat, lon);
    }

    public static String getOnlineDate() throws ParseException {
        String initDateFormat;
        String endDateFormat;
        initDateFormat = "yyyy-MM-dd HH:mm";

        endDateFormat = "yyyy-MM-dd";
        Date initDate = new SimpleDateFormat(initDateFormat).parse(time);
        SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
        date = formatter.format(initDate);

        return date;
    }

    public static String getOnlineTime() throws ParseException {
        String initDateFormat;
        String endDateFormat;
        initDateFormat = "yyyy-MM-dd HH:mm";

        endDateFormat = "HH:mm";
        Date initDate = new SimpleDateFormat(initDateFormat).parse(time);
        SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
        onlineTime = formatter.format(initDate);

        return onlineTime;
    }

    private void fetchTimeFromInternet(String lat, String lon) {

        StringRequest strReq = new StringRequest(Request.Method.GET,
                "http://api.geonames.org/timezoneJSON?lat="+lat+"&lng="+lon+"&username=jancuk", new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    time = obj.getString("time");
                } catch (Exception e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(SplashScreenActivity.this, "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(SplashScreenActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }
    public static String compressImage(String imageUri,String imageFileName) {

        // String filePath = getRealPathFromURI(imageUri);
        String filePath = imageUri;
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow abbott to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;




        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename(imageFileName);
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public static String getFilename(String imageFileName) {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "/U2FwaGlyZV9waWN1cGxvYWQ/CompressImage");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + imageFileName + ".jpg");
        return uriSting;

    }
    public void TrackLog(String userId)
    {
        Log.d(TAG, "Tracklog ");
        //toastHandler.sendEmptyMessage(0);
        try {
            //   setGeoLocation();
            String time = getCurrentDateandTime();
            String lng = getLongitude();
            String lat = getLatitude();
            Log.e("TrackLogService", "time="+time+"  longitude="+lng+"  latitude="+lat);
            TableTrackLogAdapter adapter = new TableTrackLogAdapter(context);

            adapter.insertData(new TableTrackLog(), generateId("track",userId), userId, getCurrentDateandTime(),
                    getLongitude(), getLatitude(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String generateId(String nameid,String userId) {
        long unix = System.currentTimeMillis() / 1000L;
        Random r = new Random();
        int a = r.nextInt(80 - 65) + 65;
        StringBuilder sb = new StringBuilder();
        sb.append(nameid);
        sb.append("_");
        sb.append(userId);
        sb.append("_");
        sb.append(unix);
        sb.append(String.valueOf(a));

        sb.toString();
        return sb.toString();
    }

    public void DeleteTask()
    {
        Log.d(TAG, "DeleteTask ");
        //toastHandler.sendEmptyMessage(0);
        try {
//            TableDetailingAdapter detailingAdapter = new TableDetailingAdapter(context);
//            List<TableDetailing> listDetailing = detailingAdapter.getAllData();
//            for (int i=0;i<listDetailing.size();i++) {
//                TableDetailing item = listDetailing.get(i);
//                if (getWeeks(item.getClockin(), getCurrentDate()) >= 14) {
//                    detailingAdapter.delete(context, item.getId());
//                }
//            }

            TableAttendanceAdapter adapter = new TableAttendanceAdapter(context);
            List<TableAttendance> listAttendance = adapter.getAllData();
            for (int i=0;i<listAttendance.size();i++) {
                TableAttendance item = listAttendance.get(i);
                if (getWeeks(item.getTanggal(), getCurrentDate()) >= 14) {
                    adapter.delete(context, item.getId());
                }
            }

            TableImageAdapter imageAdapter = new TableImageAdapter(context);
            List<TableImage> listImage = imageAdapter.getAllData();
            for (int i=0;i<listImage.size();i++) {
                TableImage item = listImage.get(i);
                if (getWeeks(item.getDate(), getCurrentDateandTime()) >= 14) {
                    imageAdapter.delete(context, item.getId());
                }
            }

            TableJadwalAbsenAdapter JadwalAdapter = new TableJadwalAbsenAdapter(context);
            List<TableJadwalAbsen> listjadwal = JadwalAdapter.getAllData();
            for (int i=0;i<listjadwal.size();i++) {
                TableJadwalAbsen item = listjadwal.get(i);
                if (getWeeks(item.getDate(), getCurrentDateandTime()) >= 14) {
                    JadwalAdapter.delete(context, item.getId());
                }
            }
            TableJadwalAbsenMobileAdapter JadwalMobileAdapter = new TableJadwalAbsenMobileAdapter(context);
            List<TableJadwalAbsenMobile> listjadwalmobile = JadwalMobileAdapter.getAllData();
            for (int i=0;i<listjadwal.size();i++) {
                TableJadwalAbsenMobile item = listjadwalmobile.get(i);
                if (getWeeks(item.getDate(), getCurrentDateandTime()) >= 14) {
                    JadwalMobileAdapter.delete(context, item.getId());
                }
            }

//            TableTansOfftakeAdapter transOffAdapter = new TableTansOfftakeAdapter(context);
//            List<TableTansOfftake> listTransOfftake = transOffAdapter.getAllData();
//            for (int i=0;i<listTransOfftake.size();i++) {
//                TableTansOfftake item = listTransOfftake.get(i);
//                if (getWeeks(item.getTime(), getCurrentDateandTime()) >= 14) {
//                    transOffAdapter.delete(context, item.getId());
//                }
//            }
//
//            TableTransOGimmickAdapter transOutGimmick = new TableTransOGimmickAdapter(context);
//            List<TableTransOutGimmick> listGimmick = transOutGimmick.getAllData();
//            for (int i=0;i<listGimmick.size();i++) {
//                TableTransOutGimmick item = listGimmick.get(i);
//                if (getWeeks(item.getDate(), getCurrentDateandTime()) >= 14) {
//                    transOutGimmick.delete(context, item.getId());
//                }
//            }
//
//            TableVisibilityAdapter visibilityAdapter = new TableVisibilityAdapter(context);
//            List<TableVisibility> listVisibility = visibilityAdapter.getAllData();
//            for (int i=0;i<listVisibility.size();i++) {
//                TableVisibility item = listVisibility.get(i);
//                if (getWeeks(item.getClockin(), getCurrentDateandTime()) >= 14) {
//                    visibilityAdapter.delete(context, item.getVisibility_id());
//                }
//            }
//
//            TableFormUpdateAdapter formUpdateAdapter = new TableFormUpdateAdapter(context);
//            List<TableFormUpdate> listFormUpdate = formUpdateAdapter.getAllData();
//            for (int i=0;i<listFormUpdate.size();i++) {
//                TableFormUpdate item = listFormUpdate.get(i);
//                if (getWeeks(item.getLup(), getCurrentDateandTime()) >= 14) {
//                    formUpdateAdapter.delete(context, item.getId());
//                }
//            }
            TableTrackLogAdapter tracklogAdapter = new TableTrackLogAdapter(context);
            List<TableTrackLog> listtracklog = tracklogAdapter.getAllData();
            for (int i=0;i<listjadwal.size();i++) {
                TableTrackLog item = listtracklog.get(i);
                if (getWeeks(item.getDate(), getCurrentDateandTime()) >= 14) {
                    tracklogAdapter.delete(context, 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static int getWeeks(String aa, String bb) {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date a, b;
        int workDays = 0;

        try {
            a = df.parse(aa);
            b = df.parse(bb);

            Calendar startCal = Calendar.getInstance();
            startCal.setTime(a);

            Calendar endCal = Calendar.getInstance();
            endCal.setTime(b);

            //Return 0 if start and end are the same
            if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
                return 0;
            }

            if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
                startCal.setTime(a);
                endCal.setTime(b);
            }

            do {
                //excluding start date
                startCal.add(Calendar.DAY_OF_MONTH, 1);
                if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                    ++workDays;
                }
            } while (startCal.getTimeInMillis() < endCal.getTimeInMillis()); //excluding end date

        } catch (Exception e) {
            e.printStackTrace();
        }
        return workDays;
    }
    public String getCurrentDateandTimeSec(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return  formattedDate;
    }
    public void Upload_Tracklog() {
        try {
            TableTrackLogAdapter adapter = new TableTrackLogAdapter(context);

            List<TableTrackLog> list = adapter.getDatabyCondition("flag", 0);
            Log.e(TAG, "tracklogsend ="+list.size());
            //adapter.delete(ctx, 0);
            for (int j=0; j<list.size();j++) {
                sendDataTracklog(list.get(j));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public void sendDataTracklog(final TableTrackLog item) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.TRACKLOG_ENTRY, new Response.Listener<String>() {
            TableTrackLogAdapter adapter = new TableTrackLogAdapter(context);

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        //listAttendanceModel.clear();
                        adapter.updatePartial(context, "flag", 1, "trackid", obj.getString("trackid"));
                        adapter.delete(context, 1);
                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(SplashScreenActivity.this, "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("userid", item.getUserid());
                params.put("date", item.getDate());
                params.put("trackid", item.getTrackid());
                params.put("lng", item.getLng());
                params.put("lat", item.getLat());

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }
}
