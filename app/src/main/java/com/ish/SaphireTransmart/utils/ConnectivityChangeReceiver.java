package com.ish.SaphireTransmart.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ish.SaphireTransmart.database.TableAttendance;
import com.ish.SaphireTransmart.database.TableCompetitor;
import com.ish.SaphireTransmart.database.TableDamageGood;
import com.ish.SaphireTransmart.database.TableFeedback;
import com.ish.SaphireTransmart.database.TableImage;
import com.ish.SaphireTransmart.database.TablePromo;
import com.ish.SaphireTransmart.database.TableTansSellOut;
import com.ish.SaphireTransmart.database.database_adapter.TableAttendanceAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableCompetitorAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableDamageGoodAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableFeedbackAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableImageAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TablePromoAdapter;
import com.ish.SaphireTransmart.database.database_adapter.TableTansSellOutAdapter;
import com.ish.SaphireTransmart.gcm.app.MyApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by adminmc on 05/09/16.
 */
public class ConnectivityChangeReceiver extends BroadcastReceiver {

    private String TAG = ConnectivityChangeReceiver.class.getSimpleName();
    @Override
    public void onReceive(final Context context, final Intent intent) {

        int status = Utility.getConnectivityStatus(context);

        if (status==0) {

        } else {
            TableAttendanceAdapter adapter = new TableAttendanceAdapter(context);

            TableImageAdapter imageAdapter = new TableImageAdapter(context);
            TablePromoAdapter promoAdapter = new TablePromoAdapter(context);
            TableTansSellOutAdapter transSellOutAdapter = new TableTansSellOutAdapter(context);
            TableDamageGoodAdapter damagegoodAdapter = new TableDamageGoodAdapter(context);
            TableCompetitorAdapter competitorAdapter = new TableCompetitorAdapter(context);
            TableFeedbackAdapter feedbackAdapter = new TableFeedbackAdapter(context);


            List<TableAttendance> list = adapter.getDatabyCondition(TableAttendance.FLAG, 0);
            for (int i=0; i<list.size(); i++) {
                TableAttendance item = list.get(i);
                if (item!=null) {
                    sendData(item, context);
                }
            }



            List<TableImage> listImage = imageAdapter.getDatabyCondition("flag", 0);
            for (int i =0; i<listImage.size(); i++) {
                TableImage itemImage = listImage.get(i);
                uploadImage(itemImage, context);
            }

            List<TablePromo> listPromo = promoAdapter.getDatabyCondition("flag", 0);
            for (int i=0; i<listPromo.size(); i++) {

                TablePromo item = listPromo.get(i);
                if (item!=null) {
                    sendDataPromo(item, context);
                }
            }

            List<TableTansSellOut> listTransSell = transSellOutAdapter.getDatabyCondition("flag", 0);
            for (int i=0; i<listTransSell.size(); i++) {
                TableTansSellOut item = listTransSell.get(i);
                if (item!=null) {
                    sendDataSellOut(item, context);
                }
            }

            List<TableDamageGood> listDamage = damagegoodAdapter.getDatabyCondition("flag", 0);
            for (int i=0; i<listDamage.size(); i++) {

                TableDamageGood item = listDamage.get(i);
                if (item!=null) {
                    sendDataDamage(item, context);
//if (picturePath != null && picturePath.length() > 1)

                }
            }
            List<TableCompetitor> listCompetitor = competitorAdapter.getDatabyCondition("flag", 0);
            for (int i=0; i<listCompetitor.size(); i++) {

                TableCompetitor item = listCompetitor.get(i);
                if (item!=null) {
                    sendDataCompetitor(item, context);
//if (picturePath != null && picturePath.length() > 1)

                }
            }

            List<TableFeedback> listFeed = feedbackAdapter.getDatabyCondition("flag", 0);
            for (int i=0; i<listFeed.size(); i++) {

                TableFeedback item = listFeed.get(i);
                if (item!=null) {
                    sendDataFeedback(item, context);
//if (picturePath != null && picturePath.length() > 1)

                }
            }


        }
        //Toast.makeText(context, status, Toast.LENGTH_LONG).show();
    }


    private void sendDataFeedback(final TableFeedback item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.SELLOUT_FEEDBACK, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableFeedbackAdapter adapter = new TableFeedbackAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "unixId", obj.getString("unixId"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {

                    params.put("kode_office", item.getKode_office());
//                    params.put("kategori_sellout", item.getKategori_sellout());
                    params.put("product_type", item.getProduct_type());
                    params.put("customer_name", item.getCustomer_name());;
                    params.put("phone", item.getPhone());
                    params.put("feedback", item.getFeedback());
                    params.put("tgl_input", item.getTgl_input());
                    params.put("status", item.getStatus());
                    params.put("userid", item.getUserid());
                    params.put("unixId", item.getUnixId());

                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }


    private void sendDataCompetitor(final TableCompetitor item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.SELLOUT_COMPETITOR, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableCompetitorAdapter adapter = new TableCompetitorAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "unixId", obj.getString("unixId"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("perner", item.getPerner());
                    params.put("nama", item.getNama());
                    params.put("kode_office", item.getKode_office());
//                    params.put("kategori_sellout", item.getKategori_sellout());
                    params.put("product", item.getProduct());
                    params.put("normalprice", String.valueOf(item.getNormalPrice()));;
                    params.put("promoprice", String.valueOf(item.getPromoPrice()));;
                    params.put("competitor", String.valueOf(item.getCompetitor()));;
                    params.put("keterangan", item.getKeterangan());
                    params.put("periodeawal", item.getPeriodeawal());
                    params.put("periodeakhir", item.getPeriodeakhir());
                    params.put("tgl_input", item.getTgl_input());
                    params.put("flag", String.valueOf(item.getFlag()));
                    params.put("unixId", item.getUnixId());
                    if(item.getPhoto() == null || item.getPhoto() == ""){
                        params.put("photo", "kosong");
                    }else{
                        params.put("photo", item.getPhoto());
                    }
                    if(item.getPhoto2() == null || item.getPhoto2() == ""){
                        params.put("photo2", "kosong.jpg");
                    }else{
                        params.put("photo2", item.getPhoto2());
                    }
                    if(item.getPhoto3() == null || item.getPhoto3() == ""){
                        params.put("photo3", "kosong.jpg");
                    }else{
                        params.put("photo3", item.getPhoto3());
                    }
                    if(item.getPhoto4() == null || item.getPhoto4() == ""){
                        params.put("photo4", "kosong.jpg");
                    }else{
                        params.put("photo4", item.getPhoto4());
                    }
                    if(item.getPhoto5() == null || item.getPhoto5() == ""){
                        params.put("photo5", "kosong.jpg");
                    }else{
                        params.put("photo5", item.getPhoto5());
                    }
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void sendDataDamage(final TableDamageGood item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.SELLOUT_DAMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableDamageGoodAdapter adapter = new TableDamageGoodAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "unixId", obj.getString("unixId"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("kode_office", item.getKode_office());
                    params.put("product_type", item.getProduct_type());
                    params.put("serialnumber", String.valueOf(item.getSerial_number()));;
                    params.put("qty", String.valueOf(item.getQty()));;
                    params.put("typedamage", String.valueOf(item.getType_damage()));;
                    params.put("keterangan", item.getKeterangan());
                    params.put("unixId", item.getUnixId());
                    params.put("status", item.getStatus());

                    params.put("tgl_input", item.getTgl_input());
                    params.put("userid", item.getUserid());
                    params.put("flag", String.valueOf(item.getFlag()));
                    params.put("unixId", item.getUnixId());
                    if(item.getPhoto() == null || item.getPhoto() == ""){
                        params.put("photo", "kosong");
                    }else{
                        params.put("photo", item.getPhoto());
                    }
                    if(item.getPhoto2() == null || item.getPhoto2() == ""){
                        params.put("photo2", "kosong.jpg");
                    }else{
                        params.put("photo2", item.getPhoto2());
                    }
                    if(item.getPhoto3() == null || item.getPhoto3() == ""){
                        params.put("photo3", "kosong.jpg");
                    }else{
                        params.put("photo3", item.getPhoto3());
                    }
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void sendDataSellOut(final TableTansSellOut item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.SELLOUT_ENTRY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableTansSellOutAdapter adapter = new TableTansSellOutAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "unixId", obj.getString("unixId"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("userid", item.getUserid());
                    params.put("tgl_input", item.getTgl_input());
                    params.put("kode_office", item.getKode_office());
//                    params.put("kategori_sellout", item.getKategori_sellout());
                    params.put("id_product", item.getId_product());
                    params.put("total_jual", String.valueOf(item.getTotal_jual()));
                    params.put("qty", String.valueOf(item.getQty()));
                    params.put("flag", String.valueOf(item.getFlag()));
//                    params.put("clockin", item.getClockin());
                    params.put("unixId", item.getUnixId());
                    params.put("name", item.getNama());
                    params.put("address", item.getAddress());
                    params.put("phone", item.getPhone());
                    params.put("instalation", String.valueOf(item.getInstalation()));
                    if(item.getPhoto() == null || item.getPhoto() == ""){
                        params.put("photo", "kosong");
                    }else{
                        params.put("photo", item.getPhoto());
                    }

                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void sendDataPromo(final TablePromo item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.SELLOUT_PROMO, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TablePromoAdapter adapter = new TablePromoAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "unixId", obj.getString("unixId"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }



                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {

                    params.put("kode_office", item.getKode_office());
//                    params.put("kategori_sellout", item.getKategori_sellout());
                    params.put("product_type", item.getProduct_type());
                    params.put("competitor", item.getCompetitor());
                    params.put("activity", item.getActivity());
                    params.put("area_activity", item.getArea_activity());

                    params.put("tgl_input", item.getTgl_input());
                    params.put("userid", item.getUserid());
                    params.put("flag", String.valueOf(item.getFlag()));
                    params.put("unixId", item.getUnixId());
                    params.put("keterangan", item.getKeterangan());
                    params.put("periodeawal", item.getPeriodeawal());
                    params.put("periodeakhir", item.getPeriodeakhir());
                    if(item.getPhoto() == null || item.getPhoto() == ""){
                        params.put("photo", "kosong");
                    }else{
                        params.put("photo", item.getPhoto());
                    }
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void sendData(final TableAttendance item,final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.ATTENDANCE_ENTRY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableAttendanceAdapter adapter = new TableAttendanceAdapter(context);
                        adapter.updatePartial(context, TableAttendance.FLAG, 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("id_a", item.getId());
                    params.put("user_id", item.getUserid());
                    params.put("date", item.getTanggal());
                    params.put("waktu", item.getWaktu());
                    params.put("status", item.getStatus());
                    params.put("lng", item.getLng());
                    params.put("lat", item.getLat());
                    params.put("office", item.getOffice());
                    params.put("jenis", item.getJenis());
                    params.put("photo_status", item.getPhoto_status());
                    params.put("photo_name", item.getPhoto_name());
                    params.put("ip_address", item.getIp_address());
                    params.put("mc_address", item.getMc_address());
                    params.put("imei", item.getImei());

                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }



    private void uploadImage(final TableImage item, final Context context) {
        StringRequest strReq = new StringRequest(Request.Method.POST,
                ConnectionManager.UPLOAD_IMAGE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
                    if (obj.getBoolean("error") == false) {
                        TableImageAdapter adapter = new TableImageAdapter(context);
                        adapter.updatePartial(context, "flag", 1, "id", obj.getString("id"));

                    } else {
                        // error in fetching chat rooms
                        //Toast.makeText(getActivity(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    //Toast.makeText(getActivity(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                //Toast.makeText(getActivity(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if (item!=null) {
                    params.put("id", String.valueOf(item.getId()));
                    params.put("image", item.getImage());
                    params.put("name", item.getName());
                    params.put("id_image", item.getId_image());
                    params.put("m_path", item.getM_path());
                    params.put("userid", item.getUserid());
                    params.put("index", String.valueOf(item.getIndex()));
                    params.put("total", String.valueOf(item.getTotal()));
                    params.put("type", item.getType());
                    params.put("id_absen", item.getId_absen());
                }

                Log.e(TAG, "params: " + params.toString());
                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }


}
