package com.ish.SaphireTransmart.utils.listitem_object;

import java.io.Serializable;

/**
 * Created by Lincoln on 07/01/16.
 */
public class User implements Serializable {
    String id, name, email, level, jabatan, area, klienLayanan, region,teritory,kd_area;

    public User() {
    }

    public User(String id, String name, String email, String region, String teritory,String level,String kd_area) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.region = region;
        this.teritory = teritory;
        this.level = level;
        this.kd_area = kd_area;
    }

    public User(String id, String name, String level, String jabatan, String area,
                String klienLayanan, String region,String teritory,String kd_area) {
        this.id = id;
        this.name = name;
        this.level = level;
        this.jabatan = jabatan;
        this.area = area;
        this.klienLayanan = klienLayanan;
        this.region = region;
        this.teritory = teritory;
        this.kd_area = kd_area;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getArea() {
        return area;
    }

    public void setKdArea(String kd_area) {
        this.kd_area = kd_area;
    }

    public String getKdArea() {
        return kd_area;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setKlienLayanan(String klienLayanan) {
        this.klienLayanan = klienLayanan;
    }

    public String getKlienLayanan() {
        return klienLayanan;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getLevel() {
        return level;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setTeritory(String teritory) {
        this.teritory = teritory;
    }

    public String getTeritory() {
        return teritory;
    }
}
