package com.ish.SaphireTransmart.utils;

import android.os.Environment;
import android.util.Log;

import java.io.File;

public final class BaseAlbumDirFactory extends AlbumStorageDirFactory {

	// Standard storage location for digital camera files

	private static final String CAMERA_DIR = "/";
	private  String storageDir ;

	@Override
	public File getAlbumStorageDir(String albumName) {
		storageDir = Environment.getExternalStorageDirectory() + "/U2FwaGlyZV9waWN1cGxvYWQ=";
		File dir = new File(storageDir);

		Log.i("photo", "photo location = " + storageDir);
		if (!dir.exists())
			dir.mkdir();

		return new File (
//				Environment.getExternalStorageDirectory()
//				+ CAMERA_DIR
				storageDir+"/"
				+ albumName
		);
	}
}
