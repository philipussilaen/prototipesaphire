package com.ish.SaphireTransmart.utils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Hari H on 7/28/2016.
 */
public class ConnectionManager {

    //public static final String BASE_URL           = "http://192.168.84.219/gcm_chat/v1";
    public static final String LOGIN                = Config.makeUrlString("saphire_qr/v1/user/login");// BASE_URL + "/user/login";
    public static final String USER                 = Config.makeUrlString("saphire_qr/v1/user/_ID_");//BASE_URL + "/user/_ID_";
    public static final String CHAT_ROOMS           = Config.makeUrlString("saphire_qr/v1/chat_rooms");//BASE_URL + "/chat_rooms";
    public static final String CHAT_USERS           = Config.makeUrlString("saphire_qr/v1/chat_users");//BASE_URL + "/chat_users";
    public static final String CHAT_THREAD          = Config.makeUrlString("saphire_qr/v1/chat_rooms/_ID_");//BASE_URL + "/chat_rooms/_ID_";
    public static final String CHAT_THREAD_USERS    = Config.makeUrlString("saphire_qr/v1/chat_users/_ID_");//BASE_URL + "/chat_users/_ID_";
    public static final String CHAT_ROOM_MESSAGE    = Config.makeUrlString("saphire_qr/v1/chat_rooms/_ID_/message");//BASE_URL + "/chat_rooms/_ID_/message";
    public static final String USERS_MESSAGE        = Config.makeUrlString("saphire_qr/v1/chat_users/_ID_/message");//BASE_URL + "/chat_users/_ID_/message";
    public static final String LIST_CATEGORY        = Config.makeUrlString("saphire_qr/v1/list_catalog");
    public static final String MASTER_ABSEN         = Config.makeUrlString("saphire_qr/v1/master/jadwal_absen");
    public static final String MASTER_ABSEN_MOBILE  = Config.makeUrlString("saphire_qr/v1/master/jadwal_absen_mobile");

    public static final String MASTER_APP_VERSION   = Config.makeUrlString("saphire_qr/v1/master/app_version");
    public static final String MASTER_M_OFFICE      = Config.makeUrlString("saphire_qr/v1/master/m_office");
    public static final String MASTER_M_LOGIN       = Config.makeUrlString("saphire_qr/v1/master/m_login");
    public static final String MASTER_M_PRODUK      = Config.makeUrlString("saphire_qr/v1/master/m_product");
    public static final String MASTER_M_PRODUK_C    = Config.makeUrlString("saphire_qr/v1/master/m_product_category");
  //  public static final String MASTER_M_PRODUK      = Config.makeUrlString("saphire_haier/v1/master/m_product");
    public static final String EDUCATION_ENTRY       = Config.makeUrlString("saphire_qr/v1/education/entry");
    public static final String EDUCATIONPLAN_ENTRY          = Config.makeUrlString("saphire_qr/v1/educationplan/entry");
    public static final String MASTER_M_JUDUL      = Config.makeUrlString("saphire_qr/v1/master/m_judul");
    public static final String MASTER_M_PEMBICARA      = Config.makeUrlString("saphire_qr/v1/master/m_pembicara");
    public static final String MASTER_M_GIMMICK     = Config.makeUrlString("saphire_qr/v1/master/trans_stock_gimmick");
    public static final String ATTENDANCE_MOBILE    = Config.makeUrlString("saphire_qr/v1/attendace_data/mobile");
    public static final String ATTENDANCE_FETCH     = Config.makeUrlString("saphire_qr/v1/attendace_data");
    public static final String ATTENDANCE_ENTRY     = Config.makeUrlString("saphire_qr/v1/attendace/entry");
    public static final String UPLOAD_SIGNATURE     = Config.makeUrlString("saphire_qr/v1/image/signature");
    public static final String UPLOAD_IMAGE         = Config.makeUrlString("saphire_qr/v1/image/upload");
    public static final String UPLOAD_IMAGE2         = Config.makeUrlString("saphire_qr/v1/upload_photoabsenmobile.php");
    public static final String SELLOUT_COMPETITOR         = Config.makeUrlString("saphire_qr/v1/competitor/entry");
    public static final String SELLOUT_DAMAGE       = Config.makeUrlString("saphire_qr/v1/damagegood/entry");
    public static final String SELLOUT_FEEDBACK       = Config.makeUrlString("saphire_qr/v1/feedback/entry");
    public static final String SELLOUT_PROMO       = Config.makeUrlString("saphire_qr/v1/promo/entry");
    public static final String SELLOUT_ENTRY          = Config.makeUrlString("saphire_qr/v1/sellout/entry");
    public static final String EDUCATIONPLANGET_ENTRY          = Config.makeUrlString("saphire_qr/v1/educationplan/get");

    public static final String STOCK_ENTRY          = Config.makeUrlString("saphire_qr/v1/stock/entry");
    public static final String VISIBILITY_ENTRY     = Config.makeUrlString("saphire_qr/v1/visibility/entry");
    public static final String TRACKLOG_ENTRY       = Config.makeUrlString("saphire_qr/v1/tracklog/entry");
    public static final String DETAILING_ENTRY      = Config.makeUrlString("saphire_qr/v1/detailing/entry");
    public static final String TRANS_GIMMICK        = Config.makeUrlString("saphire_qr/v1/trans_gimmick/entry");
    public static final String TRANS_INSERT         = Config.makeUrlString("saphire_qr/v1/trans_insertion/entry");
    public static final String OTHER_INFO           = Config.makeUrlString("saphire_qr/v1/otherinfo/entry");
    public static final String UPDATE_M_OFFICE      = Config.makeUrlString("saphire_qr/v1/m_office/update");
    public static final String MASTER_M_KATEGORI     = Config.makeUrlString("saphire_qr/v1/master/m_kategori");
    public static final String MASTER_M_TYPEDAMAGE     = Config.makeUrlString("saphire_qr/v1/master/m_type");
    public static final String MASTER_M_COMPETITOR     = Config.makeUrlString("saphire_qr/v1/master/m_competitor");
    public static final String MASTER_M_STATUSDAMAGE     = Config.makeUrlString("saphire_qr/v1/master/m_statusdmg");
    public static final String MASTER_M_STATUSFEED     = Config.makeUrlString("saphire_qr/v1/master/m_statusfeed");
    public static final String DAMAGE_ENTRY     = Config.makeUrlString("saphire_qr/v1/damagegood/entry");
    public static final String QR_ENTRY     = Config.makeUrlString("saphire_qr/v1/qr/entry");
    public static final String DISPLAY_ENTRY        = Config.makeUrlString("saphire_qr/v1/display/entry");



//




//    public static final String LOGIN                = Config.makeUrlString("saphire_ish/v1/user/login");// BASE_URL + "/user/login";
//    public static final String USER                 = Config.makeUrlString("saphire_ish/v1/user/_ID_");//BASE_URL + "/user/_ID_";
//    public static final String CHAT_ROOMS           = Config.makeUrlString("saphire_ish/v1/chat_rooms");//BASE_URL + "/chat_rooms";
//    public static final String CHAT_USERS           = Config.makeUrlString("saphire_ish/v1/chat_users");//BASE_URL + "/chat_users";
//    public static final String CHAT_THREAD          = Config.makeUrlString("saphire_ish/v1/chat_rooms/_ID_");//BASE_URL + "/chat_rooms/_ID_";
//    public static final String CHAT_THREAD_USERS    = Config.makeUrlString("saphire_ish/v1/chat_users/_ID_");//BASE_URL + "/chat_users/_ID_";
//    public static final String CHAT_ROOM_MESSAGE    = Config.makeUrlString("saphire_ish/v1/chat_rooms/_ID_/message");//BASE_URL + "/chat_rooms/_ID_/message";
//    public static final String USERS_MESSAGE        = Config.makeUrlString("saphire_ish/v1/chat_users/_ID_/message");//BASE_URL + "/chat_users/_ID_/message";
//    public static final String LIST_CATEGORY        = Config.makeUrlString("saphire_ish/v1/list_catalog");
//    public static final String MASTER_ABSEN         = Config.makeUrlString("saphire_ish/v1/master/jadwal_absen");
//    public static final String MASTER_ABSEN_MOBILE  = Config.makeUrlString("saphire_ish/v1/master/jadwal_absen_mobile");
//
//    public static final String MASTER_APP_VERSION   = Config.makeUrlString("saphire_ish/v1/master/app_version");
//    public static final String MASTER_M_OFFICE      = Config.makeUrlString("saphire_ish/v1/master/m_office");
//    public static final String MASTER_M_LOGIN       = Config.makeUrlString("saphire_ish/v1/master/m_login");
//    public static final String MASTER_M_PRODUK      = Config.makeUrlString("saphire_ish/v1/master/m_product");
//    public static final String MASTER_M_PRODUK_C    = Config.makeUrlString("saphire_ish/v1/master/m_product_category");
//  //  public static final String MASTER_M_PRODUK      = Config.makeUrlString("saphire_haier/v1/master/m_product");
//    public static final String EDUCATION_ENTRY       = Config.makeUrlString("saphire_ish/v1/education/entry");
//    public static final String EDUCATIONPLAN_ENTRY          = Config.makeUrlString("saphire_ish/v1/educationplan/entry");
//    public static final String MASTER_M_JUDUL      = Config.makeUrlString("saphire_ish/v1/master/m_judul");
//    public static final String MASTER_M_PEMBICARA      = Config.makeUrlString("saphire_ish/v1/master/m_pembicara");
//    public static final String MASTER_M_GIMMICK     = Config.makeUrlString("saphire_ish/v1/master/trans_stock_gimmick");
//    public static final String ATTENDANCE_MOBILE    = Config.makeUrlString("saphire_ish/v1/attendace_data/mobile");
//    public static final String ATTENDANCE_FETCH     = Config.makeUrlString("saphire_ish/v1/attendace_data");
//    public static final String ATTENDANCE_ENTRY     = Config.makeUrlString("saphire_ish/v1/attendace/entry");
//    public static final String UPLOAD_SIGNATURE     = Config.makeUrlString("saphire_ish/v1/image/signature");
//    public static final String UPLOAD_IMAGE         = Config.makeUrlString("saphire_ish/v1/image/upload");
//    public static final String UPLOAD_IMAGE2         = Config.makeUrlString("saphire_ish/v1/upload_photoabsenmobile.php");
//    public static final String SELLOUT_COMPETITOR         = Config.makeUrlString("saphire_ish/v1/competitor/entry");
//    public static final String SELLOUT_DAMAGE       = Config.makeUrlString("saphire_ish/v1/damagegood/entry");
//    public static final String SELLOUT_FEEDBACK       = Config.makeUrlString("saphire_ish/v1/feedback/entry");
//    public static final String SELLOUT_PROMO       = Config.makeUrlString("saphire_ish/v1/promo/entry");
//    public static final String SELLOUT_ENTRY          = Config.makeUrlString("saphire_ish/v1/sellout/entry");
//    public static final String EDUCATIONPLANGET_ENTRY          = Config.makeUrlString("saphire_ish/v1/educationplan/get");
//    public static final String DISPLAY_ENTRY        = Config.makeUrlString("saphire_ish/v1/display/entry");
//
//    public static final String STOCK_ENTRY          = Config.makeUrlString("saphire_ish/v1/stock/entry");
//    public static final String VISIBILITY_ENTRY     = Config.makeUrlString("saphire_ish/v1/visibility/entry");
//    public static final String TRACKLOG_ENTRY       = Config.makeUrlString("saphire_ish/v1/tracklog/entry");
//    public static final String DETAILING_ENTRY      = Config.makeUrlString("saphire_ish/v1/detailing/entry");
//    public static final String TRANS_GIMMICK        = Config.makeUrlString("saphire_ish/v1/trans_gimmick/entry");
//    public static final String TRANS_INSERT         = Config.makeUrlString("saphire_ish/v1/trans_insertion/entry");
//    public static final String OTHER_INFO           = Config.makeUrlString("saphire_ish/v1/otherinfo/entry");
//    public static final String UPDATE_M_OFFICE      = Config.makeUrlString("saphire_ish/v1/m_office/update");
//    public static final String MASTER_M_KATEGORI     = Config.makeUrlString("saphire_ish/v1/master/m_kategori");
//    public static final String MASTER_M_TYPEDAMAGE     = Config.makeUrlString("saphire_ish/v1/master/m_type");
//    public static final String MASTER_M_COMPETITOR     = Config.makeUrlString("saphire_ish/v1/master/m_competitor");
//    public static final String MASTER_M_STATUSDAMAGE     = Config.makeUrlString("saphire_ish/v1/master/m_statusdmg");
//    public static final String MASTER_M_STATUSFEED     = Config.makeUrlString("saphire_ish/v1/master/m_statusfeed");
//    public static final String DAMAGE_ENTRY     = Config.makeUrlString("saphire_ish/v1/damagegood/entry");
//    public static final String QR_ENTRY     = Config.makeUrlString("saphire_ish/v1/qr/entry");


    private static String executeHttpClientPost(String requestURL, HashMap<String, String> params, Context context) throws IOException {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(params));

            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            }
            else {
                response="";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private static String executeHttpClientGet(String requestURL, Context context) throws  IOException {
        URL url;
        //String response = "";
        try {
            URL obj = new URL(requestURL);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            con.setReadTimeout(10000);
            con.setConnectTimeout(10000);
            con.setDoInput(true);
            con.setDoOutput(true);

            int responseCode = con.getResponseCode();
            //System.out.println("\nSending 'GET' request to URL : " + url);
            //System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //print result
            System.out.println(response.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static String executeHttpClientPost(String requestURL, String params, Context context) throws IOException {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(params);

            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            }
            else {
                response="";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public static String connectHttpPost(String url, HashMap<String, String> params, Context context) throws IOException {
        return executeHttpClientPost(url, params, context);
    }

    public static String connectHttpGet(String url, Context context) throws IOException {
        return executeHttpClientGet(url, context);
    }

    public static String connectHttpPost(String url,  String params, Context context) throws IOException {
        return executeHttpClientPost(url, params, context);
    }

    public static String requestLogin (String url, String username, String password, String imei,String version,
                                       Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("username", username);
        param.put("password", password);
        param.put("imei", imei);
        param.put("version", version);
        return connectHttpPost(url, param, context);
    }

    public static String requestPostData (String url, ArrayList<String> keys, ArrayList<String> params,
                                          Context context) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        try {
            if (keys.size()==params.size()) {
                for (int i=0; i<keys.size(); i++) {
                    param.put(keys.get(i), params.get(i));
                }
                return connectHttpPost(url, param, context);
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
