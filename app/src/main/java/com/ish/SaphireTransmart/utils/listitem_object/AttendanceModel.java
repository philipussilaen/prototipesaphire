package com.ish.SaphireTransmart.utils.listitem_object;

import java.io.Serializable;

/**
 * Created by adminmc on 28/09/16.
 */
public class AttendanceModel implements Serializable {

    String userid, tanggal, waktu, status, jenis, office, kode_office;

    public AttendanceModel(String userid, String tanggal, String waktu, String status, String jenis,
                           String office, String kode_office) {
        this.userid = userid;
        this.tanggal = tanggal;
        this.waktu = waktu;
        this.status = status;
        this.jenis = jenis;
        this.office = office;
        this.kode_office = kode_office;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserid() {
        if (userid!=null) {
            return userid;
        }
        return "";
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getJenis() {
        if (jenis!=null) {
            return jenis;
        }
        return "";
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getOffice() {
        if (office!=null) {
            return office;
        }
        return "";
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        if (status!=null) {
            return status;
        }
        return "";
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getTanggal() {
        if (tanggal!=null) {
            return tanggal;
        }
        return "";
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getWaktu() {
        if (waktu!=null) {
            return waktu;
        }
        return "";
    }

    public void setKode_office(String kode_office) {
        this.kode_office = kode_office;
    }

    public String getKode_office() {
        if (kode_office==null) {
            return "";
        }
        return kode_office;
    }
}
