package com.ish.SaphireTransmart.utils.listitem_object;

import java.io.Serializable;

/**
 * Created by adminmc on 20/09/16.
 */
public class Stock implements Serializable {

    private String nama;
    private String gram;
    private String image;
    private int qty;
    private String stock;

    public Stock(String nama, String gram, String image, int qty, String stock) {
        this.nama = nama;
        this.gram = gram;
        this.image = image;
        this.qty = qty;
        this.stock = stock;
    }

    public void setGram(String gram) {
        this.gram = gram;
    }

    public String getGram() {
        return gram;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getQty() {
        return qty;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getStock() {
        return stock;
    }
}
