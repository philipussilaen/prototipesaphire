package com.ish.SaphireTransmart.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ish.SaphireTransmart.database.TableAppVersion;
import com.ish.SaphireTransmart.database.TableAttendance;
import com.ish.SaphireTransmart.database.TableCompetitor;
import com.ish.SaphireTransmart.database.TableDamageGood;
import com.ish.SaphireTransmart.database.TableDetailing;
import com.ish.SaphireTransmart.database.TableDisplay;
import com.ish.SaphireTransmart.database.TableFeedback;
import com.ish.SaphireTransmart.database.TableImage;
import com.ish.SaphireTransmart.database.TableInsertion;
import com.ish.SaphireTransmart.database.TableJadwalAbsen;
import com.ish.SaphireTransmart.database.TableJadwalAbsenMobile;
import com.ish.SaphireTransmart.database.TableJudul;
import com.ish.SaphireTransmart.database.TableKategori;
import com.ish.SaphireTransmart.database.TableMCompetitor;
import com.ish.SaphireTransmart.database.TableMLogin;
import com.ish.SaphireTransmart.database.TableMOffice;
import com.ish.SaphireTransmart.database.TableMSIgnature;
import com.ish.SaphireTransmart.database.TablePembicara;
import com.ish.SaphireTransmart.database.TableProduk;
import com.ish.SaphireTransmart.database.TableProdukKategori;
import com.ish.SaphireTransmart.database.TablePromo;
import com.ish.SaphireTransmart.database.TableQr;
import com.ish.SaphireTransmart.database.TableSellOut;
import com.ish.SaphireTransmart.database.TableStatusDamage;
import com.ish.SaphireTransmart.database.TableStatusFeed;
import com.ish.SaphireTransmart.database.TableTansOfftake;
import com.ish.SaphireTransmart.database.TableTansSellOut;
import com.ish.SaphireTransmart.database.TableTrackLog;
import com.ish.SaphireTransmart.database.TableTransEducation;
import com.ish.SaphireTransmart.database.TableTransEducationPlan;
import com.ish.SaphireTransmart.database.TableTransGimmick;
import com.ish.SaphireTransmart.database.TableTransInsertion;
import com.ish.SaphireTransmart.database.TableTransOutGimmick;
import com.ish.SaphireTransmart.database.TableTypeDamage;
import com.ish.SaphireTransmart.database.TableVisibility;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by Hari H on 7/28/2016.
 */
public class DatabaseManager extends OrmLiteSqliteOpenHelper {


    private Dao<TableMLogin, String> tableListMLoginDAO = null;
    private Dao<TableAttendance, String> tableAttendanceDAO = null;
    private Dao<TableMOffice, Integer> tableMOfficeDAO = null;
    private Dao<TableJadwalAbsen, Integer> tableJadwalAbsenDAO = null;
    private Dao<TableAppVersion, String> tableAppVersionDAO = null;
    private Dao<TableTrackLog, String> tableTrackLogDAO = null;
    private Dao<TableImage, Integer> tableImageDAO = null;
//    private Dao<TableCompetitor, Integer> tableCompetitor = null;
    private Dao<TableStatusFeed, Integer> tableStatusFeed = null;
    private Dao<TableTransEducation, Integer> tableTransEducationDAO = null;
    private Dao<TableTransEducationPlan, Integer> tableTransEducationPlanDAO = null;
    private Dao<TableTransGimmick, String> tableTransGimmicksDAO = null;
    private Dao<TableDetailing, String> tableDetailingDAO = null;
    private Dao<TableKategori, String> tableKategoriDAO = null;
    private Dao<TableCompetitor, Integer> tableCompetitor = null;
    private Dao<TableSellOut, Integer> tableSellOut = null;
    private Dao<TableDamageGood, Integer> tableDamageGood = null;
    private Dao<TableStatusDamage, Integer> tableStatusDamage = null;
    private Dao<TableMSIgnature, String> tableSignDAO = null;
    private Dao<TableProdukKategori, String> tableProdukKategoriDAO = null;
    private Dao<TableTansOfftake, Integer> tableTransOfftakeDAO = null;
    private Dao<TableInsertion, String> tableInsertionDAO = null;
    private Dao<TableTransInsertion, String> tableTransInsertionDAO = null;
    private Dao<TableVisibility, String> tableVisibilityDAO = null;
    private Dao<TableTransOutGimmick, String> tableTransOGimmicksDAO = null;
    private Dao<TableJudul, String> tableJudulDAO = null;
    private Dao<TablePembicara, String> tablePembicaraDAO = null;
    private Dao<TableTypeDamage, Integer> tableTypeDamage = null;
    private Dao<TableMCompetitor, Integer> tableMCompetitor = null;
    private Dao<TableFeedback, Integer> tableFeedback = null;
    private Dao<TablePromo, Integer> tablePromo = null;
//    private Dao<TableTansSellOut, Integer> tableTransSellOutDAO = null;
    private Dao<TableProduk, String> tableProdukDAO = null;
//    private Dao<TableSellOut, String> tableSellOutDAO = null;
    private Dao<TableTansSellOut, Integer> tableTransSellOutDAO = null;
    private Dao<TableQr, Integer> tableTransQrDAO = null;
    private Dao<TableDisplay, Integer> tableDisplay = null;



    private Dao<TableJadwalAbsenMobile, Integer> tableJadwalAbsenMobileDAO = null;



    public DatabaseManager(Context context) {
        super(context, Config.DATABASE_PATH + Config.DATABASE_NAME, null, Config.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {

            TableUtils.createTable(connectionSource, TableMLogin.class);
            TableUtils.createTable(connectionSource, TableAttendance.class);
            TableUtils.createTable(connectionSource, TableMOffice.class);
            TableUtils.createTable(connectionSource, TableJadwalAbsen.class);
            TableUtils.createTable(connectionSource, TableAppVersion.class);
            TableUtils.createTable(connectionSource, TableKategori.class);
            TableUtils.createTable(connectionSource, TableCompetitor.class);
            TableUtils.createTable(connectionSource, TableSellOut.class);
            TableUtils.createTable(connectionSource, TableVisibility.class);
            TableUtils.createTable(connectionSource, TableTransGimmick.class);
            TableUtils.createTable(connectionSource, TableDetailing.class);
            TableUtils.createTable(connectionSource, TableProdukKategori.class);
            TableUtils.createTable(connectionSource, TableInsertion.class);
            TableUtils.createTable(connectionSource, TableTansOfftake.class);
            TableUtils.createTable(connectionSource, TableTransInsertion.class);
            TableUtils.createTable(connectionSource, TableTransEducation.class);
            TableUtils.createTable(connectionSource, TableTransEducationPlan.class);
            TableUtils.createTable(connectionSource, TableDamageGood.class);
            TableUtils.createTable(connectionSource, TableTypeDamage.class);
            TableUtils.createTable(connectionSource, TableMCompetitor.class);
            TableUtils.createTable(connectionSource, TableFeedback.class);
            TableUtils.createTable(connectionSource, TablePromo.class);
            TableUtils.createTable(connectionSource, TableProduk.class);
            TableUtils.createTable(connectionSource, TableStatusFeed.class);
            TableUtils.createTable(connectionSource, TableMSIgnature.class);
            TableUtils.createTable(connectionSource, TableTransOutGimmick.class);
            TableUtils.createTable(connectionSource, TableJudul.class);
            TableUtils.createTable(connectionSource, TablePembicara.class);
//            TableUtils.createTable(connectionSource, TableSellOut.class);
//            TableUtils.createTable(connectionSource, TableTansSellOut.class);
            TableUtils.createTable(connectionSource, TableTansSellOut.class);
            TableUtils.createTable(connectionSource, TableStatusDamage.class);

            TableUtils.createTable(connectionSource, TableImage.class);

            TableUtils.createTable(connectionSource, TableJadwalAbsenMobile.class);
            TableUtils.createTable(connectionSource, TableQr.class);
            TableUtils.createTable(connectionSource, TableDisplay.class);

        } catch (SQLException e) {
            Log.e("this", "Can't create database", e);
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {
        try {

            TableUtils.dropTable(connectionSource, TableMLogin.class, true);
            TableUtils.dropTable(connectionSource, TableAttendance.class, true);
            TableUtils.dropTable(connectionSource, TableMOffice.class, true);
            TableUtils.dropTable(connectionSource, TableJadwalAbsen.class, true);
            TableUtils.dropTable(connectionSource, TableAppVersion.class, true);
            TableUtils.dropTable(connectionSource, TableKategori.class, true);
            TableUtils.dropTable(connectionSource, TableDetailing.class, true);
            TableUtils.dropTable(connectionSource, TableTransGimmick.class, true);
            TableUtils.dropTable(connectionSource, TableTransOutGimmick.class, true);
            TableUtils.dropTable(connectionSource, TableProdukKategori.class, true);
            TableUtils.dropTable(connectionSource, TableTansOfftake.class, true);
            TableUtils.dropTable(connectionSource, TableInsertion.class, true);
            TableUtils.dropTable(connectionSource, TableTransInsertion.class, true);
            TableUtils.dropTable(connectionSource, TableCompetitor.class, true);
            TableUtils.dropTable(connectionSource, TableSellOut.class, true);
            TableUtils.dropTable(connectionSource, TableDamageGood.class, true);
            TableUtils.dropTable(connectionSource, TableTypeDamage.class, true);
            TableUtils.dropTable(connectionSource, TableMCompetitor.class, true);
            TableUtils.dropTable(connectionSource, TableFeedback.class, true);
            TableUtils.dropTable(connectionSource, TablePromo.class, true);
            TableUtils.dropTable(connectionSource, TableProduk.class, true);
            TableUtils.dropTable(connectionSource, TableVisibility.class, true);
            TableUtils.dropTable(connectionSource, TableTransEducation.class, true);
            TableUtils.dropTable(connectionSource, TableTransEducationPlan.class, true);
            TableUtils.dropTable(connectionSource, TableJudul.class, true);
            TableUtils.dropTable(connectionSource, TablePembicara.class, true);
//            TableUtils.dropTable(connectionSource, TableSellOut.class, true);
            TableUtils.dropTable(connectionSource, TableTansSellOut.class, true);
            TableUtils.dropTable(connectionSource, TableStatusDamage.class, true);
            TableUtils.dropTable(connectionSource, TableStatusFeed.class, true);
            TableUtils.dropTable(connectionSource, TableMSIgnature.class, true);

            TableUtils.dropTable(connectionSource, TableImage.class, true);

            TableUtils.dropTable(connectionSource, TableJadwalAbsenMobile.class, true);
            TableUtils.dropTable(connectionSource, TableQr.class, true);
            TableUtils.dropTable(connectionSource, TableDisplay.class, true);

            onCreate(sqLiteDatabase, connectionSource);

        } catch (SQLException e) {
            Log.e("this", "Can't create database", e);
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Dao<TableTransGimmick, String> getTableTransGimmickDAO() {
        if (null == tableTransGimmicksDAO) {
            try {
                tableTransGimmicksDAO = getDao(TableTransGimmick.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableTransGimmicksDAO;
    }

    public Dao<TableDetailing, String> getTableDetailingDAO() {
        if (null == tableDetailingDAO) {
            try {
                tableDetailingDAO = getDao(TableDetailing.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableDetailingDAO;
    }

    public Dao<TableDisplay, Integer> getTableDisplay() {
        if (null == tableDisplay) {
            try {
                tableDisplay = getDao(TableDisplay.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableDisplay;
    }

    public Dao<TablePembicara, String> getTablePembicaraDAO() {
        if (null == tablePembicaraDAO) {
            try {
                tablePembicaraDAO = getDao(TablePembicara.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tablePembicaraDAO;
    }

    public Dao<TableJudul, String> getTableJudulDAO() {
        if (null == tableJudulDAO) {
            try {
                tableJudulDAO = getDao(TableJudul.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableJudulDAO;
    }

    public Dao<TableInsertion, String> getTableInsertionDAO() {
        if (null == tableInsertionDAO) {
            try {
                tableInsertionDAO = getDao(TableInsertion.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableInsertionDAO;
    }

    public Dao<TableTransEducation, Integer> getTableTransEducationDAO() {
        if (null == tableTransEducationDAO) {
            try {
                tableTransEducationDAO = getDao(TableTransEducation.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableTransEducationDAO;
    }
    public Dao<TableTransEducationPlan, Integer> getTableTransEducationPlanDAO() {
        if (null == tableTransEducationPlanDAO) {
            try {
                tableTransEducationPlanDAO = getDao(TableTransEducationPlan.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableTransEducationPlanDAO;
    }

    public Dao<TableTansOfftake, Integer> getTableTransOfftakeDAO() {
        if (null == tableTransOfftakeDAO) {
            try {
                tableTransOfftakeDAO = getDao(TableTansOfftake.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableTransOfftakeDAO;
    }

    public Dao<TableTransInsertion, String> getTableTransInsertionDAO() {
        if (null == tableTransInsertionDAO) {
            try {
                tableTransInsertionDAO = getDao(TableTransInsertion.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableTransInsertionDAO;
    }

    public Dao<TableProdukKategori, String> getTableProdukKategoriDAO() {
        if (null == tableProdukKategoriDAO) {
            try {
                tableProdukKategoriDAO = getDao(TableProdukKategori.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableProdukKategoriDAO;
    }

    public Dao<TableTransOutGimmick, String> getTableTransOGimmickDAO() {
        if (null == tableTransOGimmicksDAO) {
            try {
                tableTransOGimmicksDAO = getDao(TableTransOutGimmick.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableTransOGimmicksDAO;
    }

    public Dao<TableJadwalAbsenMobile, Integer> getTableJadwalAbsenMobileDAO() {
        if (null == tableJadwalAbsenMobileDAO) {
            try {
                tableJadwalAbsenMobileDAO = getDao(TableJadwalAbsenMobile.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableJadwalAbsenMobileDAO;
    }
    public Dao<TableTansSellOut, Integer> getTableTransSellOutDAO() {
        if (null == tableTransSellOutDAO) {
            try {
                tableTransSellOutDAO = getDao(TableTansSellOut.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableTransSellOutDAO;
    }

    public Dao<TableVisibility, String> getTableVisibilityDAO() {
        if (null == tableVisibilityDAO) {
            try {
                tableVisibilityDAO = getDao(TableVisibility.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableVisibilityDAO;
    }

    public Dao<TableMSIgnature, String> getTableSignatureDAO() {
        if (null == tableSignDAO) {
            try {
                tableSignDAO = getDao(TableMSIgnature.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableSignDAO;
    }
    public Dao<TableProduk, String> getTableProdukDAO() {
        if (null == tableProdukDAO) {
            try {
                tableProdukDAO = getDao(TableProduk.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableProdukDAO;
    }

    public Dao<TableStatusDamage, Integer> getTableStatusDamage() {
        if (null == tableStatusDamage) {
            try {
                tableStatusDamage = getDao(TableStatusDamage.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableStatusDamage;
    }

    public Dao<TableQr, Integer> getTableQrDAO() {
        if (null == tableTransQrDAO) {
            try {
                tableTransQrDAO = getDao(TableQr.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableTransQrDAO;
    }

    public Dao<TableStatusFeed, Integer> getTableStatusFeed() {
        if (null == tableStatusFeed) {
            try {
                tableStatusFeed = getDao(TableStatusFeed.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableStatusFeed;
    }

//    public  Dao<TableSellOut, String> getTableSellOutDAO() {
//        if (null == tableSellOutDAO) {
//            try {
//                tableSellOutDAO = getDao(TableSellOut.class);
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
//        return tableSellOutDAO;
//    }
//
//    public Dao<TableTansSellOut, Integer> getTableTransSellOutDAO() {
//        if (null == tableTransSellOutDAO) {
//            try {
//                tableTransSellOutDAO = getDao(TableTansSellOut.class);
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
//        return tableTransSellOutDAO;
//    }
    public Dao<TableMLogin, String> getTableMLoginDAO() {
        if (null == tableListMLoginDAO) {
            try {
                tableListMLoginDAO = getDao(TableMLogin.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableListMLoginDAO;
    }

    public Dao<TableAttendance, String> getTableAttendanceDAO() {
        if (null == tableAttendanceDAO) {
            try {
                tableAttendanceDAO = getDao(TableAttendance.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableAttendanceDAO;
    }

    public Dao<TableMOffice, Integer> getTableMOfficeDAO() {
        if (null == tableMOfficeDAO) {
            try {
                tableMOfficeDAO = getDao(TableMOffice.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableMOfficeDAO;
    }

    public Dao<TableJadwalAbsen, Integer> getTableJadwalAbsenDAO() {
        if (null == tableJadwalAbsenDAO) {
            try {
                tableJadwalAbsenDAO = getDao(TableJadwalAbsen.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableJadwalAbsenDAO;
    }

    public Dao<TableAppVersion, String> getTableAppVersionDAO() {
        if (null == tableAppVersionDAO) {
            try {
                tableAppVersionDAO = getDao(TableAppVersion.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableAppVersionDAO;
    }

    public Dao<TableKategori, String> getTableKategoriDAO() {
        if (null == tableKategoriDAO) {
            try {
                tableKategoriDAO = getDao(TableKategori.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableKategoriDAO;
    }

    public Dao<TableImage, Integer> getTableImageDAO() {
        if (null == tableImageDAO) {
            try {
                tableImageDAO = getDao(TableImage.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableImageDAO;
    }


    public Dao<TableTrackLog, String> getTableTrackLogDAO() {
        if (null == tableTrackLogDAO) {
            try {
                tableTrackLogDAO = getDao(TableTrackLog.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableTrackLogDAO;
    }

    public Dao<TableCompetitor, Integer> getTableCompetitor() {
        if (null == tableCompetitor) {
            try {
                tableCompetitor = getDao(TableCompetitor.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableCompetitor;
    }

    public Dao<TableSellOut, Integer> getTableSellOut() {
        if (null == tableSellOut) {
            try {
                tableSellOut = getDao(TableSellOut.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableSellOut;
    }
    public Dao<TableDamageGood, Integer> getTableDamageGood() {
        if (null == tableDamageGood) {
            try {
                tableDamageGood = getDao(TableDamageGood.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableDamageGood;
    }

    public Dao<TableTypeDamage, Integer> getTableTypeDamage() {
        if (null == tableTypeDamage) {
            try {
                tableTypeDamage = getDao(TableTypeDamage.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableTypeDamage;
    }

    public Dao<TableMCompetitor, Integer> getTableMCompetitor() {
        if (null == tableMCompetitor) {
            try {
                tableMCompetitor = getDao(TableMCompetitor.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableMCompetitor;
    }

    public Dao<TableFeedback, Integer> getTableFeedback() {
        if (null == tableFeedback) {
            try {
                tableFeedback = getDao(TableFeedback.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tableFeedback;
    }

    public Dao<TablePromo, Integer> getTablePromo() {
        if (null == tablePromo) {
            try {
                tablePromo = getDao(TablePromo.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return tablePromo;
    }


    @Override
    public void close() {
        super.close();

        tableListMLoginDAO = null;
        tableAttendanceDAO = null;
        tableMOfficeDAO = null;
        tableJadwalAbsenDAO = null;
        tableAppVersionDAO = null;
        tableTrackLogDAO = null;
        tableImageDAO = null;
        tableSignDAO = null;
        tableVisibilityDAO = null;
        tableTransGimmicksDAO = null;
        tableTransOGimmicksDAO = null;
        tableJadwalAbsenMobileDAO = null;
        tableKategoriDAO = null;
        tableCompetitor = null;
        tableSellOut = null;
        tableDamageGood = null;
        tableTypeDamage = null;
        tableMCompetitor = null;
        tableFeedback = null;
        tablePembicaraDAO = null;
        tableJudulDAO = null;
        tableStatusDamage = null;
        tableStatusFeed = null;
        tablePromo = null;
        tableInsertionDAO = null;
        tableTransInsertionDAO = null;
        tableTransOfftakeDAO = null;
        tableTransEducationDAO = null;
        tableTransEducationPlanDAO = null;
        tableDisplay = null;

//        tableProdukDAO = null;
        tableTransSellOutDAO = null;
        tableTransQrDAO = null;
//        tableSellOutDAO = null;
    }
}
