package com.ish.SaphireTransmart.utils.listitem_object;

import android.content.Context;

/**
 * Created by Hari Hendryan on 10/23/2015.
 */
public class MainMenuListItem {
    private Context context;
    private int id;
    private String menuTitle;
    private int menuImage;

    public MainMenuListItem(Context context) {
        this.context    = context;
    }

    public MainMenuListItem(MainMenuListItem item) {
        setId(item.getId());
        setMenuTitle(item.getMenuTitle());
        setMenuImage(item.getMenuImage());
    }

    public MainMenuListItem(int id, String titleMenu, int imageMenu) {
        setId(id);
        setMenuTitle(titleMenu);
        setMenuImage(imageMenu);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setMenuTitle(String svalues) {
        this.menuTitle = svalues;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuImage(int imageMenu) {
        this.menuImage = imageMenu;
    }

    public int getMenuImage() {
        return menuImage;
    }

}